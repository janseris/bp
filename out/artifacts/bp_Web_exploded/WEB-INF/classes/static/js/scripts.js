//if uploaded file size exceeds 100kB, the submit button will be disabled
function checkUploadedFileSize() {
    if(document.getElementById('contentSourceTypeField') != null){
        selectFileInputType();
    }
    var input, file;

    input = document.getElementById('libraryFileInput');
    file = input.files[0];
    //bodyAppend("p", "File " + file.name + " is " + file.size + " bytes in size");
    var submitButton = document.getElementById('formSubmitButton');
    if(file.size > 32*1024){ //file too big
        submitButton.disabled = true; //do not allow form submit
        bodyAppend("p", "Velikost souboru přesahuje 32kB! Prosím, vyberte menší soubor."); //show warning message
    } else { //file ok
        if(submitButton.disabled == true){
            submitButton.disabled = false; //if it was disabled, enable
        }
        var fileUploadWarningParagraph = document.getElementById('fileUploadWarning');
        if(fileUploadWarningParagraph != null){
            fileUploadWarningParagraph.remove();
        }
    }
}

//add an html element into webpage
function bodyAppend(tagName, innerHTML) {
    var elem;
    elem = document.createElement(tagName);
    elem.innerHTML = innerHTML; //the html code contained inside this element (for paragraph, this results in text attribute if no html tag detected)
    elem.setAttribute("class", "errorMessage"); //for class style applied from css
    elem.setAttribute("id", "fileUploadWarning"); //for identification while checking content
    document.body.appendChild(elem);
}

function toggleLibraryContentsVisibility(callerElem){
    var parentTable = callerElem.closest("table");  //closest parent table
    var libraryTextDiv = parentTable.getElementsByClassName("libraryContents")[0]; //first

    if (libraryTextDiv.style.display === 'none') { //toto se udělá, když se zobrazí div
        libraryTextDiv.style.display = 'inline';
        callerElem.innerHTML = "Schovat obsah";
    } else { //když se schová div
        libraryTextDiv.style.display = 'none';
        callerElem.innerHTML = "Ukázat obsah";
        if(navigator.appCodeName == 'Mozilla'){
            var gridContainer = parentTable.closest("div");
            reloadHtmlElement(gridContainer);
            console.log("grid container div reloaded");
        }
    }
}

//disables listeners though.
function reloadHtmlElement(element){
    var content = element.innerHTML;
    element.innerHTML = content;
}

function showLibraryDeleteConfirmation(){
    document.getElementById("libraryConfirmDeleteButton").style.display = 'inline';
    document.getElementById("libraryCancelDeleteButton").style.display = 'inline';
}
function hideLibraryDeleteConfirmation(){
    document.getElementById("libraryConfirmDeleteButton").style.display = 'none';
    document.getElementById("libraryCancelDeleteButton").style.display = 'none';

}

//https://stackoverflow.com/questions/20982683/spring-mvc-3-2-thymeleaf-ajax-fragments
function loadPublicLibrariesWithCustomOrdering() {
    var url = '/publicLibraries';
    document.getElementById('publicLibraries').load(url);
}

function selectFileInputType(){
    document.getElementById('contentSourceTypeField').selectedIndex = 1;
}

function selectTextInputType(){
    document.getElementById('contentSourceTypeField').selectedIndex = 0;
}

function setTextFileAccessedFlag() {
    document.getElementById('textFieldAccessed').setAttribute('value', 'true');
    selectTextInputType();
}

function generateSubspacesGenerateTaskTypeOptionSelected(selectElement){
    alert("option " + selectElement.selectedIndex + " selected");
    if(selectElement.selectedIndex == 0){
        generateSubspacesGenerateTaskTypeOption1Selected();
    } else if (selectElement.selectedIndex == 1){
        generateSubspacesGenerateTaskTypeOption1Selected();
    }  else if (selectElement.selectedIndex == 2){
        generateSubspacesGenerateTaskTypeOption2Selected();
    }  else if (selectElement.selectedIndex == 3){
        generateSubspacesGenerateTaskTypeOption3Selected();
    }  else if (selectElement.selectedIndex == 4){
        generateSubspacesGenerateTaskTypeOption4Selected();
    }  else if (selectElement.selectedIndex == 5){
        generateSubspacesGenerateTaskTypeOption5Selected();
    }
}

function generateSubspacesGenerateTaskRelationTypeOptionSelected(selectElement){
    if(selectElement.selectedIndex == 0){
        generateSubspacesGenerateTaskTypeOption2aSelected();
    } else if (selectElement.selectedIndex == 1){
        generateSubspacesGenerateTaskTypeOption2bSelected();
    }  else if (selectElement.selectedIndex == 2){
        generateSubspacesGenerateTaskTypeOption2cSelected();
    }
}

function generateSubspacesGenerateTaskTypeOption1Selected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2Selected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2aSelected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2bSelected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2cSelected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption3Selected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption4Selected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption5Selected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'inline';
}