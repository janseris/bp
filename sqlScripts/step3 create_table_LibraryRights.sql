/****** Object:  Table [dbo].[LibraryRights]    Script Date: 17.03.2020 17:34:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LibraryRights](
	[LibraryID] [uniqueidentifier] NOT NULL,
	[UserID] [uniqueidentifier] NOT NULL,
	[Rights] [int] NOT NULL,
 CONSTRAINT [PK_LibraryRights] PRIMARY KEY CLUSTERED 
(
	[LibraryID] ASC,
	[UserID] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LibraryRights]  WITH CHECK ADD  CONSTRAINT [FK_LibraryRights_Libraries] FOREIGN KEY([LibraryID])
REFERENCES [dbo].[Libraries] ([ID])
GO

ALTER TABLE [dbo].[LibraryRights] CHECK CONSTRAINT [FK_LibraryRights_Libraries]
GO

ALTER TABLE [dbo].[LibraryRights]  WITH CHECK ADD  CONSTRAINT [FK_LibraryRights_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO

ALTER TABLE [dbo].[LibraryRights] CHECK CONSTRAINT [FK_LibraryRights_Users]
GO

