USE [AnalyticGeometryOnline]
GO

/****** Object:  Table [dbo].[Libraries]    Script Date: 24.04.2020 22:16:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Libraries](
	[UserID] [uniqueidentifier] NOT NULL,
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Contents] [varbinary](max) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[LastDateModified] [datetime] NOT NULL,
	[LastDateUsed] [datetime] NOT NULL,
	[PublicAccessibility] [int] NOT NULL,
	[UsageCount] [int] NOT NULL,
 CONSTRAINT [PK_Libraries] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Libraries] ADD  CONSTRAINT [DF_Libraries_PublicAccessibility]  DEFAULT ((0)) FOR [PublicAccessibility]
GO

ALTER TABLE [dbo].[Libraries] ADD  CONSTRAINT [DF_Libraries_UsageCount]  DEFAULT ((0)) FOR [UsageCount]
GO

ALTER TABLE [dbo].[Libraries]  WITH CHECK ADD  CONSTRAINT [FK_Libraries_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
GO

ALTER TABLE [dbo].[Libraries] CHECK CONSTRAINT [FK_Libraries_Users]
GO


