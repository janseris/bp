PŘÍKLAD 1 - vzájemná poloha

5

-5+6t_1-t_2
6-t_1
-2+2t_1-t_2

-2x_2-x_3+10=0


PŘÍKLAD 2 - průnik

4

3+t_1-t_2
-1-2t_1+2t_2
-5t_1+t_2
2-2t_1-t_2

-23x_1+3x_3-19x_4+17=0

PŘÍKLAD 3 - vzájemná poloha

5

4+10t_1-6t_2
-8-13t_1+9t_2
4+t_1-2t_2
-2+14t_1-8t_2

12x_1+4x_2+2x_3-5x_4-34=0

PŘÍKLAD 4 - odchylka

7

-1-4t_1+t_2
-4-4t_1+t_2
1-t_1+t_2

x_2-x_3-2=0

PŘÍKLAD 5 - protínají se?

3

-x_1+7x_2-10x_3+4=0
-7x_2+11x_3-x_4-6=0

-7x_1+9x_2-5x_3-10x_4-2=0


PŘÍKLAD 6 - popiš podprostory

1

-x_1+7x_2-10x_3+4=0
-7x_2+11x_3-x_4-6=0