/**
 * This class introduces vectors and enables basic operations with them. The coordinates are stored as a field of <code>Fraction</code>s.
 * 
 * @author  Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since   2014-04-01
 */

class Vector
{
    /** dimension of the space, ie. number of vector's coordinates */
    private int dim;
    /** coordinates of the vector */
    private Fraction[] data;
    /** {@link Fraction} 0. */
    private static final Fraction ZERO = new Fraction(0);
    /** {@link Fraction} 1. */
    private static final Fraction ONE = new Fraction(1);
    /** {@link Fraction} -1. */
    private static final Fraction NEG = new Fraction(-1);

    /**
     * Constructs a null vector of dimension 0.
     */
    Vector()
    {
       this.dim = 0;
       this.data = new Fraction[0];
    }

    /**
     * Constructs a vector from a field of {@link Fraction}s.
     * 
     * @param V field of {@link Fraction}s, containing the vector's coordinates
     * @param d dimension of the vector, ie. number of its coordinates
     */
    Vector(Fraction[] V, int d)
    {
       this.dim = d;
       this.data = new Fraction[d];
       for (int i = 0; i<=d-1; i++) {
           if (V[i] == null)
               this.data[i]=ZERO;
           else
               this.data[i]=V[i];
       }
    }
    
    /**
     * Constructs a zero vector of a given dimension.
     * 
     * @param d dimension of the vector, ie. number of its coordinates
     */
    Vector(int d)
    {
       this.dim = d;
       this.data = new Fraction[d];
       for (int i = 0; i<=d-1; i++) {
           this.data[i]=ZERO;
       }
    }
    
    /**
     * Constructs a vector from two given points.
     * 
     * @param A initial point
     * @param B terminal point
     * 
     * @throws UnsupportedOperationException Throws an exception if the points don't have equal dimension.
     */
    Vector(Point A, Point B)
    {
       if (A.getDim() != B.getDim()) throw new UnsupportedOperationException("The points don't have equal dimension!");
       this.dim = A.getDim();
       this.data = new Fraction[this.dim];
       for (int i = 0; i<this.dim; i++) {
           this.data[i] = B.get(i+1).subtract(A.get(i+1));
       }
    }
    
    /**
     * Constructs a vector from a point (initial point will be the origin).
     * 
     * @param B terminal point
     * 
     * @throws UnsupportedOperationException Throws an exception if the points don't have equal dimension.
     */
    Vector(Point B)
    {
       this.dim = B.getDim();
       this.data = new Fraction[this.dim];
       for (int i = 0; i<this.dim; i++) {
           this.data[i] = B.get(i+1);
       }
    }

    /**
     * Gets the dimensions of the vector.
     * 
     * @return dimension of the vector
     */
    int getDim()
    {
        return this.dim;
    }
    
    /**
     * The getter of this class - returns the value of a given coordinate.
     * 
     * @param i the index of desired coordinate
     * 
     * @return the <tt>i</tt>th coordinate value (a {@link Fraction})
     * 
     * @throws IndexOutOfBoundsException Throws an exception if the index isn't of &lt;1,<i>dimension of the vector</i>&gt;.
     */
    Fraction get(int i)
    {
        if (i>this.dim)  throw new IndexOutOfBoundsException("Vector only has "+this.dim+" coefficients!");
        if (i<=0)        throw new IndexOutOfBoundsException("The index ought to be positive!");
        return this.data[i-1];
    }

    /**
     * The setter of this class - sets given coordinate to a given value.
     * 
     * @param F the <tt>i</tt>th coordinate desired value (a {@link Fraction})
     * @param i the index of desired coordinate
     * 
     * @throws IndexOutOfBoundsException Throws an exception if the index isn't of &lt;1,<i>dimension of the vector</i>&gt;.
     */
    void set(Fraction F, int i)
    {
        if (i>this.dim)  throw new IndexOutOfBoundsException("Vector only has "+this.dim+" coefficients!");
        if (i<=0)        throw new IndexOutOfBoundsException("The index ought to be positive!");
        this.data[i-1] = F;
    }
    
    /**
     * Copies another vector onto this vector. This is the correct way of assigning vectors, not by <code>=</code>.
     * 
     * @param I the image vector
     */
    void copy(Vector I)
    {
        if (I != null) {
            this.dim = I.dim;
            this.data = new Fraction[I.dim];
            for (int i = 1; i<=I.dim; i++) {
                this.set(I.get(i),i);
            }
        }
    }
    
    /**
     * Checks if two vectors are equal.
     * 
     * @param C comparand vector
     * 
     * @return true if this vector is equal to the comparand, false otherwise
     * 
     * @throws UnsupportedOperationException Throws an exception if the vectors don't have equal dimension.
     */
    boolean isEqual(Vector C)
    {
        if (this.dim != C.dim) throw new UnsupportedOperationException("The vectors don't have equal dimension!");
        
        for (int i = 1; i<=this.dim; i++) {
            if (!(this.get(i).isEqual(C.get(i)))) return false;
        }
        
        return true;
    }
    
    /**
     * Checks whether or not this vector is a zero vector.
     * 
     * @return true if this vector is a zero vector or a null vector, false otherwise
     */
    boolean isZeroVector() {
        for (int i=1; i<=this.dim; i++) {
            if (!(this.get(i).isEqual(ZERO))) return false;
        }
        
        return true;
    }
    
    /**
     * Converts the vector to a 1-row matrix.
     * 
     * @returns matrix of vector's coordinates (in a row)
     */
    Matrix toMatrixRow()
    {
        return new Matrix(this.data,1,this.dim);
    }
    
    /**
     * Converts the vector to a 1-column matrix.
     * 
     * @returns matrix of vector's coordinates (in a column)
     */
    Matrix toMatrixColumn()
    {
        return new Matrix(this.data,this.dim,1);
    }
    
    /**
     * Multiplies this vector by a scalar.
     * 
     * @param F factor
     * 
     * @return <code>F</code>-multiple of this vector
     */
    Vector multiply(Fraction F)
    {   
        Vector V = new Vector();
        V.copy(this);
        
        for (int i = 1; i<=V.dim; i++) {
           V.set(V.get(i).multiply(F),i);
        }
        
        return V;
    }
    
    /**
     * Finds ppposite vector to this vector.
     * 
     * @return the opposite vector
     */
    Vector opposite()
    {
        return this.multiply(NEG);
    }
    
    /**
     * Adds another vector to this vector.
     * 
     * @param A addend vector
     * 
     * @throws UnsupportedOperationException Throws an exception if the vectors don't have equal dimension.
     */
    Vector add(Vector A)
    {
        Vector V = new Vector();
        V.copy(this);
        
        if (V.dim != A.dim) throw new UnsupportedOperationException("The vectors don't have equal dimension!");
        
        for (int i = 1; i<=V.dim; i++) {
           V.set(V.get(i).add(A.get(i)),i);
        }
        
        return V;
    }
    
    /**
     * Subtracts another vector from this vector.
     * 
     * @param S subtrahend vector
     * 
     * @throws UnsupportedOperationException Throws an exception if the vectors don't have equal dimension.
     */
    Vector subtract(Vector S)
    {
        return this.add(S.opposite());
    }
    
    /**
     * Computes scalar product of two vectors.
     * 
     * @param F factor vector
     * 
     * @throws UnsupportedOperationException Throws an exception if the vectors don't have equal dimension.
     */
    Fraction scalarProduct(Vector F)
    {
        if (this.dim != F.dim) throw new UnsupportedOperationException("The vectors don't have equal dimension!");
        
        Fraction tempFrac = ZERO;
        
        for (int i = 1; i<=this.dim; i++) {
           tempFrac = tempFrac.add(this.get(i).multiply(F.get(i)));
        }
        
        return tempFrac;
    }
    
    /**
     * Computes scalar product of two vectors after normalizing them.
     * 
     * @param F factor vector
     * 
     * @throws UnsupportedOperationException Throws an exception if the vectors don't have equal dimension.
     */
    SquareRootFraction scalarProductNorm(Vector F)
    {
        Fraction tempFrac = this.scalarProduct(F);
        SquareRootFraction S = new SquareRootFraction(tempFrac.multiply(tempFrac));
        S.n *= tempFrac.compare(0);
        S.divide(this.length());
        S.divide(F.length());
        
        return S;
    }
    
    /**
     * Computes length of this vector. This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @return length of this vector
     */
    SquareRootFraction length()
    {
        return length('v', false);
    }
    
    /**
     * Computes length of this vector.
     * 
     * @param label label of the matrix
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return length of this vector
     */
    SquareRootFraction length(char label, boolean verbose)
    {
        if (verbose) {
            String scalarProductText = "sqrt(";
            String scalarProductTeX = "\\sqrt{";
            for (int i = 1; i<=dim; i++) {
                String printCoord = (this.get(i).compare(ZERO) == -1 ? "(" + this.get(i).toText() + ")" : this.get(i).toText());
                scalarProductText += (i == 1 ? "" : " + ") + printCoord + "*" + printCoord;
                printCoord = (this.get(i).compare(ZERO) == -1 ? "\\left(" + this.get(i).toTeX() + "\\right)" : this.get(i).toTeX());
                scalarProductTeX += (i == 1 ? "" : " + ") + printCoord + "\\cdot" + printCoord;
            }
            Fraction scalarProduct = this.scalarProduct(this);
            SquareRootFraction result = new SquareRootFraction(scalarProduct);
            scalarProductText += ") = sqrt(" + scalarProduct.toText() + ")" + (result.n == 1 && scalarProduct.d == 1 ? "" : " = " + result.toText());
            scalarProductTeX += "} = \\sqrt{" + scalarProduct.toTeX() + "}" + (result.n == 1 && scalarProduct.d == 1 ? "" : " = " + result.toTeX());
            Utility.textOut("Vypočteme velikost vektoru " + label + ":\r\n\t" + "||" + label + "|| = " + scalarProductText + ".");
            Utility.texOut("Vypočteme velikost vektoru $\\vec{" + label + "}$:\n\\begin{center}$\\displaystyle" + "\\|\\vec{" + label + "}\\| = " + scalarProductTeX
                                + ".$\\end{center}\n");
            
            return result;
        }
        
        return new SquareRootFraction(this.scalarProduct(this));
    }
    
    /**
     * Calculates the angle of two direction vectors of lines (the resulting angle will be from 0 to &pi;/2). This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param V the other vector
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleDirectionVectors(Vector W)
    {
        return angleDirectionVectors(W, 'u', 'v', false);
    }
    
    /**
     * Calculates the angle of two direction vectors of lines (the resulting angle will be from 0 to &pi;/2).
     * 
     * @param V the other vector
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleDirectionVectors(Vector W, char firstLabel, char secondLabel, boolean verbose)
    {
        // numerator
        Fraction N = this.scalarProduct(W).abs();
        // denominator
        SquareRootFraction D = this.length();
        D = D.multiply(W.length());
        // the result
        SquareRootFraction R = N.convertToSRF();
        R = R.divide(D);
        Cyclometric result = new Cyclometric(R,false);
        
        if (verbose) {
            Utility.textOut("Pro odchylku platí:\r\n\tcos(phi) = |v_" + firstLabel + "*v_" + secondLabel + "|/(||v_" + firstLabel + "||*||v_" + secondLabel + "||),\r\n"
                        + "v našem případě:\r\n\tcos(phi) = |" + this.toText() + "*" + W.toText() + "|/(||" + this.toText() + "||*||" + W.toText() + "||) = \r\n\t"
                        + "         = |" + this.scalarProduct(W).toText() + "|/(" + this.length().toText() + "*" + W.length().toText() + ") = \r\n\t"
                        + "         = (" + N.toText() + ")/(" + D.toText() + ") = " + R.toText() + ".\r\n\r\nOdtud:\r\n\tphi = " + result.toText() + ".");
            Utility.texOut("Pro odchylku platí:\n\\begin{align*}\\cos(\\phi) &= \\frac{\\abs{\\vec{v}_{\\mathcal{" + firstLabel + "}}\\cdot\\vec{v}_{\\mathcal{" + secondLabel + "}}}}"
                        + "{\\|\\vec{v}_{\\mathcal{" + firstLabel + "}}\\|\\cdot\\|\\vec{v}_{\\mathcal{" + secondLabel + "}}\\|},\n"
                        + "\\intertext{v~našem případě:}\n\\cos(\\phi) &= \\frac{\\abs{" + this.toTeX() + "\\cdot" + W.toTeX() + "}}{\\left\\|" + this.toTeX() + "\\right\\|\\cdot\\left\\|"
                            + W.toTeX() + "\\right\\|} = \\\\\n"
                        + " &= \\frac{\\abs{" + this.scalarProduct(W).toTeX() + "}}{" + this.length().toTeX() + "\\cdot" + W.length().toTeX() + "} = \\\\\n"
                        + " &= \\frac{" + N.toTeX() + "}{" + D.toTeX() + "} = " + R.toTeX() + ".\\end{align*}\n\nOdtud:\n\\[\\phi = " + result.toTeX() + ".\\]\n");
        }
        
        return result;
    }
    
    /**
     * Calculates the angle of two normal vectors of lines (the resulting angle will be from 0 to &pi;/2). This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param V the other vector
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleNormalVectors(Vector W)
    {
        return angleNormalVectors(W, 'u', 'v', false);
    }
    
    /**
     * Calculates the angle of two normal vectors of lines (the resulting angle will be from 0 to &pi;/2).
     * 
     * @param V the other vector
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleNormalVectors(Vector W, char firstLabel, char secondLabel, boolean verbose)
    {
        // numerator
        Fraction N = this.scalarProduct(W).abs();
        // denominator
        SquareRootFraction D = this.length();
        D = D.multiply(W.length());
        // the result
        SquareRootFraction R = N.convertToSRF();
        R = R.divide(D);
        Cyclometric result = new Cyclometric(R,false);
        
        if (verbose) {
            Utility.textOut("Pro odchylku platí:\r\n\tcos(phi) = |n_" + firstLabel + "*n_" + firstLabel + "|/(||n_" + firstLabel + "||*||n_" + firstLabel + "||),\r\n"
                        + "v našem případě:\r\n\tcos(phi) = |" + this.toText() + "*" + W.toText() + "|/(||" + this.toText() + "||*||" + W.toText() + "||) = \r\n\t"
                        + "         = |" + this.scalarProduct(W).toText() + "|/(" + this.length().toText() + "*" + W.length().toText() + ") = \r\n\t"
                        + "         = (" + N.toText() + ")/(" + D.toText() + ") = " + R.toText() + ".\r\n\r\nOdtud:\r\n\tphi = " + result.toText() + ".");
            Utility.texOut("Pro odchylku platí:\n\\begin{align*}\\cos(\\phi) &= \\frac{\\abs{\\vec{n}_{\\mathcal{" + firstLabel + "}}\\cdot\\vec{n}_{\\mathcal{" + firstLabel + "}}}}"
                        + "{\\|\\vec{n}_{\\mathcal{" + firstLabel + "}}\\|\\cdot\\|\\vec{n}_{\\mathcal{" + secondLabel + "}}\\|},\n"
                        + "\\intertext{v~našem případě:}\n\\cos(\\phi) &= \\frac{\\abs{" + this.toTeX() + "\\cdot" + W.toTeX() + "}}{\\left\\|" + this.toTeX() + "\\right\\|\\cdot\\left\\|"
                            + W.toTeX() + "\\right\\|} = \\\\\n"
                        + " &= \\frac{\\abs{" + this.scalarProduct(W).toTeX() + "}}{" + this.length().toTeX() + "\\cdot" + W.length().toTeX() + "} = \\\\\n"
                        + " &= \\frac{" + N.toTeX() + "}{" + D.toTeX() + "} = " + R.toTeX() + ".\\end{align*}\n\nOdtud:\n\\[\\phi = " + result.toTeX() + ".\\]\n");
        }
        
        return result;
    }
    
    /**
     * Calculates the angle of a direction vector and a normal vector of lines (the resulting angle will be from 0 to &pi;/2).
     * This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param V the other vector
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleNormalVectorDirectionVector(Vector W)
    {
        return angleNormalVectorDirectionVector(W, 'u', 'v', false);
    }
    
    /**
     * Calculates the angle of a direction vector and a normal vector of lines (the resulting angle will be from 0 to &pi;/2).
     * 
     * @param V the other vector
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleNormalVectorDirectionVector(Vector W, char firstLabel, char secondLabel, boolean verbose)
    {
        // numerator
        Fraction N = this.scalarProduct(W).abs();
        // denominator
        SquareRootFraction D = this.length();
        D = D.multiply(W.length());
        // the result
        SquareRootFraction R = N.convertToSRF();
        R = R.divide(D);
        Cyclometric result = new Cyclometric(R,true);
        
        if (verbose) {
            Utility.textOut("Pro odchylku platí:\r\n\tsin(phi) = |n_" + firstLabel + "*v_" + firstLabel + "|/(||n_" + firstLabel + "||*||v_" + firstLabel + "||),\r\n"
                        + "v našem případě:\r\n\tsin(phi) = |" + this.toText() + "*" + W.toText() + "|/(||" + this.toText() + "||*||" + W.toText() + "||) = \r\n\t"
                        + "         = |" + this.scalarProduct(W).toText() + "|/(" + this.length().toText() + "*" + W.length().toText() + ") = \r\n\t"
                        + "         = (" + N.toText() + ")/(" + D.toText() + ") = " + R.toText() + ".\r\n\r\nOdtud:\r\n\tphi = " + result.toText() + ".");
            Utility.texOut("Pro odchylku platí:\n\\begin{align*}\\sin(\\phi) &= \\frac{\\abs{\\vec{n}_{\\mathcal{" + firstLabel + "}}\\cdot\\vec{v}_{\\mathcal{" + firstLabel + "}}}}"
                        + "{\\|\\vec{n}_{\\mathcal{" + firstLabel + "}}\\|\\cdot{v}_{\\mathcal{" + secondLabel + "}}\\|},\n"
                        + "\\intertext{v~našem případě:}\n\\sin(\\phi) &= \\frac{\\abs{" + this.toTeX() + "\\cdot" + W.toTeX() + "}}{\\left\\|" + this.toTeX() + "\\right\\|\\cdot\\left\\|"
                            + W.toTeX() + "\\right\\|} = \\\\\n"
                        + " &= \\frac{\\abs{" + this.scalarProduct(W).toTeX() + "}}{" + this.length().toTeX() + "\\cdot" + W.length().toTeX() + "} = \\\\\n"
                        + " &= \\frac{" + N.toTeX() + "}{" + D.toTeX() + "} = " + R.toTeX() + ".\\end{align*}\n\nOdtud:\n\\[\\phi = " + result.toTeX() + ".\\]\n");
        }
        
        return result;
    }
    
    /**
     * Finds the orthogonal projection of this vector onto a given difference space. This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param W difference space onto which we project the vector
     * 
     * @return the orthogonal projection of this vector onto a given difference space
     */
    
    Vector orthogonalProjection(Matrix W)
    {
        return orthogonalProjection(W, false);
    }
    
    /**
     * Finds the orthogonal projection of this vector onto a given difference space.
     * 
     * @param W difference space onto which we project the vector
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return the orthogonal projection of this vector onto a given difference space
     */
    
    Vector orthogonalProjection(Matrix W, boolean verbose)
    {
        if (verbose) {
            Utility.textOut("\r\n\r\n" + "nalezení báze zaměření" + "\r\n----------------------" + "\r\n");
            Utility.texOut("\n\\subsection*{nalezení báze zaměření}\n");
        }
        Vector[] base = W.getEchelonForm(verbose).rowsToVectors();
        int rank = base.length;
        
        Matrix M = new Matrix(rank,rank + 1);
        
        for (int i = 1; i <= rank; i++) {
            for (int j = 1; j <= rank; j++) {
                M.set(base[i-1].scalarProduct(base[j-1]), i, j);
            }
            M.set(this.scalarProduct(base[i-1]), i, rank+1);
        }
        
        String baseText = "", baseTeX = "", projText = "", projTeX = "", projConcreteText = "", projConcreteTeX = "", eqnGeneralText = "", eqnGeneralTeX = "";
        if (verbose) {
            Subspace fake = new Subspace(M);
            for (int i = 1; i <= rank; i++) {
                baseText += (i == 1 ? "" : ", ") + "u_" + i + " = " + base[i-1].toText();
                baseTeX += (i == 1 ? "" : ", ") + "\\vec{u}_{" + i + "} = " + base[i-1].toTeX();
                projText += (i == 1 ? "" : " + ") + "x_" + i + "*u_" + i;
                projTeX += (i == 1 ? "" : " + ") + "x_{" + i + "}\\vec{u}_{" + i + "}";
                for (int j = 1; j <= rank; j++) {
                    eqnGeneralText += (j == 1 ? "\t" : " + ") + "x_" + j + "(u_" + j + "*u_" + i + ")";
                    eqnGeneralTeX += (j == 1 ? "" : " + ") + "x_{" + j + "}(\\vec{u}_{" + j + "}\\cdot\\vec{u}_{" + i + "})";
                }
                eqnGeneralText += " + (x*u_" + i + ") = 0\r\n";
                eqnGeneralTeX += " + (\\vec{x}\\cdot\\vec{u}_{" + i + "}) &= 0\\\\\n";
            }
            Utility.textOut(" Báze podprostoru, na který vektor projektujeme, je tedy " + baseText + ". Abychom nalezli ortogonální projekci, musíme vyřešit soustavu rovnic:\r\n"
                                + eqnGeneralText + "V našem případě po výpočtu skalárních součinů dostáváme soustavu rovnic:\r\n" + fake.toTextGeneral(true)
                                + "\r\n\r\nnalezení ortogonální projekce" + "\r\n-----------------------------\r\n");
            Utility.texOut(" Báze podprostoru, na který vektor projektujeme, je tedy $" + baseTeX + "$. Abychom nalezli ortogonální projekci, musíme vyřešit soustavu rovnic:\n"
                                + "\\begin{align*}\n" + eqnGeneralTeX + "\\end{align*}\n" + "V našem případě po výpočtu skalárních součinů dostáváme soustavu rovnic:\n\\[\n"
                                + fake.toTeXGeneral() + "\\]\n\\subsection*{nalezení ortogonální projekce}\n");
        }
        
        M = M.solve('M', verbose);
        
        Vector projection = new Vector(base[0].dim);
        for (int i = 1; i <= rank; i++) {
            projection = projection.add(base[i-1].multiply(M.get(i, M.getC())));
        }
        
        if (verbose) {
            for (int i = 1; i <= rank; i++) {
                projConcreteText += (i == 1 ? "" : " + ") + M.get(i, M.getC()).toText() + "*" + base[i-1].toText();
                projConcreteTeX += (i == 1 ? "" : " + ") + M.get(i, M.getC()).toTeX() + "\\cdot" + base[i-1].toTeX();
            }
            Utility.textOut("Ortogonální projekce je pak y = " + projText + " = " + projConcreteText + " = " + projection.toText() + ".");
            Utility.texOut("Ortogonální projekce je pak $\\vec{y} = " + projTeX + " = " + projConcreteTeX + " = " + projection.toTeX() + "$.");
        }
        
        return projection;
    }
    
    /**
     * Finds the orthogonal component of the vector onto a given difference space. This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param W difference space onto which we project the vector
     * 
     * @return the orthogonal component of this vector onto a given difference space
     */
    
    Vector orthogonalComponent(Matrix W)
    {
        return this.subtract(this.orthogonalProjection(W, false));
    } 
    
    /**
     * Finds the orthogonal component of the vector onto a given difference space.
     * 
     * @param W difference space onto which we project the vector
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return the orthogonal component of this vector onto a given difference space
     */
    
    Vector orthogonalComponent(Matrix W, boolean verbose)
    {
        if (!verbose) return orthogonalComponent(W);
        return this.subtract(this.orthogonalProjection(W, true));
    } 
    
    
    
    
    
    /**
     * Generates two random vectors, while the angle of the vectors will be one of 0, &pi;/6, &pi;/4, &pi;/3, or &pi;/2.
     * 
     * @param angle the desired angle of vectors
     * @DIM dimension of the vectors
     * 
     * @return vectors with desired angle
     * 
     * @throws UnsupportedOperationException if angle is none of 0, &pi;/6, &pi;/4, &pi;/3, or &pi;/2
     */
    static Vector[] generateRandomVectorsWithGivenAngle(PiFraction angle, int DIM) {
        if (angle.isEqual(new PiFraction(0)))   return generateRandomParallelVectors(DIM);
        if (angle.isEqual(new PiFraction(1,6))) return generateRandomVectorsWithAngle30(DIM);
        if (angle.isEqual(new PiFraction(1,4))) return generateRandomVectorsWithAngle45(DIM);
        if (angle.isEqual(new PiFraction(1,3))) return generateRandomVectorsWithAngle60(DIM);
        if (angle.isEqual(new PiFraction(1,2))) return generateRandomPerpendicularVectors(DIM);
        throw new UnsupportedOperationException("I can only generate vectors with angles 0, pi/6, pi/4, pi/3, or pi/2.");
    }
    
    /**
     * Generates two random parallel vectors of given dimension.
     * 
     * @param DIM dimension of the vectors
     * 
     * @return vectors with desired angle
     * 
     * @throws UnsupportedOperationException if DIM is zero
     */
    static Vector[] generateRandomParallelVectors(int DIM) {
        if (DIM == 0) throw new UnsupportedOperationException("I can only generate vectors of dimensions at least 1.");
        
        int[] coeffs = Utility.randNTuple(-20, 20, DIM);
        Fraction[] fracCoeffs = new Fraction[DIM];
        for (int i = 0; i<DIM; i++) fracCoeffs[i] = new Fraction(coeffs[i]);
        Vector a = new Vector(fracCoeffs, DIM);
        
        if (a.isZeroVector()) return generateRandomParallelVectors(DIM);
        
        Fraction c = new Fraction(Utility.randIntUniform(-8, 8),Utility.randIntUniform(1, 4));
        while (c.isEqual(0)) c = new Fraction(Utility.randIntUniform(-8, 8),Utility.randIntUniform(1, 4));
        Vector b = a.multiply(c);
        
        Vector[] out = new Vector[2];
        out[0] = new Vector();
        out[0].copy(a);
        out[1] = new Vector();
        out[1].copy(b);
        
        return out;
    }
    
    /**
     * Generates two random vectors of given dimension whose angle is &pi;/6.
     * 
     * @DIM dimension of the vectors
     * 
     * @return vectors with desired angle
     * 
     * @throws UnsupportedOperationException if DIM is less than three
     */
    static Vector[] generateRandomVectorsWithAngle30(int DIM) {
        Vector[] out = new Vector[2];
        out = generateRandomVectorsWithAngle60(DIM);
        out[1] = out[1].add(out[0]);
        
        return out;
    }
    
    /**
     * Generates two random vectors of given dimension whose angle is &pi;/4.
     * 
     * @DIM dimension of the vectors
     * 
     * @return vectors with desired angle
     * 
     * @throws UnsupportedOperationException if DIM is less than two
     */
    static Vector[] generateRandomVectorsWithAngle45(int DIM) {
        int[] coeffs;
        Fraction[] fracCoeffs;
        Fraction[] secondFracCoeffs;
        Vector[] out = new Vector[2];
        Vector[] temp = new Vector[2];
        
        switch (DIM) {
            case 0:
            case 1:     throw new UnsupportedOperationException("I can only generate vectors of dimensions at least 2.");
            
            case 2:     fracCoeffs = new Fraction[2];
                        coeffs = Utility.randNTuple(-15, 15, 2);
                        for (int i = 0; i<2; i++) fracCoeffs[i] = new Fraction(coeffs[i]);
                        secondFracCoeffs = new Fraction[2];
                        secondFracCoeffs[0] = fracCoeffs[0].subtract(fracCoeffs[1]);
                        secondFracCoeffs[1] = fracCoeffs[0].add(fracCoeffs[1]);
                        out[0] = new Vector(fracCoeffs, 2);
                        out[1] = new Vector(secondFracCoeffs, 2);                    
                        break;
                        
            default:    int numberOfRuns = DIM / 2 - 1;
                        out = generateRandomVectorsWithAngle45(2);
                        for (int i = 0; i < numberOfRuns; i++) {
                            temp = generateRandomVectorsWithAngle45(2);
                            out[0] = out[0].mergeFromRight(temp[0]);
                            out[1] = out[1].mergeFromRight(temp[1]);
                        }
            
                        if ((DIM % 2) != 0) {// lichá dimenze
                            temp = new Vector[2];
                            temp[0] = new Vector(1);
                            temp[1] = new Vector(1);
                            out[0] = out[0].mergeFromRight(temp[0]);
                            out[1] = out[1].mergeFromRight(temp[1]);
                        }      
                        break;
        }
        
        int[] permutation = Utility.generatePermutation(DIM);
        
        Vector originalVector = new Vector();
        originalVector.copy(out[0]);
        for (int i=1; i<=DIM; i++) out[0].set(originalVector.get(permutation[i-1]), i);
        originalVector.copy(out[1]);
        for (int i=1; i<=DIM; i++) out[1].set(originalVector.get(permutation[i-1]), i);
        
        return out;
    }
    
    /**
     * Generates two random vectors of given dimension whose angle is &pi;/3.
     * 
     * @DIM dimension of the vectors
     * 
     * @return vectors with desired angle
     * 
     * @throws UnsupportedOperationException if DIM is less than three
     */
    static Vector[] generateRandomVectorsWithAngle60(int DIM) {
        Fraction[] fracCoeffs;
        Fraction[] secondFracCoeffs;
        Vector[] out = new Vector[2];
        Vector[] temp = new Vector[2];
        boolean whichMethod = Utility.randBool();
        int a, b;
        
        switch (DIM) {
            case 0:
            case 1:     
            case 2:     throw new UnsupportedOperationException("I can only generate vectors of dimensions at least 3.");
            
            case 3:     a = Utility.randInt(-15, 15);
            
                        fracCoeffs = new Fraction[3];
                        fracCoeffs[0] = new Fraction(1);
                        fracCoeffs[1] = new Fraction(a);
                        fracCoeffs[2] = new Fraction(a+1);
                        
                        secondFracCoeffs = new Fraction[3];
                        if (whichMethod) {
                            secondFracCoeffs[0] = new Fraction(-a);
                            secondFracCoeffs[1] = new Fraction(a+1);
                            secondFracCoeffs[2] = new Fraction(1);
                        }
                        else {
                            secondFracCoeffs[0] = new Fraction(a+1);
                            secondFracCoeffs[1] = new Fraction(-1);
                            secondFracCoeffs[2] = new Fraction(a);
                        }
                        
                        out[0] = new Vector(fracCoeffs, 3);
                        out[1] = new Vector(secondFracCoeffs, 3);                    
                        break;
            
            case 4:     a = Utility.randInt(-15, 15);
                        b = Utility.randInt(-15, 15);
                        while ((a==0) && (b==0)) b = Utility.randInt(-10, 10);
                        
                        fracCoeffs = new Fraction[4];
                        fracCoeffs[0] = new Fraction(a);
                        fracCoeffs[1] = new Fraction(b);
                        fracCoeffs[2] = new Fraction(a);
                        fracCoeffs[3] = new Fraction(b);
                        
                        secondFracCoeffs = new Fraction[4];
                        if (whichMethod) {
                            secondFracCoeffs[0] = new Fraction(a);
                            secondFracCoeffs[1] = new Fraction(-a);
                            secondFracCoeffs[2] = new Fraction(b);
                            secondFracCoeffs[3] = new Fraction(b);
                        }
                        else {
                            secondFracCoeffs[0] = new Fraction(a);
                            secondFracCoeffs[1] = new Fraction(a);
                            secondFracCoeffs[2] = new Fraction(-b);
                            secondFracCoeffs[3] = new Fraction(b);
                        }
                        
                        out[0] = new Vector(fracCoeffs, 4);
                        out[1] = new Vector(secondFracCoeffs, 4);                    
                        break;
            
            case 5:     a = Utility.randInt(-15, 15);
            
                        fracCoeffs = new Fraction[5];
                        secondFracCoeffs = new Fraction[5];
                        
                        if (whichMethod) {
                            fracCoeffs[0] = new Fraction(1);
                            fracCoeffs[1] = new Fraction(2*a);
                            fracCoeffs[2] = new Fraction(2*a);
                            fracCoeffs[3] = new Fraction(4*a);
                            fracCoeffs[4] = new Fraction(4*a+1);
                            secondFracCoeffs[0] = new Fraction(4*a+1);
                            secondFracCoeffs[1] = new Fraction(-2*a);
                            secondFracCoeffs[2] = new Fraction(-1);
                            secondFracCoeffs[3] = new Fraction(4*a);
                            secondFracCoeffs[4] = new Fraction(2*a);
                        }
                        else {
                            fracCoeffs[0] = new Fraction(1);
                            fracCoeffs[1] = new Fraction(2*a);
                            fracCoeffs[2] = new Fraction(2*a);
                            fracCoeffs[3] = new Fraction(4*a-1);
                            fracCoeffs[4] = new Fraction(4*a);
                            secondFracCoeffs[0] = new Fraction(-4*a+1);
                            secondFracCoeffs[1] = new Fraction(-2*a);
                            secondFracCoeffs[2] = new Fraction(1);
                            secondFracCoeffs[3] = new Fraction(2*a);
                            secondFracCoeffs[4] = new Fraction(4*a);
                        }
                        
                        out[0] = new Vector(fracCoeffs, 5);
                        out[1] = new Vector(secondFracCoeffs, 5);                    
                        break;
                        
            default:    out = generateRandomVectorsWithAngle60(3 + (DIM % 3));
                        
                        int numberOfRuns = (DIM - (DIM % 3)) / 3 - 1;
                        
                        for (int i = 0; i < numberOfRuns; i++) {
                            temp = generateRandomVectorsWithAngle60(3);
                            out[0] = out[0].mergeFromRight(temp[0]);
                            out[1] = out[1].mergeFromRight(temp[1]);
                        }
                        break;
        }
        
        int[] permutation = Utility.generatePermutation(DIM);
        
        Vector originalVector = new Vector();
        originalVector.copy(out[0]);
        for (int i=1; i<=DIM; i++) out[0].set(originalVector.get(permutation[i-1]), i);
        originalVector.copy(out[1]);
        for (int i=1; i<=DIM; i++) out[1].set(originalVector.get(permutation[i-1]), i);
        
        return out;
    }
    
    /**
     * Generates two random perpendicular vectors of given dimension.
     * 
     * @DIM dimension of the vectors
     * 
     * @return vectors with desired angle
     * 
     * @throws UnsupportedOperationException if DIM is zero
     */
    static Vector[] generateRandomPerpendicularVectors(int DIM) {
        if (DIM == 0) throw new UnsupportedOperationException("I can only generate vectors of dimensions at least 1.");
        
        int[] coeffs = Utility.randNTuple(-20, 20, DIM);
        Fraction[] fracCoeffs = new Fraction[DIM];
        for (int i = 0; i<DIM; i++) fracCoeffs[i] = new Fraction(coeffs[i]);
        Vector a = new Vector(fracCoeffs, DIM);
        coeffs = Utility.randNTuple(-20, 20, DIM);
        fracCoeffs = new Fraction[DIM];
        for (int i = 0; i<DIM; i++) fracCoeffs[i] = new Fraction(coeffs[i]);
        Vector b = new Vector(fracCoeffs, DIM);
        
        if (a.isZeroVector()) return generateRandomPerpendicularVectors(DIM);
        int pos = Utility.randIntUniform(1, DIM);
        while (a.get(pos).isEqual(0)) pos = Utility.randIntUniform(1, DIM);
        
        Fraction c = a.scalarProduct(b);
        c = c.subtract(a.get(pos).multiply(b.get(pos)));
        c = c.divide(a.get(pos));
        c = c.multiply(-1);
        b.set(c, pos);
        if ((a.isZeroVector()) || (b.isZeroVector())) return generateRandomPerpendicularVectors(DIM);
        
        Vector[] out = new Vector[2];
        out[0] = new Vector();
        out[0].copy(a);
        out[1] = new Vector();
        out[1].copy(b);
        
        return out;
    }
    
    /**
     * Returns a vector that is de facto original vector to which we glue another vector from right.
     * 
     * @return the vectors glued together
     * 
     * @param v vector that will be glued from right
     */
    private Vector mergeFromRight(Vector v) {
        int origDim = this.dim;
        Vector out = new Vector(origDim+v.dim);
        for (int i = 1; i <= origDim; i++) {
            out.set(this.get(i), i);
        }
        
        for (int i = origDim+1, j = 1; i <= out.dim; i++, j++) {
            out.set(v.get(j), i);
        }
        
        return out;
    }

    /**
     * Prints this vector formatted as text.
     * 
     * @return plain text description of this vector; it will begin with <code>(</code> and end with <code>)</code>
     */
    String toText()
    {
        String S = "";
        
        S += "(";
        for (int i = 1; i<=this.dim; i++) {
            if (i>1) S += ", ";
            S += this.get(i).toText();
        }
        S += ")";
        
        return S;
    }

    /**
     * Prints this vector formatted as TeX code.
     * 
     * @return TeX code for this vector
     */
    String toTeX()
    {
        String S = "";
        
        S += "\\left(";
        for (int i = 1; i<=this.dim; i++) {
            if (i>1) S += ",";
            S += this.get(i).toTeX();
        }
        S += "\\right)";
        
        return S;
    }
}
