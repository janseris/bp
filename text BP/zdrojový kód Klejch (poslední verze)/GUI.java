/**
 * This is the computational core of the package. It introduces subspaces and enables basic operations with them, using all the other classes for that.
 * 
 * @author  Jakub Klejch <323464@mail.muni.cz>
 * @version 1.0
 * @since   2014-12-01
 */

import java.nio.charset.Charset;
import javax.swing.UIManager.LookAndFeelInfo;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.geom.*;
import javax.swing.border.*;
import javax.swing.*;
import java.util.Date;
import java.util.concurrent.*;
import java.io.*;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import org.scilab.forge.jlatexmath.TeXConstants;

public class GUI extends JFrame {
    private JMenuBar menuBar;
    private JButton solveButton, generateButton, textOutputButton, texOutputButton, firstInputPDF, secondInputPDF, outInputPDF, libraryButton, closeLibraryButton, libraryDeleteButton;
    private JComboBox solveCombobox, generateCombobox, angleCombobox, relationCombobox, disjointCombobox, libraryCombobox;
    private JRadioButton firstGeneralRadioButton, firstParametricRadioButton, secondGeneralRadioButton, secondParametricRadioButton;
    private JScrollPane firstInputScroll, secondInputScroll, outputScroll;
    private JTextArea firstInputTextarea, secondInputTextarea, outputTextarea;
    private JTextField dim1Textfield, dim2Textfield, DIMTextfield, distanceTextfield, dimTextfield;
    private JLabel firstInvalid, secondInvalid, dim1Label, dim2Label, DIMLabel, distanceLabel, angleLabel, outputLabel, disjointLabel, dimLabel, dimSubLabel, outInvalid, intLabel;
    private JPanel inputPanel, taskPanel, generatePanel, firstGeneratePanel, secondGeneratePanel, outputPanel, libraryPanel;
    private Border blackLineBorder = BorderFactory.createLineBorder(Color.black), titledBorder;
    private final String piSymbol = "\u03C0";

    public GUI(){
        BufferedImage smallRedCircle = new BufferedImage(20, 20, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D img = smallRedCircle.createGraphics();
        img.setColor(Color.RED);
        img.fillOval(0, 0, 10, 10);

        setTitle("CVIČEBNICE ANALYTICKÉ GEOMETRIE");
        setSize(633,492);
        generateMenu();
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel(null);
        contentPane.setPreferredSize(new Dimension(633,492));
        contentPane.setBackground(new Color(192,192,192));

        inputPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "vstup pro řešení / výstup pro generování");
        inputPanel.setBorder(titledBorder);
        inputPanel.setBounds(25,25,584,196);
        inputPanel.setBackground(new Color(192,192,192));
        inputPanel.setForeground(new Color(0,0,0));
        inputPanel.setEnabled(true);
        inputPanel.setFont(new Font("sansserif",0,12));
        inputPanel.setVisible(true);
        inputPanel.setLayout(null);

        libraryPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "Zadejte číslo příkladu");
        libraryPanel.setBorder(titledBorder);
        libraryPanel.setBounds(92,60,400,110);
        libraryPanel.setBackground(new Color(214,217,223));
        libraryPanel.setForeground(new Color(0,0,0));
        libraryPanel.setEnabled(true);
        libraryPanel.setFont(new Font("sansserif",0,12));
        libraryPanel.setVisible(false);
        libraryPanel.setLayout(null);

        /*
        String[] libraryList = {"asdasd","asdasdad"};
        libraryCombobox = new JComboBox<String>(libraryList);
        libraryCombobox.setBounds(20,30,50,30);
        libraryCombobox.setBackground(new Color(214,217,223));
        libraryCombobox.setForeground(new Color(0,0,0));
        libraryCombobox.setEnabled(true);
        libraryCombobox.setFont(new Font("sansserif",0,12));
        libraryCombobox.setVisible(false);
         */

        closeLibraryButton = new JButton();
        closeLibraryButton.setBounds(160,73,80,20);
        closeLibraryButton.setBackground(new Color(214,217,223));
        closeLibraryButton.setForeground(new Color(0,0,0));
        closeLibraryButton.setEnabled(true);
        closeLibraryButton.setFont(new Font("sansserif",0,12));
        closeLibraryButton.setText("zavřít");
        closeLibraryButton.setVisible(true);
        closeLibraryButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt){
                    libraryPanel.setVisible(false);
                    libraryCombobox.setVisible(false);
                    //closeLibraryButton.setVisible(false);
                }
            }
        );

        libraryButton = new JButton();
        libraryButton.setBounds(290,30,90,30);
        libraryButton.setBackground(new Color(214,217,223));
        libraryButton.setForeground(new Color(0,0,0));
        libraryButton.setEnabled(true);
        libraryButton.setFont(new Font("sansserif",0,12));
        libraryButton.setText("nahrát");
        libraryButton.setVisible(true);
        libraryButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt){
                    libraryPanel.setVisible(false);
                    libraryCombobox.setVisible(false);

                    try {
                        libraryLoad();
                    }
                    catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny, zkontorolujte obsah souboru library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        );

        libraryDeleteButton = new JButton();
        libraryDeleteButton.setBounds(290,30,90,30);
        libraryDeleteButton.setBackground(new Color(214,217,223));
        libraryDeleteButton.setForeground(new Color(0,0,0));
        libraryDeleteButton.setEnabled(true);
        libraryDeleteButton.setFont(new Font("sansserif",0,12));
        libraryDeleteButton.setText("odstranit");
        libraryDeleteButton.setVisible(true);
        libraryDeleteButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt){
                    libraryPanel.setVisible(false);
                    libraryCombobox.setVisible(false);

                    try {
                        libraryDelete();
                    }
                    catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny, zkontorolujte obsah souboru library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        );

        firstInputPDF = new JButton();
        firstInputPDF.setBounds(87,20,112,20);
        firstInputPDF.setBackground(new Color(214,217,223));
        firstInputPDF.setForeground(new Color(0,0,0));
        firstInputPDF.setEnabled(true);
        firstInputPDF.setFont(new Font("sansserif",0,12));
        firstInputPDF.setText("podprostor A");
        firstInputPDF.setVisible(true);
        firstInputPDF.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    try {
                        String in = firstInputTextarea.getText();
                        InputReader reader = new InputReader();
                        Subspace first = reader.getSubspace(in);
                        if (first.isNull()) throw new Exception();
                        String latex = (first.getOriginalForm() ? "\\[" + first.toTeXGeneral() + "\\]" : "\\[" + first.toTeXParametric() + "\\]");
                        TeXFormula formula = new TeXFormula(latex);
                        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                        icon.setInsets(new Insets(5, 5, 5, 5));

                        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                        Graphics2D output = image.createGraphics();
                        output.setColor(Color.white);
                        output.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                        JLabel typeout = new JLabel();
                        typeout.setForeground(new Color(0, 0, 0));
                        icon.paintIcon(typeout, output, 0, 0);

                        JLabel texLabel = new JLabel(icon);
                        JFrame texFrame = new JFrame("Podprostor A");
                        texFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        texFrame.getContentPane().add(texLabel, BorderLayout.CENTER);
                        texFrame.setLocationRelativeTo(firstInputTextarea);
                        texFrame.pack();
                        texFrame.setVisible(true);
                    }
                    catch (Exception e) {
                        firstInvalid.setVisible(true);
                        Timer t = new Timer(3000, new ActionListener()
                                {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        firstInvalid.setVisible(false);
                                    }
                                }
                            );
                        t.setRepeats(false);
                        t.start();
                        return;
                    }
                }
            }
        );

        firstInvalid = new JLabel(new ImageIcon(smallRedCircle));
        firstInvalid.setBounds(263,25,20,20);
        firstInvalid.setVisible(false);

        firstInputTextarea = new JTextArea();
        firstInputTextarea.setBackground(new Color(255,255,255));
        firstInputTextarea.setForeground(new Color(0,0,0));
        firstInputTextarea.setEnabled(true);
        firstInputTextarea.setFont(new Font("sansserif",0,12));
        firstInputTextarea.setText("");
        firstInputTextarea.setVisible(true);

        firstInputScroll = new JScrollPane(firstInputTextarea);
        firstInputScroll.setBounds(5,45,278,145);
        firstInputScroll.setBackground(new Color(255,255,255));
        firstInputScroll.setForeground(new Color(0,0,0));
        firstInputScroll.setEnabled(true);
        firstInputScroll.setBorder(BorderFactory.createBevelBorder(1));
        firstInputScroll.setVisible(true);

        secondInputPDF = new JButton();
        secondInputPDF.setBounds(382,20,112,20);
        secondInputPDF.setBackground(new Color(214,217,223));
        secondInputPDF.setForeground(new Color(0,0,0));
        secondInputPDF.setEnabled(true);
        secondInputPDF.setFont(new Font("sansserif",0,12));
        secondInputPDF.setText("podprostor B");
        secondInputPDF.setVisible(true);
        secondInputPDF.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    try {
                        String in = secondInputTextarea.getText();
                        InputReader reader = new InputReader();
                        Subspace second = reader.getSubspace(in);
                        if (second.isNull()) throw new Exception();
                        String latex = (second.getOriginalForm() ? "\\[" + second.toTeXGeneral() + "\\]" : "\\[" + second.toTeXParametric() + "\\]");
                        TeXFormula formula = new TeXFormula(latex);
                        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                        icon.setInsets(new Insets(5, 5, 5, 5));

                        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                        Graphics2D output = image.createGraphics();
                        output.setColor(Color.white);
                        output.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                        JLabel typeout = new JLabel();
                        typeout.setForeground(new Color(0, 0, 0));
                        icon.paintIcon(typeout, output, 0, 0);

                        JLabel texLabel = new JLabel(icon);
                        JFrame texFrame = new JFrame("Podprostor B");
                        texFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        texFrame.getContentPane().add(texLabel, BorderLayout.CENTER);
                        texFrame.setLocationRelativeTo(firstInputTextarea);
                        texFrame.pack();
                        texFrame.setVisible(true);
                    }
                    catch (Exception e) {
                        secondInvalid.setVisible(true);
                        Timer t = new Timer(3000, new ActionListener()
                                {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        secondInvalid.setVisible(false);
                                    }
                                }
                            );
                        t.setRepeats(false);
                        t.start();
                        return;
                    }
                }
            }
        );

        secondInvalid = new JLabel(new ImageIcon(smallRedCircle));
        secondInvalid.setBounds(558,25,20,20);
        secondInvalid.setVisible(false);

        secondInputTextarea = new JTextArea();
        secondInputTextarea.setBackground(new Color(255,255,255));
        secondInputTextarea.setForeground(new Color(0,0,0));
        secondInputTextarea.setEnabled(true);
        secondInputTextarea.setFont(new Font("sansserif",0,12));
        secondInputTextarea.setText("");
        secondInputTextarea.setVisible(true);

        secondInputScroll = new JScrollPane(secondInputTextarea);
        secondInputScroll.setBounds(300,45,278,145);
        secondInputScroll.setBackground(new Color(255,255,255));
        secondInputScroll.setForeground(new Color(0,0,0));
        secondInputScroll.setEnabled(true);
        secondInputScroll.setBorder(BorderFactory.createBevelBorder(1));
        secondInputScroll.setVisible(true);

        taskPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "parametry úlohy");
        taskPanel.setBorder(titledBorder);
        taskPanel.setBounds(25,222,413,160);
        taskPanel.setBackground(new Color(214,217,223));
        taskPanel.setForeground(new Color(0,0,0));
        taskPanel.setEnabled(true);
        taskPanel.setFont(new Font("sansserif",0,12));
        taskPanel.setVisible(true);
        taskPanel.setLayout(null);

        String[] solveTasklist = {"Vyber úlohu:","Popiš podprostor(y).","Převeď na obecný/parametrický tvar.","Protínají se oba podprostory?","Najdi průnik podprostorů."/*,"Jsou podprostory rovnoběžné?"*/,
                "Urči vzájemnou polohu podprostorů.","Spočítej vzdálenost podprostorů.","Spočítej odchylku podprostorů.","Spočítej ortogonální doplněk k podprostoru."};
        solveCombobox = new JComboBox<String>(solveTasklist);
        solveCombobox.setBounds(20,30,260,35);
        solveCombobox.setBackground(new Color(214,217,223));
        solveCombobox.setForeground(new Color(0,0,0));
        solveCombobox.setEnabled(true);
        solveCombobox.setFont(new Font("sansserif",0,12));
        solveCombobox.setVisible(true);
        solveCombobox.addActionListener (new ActionListener ()
            {
                public void actionPerformed(ActionEvent evt) {
                    if(solveCombobox.getSelectedItem().toString()=="Vyber úlohu:")      solveButton.setEnabled(false);
                    else                                                                solveButton.setEnabled(true);
                }
            }
        );

        solveButton = new JButton();
        solveButton.setBounds(293,30,100,35);
        solveButton.setBackground(new Color(214,217,223));
        solveButton.setForeground(new Color(0,0,0));
        solveButton.setEnabled(false);
        solveButton.setFont(new Font("sansserif",0,12));
        solveButton.setText("Vyřeš");
        solveButton.setVisible(true);
        solveButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {

                    try {
                        solve(solveCombobox.getSelectedItem().toString());
                    }

                    catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Při řešení došlo k neočekávané chybě. Pravděpodobně došlo k přetečení číselného formátu, tzn. že při řešení "
                            + "se neúměrně zvyšovala některá čísla. Zejména případě náhodně generovaných úloh k takovýmto chybám "
                            + "může docházet. Zadejte prosím jinou úlohu.", "Chyba", JOptionPane.ERROR_MESSAGE);
                    }

                }
            }
        );

        String[] generateTasklist = {"Vyber úlohu:","náhodné podprostory","podprostory s danou vzájemnou polohou","podprostory s daným průnikem","podprostory s danou vzdáleností","podprostory s danou odchylkou"};
        generateCombobox = new JComboBox<String>(generateTasklist);
        generateCombobox.setBounds(20,70,260,35);
        generateCombobox.setBackground(new Color(214,217,223));
        generateCombobox.setForeground(new Color(0,0,0));
        generateCombobox.setEnabled(true);
        generateCombobox.setFont(new Font("sansserif",0,12));
        generateCombobox.setVisible(true);
        generateCombobox.addActionListener (new ActionListener ()
            {
                public void actionPerformed(ActionEvent evt) {
                    if(generateCombobox.getSelectedItem().toString()=="Vyber úlohu:") {
                        generatePanel.setVisible(false);
                        generateButton.setEnabled(false);
                    }else {
                        generatePanel.setVisible(true);
                        generateButton.setEnabled(true);
                    }

                    if(generateCombobox.getSelectedItem().toString()=="podprostory s danou vzdáleností") {
                        distanceLabel.setVisible(true);
                        distanceTextfield.setVisible(true);
                    }else {
                        distanceLabel.setVisible(false);
                        distanceTextfield.setVisible(false);
                    }

                    if(generateCombobox.getSelectedItem().toString()=="podprostory s danou vzájemnou polohou") {
                        relationCombobox.setVisible(true);
                        if(relationCombobox.getSelectedItem().toString()=="rovnoběžné") {
                            disjointLabel.setVisible(true);
                            disjointCombobox.setVisible(true);
                        }else{
                            disjointLabel.setVisible(false);
                            disjointCombobox.setVisible(false);
                        }

                        if(relationCombobox.getSelectedItem().toString()=="různoběžné") {
                            dimSubLabel.setVisible(true);
                            dimTextfield.setVisible(true);
                        }else{
                            dimSubLabel.setVisible(false);
                            dimTextfield.setVisible(false);
                        }

                        if(relationCombobox.getSelectedItem().toString()=="mimoběžné") {
                            dimLabel.setVisible(true);
                            dimTextfield.setVisible(true);
                        }else{
                            dimLabel.setVisible(false);
                        }
                    }else{
                        relationCombobox.setVisible(false);
                        dimLabel.setVisible(false);
                        dimTextfield.setVisible(false);
                        dimSubLabel.setVisible(false);
                        disjointLabel.setVisible(false);
                        disjointCombobox.setVisible(false);
                    }

                    if(generateCombobox.getSelectedItem().toString()=="podprostory s daným průnikem") {
                        outputTextarea.setEnabled(true);
                        outInputPDF.setVisible(true);
                        intLabel.setVisible(true);
                        outputTextarea.setText("");
                    }else{
                        outputTextarea.setEnabled(false);
                        outInputPDF.setVisible(false);
                        intLabel.setVisible(false);
                    }

                    if(generateCombobox.getSelectedItem().toString()=="podprostory s danou odchylkou") {
                        angleLabel.setVisible(true);
                        angleCombobox.setVisible(true);
                    }else{
                        angleLabel.setVisible(false);
                        angleCombobox.setVisible(false);
                    }
                }
            }
        );

        generateButton = new JButton();
        generateButton.setBounds(293,70,100,35);
        generateButton.setBackground(new Color(214,217,223));
        generateButton.setForeground(new Color(0,0,0));
        generateButton.setEnabled(false);
        generateButton.setFont(new Font("sansserif",0,12));
        generateButton.setText("Vygeneruj");
        generateButton.setVisible(true);
        generateButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {      

                    try {

                        generate(generateCombobox.getSelectedItem().toString());

                    }
                    catch (Exception e) {

                        JOptionPane.showMessageDialog(null, "Při řešení došlo k neočekávané chybě. Opakujte prosím požadavek.", "Chyba", JOptionPane.ERROR_MESSAGE);

                    } 

                }
            }
        );

        distanceLabel = new JLabel();
        distanceLabel.setBounds(20,110,150,35);
        distanceLabel.setBackground(new Color(255,255,255));
        distanceLabel.setForeground(new Color(0,0,0));
        distanceLabel.setEnabled(true);
        distanceLabel.setFont(new Font("sansserif",0,12));
        distanceLabel.setText("Požadovaná vzdálenost:");
        distanceLabel.setVisible(false);

        distanceTextfield = new JTextField();
        distanceTextfield.setBounds(153,110,50,35);
        distanceTextfield.setBackground(new Color(255,255,255));
        distanceTextfield.setForeground(new Color(0,0,0));
        distanceTextfield.setEnabled(true);
        distanceTextfield.setFont(new Font("sansserif",0,12));
        distanceTextfield.setText("");
        distanceTextfield.setVisible(false);

        angleLabel = new JLabel();
        angleLabel.setBounds(20,110,150,35);
        angleLabel.setBackground(new Color(255,255,255));
        angleLabel.setForeground(new Color(0,0,0));
        angleLabel.setEnabled(true);
        angleLabel.setFont(new Font("sansserif",0,12));
        angleLabel.setText("Požadovaná odchylka:");
        angleLabel.setVisible(false);

        String[] angleList = {"0 rad (tj. 0°)",piSymbol+"/6 rad (tj. 30°)",piSymbol+"/4 rad (tj. 45°)",piSymbol+"/3 rad (tj. 60°)",piSymbol+"/2 rad (tj. 90°)"};
        angleCombobox = new JComboBox<String>(angleList);
        angleCombobox.setBounds(153,110,127,35);
        angleCombobox.setBackground(new Color(214,217,223));
        angleCombobox.setForeground(new Color(0,0,0));
        angleCombobox.setEnabled(true);
        angleCombobox.setFont(new Font("sansserif",0,12));
        angleCombobox.setVisible(false);

        String[] relationList = {"různoběžné", "mimoběžné", "rovnoběžné"};
        relationCombobox = new JComboBox<String>(relationList);
        relationCombobox.setBounds(20,110,110,35);
        relationCombobox.setBackground(new Color(214,217,223));
        relationCombobox.setForeground(new Color(0,0,0));
        relationCombobox.setEnabled(true);
        relationCombobox.setFont(new Font("sansserif",0,12));
        relationCombobox.setVisible(false);
        relationCombobox.addActionListener (new ActionListener ()
            {
                public void actionPerformed(ActionEvent evt) {             
                    if(relationCombobox.getSelectedItem().toString()=="rovnoběžné") {
                        disjointLabel.setVisible(true);
                        disjointCombobox.setVisible(true);
                    }else{
                        disjointLabel.setVisible(false);
                        disjointCombobox.setVisible(false);
                    }

                    if(relationCombobox.getSelectedItem().toString()=="různoběžné") {
                        dimSubLabel.setVisible(true);
                        dimTextfield.setVisible(true);
                    }else{
                        dimSubLabel.setVisible(false);
                        dimTextfield.setVisible(false);
                    }

                    if(relationCombobox.getSelectedItem().toString()=="mimoběžné") {
                        dimLabel.setVisible(true);
                        dimTextfield.setVisible(true);
                    }else{
                        dimLabel.setVisible(false);
                    }
                }
            }
        );

        disjointLabel = new JLabel();
        disjointLabel.setBounds(155,110,150,35);
        disjointLabel.setBackground(new Color(255,255,255));
        disjointLabel.setForeground(new Color(0,0,0));
        disjointLabel.setEnabled(true);
        disjointLabel.setFont(new Font("sansserif",0,12));
        disjointLabel.setText("disjunktní");
        disjointLabel.setVisible(false);

        String[] yesNoList = {"ANO", "NE"};
        disjointCombobox = new JComboBox<String>(yesNoList);
        disjointCombobox.setBounds(220,110,60,35);
        disjointCombobox.setBackground(new Color(214,217,223));
        disjointCombobox.setForeground(new Color(0,0,0));
        disjointCombobox.setEnabled(true);
        disjointCombobox.setFont(new Font("sansserif",0,12));
        disjointCombobox.setVisible(false);

        dimLabel = new JLabel();
        dimLabel.setBounds(156,110,240,35);
        dimLabel.setBackground(new Color(255,255,255));
        dimLabel.setForeground(new Color(0,0,0));
        dimLabel.setEnabled(true);
        dimLabel.setFont(new Font("sansserif",0,12));
        dimLabel.setText("dim. průniku zaměření (nepovinné):");
        dimLabel.setVisible(false);

        dimSubLabel = new JLabel();
        dimSubLabel.setBounds(140,110,240,35);
        dimSubLabel.setBackground(new Color(255,255,255));
        dimSubLabel.setForeground(new Color(0,0,0));
        dimSubLabel.setEnabled(true);
        dimSubLabel.setFont(new Font("sansserif",0,12));
        dimSubLabel.setText("dim. průniku podprostorů (nepovinné):");
        dimSubLabel.setVisible(false);

        dimTextfield = new JTextField();
        dimTextfield.setBounds(358,110,35,35);
        dimTextfield.setBackground(new Color(255,255,255));
        dimTextfield.setForeground(new Color(0,0,0));
        dimTextfield.setEnabled(true);
        dimTextfield.setFont(new Font("sansserif",0,12));
        dimTextfield.setText("");
        dimTextfield.setVisible(false);

        intLabel = new JLabel();
        intLabel.setBounds(20,110,290,35);
        intLabel.setBackground(new Color(255,255,255));
        intLabel.setForeground(new Color(0,0,0));
        intLabel.setEnabled(true);
        intLabel.setFont(new Font("sansserif",0,12));
        intLabel.setText("Průnik zadejte jako podprostor do pole výsledek");
        intLabel.setVisible(false);

        outInputPDF = new JButton();
        outInputPDF.setBounds(293,115,80,25);
        outInputPDF.setBackground(new Color(214,217,223));
        outInputPDF.setForeground(new Color(0,0,0));
        outInputPDF.setEnabled(true);
        outInputPDF.setFont(new Font("sansserif",0,12));
        outInputPDF.setText("ověření");
        outInputPDF.setVisible(false);
        outInputPDF.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    try {
                        String in = outputTextarea.getText();
                        InputReader reader = new InputReader();
                        Subspace first = reader.getSubspace(in);
                        if (first.isNull()) throw new Exception();
                        String latex = (first.getOriginalForm() ? "\\[" + first.toTeXGeneral() + "\\]" : "\\[" + first.toTeXParametric() + "\\]");
                        TeXFormula formula = new TeXFormula(latex);
                        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                        icon.setInsets(new Insets(5, 5, 5, 5));

                        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                        Graphics2D output = image.createGraphics();
                        output.setColor(Color.white);
                        output.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                        JLabel typeout = new JLabel();
                        typeout.setForeground(new Color(0, 0, 0));
                        icon.paintIcon(typeout, output, 0, 0);

                        JLabel texLabel = new JLabel(icon);
                        JFrame texFrame = new JFrame("Průnik podprostorů");
                        texFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        texFrame.getContentPane().add(texLabel, BorderLayout.CENTER);
                        texFrame.setLocationRelativeTo(firstInputTextarea);
                        texFrame.pack();
                        texFrame.setVisible(true);
                    }
                    catch (Exception e) {
                        outInvalid.setVisible(true);
                        Timer t = new Timer(3000, new ActionListener()
                                {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        outInvalid.setVisible(false);
                                    }
                                }
                            );
                        t.setRepeats(false);
                        t.start();
                        return;
                    }
                }
            }
        );

        outInvalid = new JLabel(new ImageIcon(smallRedCircle));
        outInvalid.setBounds(382,122,20,20);
        outInvalid.setVisible(false);

        outputPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "výstup pro řešení");
        outputPanel.setBorder(titledBorder);
        outputPanel.setBounds(25,383,584,85);
        outputPanel.setBackground(new Color(192,192,192));
        outputPanel.setForeground(new Color(0,0,0));
        outputPanel.setEnabled(true);
        outputPanel.setFont(new Font("sansserif",0,12));
        outputPanel.setVisible(true);
        outputPanel.setLayout(null);

        outputLabel = new JLabel();
        outputLabel.setBounds(5,20,55,60);
        outputLabel.setBackground(new Color(255,255,255));
        outputLabel.setForeground(new Color(0,0,0));
        outputLabel.setEnabled(true);
        outputLabel.setFont(new Font("sansserif",0,12));
        outputLabel.setText("Výsledek:");
        outputLabel.setVisible(true);

        outputTextarea = new JTextArea();
        outputTextarea.setBackground(new Color(255,255,255));
        outputTextarea.setForeground(new Color(0,0,0));
        outputTextarea.setEnabled(false);
        outputTextarea.setDisabledTextColor(new Color(0,0,0));
        outputTextarea.setFont(new Font("sansserif",0,12));
        outputTextarea.setText("Zatím není k dispozici žádný výsledek.");
        outputTextarea.setVisible(true);

        outputScroll = new JScrollPane(outputTextarea);
        outputScroll.setBounds(60,20,360,60);
        outputScroll.setBackground(new Color(255,255,255));
        outputScroll.setForeground(new Color(0,0,0));
        outputScroll.setEnabled(true);
        outputScroll.setBorder(BorderFactory.createBevelBorder(1));
        outputScroll.setVisible(true);

        textOutputButton = new JButton();
        textOutputButton.setBounds(423,20,75,60);
        textOutputButton.setBackground(new Color(214,217,223));
        textOutputButton.setForeground(new Color(0,0,0));
        textOutputButton.setEnabled(false);
        textOutputButton.setFont(new Font("sansserif",0,12));
        textOutputButton.setText("<html><center>Postup<br>(text)</center></html>");
        textOutputButton.setVisible(true);
        textOutputButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    openText();
                }
            }
        );

        texOutputButton = new JButton();
        texOutputButton.setBounds(501,20,75,60);
        texOutputButton.setBackground(new Color(214,217,223));
        texOutputButton.setForeground(new Color(0,0,0));
        texOutputButton.setEnabled(false);
        texOutputButton.setFont(new Font("sansserif",0,12));
        texOutputButton.setText("<html><center>Postup<br>(pdf)</center></html>");
        texOutputButton.setVisible(true);
        texOutputButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    createPDF();
                    openPDF();
                }
            }
        );

        generatePanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "parametry podprostorů");
        generatePanel.setBorder(titledBorder);
        generatePanel.setBounds(438,222,170,160);
        generatePanel.setBackground(new Color(214,217,223));
        generatePanel.setForeground(new Color(0,0,0));
        generatePanel.setEnabled(true);
        generatePanel.setFont(new Font("sansserif",0,12));
        generatePanel.setVisible(false);
        generatePanel.setLayout(null);

        firstGeneratePanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "podpr. 1");
        firstGeneratePanel.setBorder(titledBorder);
        firstGeneratePanel.setBounds(5,15,80,110);
        firstGeneratePanel.setBackground(new Color(214,217,223));
        firstGeneratePanel.setForeground(new Color(0,0,0));
        firstGeneratePanel.setEnabled(true);
        firstGeneratePanel.setFont(new Font("sansserif",0,12));
        firstGeneratePanel.setVisible(true);
        firstGeneratePanel.setLayout(null);

        firstGeneralRadioButton = new JRadioButton();
        firstGeneralRadioButton.setBounds(5,15,70,30);
        firstGeneralRadioButton.setBackground(new Color(214,217,223));
        firstGeneralRadioButton.setForeground(new Color(0,0,0));
        firstGeneralRadioButton.setEnabled(true);
        firstGeneralRadioButton.setFont(new Font("sansserif",0,12));
        firstGeneralRadioButton.setText("obecné");
        firstGeneralRadioButton.setVisible(true);
        firstGeneralRadioButton.setSelected(true);

        firstParametricRadioButton = new JRadioButton();
        firstParametricRadioButton.setBounds(5,45,70,30);
        firstParametricRadioButton.setBackground(new Color(214,217,223));
        firstParametricRadioButton.setForeground(new Color(0,0,0));
        firstParametricRadioButton.setEnabled(true);
        firstParametricRadioButton.setFont(new Font("sansserif",0,12));
        firstParametricRadioButton.setText("param.");
        firstParametricRadioButton.setVisible(true);

        ButtonGroup subspace1 = new ButtonGroup();
        subspace1.add(firstGeneralRadioButton);
        subspace1.add(firstParametricRadioButton);

        dim1Label = new JLabel();
        dim1Label.setBounds(5,75,70,30);
        dim1Label.setBackground(new Color(214,217,223));
        dim1Label.setForeground(new Color(0,0,0));
        dim1Label.setEnabled(true);
        dim1Label.setFont(new Font("sansserif",0,12));
        dim1Label.setText("dim.:");
        dim1Label.setVisible(true);

        dim1Textfield = new JTextField();
        dim1Textfield.setBounds(35,75,40,30);
        dim1Textfield.setBackground(new Color(255,255,255));
        dim1Textfield.setForeground(new Color(0,0,0));
        dim1Textfield.setEnabled(true);
        dim1Textfield.setFont(new Font("sansserif",0,12));
        dim1Textfield.setText("");
        dim1Textfield.setVisible(true);

        secondGeneratePanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "podpr. 2");
        secondGeneratePanel.setBorder(titledBorder);
        secondGeneratePanel.setBounds(85,15,80,110);
        secondGeneratePanel.setBackground(new Color(214,217,223));
        secondGeneratePanel.setForeground(new Color(0,0,0));
        secondGeneratePanel.setEnabled(true);
        secondGeneratePanel.setFont(new Font("sansserif",0,12));
        secondGeneratePanel.setVisible(true);
        secondGeneratePanel.setLayout(null);

        secondGeneralRadioButton = new JRadioButton();
        secondGeneralRadioButton.setBounds(5,15,70,30);
        secondGeneralRadioButton.setBackground(new Color(214,217,223));
        secondGeneralRadioButton.setForeground(new Color(0,0,0));
        secondGeneralRadioButton.setEnabled(true);
        secondGeneralRadioButton.setFont(new Font("sansserif",0,12));
        secondGeneralRadioButton.setText("obecné");
        secondGeneralRadioButton.setVisible(true);
        secondGeneralRadioButton.setSelected(true);

        secondParametricRadioButton = new JRadioButton();
        secondParametricRadioButton.setBounds(5,45,70,30);
        secondParametricRadioButton.setBackground(new Color(214,217,223));
        secondParametricRadioButton.setForeground(new Color(0,0,0));
        secondParametricRadioButton.setEnabled(true);
        secondParametricRadioButton.setFont(new Font("sansserif",0,12));
        secondParametricRadioButton.setText("param.");
        secondParametricRadioButton.setVisible(true);

        ButtonGroup subspace2 = new ButtonGroup();
        subspace2.add(secondGeneralRadioButton);
        subspace2.add(secondParametricRadioButton);

        dim2Label = new JLabel();
        dim2Label.setBounds(5,75,70,30);
        dim2Label.setBackground(new Color(214,217,223));
        dim2Label.setForeground(new Color(0,0,0));
        dim2Label.setEnabled(true);
        dim2Label.setFont(new Font("sansserif",0,12));
        dim2Label.setText("dim.:");
        dim2Label.setVisible(true);

        dim2Textfield = new JTextField();
        dim2Textfield.setBounds(35,75,40,30);
        dim2Textfield.setBackground(new Color(255,255,255));
        dim2Textfield.setForeground(new Color(0,0,0));
        dim2Textfield.setEnabled(true);
        dim2Textfield.setFont(new Font("sansserif",0,12));
        dim2Textfield.setText("");
        dim2Textfield.setVisible(true);

        DIMLabel = new JLabel();
        DIMLabel.setBounds(5,125,160,30);
        DIMLabel.setBackground(new Color(214,217,223));
        DIMLabel.setForeground(new Color(0,0,0));
        DIMLabel.setEnabled(true);
        DIMLabel.setFont(new Font("sansserif",0,12));
        DIMLabel.setText("dim. celého prostoru:");
        DIMLabel.setVisible(true);

        DIMTextfield = new JTextField();
        DIMTextfield.setBounds(125,125,40,30);
        DIMTextfield.setBackground(new Color(255,255,255));
        DIMTextfield.setForeground(new Color(0,0,0));
        DIMTextfield.setEnabled(true);
        DIMTextfield.setFont(new Font("sansserif",0,12));
        DIMTextfield.setText("");
        DIMTextfield.setVisible(true);

        //adding components to contentPane panel
        inputPanel.add(libraryPanel);
        libraryPanel.add(libraryButton);
        libraryPanel.add(closeLibraryButton);
        libraryPanel.add(libraryDeleteButton);
        contentPane.add(inputPanel);
        inputPanel.add(firstInputPDF);
        inputPanel.add(firstInvalid);
        inputPanel.add(firstInputScroll);
        inputPanel.add(secondInputPDF);
        inputPanel.add(secondInvalid);
        inputPanel.add(secondInputScroll);
        contentPane.add(taskPanel);
        taskPanel.add(solveCombobox);
        taskPanel.add(solveButton);
        taskPanel.add(generateCombobox);
        taskPanel.add(generateButton);
        taskPanel.add(distanceLabel);
        taskPanel.add(distanceTextfield);
        taskPanel.add(angleLabel);
        taskPanel.add(angleCombobox);
        taskPanel.add(relationCombobox);
        taskPanel.add(disjointLabel);
        taskPanel.add(disjointCombobox);
        taskPanel.add(dimTextfield);
        taskPanel.add(dimLabel);
        taskPanel.add(dimSubLabel);
        taskPanel.add(outInputPDF);
        taskPanel.add(outInvalid);
        taskPanel.add(intLabel);
        contentPane.add(generatePanel);
        generatePanel.add(firstGeneratePanel);
        firstGeneratePanel.add(firstGeneralRadioButton);
        firstGeneratePanel.add(firstParametricRadioButton);
        firstGeneratePanel.add(dim1Label);
        firstGeneratePanel.add(dim1Textfield);
        generatePanel.add(secondGeneratePanel);
        secondGeneratePanel.add(secondGeneralRadioButton);
        secondGeneratePanel.add(secondParametricRadioButton);
        secondGeneratePanel.add(dim2Label);
        secondGeneratePanel.add(dim2Textfield);
        generatePanel.add(DIMLabel);
        generatePanel.add(DIMTextfield);
        contentPane.add(outputPanel);
        outputPanel.add(outputLabel);
        outputPanel.add(outputScroll);
        outputPanel.add(textOutputButton);
        outputPanel.add(texOutputButton);
        //adding panel to JFrame and seting of window position and close operation
        getContentPane().add(contentPane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
    }

    private void solve (String task) {
        boolean firstNull, secondNull;
        boolean passedFirst = false;
        Subspace first, second;

        clearOutputFiles();

        InputReader reader = new InputReader();
        try {
            String in = firstInputTextarea.getText();
            first = reader.getSubspace(in);
            firstNull = first.isNull();
            passedFirst = true;

            in = secondInputTextarea.getText();
            second = reader.getSubspace(in);
            secondNull = second.isNull();
        }
        catch (Exception e) {
            if (e.getMessage() == "The matrix is not solvable!") {
                if (!passedFirst) 
                    JOptionPane.showMessageDialog(null, "Soustava rovnic, kterou je zadán první podprostor, není řešitelná. Nejde tak o podprostor a požadovanou operaci "
                        + "tedy nelze provést. Zadejte prosím existující podprostor.", "Chyba", JOptionPane.ERROR_MESSAGE);
                else
                    JOptionPane.showMessageDialog(null, "Soustava rovnic, kterou je zadán druhý podprostor, není řešitelná. Nejde tak o podprostor a požadovanou operaci "
                        + "tedy nelze provést. Zadejte prosím existující podprostor.", "Chyba", JOptionPane.ERROR_MESSAGE);
            }
            else JOptionPane.showMessageDialog(null, "Některý z podprostorů nebyl zadán správně. Opravte, prosím, zadání. Pokud si nejste jistí, jak má zadání vypadat, "
                    + "konzultujte nápovědu k zadávání podprostorů (v horní části okna).", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (firstNull && secondNull) {
            JOptionPane.showMessageDialog(null, "Je nutné zadat alespoň jeden podprostor.", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if ((firstNull || secondNull) && !(task == "Popiš podprostor(y)." || task == "Převeď na obecný/parametrický tvar."
            || task == "Spočítej ortogonální doplněk k podprostoru."))
        {
            JOptionPane.showMessageDialog(null, "K vybranému typu úlohy je nutné zadat oba podprostory.", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (!(task == "Popiš podprostor(y)." || task == "Převeď na obecný/parametrický tvar." || task == "Spočítej ortogonální doplněk k podprostoru.") &&
        (first.getDIM() != second.getDIM()))
        {
            JOptionPane.showMessageDialog(null, "Podprostory nejsou podprostory téhož prostoru (mají jiný počet proměnných)!", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String out = new String();

        textOut("ZADÁNÍ\r\n");
        textOut("======\r\n");
        texOut("\\section*{Zadání}");

        if (!firstNull) {
            textOut("Máme podprostor A zadaný " + (first.getOriginalForm() ? "obecně" : "parametricky") + ":\r\n\r\n");
            texOut("\\noindent Máme podprostor $\\mathcal{A}$ zadaný " + (first.getOriginalForm() ? "obecně" : "parametricky") + ":\n\\[");
            if (first.getOriginalForm()) {
                textOut(first.toTextGeneral());
                texOut(first.toTeXGeneral());
            }
            else {
                textOut(first.toTextParametric());
                texOut(first.toTeXParametric());
            }

            if (!secondNull) {
                textOut("\r\na podprostor B zadaný " + ((first.getOriginalForm()==second.getOriginalForm()) ? "také " : "")
                    + (second.getOriginalForm() ? "obecně" : "parametricky") + ":\r\n\r\n");
                texOut("\\]\na podprostor $\\mathcal{B}$ zadaný " + ((first.getOriginalForm()==second.getOriginalForm()) ? "také " : "")
                    + (second.getOriginalForm() ? "obecně" : "parametricky") + ":\n\\[");
                if (second.getOriginalForm()) {
                    textOut(second.toTextGeneral());
                    texOut(second.toTeXGeneral());
                }
                else {
                    textOut(second.toTextParametric());
                    texOut(second.toTeXParametric());
                }
            }

            textOut("\r\n");
            texOut("\\]\n");
        }
        else {
            textOut("Máme podprostor B zadaný " + (second.getOriginalForm() ? "obecně" : "parametricky") + ":\r\n\r\n");
            texOut("\\noindent Máme podprostor $\\mathcal{B}$ zadaný " + (second.getOriginalForm() ? "obecně" : "parametricky") + ":\n\\[");
            if (second.getOriginalForm()) {
                textOut(second.toTextGeneral());
                texOut(second.toTeXGeneral());
            }
            else {
                textOut(second.toTextParametric());
                texOut(second.toTeXParametric());
            }

            textOut("\r\n");
            texOut("\\]\n");
        }        
        textOut("Naším úkolem je ");
        texOut("Naším úkolem je ");

        if (task == "Popiš podprostor(y)."){
            if (firstNull == secondNull) {
                textOut("popsat oba podprostory.");
                texOut("popsat oba podprostory.");
            }
            else {
                textOut("podprostor popsat.");
                texOut("podprostor popsat.");
            }

            if (!firstNull) {
                textOut("\r\n\r\nPOPIS A\r\n");
                textOut("=======\r\n");
                texOut("\n\\section*{Popis $\\mathcal{A}$}");
                out = "A: " + first.describe('A') + "\n";
            }
            if (!secondNull) {
                textOut("\r\n\r\nPOPIS B\r\n");
                textOut("=======\r\n");
                texOut("\n\\section*{Popis $\\mathcal{B}$}");
                out += "B: " + second.describe('B');
            }
        }            

        if (task == "Převeď na obecný/parametrický tvar."){
            if (firstNull == secondNull) {
                if (first.getOriginalForm() == second.getOriginalForm()) {
                    textOut("oba podprostory převést na " + (first.getOriginalForm() ? "parametrický tvar." : "obecný tvar."));
                    texOut("oba podprostory převést na " + (first.getOriginalForm() ? "parametrický tvar." : "obecný tvar."));
                }
                else {
                    textOut("podprostor A převést na " + (first.getOriginalForm() ? "parametrický " : "obecný ")
                        + "podprostor B na " + (second.getOriginalForm() ? "parametrický tvar." : "obecný tvar."));
                    texOut("podprostor A převést na " + (first.getOriginalForm() ? "parametrický " : "obecný ")
                        + "podprostor B na " + (second.getOriginalForm() ? "parametrický tvar." : "obecný tvar."));
                }
            }
            else {
                textOut("podprostor převést na ");
                if (firstNull) textOut(second.getOriginalForm() ? "parametrický tvar." : "obecný tvar.");
                else           textOut(first.getOriginalForm() ? "parametrický tvar." : "obecný tvar.");
                texOut("podprostor převést na ");
                if (firstNull) texOut(second.getOriginalForm() ? "parametrický tvar." : "obecný tvar.");
                else           texOut(first.getOriginalForm() ? "parametrický tvar." : "obecný tvar.");
            }

            if (!firstNull) {
                textOut("\r\n\r\nPŘEVOD A NA " + (first.getOriginalForm() ? "PARAMETRICKÝ TVAR" : "OBECNÝ TVAR") + "\r\n");
                textOut("============" + (first.getOriginalForm() ? "=================" : "===========") + "\r\n");
                texOut("\\section*{Převod $\\mathcal{A}$ na "  + (first.getOriginalForm() ? "parametrický tvar" : "obecný tvar") + "}\n");
                if (first.getOriginalForm()) {
                    first.checkParametricForm('A', true);
                    out = "A: " + first.toTextParametric() + "\n";
                }
                else {
                    first.checkGeneralForm('A', true);
                    if (first.isGeneralFormAvailable())
                        out = "A: " + first.toTextGeneral() + "\n";
                    else
                        out = "A: celý prostor nemá obecné vyjádření\n";
                }
            }
            if (!secondNull) {
                textOut("\r\n\r\nPŘEVOD B NA " + (second.getOriginalForm() ? "PARAMETRICKÝ TVAR" : "OBECNÝ TVAR") + "\r\n");
                textOut("============" + (second.getOriginalForm() ? "=================" : "===========") + "\r\n");
                texOut("\\section*{Převod $\\mathcal{B}$ na "  + (second.getOriginalForm() ? "parametrický tvar" : "obecný tvar") + "}\n");
                if (second.getOriginalForm()) {
                    second.checkParametricForm('B', true);
                    out += "B: " + second.toTextParametric() + "\n";
                }
                else {
                    second.checkGeneralForm('B', true);
                    if (second.isGeneralFormAvailable())
                        out += "B: " + second.toTextGeneral() + "\n";
                    else
                        out += "B: celý prostor nemá obecné vyjádření\n";
                }
            }
        }

        if (task == "Urči vzájemnou polohu podprostorů."){
            textOut("určit vzájemnou polohu podprostorů.");
            texOut("určit vzájemnou polohu podprostorů.");
            textOut("\r\n\r\nURČENÍ VZÁJEMNÉ POLOHY PODPROSTORŮ\r\n");
            textOut("==================================\r\n");
            texOut("\\section*{URČENÍ VZÁJEMNÉ POLOHY PODPROSTORŮ}\n");
            out = "Podprostory jsou " + first.relativePosition(second,'A','B',true) + ".\n";
        }

        if (task == "Najdi průnik podprostorů."){
            textOut("najít průnik podprostorů.");
            texOut("najít průnik podprostorů.");
            textOut("\r\n\r\nHLEDÁNÍ PRŮNIKU\r\n");
            textOut("===============\r\n");
            texOut("\\section*{HLEDÁNÍ PRŮNIKU}\n");
            out = "Průnik: " + first.intersection(second,'A','B',true) + "\n";
        }

        if (task == "Jsou podprostory rovnoběžné?"){
            textOut("zjistit, jestli jsou oba podprostory rovnoběžné.");
            texOut("zjistit, jestli jsou oba podprostory rovnoběžné.");
            textOut("\r\n\r\nOVĚŘENÍ ROVNOBĚŽNOSTI PODPROSTORŮ\r\n");
            textOut("=================================\r\n");
            texOut("\\section*{Ověření rovnoběžnosti podprostorů}\n");
            out = first.isParallel(second, true) ? "Podprostory jsou rovnoběžné." : "Podprostory nejsou rovnoběžné.";
        }

        if (task == "Protínají se oba podprostory?"){
            textOut("zjistit, jestli se podprostory protínají.");
            texOut("zjistit, jestli se podprostory protínají.");
            textOut("\r\n\r\nOVĚŘENÍ DISJUNKTNOSTI PODPROSTORŮ\r\n");
            textOut("=================================\r\n");
            texOut("\\section*{Ověření disjunktnosti podprostorů}\n");
            out = first.intersects(second,'A','B',true) ? "Podprostory se protínají." : "Podprostory se neprotínají.";
        }

        if (task == "Spočítej vzdálenost podprostorů."){
            textOut("spočítat vzdálenost těchto podprostorů.");
            texOut("spočítat vzdálenost těchto podprostorů.");
            textOut("\r\n\r\nVÝPOČET VZDÁLENOSTI\r\n");
            textOut("===================\r\n");
            texOut("\\section*{Výpočet vzdálenosti}\n");
            out = "Vzdálenost podprostorů je: " + first.distance(second,'A','B',true).toText() + ".";
        }

        if (task == "Spočítej odchylku podprostorů."){
            textOut("spočítat odchylku těchto podprostorů.");
            texOut("spočítat odchylku těchto podprostorů.");
            textOut("\r\n\r\nVÝPOČET ODCHYLKY\r\n");
            textOut("================\r\n");
            texOut("\\section*{Výpočet odchylky}\n");
            out = "Odchylka podprostorů je: " + first.angle(second,'A','B',true).toText() + ".";
        }

        if (task == "Spočítej ortogonální doplněk k podprostoru."){
            if (firstNull == secondNull) {
                textOut("najít ortogonální doplněk k oběma podprostorům.");
                texOut("najít ortogonální doplněk k oběma podprostorům.");
            }
            else {
                textOut("najít ortogonální doplněk k tomuto podprostoru.");
                texOut("najít ortogonální doplněk k tomuto podprostoru.");
            }

            if (!firstNull) {
                textOut("\r\n\r\nORTOGONÁLNÍ DOPLNĚK K A\r\n");
                textOut("=======================\r\n");
                texOut("\n\\section*{Ortogonální doplněk k~$\\mathcal{A}$}");
                out = "Ort. dopl. k A: " + first.orthogonalSupplement('A',true).toTextGeneral() + "\n";
            }
            if (!secondNull) {
                textOut("\r\n\r\nORTOGONÁLNÍ DOPLNĚK K B\r\n");
                textOut("=======================\r\n");
                texOut("\n\\section*{Ortogonální doplněk k~$\\mathcal{B}$}");
                out = "Ort. dopl. k B: " + second.orthogonalSupplement('B',true).toTextGeneral() + "\n";
            }
        }

        outputTextarea.setText(out);
        textOutputButton.setEnabled(true);
        texOutputButton.setEnabled(true);
    }

    private void generate (String task) {
        boolean generateFirst = true, generateSecond = true;
        int dim1 = 0, dim2 = 0, DIM;

        try {
            try {
                dim1 = Integer.parseInt(dim1Textfield.getText());
            }
            catch(NumberFormatException exception) {
                generateFirst = false;
            }
            try {
                dim2 = Integer.parseInt(dim2Textfield.getText());
            }
            catch(NumberFormatException exception) {
                generateSecond = false;
            }
            if (!generateFirst && !generateSecond) throw new NumberFormatException();
            DIM = Integer.parseInt(DIMTextfield.getText());
            if (dim1 < 0 || dim2 < 0 || DIM < 0) throw new NumberFormatException();

            if (dim1 > DIM || dim2 > DIM) throw new IllegalArgumentException("Subspace cannot have dimension larger than its superspace.");
            if ((dim1 == DIM && firstGeneralRadioButton.isSelected()) || (dim2 == DIM) && secondGeneralRadioButton.isSelected())
                throw new IllegalArgumentException("Whole space does not have a general form available.");
        }
        catch(NumberFormatException exception) {
            JOptionPane.showMessageDialog(null, "Do políček označujících dimenze podprostorů a prostorů pro generování je nutné vepsat nezáporná čísla."
                + (!generateFirst || !generateSecond ?
                        "\n(Pozn.: Pokud zadáte pouze jednu z dimenzí podprostorů, bude vygenerován jen tento podprostor.)" : ""),
                "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }
        catch(IllegalArgumentException exception) {
            if (exception.getMessage() == "Subspace cannot have dimension larger than its superspace.")
                JOptionPane.showMessageDialog(null, "Dimenze podprostoru nemůže být větší než dimenze celého prostoru!", "Chyba", JOptionPane.ERROR_MESSAGE);
            if (exception.getMessage() == "Whole space does not have a general form available.")
                JOptionPane.showMessageDialog(null, "Celý prostor nemá obecné vyjádření, takže dimenze podprostoru musí být při volbě obecného vyjádření "
                    + "menší než dimenze celého prostoru!", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (task == "Vyber úlohu:") return;

        boolean firstParam = firstParametricRadioButton.isSelected();
        boolean secondParam = secondParametricRadioButton.isSelected();

        String out1 = new String();
        String out2 = new String();

        Subspace first = new Subspace();
        Subspace second = new Subspace();
        Subspace[] firstAndSecond;

        if (task == "náhodné podprostory") {
            if (generateFirst) {
                if (firstParam) {
                    first = Subspace.generateRandomParametricSubspace(dim1,DIM);
                    first.beautifyParametric();
                    out1 = first.toTextParametric(false);
                }
                else {
                    first = Subspace.generateRandomGeneralSubspace(dim1,DIM);
                    first.beautifyGeneral();
                    out1 = first.toTextGeneralOut(false, 'x');
                }
            }
            if (generateSecond) {            
                if (secondParam) {
                    second = Subspace.generateRandomParametricSubspace(dim2,DIM);
                    second.beautifyParametric();
                    out2 = second.toTextParametric(false);
                }
                else {
                    second = Subspace.generateRandomGeneralSubspace(dim2,DIM);
                    second.beautifyGeneral();
                    out2 = second.toTextGeneralOut(false, 'x');
                }
            }
        }
        else if (!generateFirst || !generateSecond) {
            JOptionPane.showMessageDialog(null, "Pro tento typ úlohy jsou zapotřebí oba podprostory; zadejte dimenze obou podprostorů.",
                "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (task == "podprostory s danou vzdáleností") {
            if (dim1 == DIM || dim2 == DIM) {
                JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
                    "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Fraction distance = new Fraction(0);
            try {
                String dist = distanceTextfield.getText();
                if(dist.contains("/")) {
                    String[] fractionParts = dist.split("/");
                    distance = new Fraction(Integer.parseInt(fractionParts[0]),Integer.parseInt(fractionParts[1]));
                }
                else distance = new Fraction(Integer.parseInt(dist));
                if (distance.compare(0) == -1) throw new NumberFormatException();
            }
            catch(NumberFormatException exception) {
                JOptionPane.showMessageDialog(null, "Do políčka pro požadovanou vzdálenost je nutné vepsat nezáporné číslo.", "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            firstAndSecond = Subspace.generateRandomSubspacesWithGivenDistance(dim1, dim2, DIM, distance);
            first.copy(firstAndSecond[0]);
            second.copy(firstAndSecond[1]);

            if (firstParam) {
                first.checkParametricForm();
                first.beautifyParametric();
                out1 = first.toTextParametric(false);
            }
            else {
                first.beautifyGeneral();
                out1 = first.toTextGeneralOut(false, 'x');
            }

            if (secondParam) {
                second.checkParametricForm();
                second.beautifyParametric();
                out2 = second.toTextParametric(false);
            }
            else {
                second.beautifyGeneral();
                out2 = second.toTextGeneralOut(false, 'x');
            }
        }

        if (task == "podprostory s danou odchylkou") {
            if (dim1 == 0 || dim2 == 0 || dim1 == DIM || dim2 == DIM) {
                JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou ani bod, ani celý prostor.",
                    "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            PiFraction angle = new PiFraction(0);
            if     (angleCombobox.getSelectedItem().toString().contains("30°")) {
                if (DIM < 3) {
                    JOptionPane.showMessageDialog(null, "Nelze vygenerovat přímky v rovině s celočíselnými koeficienty tak, aby svíraly úhel 30° nebo 60°. Vyberte, prosím, "
                        + "jinou odchylku nebo dimenzi.", "Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                angle = new PiFraction(1,6);
            }
            else if(angleCombobox.getSelectedItem().toString().contains("60°")) {
                if (DIM < 3) {
                    JOptionPane.showMessageDialog(null, "Nelze vygenerovat přímky v rovině s celočíselnými koeficienty tak, aby svíraly úhel 30° nebo 60°. Vyberte, prosím, "
                        + "jinou odchylku nebo dimenzi.", "Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                angle = new PiFraction(1,3);
            }
            else if(angleCombobox.getSelectedItem().toString().contains("45°")) angle = new PiFraction(1,4);
            else if(angleCombobox.getSelectedItem().toString().contains("90°")) angle = new PiFraction(1,2);

            firstAndSecond = Subspace.generateRandomSubspacesWithGivenAngle(dim1, dim2, DIM, angle);
            first.copy(firstAndSecond[0]);
            second.copy(firstAndSecond[1]);

            if (firstParam) {
                first.checkParametricForm();
                first.beautifyParametric();
                out1 = first.toTextParametric(false);
            }
            else {
                first.checkGeneralForm();
                first.beautifyGeneral();
                out1 = first.toTextGeneralOut(false, 'x');
            }

            if (secondParam) {
                second.checkParametricForm();
                second.beautifyParametric();
                out2 = second.toTextParametric(false);
            }
            else {
                second.checkGeneralForm();
                second.beautifyGeneral();
                out2 = second.toTextGeneralOut(false, 'x');
            }
        }

        if (task == "podprostory s danou vzájemnou polohou") {
            /*
            if (dim1 == DIM || dim2 == DIM) {
            JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
            "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
            }
             */
            if(relationCombobox.getSelectedItem().toString()=="různoběžné"){
                if (dim1 == DIM || dim2 == DIM) {
                    JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
                        "Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (dim1 == 0 || dim2 == 0) {
                    JOptionPane.showMessageDialog(null, "Různoběžné podprostory nemohou být dimenze 0.",
                        "Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int minDim = (dim1 < dim2 ? dim1 : dim2);
                int intersectionDim;
                if (dimTextfield.getText().isEmpty()) {
                    intersectionDim = Utility.randIntUniform(Math.max(0 , dim1+dim2-DIM),minDim-1);
                }else{
                    String dim = dimTextfield.getText();
                    intersectionDim = Integer.parseInt(dim); 
                }

                if (intersectionDim > minDim-1 || intersectionDim < Math.max(0 , dim1+dim2-DIM)) {
                    JOptionPane.showMessageDialog(null, "Dimenze průniku má nesprávnou hodnotu.","Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                firstAndSecond = Subspace.generateRandomSubspacesWithGivenIntersectionDimension(dim1, dim2, DIM, intersectionDim);
                first.copy(firstAndSecond[0]);
                second.copy(firstAndSecond[1]);

                if (firstParam) {
                    first.checkParametricForm();
                    first.beautifyParametric();
                    out1 = first.toTextParametric(false);
                }
                else {
                    first.checkGeneralForm();
                    first.beautifyGeneral();
                    out1 = first.toTextGeneralOut(false, 'x');
                }

                if (secondParam) {
                    second.checkParametricForm();
                    second.beautifyParametric();
                    out2 = second.toTextParametric(false);
                }
                else {
                    second.checkGeneralForm();
                    second.beautifyGeneral();
                    out2 = second.toTextGeneralOut(false, 'x');
                }
            }

            if(relationCombobox.getSelectedItem().toString()=="mimoběžné"){
                if (dim1 == DIM || dim2 == DIM) {
                    JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
                        "Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (dim1 == 0 || dim2 == 0) {
                    JOptionPane.showMessageDialog(null, "Mimoběžné podprostory nemohou být dimenze 0.",
                        "Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (dim1 > DIM-2 || dim2 > DIM-2) {
                    JOptionPane.showMessageDialog(null, "Podprostory zadaných dimenzí nemohou být mimoběžné (dimenze podprostoru může být maximálně dimenze celého prostoru -2).","Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int minDim = (dim1 < dim2 ? dim1 : dim2);
                int intersectionDim;

                if (dimTextfield.getText().isEmpty()) {
                    intersectionDim = Utility.randIntUniform(Math.max(0, dim1+dim2-DIM+1),minDim-1);
                }else{
                    String dim = dimTextfield.getText();
                    intersectionDim = Integer.parseInt(dim); 
                }

                if (intersectionDim > minDim-1 || intersectionDim < Math.max(0, dim1+dim2-DIM+1)) {
                    JOptionPane.showMessageDialog(null, "Dimenze průniku zaměření má nesprávnou hodnotu.","Chyba", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                firstAndSecond = Subspace.generateRandomSkewSubspaces(dim1, dim2, DIM, intersectionDim);
                first.copy(firstAndSecond[0]);
                second.copy(firstAndSecond[1]);

                if (firstParam) {
                    first.checkParametricForm();
                    first.beautifyParametric();
                    out1 = first.toTextParametric(false);
                }
                else {
                    first.checkGeneralForm();
                    first.beautifyGeneral();
                    out1 = first.toTextGeneralOut(false, 'x');
                }

                if (secondParam) {
                    second.checkParametricForm();
                    second.beautifyParametric();
                    out2 = second.toTextParametric(false);
                }
                else {
                    second.checkGeneralForm();
                    second.beautifyGeneral();
                    out2 = second.toTextGeneralOut(false, 'x');
                }
            }

            if(relationCombobox.getSelectedItem().toString()=="rovnoběžné"){
                if(disjointCombobox.getSelectedItem().toString()=="NE"){
                    int minDim = (dim1 < dim2 ? dim1 : dim2);
                    int intersectionDim = minDim;

                    firstAndSecond = Subspace.generateRandomSubspacesWithGivenIntersectionDimension(dim1, dim2, DIM, intersectionDim);
                }else{
                    if (dim1 == DIM || dim2 == DIM) {
                        JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
                            "Chyba", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    firstAndSecond = Subspace.generateRandomParallelDisjointSubspaces(dim1, dim2, DIM);
                }
                first.copy(firstAndSecond[0]);
                second.copy(firstAndSecond[1]);

                if (firstParam) {
                    first.checkParametricForm();
                    //first.beautifyParametric();
                    out1 = first.toTextParametric(false);
                }
                else {
                    first.checkGeneralForm();
                    first.beautifyGeneral();
                    out1 = first.toTextGeneralOut(false, 'x');
                }

                if (secondParam) {
                    second.checkParametricForm();
                    //second.beautifyParametric();
                    out2 = second.toTextParametric(false);
                }
                else {
                    second.checkGeneralForm();
                    second.beautifyGeneral();
                    out2 = second.toTextGeneralOut(false, 'x');
                }
            }
        }

        if(task=="podprostory s daným průnikem"){
            boolean intNull;
            Subspace intersection;
            InputReader reader = new InputReader();
            try {
                String in = outputTextarea.getText();
                intersection = reader.getSubspace(in);
                intNull = intersection.isNull();
            }
            catch (Exception e) {
                if (e.getMessage() == "The matrix is not solvable!") {
                    JOptionPane.showMessageDialog(null, "Soustava rovnic, kterou je zadán průnik podprostorů, není řešitelná. Nejde tak o podprostor a požadovanou operaci "
                        + "tedy nelze provést. Zadejte prosím existující podprostor.", "Chyba", JOptionPane.ERROR_MESSAGE);
                }else JOptionPane.showMessageDialog(null, "Průnik podprostorů nebyl zadán správně. Opravte, prosím, zadání. Pokud si nejste jistí, jak má zadání vypadat, "
                        + "konzultujte nápovědu k zadávání podprostorů (v horní části okna).", "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (intNull) {
                JOptionPane.showMessageDialog(null, "Je nutné zadat průnik podprostorů.", "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            int minDim = (dim1 < dim2 ? dim1 : dim2);
            int intersectionDim = intersection.getDim();

            if (intersection.getDIM()!=DIM) {
                JOptionPane.showMessageDialog(null, "Zadaný průnik není podprostor "+ DIM +"-rozměrného prostoru.","Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (intersectionDim > minDim || intersectionDim < Math.max(0 , dim1+dim2-DIM)) {
                JOptionPane.showMessageDialog(null, "Dimenze průniku podprostorů má nesprávnou hodnotu.","Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }

            firstAndSecond = Subspace.generateRandomSubspacesWithGivenIntersection(dim1, dim2, DIM, intersection);
            first.copy(firstAndSecond[0]);
            second.copy(firstAndSecond[1]);

            if (firstParam) {
                first.checkParametricForm();
                first.beautifyParametric();
                out1 = first.toTextParametric(false);
            }
            else {
                first.checkGeneralForm();
                first.beautifyGeneral();
                out1 = first.toTextGeneral(false);
            }

            if (secondParam) {
                second.checkParametricForm();
                second.beautifyParametric();
                out2 = second.toTextParametric(false);
            }
            else {
                second.checkGeneralForm();
                second.beautifyGeneral();
                out2 = second.toTextGeneral(false);
            }
        }
        firstInputTextarea.setText(out1);
        secondInputTextarea.setText(out2);
    }

    public void generateMenu(){
        menuBar = new JMenuBar();
        JMenu libraryMenu = new JMenu("Knihovna příkladů");
        JMenu helpMenu = new JMenu("Nápověda");

        JMenuItem help = new JMenuItem("Nápověda k zadávání podprostorů");
        help.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    JOptionPane.showMessageDialog(null,
                        "Podprostor můžete zadat:\nA) OBECNĚ\n=========\n"
                        + "\n1. varianta - rovnicemi (příklad):\n" + "2x_1 - 3/2x_2 + x_4 - 3 = 0\n2/3x_2 - 3x_3 = 0\n"
                        + "(Pozn.: „= 0“ je nepovinné a lze jej vynechat.)\n"
                        + "\n2. varianta - maticí (příklad):\n" + "2, -3/2, 0, 1, -3\n0, 2/3, -3, 0, 0\n"
                        + "\nB) PARAMETRICKY\n===============\n"
                        + "\n1. varianta - rovnicemi (příklad):\n" + "x_1 = t_1 - 1/3t_2\nx_2 = 1\nx_3 = -2 + 3/2t_2\n"
                        + "(Pozn.: „x_1 =“, „x_2 =“ apod. je nepovinné a lze jej vynechat.)\n"
                        + "\n2. varianta - body a vektory (příklad):\n" + "[0, 1, -2]\n(1, 0, 0)\n(-1/3, 0, 3/2)\n"
                        + "(Pozn.: Bodů může být i více, naopak vektor nemusí být zadán žádný.\nVždy ale alespoň jeden bod musí být zadán!)",
                        "Nápověda k zadávání podprostorů", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        );
        JMenuItem library = new JMenuItem("Nahrát příklad z knihovny");
        library.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    if(tryLibrary()){
                        //int i=0;
                        String[] inst = new String[20];
                        try {
                            inst = prepairToRead();
                        }
                        catch (Exception e) {
                            //int i = JOptionPane.showConfirmDialog(null, "Knihovna library.txt nenalezena, chcete ji vytvořit?","Chyba knihovny!",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                            //if(i==0){
                            //    libraryCreate();
                            //}else{
                            //    JOptionPane.showMessageDialog(null, "Knihovna library.txt nenalezena, nebo z ní nejde číst.", "Chyba", JOptionPane.ERROR_MESSAGE);
                            //}
                            JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny, zkontorolujte obsah souboru library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                        }
                        /*
                        String[] libraryList = new String[i];
                        for(int j=0; j<i; j++){
                        libraryList[j] = "Př. " + String.valueOf(j+1); 
                        }
                         */
                        libraryCombobox = new JComboBox<String>(inst);

                        libraryCombobox.setBounds(20,30,260,30);
                        libraryCombobox.setBackground(new Color(214,217,223));
                        libraryCombobox.setForeground(new Color(0,0,0));
                        libraryCombobox.setEnabled(true);
                        libraryCombobox.setFont(new Font("sansserif",0,12));
                        libraryCombobox.setVisible(true);

                        libraryPanel.add(libraryCombobox);

                        libraryPanel.setVisible(true);
                        //libraryCombobox.setVisible(true);
                        libraryButton.setVisible(true);
                        libraryDeleteButton.setVisible(false);
                    }else{
                        int i = JOptionPane.showConfirmDialog(null, "Knihovna library.txt nenalezena, chcete ji vytvořit?","Chyba knihovny!",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                        if(i==0){
                            libraryCreate();
                        }
                    }
                }
            }
        );
        JMenuItem librarySave = new JMenuItem("Uložit příklad do knihovny");
        librarySave.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    if(tryLibrary()){
                        //int i=0;

                        try {
                            librarySave();
                        }
                        catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Chyba při zapisování do library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                        }
                    }else{
                        int i = JOptionPane.showConfirmDialog(null, "Knihovna library.txt nenalezena, chcete ji vytvořit?","Chyba knihovny!",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                        if(i==0){
                            libraryCreate();
                        }
                    }
                }
            }
        );
        JMenuItem libraryDelete = new JMenuItem("Odstranit příklad z knihovny");
        libraryDelete.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    if(tryLibrary()){
                        //int i=0;
                        String[] inst = new String[20];
                        try {
                            inst = prepairToRead();
                        }
                        catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny, zkontorolujte obsah souboru library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                        }
                        /*
                        String[] libraryList = new String[i];
                        for(int j=0; j<i; j++){
                        libraryList[j] = "Př. " + String.valueOf(j+1); 
                        }
                         */
                        libraryCombobox = new JComboBox<String>(inst);

                        libraryCombobox.setBounds(20,30,260,30);
                        libraryCombobox.setBackground(new Color(214,217,223));
                        libraryCombobox.setForeground(new Color(0,0,0));
                        libraryCombobox.setEnabled(true);
                        libraryCombobox.setFont(new Font("sansserif",0,12));
                        libraryCombobox.setVisible(true);

                        libraryPanel.add(libraryCombobox);

                        libraryPanel.setVisible(true);
                        //libraryCombobox.setVisible(true);
                        libraryButton.setVisible(false);
                        libraryDeleteButton.setVisible(true);
                    }else{
                        int i = JOptionPane.showConfirmDialog(null, "Knihovna library.txt nenalezena, chcete ji vytvořit?","Chyba knihovny!",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                        if(i==0){
                            libraryCreate();
                        }
                    }
                }
            }
        );
        JMenuItem libraryCreate = new JMenuItem("Vytvořit knihovnu příkladů");
        libraryCreate.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt) {
                    if(tryLibrary()){
                        int i = JOptionPane.showConfirmDialog(null, "Knihovna library.txt již existuje, vytvořením nové knihovny vymažete veškerý její obsah. Přejete si vytvořit novou knihovnu?","Vymazat knihovnu?",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                        if(i==0){
                            try {
                                libraryCreate();
                            }
                            catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "Chyba při vytváření library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }else{
                        int i = JOptionPane.showConfirmDialog(null, "Přejete si vytvořit knihovnu příkladů library.txt?","Vytvořit knihovnu",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                        if(i==0){
                            try {
                                libraryCreate();
                            }
                            catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "Chyba při vytváření library.txt.", "Chyba", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }
            }
        );

        menuBar.add(helpMenu);
        menuBar.add(libraryMenu);
        helpMenu.add(help);
        libraryMenu.add(libraryCreate);
        libraryMenu.add(library);
        libraryMenu.add(librarySave);
        libraryMenu.add(libraryDelete);
    }

    /*
    private String read() throws IOException  {
    String file = "library.txt";
    String out = new String();

    BufferedReader reader = new BufferedReader(new FileReader (file));
    String line = null;
    StringBuilder stringBuilder = new StringBuilder();
    String ls = System.getProperty("line.separator");

    while( ( line = reader.readLine() ) != null ) {
    stringBuilder.append( line );
    stringBuilder.append( ls );
    }

    out = stringBuilder.toString();

    return out;
    }

    private int prepairToRead() throws IOException {
    String out = new String();
    try {
    out = read();
    }
    catch (Exception e) {
    JOptionPane.showMessageDialog(null, "Knihovna library.txt nenalezena, nebo z ní nejde číst.", "Chyba", JOptionPane.ERROR_MESSAGE);
    }
    String[] splitedTasks = out.split("PŘÍKLAD");
    return splitedTasks.length-2;
    }

    private void libraryWrite() throws IOException  {
    String out = new String();
    try {
    out = read();
    }
    catch (Exception e) {
    JOptionPane.showMessageDialog(null, "Knihovna library.txt nenalezena, nebo z ní nejde číst.", "Chyba", JOptionPane.ERROR_MESSAGE);
    }
    String[] splitedTasks = out.split("PŘÍKLAD");
    int taskNum = Integer.parseInt(libraryCombobox.getSelectedItem().toString().split(" ")[1])+1;

    String task = splitedTasks[taskNum];
    String[] taskParts = task.split("\r\n\r\n");

    if(taskParts.length==3){
    String taskType = taskParts[1].replaceAll("\n","");
    taskType = taskParts[1].replaceAll("\r","");

    solveCombobox.setSelectedIndex(Integer.parseInt(taskType));
    firstInputTextarea.setText(taskParts[2].replaceAll("\r",""));
    secondInputTextarea.setText("");
    }else{
    String taskType = taskParts[1].replaceAll("\n","");
    taskType = taskParts[1].replaceAll("\r","");

    if(taskParts[3].startsWith("\r\n"))taskParts[3]=taskParts[3].replaceFirst("\n","");

    solveCombobox.setSelectedIndex(Integer.parseInt(taskType));
    firstInputTextarea.setText(taskParts[2].replaceAll("\r",""));
    secondInputTextarea.setText(taskParts[3].replaceAll("\r",""));
    }
    }

    private void librarySave() throws IOException {
    String str = ("\r\n\r\nPŘÍKLAD " + String.valueOf(prepairToRead()+1) + "\r\n\r\n" + solveCombobox.getSelectedIndex() + "\r\n\r\n" 
    + firstInputTextarea.getText() + "\r\n\r\n" + secondInputTextarea.getText());
    int i = JOptionPane.showConfirmDialog(null, "Chcete toto zadání uložit?","Uložit zadání",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
    if(i==0){
    String AFileName = "library.txt";
    try {
    OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(AFileName, true), Charset.forName("windows-1250").newEncoder());
    textFile.write(str);
    textFile.close();
    } catch (Exception e){
    System.out.println("I/O error in: " + AFileName + "\n" + e.getMessage());
    }
    }
    //JOptionPane.showMessageDialog(null,"Hotovo!","Uložit zadání",JOptionPane.PLAIN_MESSAGE);
    }

     */

    private boolean tryLibrary(){
        File f = new File("library.txt");
        return (f.exists() && !f.isDirectory()); 
    }

    private String read() throws IOException  {
        String file = "library.txt";
        String out = new String();

        BufferedReader reader = new BufferedReader(new FileReader (file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = "\n";

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }

        out = stringBuilder.toString();

        return out;
    }

    private String read2() throws IOException  {
        String file = "library.txt";
        String out = new String();

        BufferedReader reader = new BufferedReader(new FileReader (file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        while( ( line = reader.readLine() ) != null ) {
            stringBuilder.append( line );
            stringBuilder.append( ls );
        }

        out = stringBuilder.toString();

        return out;
    }

    private String[] prepairToRead() throws IOException {
        String out = new String();
        try {
            out = read();
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Knihovna library.txt nenalezena, nebo z ní nejde číst.", "Chyba", JOptionPane.ERROR_MESSAGE);
        }
        String[] splitedTasks = out.split("PŘÍKLAD");

        String[] instructions = new String[splitedTasks.length-2];
        for(int i=0; i< splitedTasks.length-2; i++){
            instructions[i]=splitedTasks[i+2].split("\n")[0];
        }

        return instructions;
    }

    private void libraryLoad() throws IOException  {
        String out = new String();
        try {
            out = read();
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Knihovna library.txt nenalezena, nebo z ní nejde číst.", "Chyba", JOptionPane.ERROR_MESSAGE);
        }
        String[] splitedTasks = out.split("PŘÍKLAD");
        int taskNum = libraryCombobox.getSelectedIndex()+2;

        String task = splitedTasks[taskNum];
        String[] taskParts = task.split("\n\n");

        if(taskParts.length==3){
            String taskType = taskParts[1].replaceAll("\n","");
            taskType = taskParts[1].replaceAll("\r","");

            solveCombobox.setSelectedIndex(Integer.parseInt(taskType));
            firstInputTextarea.setText(taskParts[2].replaceAll("\r",""));
            secondInputTextarea.setText("");
        }else{
            String taskType = taskParts[1].replaceAll("\n","");
            taskType = taskParts[1].replaceAll("\r","");

            if(taskParts[3].startsWith("\r\n"))taskParts[3]=taskParts[3].replaceFirst("\n","");

            solveCombobox.setSelectedIndex(Integer.parseInt(taskType));
            firstInputTextarea.setText(taskParts[2].replaceAll("\r",""));
            secondInputTextarea.setText(taskParts[3].replaceAll("\r",""));
        }
    }

    private void librarySave() throws IOException {
        String ls = System.getProperty("line.separator");

        String firstOut = new String();
        String[] firstField = firstInputTextarea.getText().split("\n");
        for(int i=0;i<firstField.length-1;i++){
            firstOut=firstOut+firstField[i]+ls;
        }
        firstOut=firstOut+firstField[firstField.length-1];

        String secondOut = new String();
        String[] secondField = secondInputTextarea.getText().split("\n");
        for(int i=0;i<secondField.length-1;i++){
            secondOut=secondOut+secondField[i]+ls;
        }
        secondOut=secondOut+secondField[secondField.length-1];

        String description = JOptionPane.showInputDialog(null, "Zde napište krátký popis ukládaného příkladu:", "Popis příkladu", JOptionPane.INFORMATION_MESSAGE);
        if(description == null){
        }else{
            if(description.isEmpty()) {
                JOptionPane.showMessageDialog(null,"Musíte zadat popis příkladu.","Chyba!",JOptionPane.PLAIN_MESSAGE);
            }else{
                String str = (ls+ls+"PŘÍKLAD " + description + ls+ls + solveCombobox.getSelectedIndex() + ls+ls 
                        + firstOut + ls+ls + secondOut);

                String AFileName = "library.txt";
                try {
                    OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(AFileName, true), Charset.forName("windows-1250").newEncoder());
                    textFile.write(str);
                    textFile.close();
                    JOptionPane.showMessageDialog(null,"Zadání příkladu bylo uloženo do knihovny.","Uložit zadání",JOptionPane.PLAIN_MESSAGE);
                } catch (Exception e){
                    System.out.println("I/O error in: " + AFileName + "\n" + e.getMessage());
                }

            }
        }
    }

    private void libraryDelete() throws IOException  {
        int yesNo = JOptionPane.showConfirmDialog(null, "Opravdu chcete toto zadání odstranit?","Odstranit zadání příkladu",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        if(yesNo==0){
            String out = new String();
            try {
                out = read2();
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Knihovna library.txt nenalezena, nebo z ní nejde číst.", "Chyba", JOptionPane.ERROR_MESSAGE);
            }
            String[] splitedTasks = out.split("PŘÍKLAD");
            int taskNum = libraryCombobox.getSelectedIndex()+2;

            String newLibrary = new String();        

            for(int i=0; i<splitedTasks.length; i++){
                if(i!=taskNum){
                    if(i!=0){
                        newLibrary=newLibrary+"PŘÍKLAD";
                    }
                    newLibrary=newLibrary+splitedTasks[i];
                }
            }

            try {
                clearAFile("library.txt");
                OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream("library.txt", true), Charset.forName("windows-1250").newEncoder());
                textFile.write(newLibrary);
                textFile.close();
                JOptionPane.showMessageDialog(null,"Zadání příkladu bylo odstraněno z knihovny.","Uložit zadání",JOptionPane.PLAIN_MESSAGE);
            } catch (Exception e){
                System.out.println("I/O error in: library.txt\n" + e.getMessage());
            }
        }
    }

    private void libraryCreate()
    {
        String AFileName = "library.txt";
        String str = "Toto je knihovna příkladů pro CVIČEBNICI ANALYTICKÉ GEOMETRIE. Pro vkládání nových úloh dodržujte následující strukturu: \r\n"
            +"- Každý nový záznam musí být uvozen slovem \"PŘÍKLAD\" (poté může následovat popis příkladu, pro lepší orientaci doporučujeme číslovat)\r\n"
            +"- Oddělení jedním prázdným řádkem\r\n"
            +"- číslo popisující zadání úkolu (viz. očíslování úkolů).\r\n"
            +"- Oddělení jedním prázdným řádkem\r\n"
            +"- Rovnice prvního podprostoru (dle syntaxe aplikace)\r\n"
            +"- Oddělení jedním prázdným řádkem\r\n"
            +"- Rovnice druhého podprostoru\r\n"
            +"(v případě úloh 1, 2, 8 je možné zadat pouze jeden podprostor)\r\n"
            +"Očíslování úkolů:\r\n"
            +"1 - Popiš podprostory.\r\n"
            +"2 - Převeď na obecný/parametrický tvar.\r\n"
            +"3 - Protínají se oba podprostory?\r\n"
            +"4 - Najdi průnik podprostorů.\r\n"
            +"5 - Urči vzájemnou polohu podprostorů.\r\n"
            +"6 - Spočítej vzdálenost podprostorů.\r\n"
            +"7 - Spočítej odchylku podprostorů.\r\n"
            +"8 - Spočítej ortogonální doplněk k podprostoru.\r\n"
            +"(tento text nemažte!)";

        try {
            clearAFile("library.txt");
            OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(AFileName, true), Charset.forName("windows-1250").newEncoder());
            textFile.write(str);
            textFile.close();
            JOptionPane.showMessageDialog(null,"Knihovna příkladů byla vytvořena.","Vytváření knihovny",JOptionPane.PLAIN_MESSAGE);
        } catch (Exception e){
            System.out.println("I/O error in: " + AFileName + "\n" + e.getMessage());
        }
    }

    private void makeDir(String dirName) {
        File theDir = new File(dirName);

        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            }
            catch(SecurityException se){
                JOptionPane.showMessageDialog(null, "Nelze vytvářet složky!", "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }        
        }
    }

    private void openText() {
        makeDir("results");
        String fileName = "results/resultText.txt";

        if (Desktop.isDesktopSupported()) {
            try {
                File outputTxt = new File(fileName);
                Desktop.getDesktop().open(outputTxt);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Nelze otevírat textové soubory!", "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
    }

    private void createPDF() {
        finalizeTeXFile();
        try {
            Process pdflatex=Runtime.getRuntime().exec("pdflatex -interaction=batchmode -output-directory=results resultTeX.tex");
            pdflatex.waitFor();
        }
        catch(IOException ex) {
            JOptionPane.showMessageDialog(null, "Nelze vytvářet soubory pdf! Nainstalujte prosím LaTeX.", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }
        catch(InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Proces byl přerušen v průběhu generování pdf.", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

    private void openPDF() {
        makeDir("results");
        String fileName = "results/resultTeX.pdf";

        if (Desktop.isDesktopSupported()) {
            try {
                File outputPDF = new File(fileName);
                Desktop.getDesktop().open(outputPDF);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Nelze otevírat soubory pdf!", "Chyba", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
    }

    private void clearOutputFiles() {
        makeDir("results");
        clearAFile("results/resultText.txt");
        clearAFile("results/resultTeX.tex");
        initializeTeXFile();
    }

    private void clearAFile(String fileName) {
        try {
            PrintWriter delete = new PrintWriter(fileName);
            delete.close();           
        }
        catch(FileNotFoundException ex) {

        }
    }

    private void initializeTeXFile() {
        texOut("\\documentclass[12pt,a4paper]{article}\n\n\\usepackage{cmap}\n\\usepackage[utf8]{inputenc}\n\\usepackage[czech]{babel}\n\\usepackage[T1]{fontenc}\n\n"
            + "\\usepackage{amsmath}\n\\usepackage{amsfonts}\n\\usepackage{amssymb}\n\\usepackage{textcomp}\n\\usepackage{mathtools}\n\n\\usepackage{array}\n\n"
            + "\\newcommand{\\abs}[1]{\\left|#1\\right|}\n\\let\\phi\\varphi\n\n"
            + "\\setlength{\\parindent}{0pt}\n\\setlength{\\parskip}{3mm plus 2mm minus 1mm}\n\n"
            + "\\begin{document}\n");
    }

    private void finalizeTeXFile() {
        texOut("\n\\end{document}");
    }

    private static void textOut(String str)
    {
        Utility.textOut(str);
    }

    private static void texOut(String str)
    {
        Utility.texOut(str);
    }

    public static void main(String[] args){
        System.setProperty("swing.defaultlaf", "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        javax.swing.SwingUtilities.invokeLater(new Runnable()
            {
                public void run() {
                    new GUI();
                }
            }
        );
    }
}