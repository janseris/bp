/**
 * This class introduces points and enables basic operations with them. The coordinates are stored as a field of <code>Fraction</code>s.
 * 
 * @author  Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since   2014-04-01
 */

class Point
{
    /** dimension of the space, ie. number of point's coordinates */
    private int dim;
    /** coordinates of the point */
    private Fraction[] data;
    /** {@link Fraction} 0. */
    private static final Fraction ZERO = new Fraction(0);
    /** {@link Fraction} 1. */
    private static final Fraction ONE = new Fraction(1);
    /** {@link Fraction} -1. */
    private static final Fraction NEG = new Fraction(-1);
    
    /**
     * Constructs a null point of dimension 0.
     */
    Point()
    {
       this.dim = 0;
       this.data = new Fraction[0];
    }

    /**
     * Constructs a point from a field of <code>Fraction</code>s.
     * 
     * @param A field of <code>Fraction</code>s, containing the point's coordinates
     * @param d dimension of the point, ie. number of its coordinates
     */
    Point(Fraction[] A, int d)
    {
       this.dim = d;
       this.data = new Fraction[d];
       for (int i = 0; i<=d-1; i++) {
           if (A[i] == null)
               this.data[i]=ZERO;
           else
               this.data[i]=A[i];
       }
    }
    
    /**
     * Constructs the origin.
     * 
     * @param d dimension of the point, ie. number of its coordinates
     */
    Point(int d)
    {
       this.dim = d;
       this.data = new Fraction[d];
       for (int i = 0; i<=d-1; i++) {
           this.data[i]=ZERO;
       }
    }
    
    /**
     * Constructs a point from a point and a vector.
     * 
     * @param A initial point
     * @param V vector
     * 
     * @throws UnsupportedOperationException Throws an exception if the point and the vector don't have equal dimension.
     */
    Point(Point A, Vector V)
    {
       if (A.getDim() != V.getDim()) throw new UnsupportedOperationException("The point and the vector don't have equal dimension!");
       this.dim = A.dim;
       this.data = new Fraction[this.dim];
       for (int i = 0; i<=this.dim-1; i++) {
           this.data[i] = A.get(i+1).add(V.get(i+1));
       }
    }

    /**
     * Constructs a point from a vector (from the origin).
     * 
     * @param V vector
     */
    Point(Vector V)
    {
       this.dim = V.getDim();
       this.data = new Fraction[this.dim];
       for (int i = 0; i<=this.dim-1; i++) {
           this.data[i] = V.get(i+1);
       }
    }

    /**
     * Gets the dimensions of the point.
     * 
     * @return dimension of the point
     */
    int getDim()
    {
        return this.dim;
    }

    /**
     * The getter of this class - returns the value of a given coordinate.
     * 
     * @param i the index of desired coordinate
     * 
     * @return the <tt>i</tt>th coordinate value (a {@link Fraction})
     * 
     * @throws IndexOutOfBoundsException Throws an exception if the index isn't of &lt;1,<i>dimension of the point</i>&gt;.
     */
    Fraction get(int i)
    {
        if (i>this.dim)  throw new IndexOutOfBoundsException("Point only has "+this.dim+" coefficients!");
        if (i<=0)        throw new IndexOutOfBoundsException("The index ought to be positive!");
        return this.data[i-1];
    }

    /**
     * The setter of this class - sets given coordinate to a given value.
     * 
     * @param F the <tt>i</tt>th coordinate desired value (a {@link Fraction})
     * @param i the index of desired coordinate
     * 
     * @throws IndexOutOfBoundsException Throws an exception if the index isn't of &lt;1,<i>dimension of the point</i>&gt;.
     */
    void set(Fraction F, int i)
    {
        if (i>this.dim)  throw new IndexOutOfBoundsException("Point only has "+this.dim+" coefficients!");
        if (i<=0)        throw new IndexOutOfBoundsException("The index ought to be positive!");
        this.data[i-1] = F;
    }
    
    /**
     * Copies another point onto this point. This is the correct way of assigning points, not by <code>=</code>.
     * 
     * @param I the image point
     */
    void copy(Point A)
    {
        if (A != null) {
            this.dim = A.dim;
            this.data = new Fraction[A.dim];
            for (int i = 1; i<=A.dim; i++) {
                this.set(A.get(i),i);
            }
        }
    }
    
    /**
     * Checks if two points are equal.
     * 
     * @param C comparand point
     * 
     * @return true if this point is equal to the comparand, false otherwise
     * 
     * @throws UnsupportedOperationException Throws an exception if the point don't have equal dimension.
     */
    boolean isEqual(Point C)
    {
        if (this.dim != C.dim) throw new UnsupportedOperationException("The points don't have equal dimension!");
        
        for (int i = 1; i<=this.dim; i++) {
            if (!(this.get(i).isEqual(C.get(i)))) return false;
        }
        
        return true;
    }
    
    /**
     * Converts the point to a 1-row matrix.
     * 
     * @returns matrix of point's coordinates (in a row)
     */
    Matrix toMatrixRow()
    {
        return new Matrix(this.data,1,this.dim);
    }
    
    /**
     * Converts the point to a 1-column matrix.
     * 
     * @returns matrix of point's coordinates (in a column)
     */
    Matrix toMatrixColumn()
    {
        return new Matrix(this.data,this.dim,1);
    }  
    
    /**
     * Generates a random point.
     * 
     * @param DIM dimension of the point (ie. a number of variables)
     */
    static Point generateRandomPoint(int DIM)
    {
        Fraction[] coeffs = new Fraction[DIM+1];
        
        for (int i=0; i<=DIM; i++)
            coeffs[i] = new Fraction(Utility.randInt(-20,20),1);
        
        return new Point(coeffs,DIM);
    }
    
    /**
     * Prints this point formatted as text.
     * 
     * @return plain text description of this point; it will begin with <code>[</code> and end with <code>]</code>
     */
    String toText()
    {
        String S = "";
        
        S += "[";
        for (int i = 1; i<=this.dim; i++) {
            if (i>1) S += ", ";
            S += this.get(i).toText();
        }
        S += "]";
        
        return S;
    }

    /**
     * Prints this point formatted as TeX code.
     * 
     * @return TeX code for this point
     */
    String toTeX()
    {
        String S = "";
        
        S += "\\left[";
        for (int i = 1; i<=this.dim; i++) {
            if (i>1) S += ",";
            S += this.get(i).toTeX();
        }
        S += "\\right]";
        
        return S;
    }
}
