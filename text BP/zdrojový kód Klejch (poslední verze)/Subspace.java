/**
 * This is the computational core of the package. It introduces subspaces and enables basic operations with them, using all the other classes for that.
 * 
 * @author  Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since   2014-04-01
 */

class Subspace
{
    /** dimension of the subspace (ie. this subspace's superspace) */
    private int dim;
    /** dimension of the whole space (ie. this subspace's superspace) */
    private int DIM;
    /** augmented matrix of general form equations */
    private Matrix augMatrix;
    /** reference point of parametric form */
    private Point refPoint;
    /** matrix of vectors (the vectors consitute the rows) of parametric form */
    private Matrix vecMatrix;
    /** flag marking if general form is available */
    private boolean generalFormAvailable;
    /** flag marking if parametric form is available */
    private boolean parametricFormAvailable; 
    /** flag marking if the original user's input was in general form (true), or parametric form (false) */
    private boolean inputInGeneralForm;
    /** {@link Fraction} 0 */
    private static final Fraction ZERO = new Fraction(0);
    /** {@link Fraction} 1 */
    private static final Fraction ONE = new Fraction(1);
    /** {@link Fraction} -1 */
    private static final Fraction NEG = new Fraction(-1);
    /** {@link SquareRootFraction} 0 */
    private static final SquareRootFraction SRF_ZERO = new SquareRootFraction(0);

    /**
     * Constructs a null subspace.
     */
    Subspace()
    {
        DIM = -1;
        dim = -1;
        generalFormAvailable = false;
        parametricFormAvailable = false;
        inputInGeneralForm = false;
    }

    /**
     * Constructs a subspace given in general form.
     * 
     * @param A augmented matrix of equation system generating this subspace
     * 
     * @throws UnsupportedOperationException Throws an exception if the equation system is not solvable, and thus cannot generate a subspace.
     */
    Subspace(Matrix A)
    {
        augMatrix = A;
        DIM = A.getC()-1;   // DIM = number of variables
        dim = DIM-A.rank(false); // dim = DIM - number of independent equations
        generalFormAvailable = true;
        parametricFormAvailable = false;
        inputInGeneralForm = true;
    }

    /**
     * Constructs a subspace given in parametric form (ie. from at least one point and potentially some vectors).
     * 
     * @param A augmented matrix of equation system generating this subspace
     * 
     * @throws IndexOutOfBoundsException Throws an exception if no reference point was given.
     */
    Subspace(Point[] points, Vector[] vectors)
    {
        if (points.length == 0) throw new IndexOutOfBoundsException("At least one point is necessary to construct the subspace!");

        refPoint = points[0];
        Vector[] tempVectors = new Vector[vectors.length + points.length - 1];
        for (int i = 0; i < vectors.length; i++) {
            tempVectors[i] = vectors[i];
        }
        if (points.length > 1) {
            for (int i = vectors.length, j = 1; i < tempVectors.length; i++, j++) {
                tempVectors[i] = new Vector(refPoint, points[j]);
            }
        }

        if (tempVectors.length == 0) {
            vecMatrix = new Matrix();
        }
        else {
            Fraction[] fracArray = new Fraction[tempVectors.length*tempVectors[0].getDim()];
            for (int i = 0; i < fracArray.length; i++) {
                int j = i / tempVectors[0].getDim();
                int k = i % tempVectors[0].getDim();
                fracArray[i] = tempVectors[j].get(k+1); 
            }
            vecMatrix = new Matrix(fracArray, tempVectors.length, tempVectors[0].getDim());
        }
        DIM = refPoint.getDim(); // DIM = number of variables
        dim = vecMatrix.rank(false);  // dim = number of independent direction vectors
        generalFormAvailable = false;
        parametricFormAvailable = true;
        inputInGeneralForm = false;
    }

    /**
     * Returns the dimension of the subspace.
     * 
     * @return dimension of the subspace
     */
    int getDim()
    {
        return getDim(false, 'U');
    }

    /**
     * Returns the dimension of the subspace. This method could also output the whole process of computing the dimension into text and tex files.
     * 
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return dimension of the subspace
     */
    int getDim(boolean verbose)
    {
        return getDim(verbose, 'U');
    }

    /**
     * Returns the dimension of the subspace. This method could also output the whole process of computing the dimension into text and tex files.
     * 
     * @param label label of the subspace
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return dimension of the subspace
     */
    int getDim(boolean verbose, char label)
    {
        int rank, getDIM;

        if (this.inputInGeneralForm) {
            if (verbose) {
                textOut("Podprostor je zadán obecně. V takovém případě je jeho dimenze určena:\r\n\t" + "dim(" + label + ") = n - k,\r\n"
                    + "kde n je dimenze celého prostoru (tj. počet proměnných) a k je hodnost matice soustavy (tj. počet jejích lineárně nezávislých řádků).\r\n\r\n");
                texOut("Podprostor je zadán obecně. V~takovém případě je jeho dimenze určena:\\[\n" + "\\dim(\\mathcal{" + label + "}) = n - k,\\]\n"
                    + "kde $n$ je dimenze celého prostoru (tj.~počet proměnných) a $k$ je hodnost matice soustavy (tj.~počet jejích lineárně nezávislých řádků).\n\n");
                rank = this.getAugMatrix().rank();
                getDIM = this.getDIM();
                String zeroRows = new String();
                switch (rank) {
                    case 1:  zeroRows = " nenulový řádek"; break;
                    case 2:
                    case 3:
                    case 4:  zeroRows = " nenulové řádky"; break;
                    default: zeroRows = " nenulových řádků"; break;
                }
                String variables = new String();
                switch (getDIM) {
                    case 1:  variables = " proměnnou"; break;
                    case 2:
                    case 3:
                    case 4:  variables = " proměnné"; break;
                    default: variables = " proměnných"; break;
                }
                textOut(" Protože má nyní " + String.valueOf(rank) + zeroRows + ", je její hodnost právě " + String.valueOf(rank)
                    + ". A jelikož máme " + String.valueOf(getDIM) + variables + ", je dimenze podprostoru\r\n\t"  + "dim(" + label + ") = " + String.valueOf(getDIM)
                    + " - " + String.valueOf(rank) + " = " + String.valueOf(getDIM-rank) + ".\r\n\r\n");
                texOut(" Protože má nyní " + String.valueOf(rank) + zeroRows + ", je její hodnost právě " + String.valueOf(rank)
                    + ". A~jelikož máme " + String.valueOf(getDIM) + variables + ", je dimenze podprostoru\\[\n"  + "\\dim(\\mathcal{" + label + "}) = "
                    + String.valueOf(getDIM) + " - " + String.valueOf(rank) + " = " + String.valueOf(getDIM-rank) + ".\\]\n\n");
                this.augMatrix.copy(this.augMatrix.getEchelonForm(false));
            }
            else {
                rank = this.getAugMatrix().rank(false);
                getDIM = this.getDIM();
            }
            return getDIM-rank;
        }   
        else if (this.parametricFormAvailable) {
            if (verbose) {
                textOut("Podprostor je zadán parametricky. V takovém případě je jeho dimenze rovna počtu lineárně nezávislých směrových vektorů, tzn. hodnosti matice zaměření"
                    + " (tj. počtu jejích lineárně nezávislých řádků).\r\n\r\n");
                texOut("Podprostor je zadán parametricky. V~takovém případě je jeho dimenze rovna počtu lineárně nezávislých směrových vektorů, tzn.~hodnosti matice zaměření"
                    + " (tj.~počtu jejích lineárně nezávislých řádků).\r\n\r\n");
                rank = this.getVecMatrix().rank();
                String zeroRows = new String();
                switch (rank) {
                    case 1:  zeroRows = " nenulový řádek"; break;
                    case 2:
                    case 3:
                    case 4:  zeroRows = " nenulové řádky"; break;
                    default: zeroRows = " nenulových řádků"; break;
                }
                textOut(" Protože má nyní " + String.valueOf(rank) + zeroRows + ", je její hodnost, a tedy i dimenze podprostoru právě " + String.valueOf(rank)
                    + ".\r\n\r\n");
                texOut(" Protože má nyní " + String.valueOf(rank) + zeroRows + ", je její hodnost, a tedy i dimenze podprostoru právě " + String.valueOf(rank)
                    + ".\n\n");
                this.vecMatrix.copy(this.vecMatrix.getEchelonForm(false));
            }
            else {
                rank = this.getVecMatrix().rank(false);
            }
            return rank;
        }
        return -1;
    }

    /**
     * Returns the dimension of the whole space (number of variables).
     * 
     * @return dimension of the whole space
     */
    int getDIM()
    {
        return DIM;
    }

    /**
     * Returns the augmented matrix of equation system generating this subspace.
     * 
     * @return augmented matrix
     */
    Matrix getAugMatrix()
    {
        return augMatrix;
    }

    /**
     * Returns the matrix of direction vectors (in rows).
     * 
     * @return matrix of direction vectors
     */
    Matrix getVecMatrix()
    {
        return vecMatrix;
    }

    /**
     * Returns the matrix of direction vectors (in rows).
     * 
     * @return matrix of direction vectors
     */
    Point getRefPoint()
    {
        return refPoint;
    }

    Point getRandomPoint()
    {
        this.checkParametricForm();
        Point randPoint = new Point();
        randPoint.copy(refPoint);
        for (int i=0; i < vecMatrix.getR(); i++){
            randPoint = new Point(randPoint, vecMatrix.rowsToVectors()[i].multiply(new Fraction(Utility.randIntUniform(-2,2))));
        }
        return randPoint;
    }

    /**
     * Confirms if the parametric form is available.
     * 
     * @return true if parametric form is available, false otherwise
     */
    boolean isParametricFormAvailable()
    {
        return parametricFormAvailable;
    }

    /**
     * Confirms if the general form is available.
     * 
     * @return true if general form is available, false otherwise
     */
    boolean isGeneralFormAvailable()
    {
        return generalFormAvailable;
    }

    /**
     * Makes clear if the subspace was originally input in general or in parametric form
     * 
     * @return true if subspace was originally input in general form, false if the original form was parametric
     */
    boolean getOriginalForm()
    {
        return inputInGeneralForm;
    }

    /**
     * Copies the subspace.
     * 
     * @param S subspace to be copied
     */
    void copy(Subspace S)
    {
        this.augMatrix = new Matrix();
        if (S.augMatrix != null) this.augMatrix.copy(S.augMatrix);
        this.refPoint = new Point();
        if (S.refPoint != null) this.refPoint.copy(S.refPoint);
        this.vecMatrix = new Matrix();
        if (S.vecMatrix != null) this.vecMatrix.copy(S.vecMatrix);
        this.DIM = S.DIM;
        this.dim = S.dim;
        this.generalFormAvailable = S.generalFormAvailable;
        this.parametricFormAvailable = S.parametricFormAvailable;
        this.inputInGeneralForm = S.inputInGeneralForm;
    }

    /**
     * Checks if general form is already available; if it's not, it gets it. This method doesn't “talk” (creates no text nor TeX output).
     */
    void checkGeneralForm() {
        checkGeneralForm('U', false);
    }

    /**
     * Checks if general form is already available; if it's not, it gets it.
     * 
     * @label label of the subspace
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     */
    void checkGeneralForm(char label, boolean verbose) {
        if (!(this.generalFormAvailable)) this.getGeneralForm(label, verbose);
    }

    /**
     * Converts parametric form to general form.
     * 
     * @label label of the subspace
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     */
    private void getGeneralForm(char label, boolean verbose) {
        boolean originallyVerbose = verbose;
        if (this.isPoint() && verbose) {
            textOut("Jedná se, jak vidíme, o bod. Obecné vyjádření bodu je přitom stejné jako parametrické (to totiž žádný parametr vlastně neobsahuje). "
                + "Zbývá jedině převést proměnné a konstanty na jednu stranu");
            texOut("Jedná se, jak vidíme, o bod. Obecné vyjádření bodu je přitom stejné jako parametrické (to totiž žádný parametr vlastně neobsahuje). "
                + "Zbývá jedině převést proměnné a konstanty na jednu stranu");
            verbose = false;
        }
        if (verbose) {
            textOut("Na obecný tvar převedeme podprostor pomocí metody vyloučení parametrů. Vytvoříme nejprve matici ve tvaru\r\n\t(V|-E|B),\r\n"
                + "kde V je matice, jejíž sloupce tvoří směrové vektory, E je jednotková matice a sloupec B tvoří souřadnice libovolného bodu podprostoru. "
                + "U této metody ovšem potřebujeme vědět, jaká je dimenze podprostoru, tj. kolik máme k dispozici lineárně nezávislých směrových vektorů.\r\n\r\n" 
                + "zjištění dimenze podprostoru\r\n" + "----------------------------\r\n");
            texOut("Na obecný tvar převedeme podprostor pomocí metody vyloučení parametrů. Vytvoříme nejprve matici ve tvaru\n"
                + "\\[\\begin{array}{c|c|c}(V & -E & B),\\end{array}\\]\n"
                + "kde $V$ je matice, jejíž sloupce tvoří směrové vektory, $E$ je jednotková matice a sloupec $B$ tvoří souřadnice libovolného bodu podprostoru. "
                + "U~této metody ovšem potřebujeme vědět, jaká je dimenze podprostoru, tj.~kolik máme k~dispozici lineárně nezávislých směrových vektorů.\n\n"
                + "\\subsection*{zjištění dimenze podprostoru}\n");
        }
        int dim = this.vecMatrix.rank(verbose);
        if (dim == this.getDIM()) {
            this.generalFormAvailable = false;
            if (verbose) {
                textOut("\r\n\r\nVidíme, že dimenze podprostoru je " + dim
                    + ", a je tedy rovna dimenzi celého prostoru. Jedná se tedy o celý prostor - ten ovšem nemá obecné vyjádření.");
                texOut("\n\nVidíme, že dimenze podprostoru je " + dim
                    + ", a je tedy rovna dimenzi celého prostoru. Jedná se tedy o celý prostor -- ten ovšem nemá obecné vyjádření.");
            }
            return;
        }
        Matrix M = new Matrix();
        M.copy(this.vecMatrix.getEchelonForm(false).transpose());
        Matrix E = new Matrix(this.getDIM());
        for (int i=1; i<=E.getR(); i++) E.multiplyRow(i, NEG);
        M.mergeFromRight(E);
        Matrix B = new Matrix();
        B = refPoint.toMatrixColumn();
        M.mergeFromRight(B);
        if (verbose) {
            textOut("\r\n\r\nvyloučení parametrů\r\n--------------------\r\nMatice P = (V|-E|B) pak je:\r\n" + M.toText() + "\r\n");
            texOut("\n\n\\subsection*{vyloučení parametrů}\nMatice $P=\\begin{array}{c|c|c}(V&-E&B)\\end{array}$ pak je:\n\\[\n" + M.toTeX() + "\\]\n");
        }
        // now we have (V|-E|B) matrix, where V are vectors in columns, -E is negative unity matrix, and B is the reference point in column
        M.copy(M.getEchelonForm('P',dim,true));
        // now we eliminate parameters and crop the resulting matrix
        this.augMatrix = new Matrix();
        this.augMatrix.copy(M.submatrix(dim+1,M.getR(),dim+1,M.getC()));
        if (verbose) {
            String howMany = new String();
            switch (dim) {
                case 1:  howMany = "první sloupec a řádek";
                break;
                case 2:
                case 3:
                case 4:  howMany = "první " + dim + " sloupce a řádky";
                break;
                default: howMany = "prvních " + dim + " sloupců a řádků";
                break;
            }
            textOut(" Z této matice nyní vybereme submatici tak, že „odřízneme“ " + howMany + ". (Počet sloupců a řádků k „odříznutí“ je přitom dán dimenzí - v našem případě "
                + dim + ".) Tato submatice tedy je:\r\n" + this.augMatrix.toText() + "Tato submatice je přitom přímo maticí soustavy, která generuje podprostor");
            texOut(" Z~této matice nyní vybereme submatici tak, že \\uv{odřízneme} " + howMany + ". (Počet sloupců a řádků k~\\uv{odříznutí} je přitom dán dimenzí~"
                + "-- v~našem případě " + dim + ".) Tato submatice tedy je:\n\\[" + this.augMatrix.toTeX() + "\n\\]\n"
                + "Tato submatice je přitom přímo maticí soustavy, která generuje podprostor");
        }
        if (originallyVerbose) {
            textOut("; podprostor má tedy obecné vyjádření:\r\n" + this.toTextGeneral());
            texOut("; podprostor má tedy obecné vyjádření:\n\\[\n" + this.toTeXGeneral()+ "\n\\]");
        }
        this.generalFormAvailable = true;
    }

    /**
     * Checks if parametric form is already available; if it's not, it gets it. This method doesn't “talk” (creates no text nor TeX output).
     */
    void checkParametricForm() {
        checkParametricForm('U', false);
    }

    /**
     * Checks if parametric form is already available; if it's not, it gets it.
     * 
     * @label label of the subspace
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     */
    void checkParametricForm(char label, boolean verbose) {
        if (!(this.parametricFormAvailable)) this.getParametricForm(label, verbose);
        this.parametricFormAvailable = true;
    }

    /**
     * Converts general form to parametric form.
     * 
     * @label label of the subspace
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     */
    private void getParametricForm(char label, boolean verbose) {
        Matrix M = new Matrix();

        if (verbose) {
            textOut("Převod z obecného na parametrický tvar se provede jednoduše vyřešením soustavy rovnic, kterou je podprostor zadán. " 
                + "Nejprve matici soustavy převedeme na schodovitý tvar.\r\n\r\n" + "převod na schodovitý tvar\r\n-------------------------\r\n");
            texOut("Převod z obecného na parametrický tvar se provede jednoduše vyřešením soustavy rovnic, kterou je podprostor zadán. "
                + "Nejprve matici soustavy převedeme na schodovitý tvar.\n\\subsection*{převod na schodovitý tvar}\n");
        }

        M.copy(this.augMatrix.solve(label, verbose));
        this.refPoint = new Point(M.getR());
        this.vecMatrix = new Matrix();
        Matrix V = new Matrix();
        if (this.isPoint()) {
            this.vecMatrix = new Matrix(1,M.getR());
            for (int i=1; i<=this.vecMatrix.getC(); i++) {
                this.refPoint.set(M.get(i,M.getC()),i);
                this.vecMatrix.set(ZERO,1,i);
            }
        }
        else {
            V.copy(M.submatrix(1,M.getR(),this.augMatrix.rank(false)+1,M.getC()-1));
            this.vecMatrix.copy(V.transpose());
            for (int i=1; i<=this.vecMatrix.getC(); i++) {
                this.refPoint.set(M.get(i,M.getC()),i);
            }
        }

        if (verbose) {
            textOut("Jedná se přitom současně o parametrické vyjádření podprostoru " + label + ".");
            texOut("Jedná se přitom současně o parametrické vyjádření podprostoru $\\mathcal{" + label + "}$.");
        }
    }

    /**
     * Multiplies individual vectors by such numbers that their coefficients will be smallest possible integers.
     */
    void beautifyParametric() {
        vecMatrix.deleteZeroRows();
        if (vecMatrix == null) return;

        this.checkParametricForm();

        long commonDenominator, commonNumerator;        
        for (int i=1; i <= vecMatrix.getR(); i++) {
            commonDenominator = 1;
            commonNumerator = 0;

            for (int j=1; j <= vecMatrix.getC(); j++) {
                commonDenominator = Utility.lcm(commonDenominator, vecMatrix.get(i,j).d);
                commonNumerator = Utility.gcd(commonNumerator, vecMatrix.get(i,j).n);
            }

            vecMatrix.multiplyRow(i, new Fraction(commonDenominator, commonNumerator));
        }
    }

    /**
     * Multiplies individual equations by such numbers that their coefficients will be smallest possible integers.
     */
    void beautifyGeneral() {
        augMatrix.deleteZeroRows();
        this.checkGeneralForm();

        long commonDenominator, commonNumerator;        
        for (int i=1; i <= augMatrix.getR(); i++) {
            commonDenominator = 1;
            commonNumerator = 0;

            for (int j=1; j <= augMatrix.getC(); j++) {
                commonDenominator = Utility.lcm(commonDenominator, augMatrix.get(i,j).d);
                commonNumerator = Utility.gcd(commonNumerator, augMatrix.get(i,j).n);
            }

            augMatrix.multiplyRow(i, new Fraction(commonDenominator, commonNumerator));
        }
    }

    /**
     * Checks, if this subspace is a null space.
     * 
     * @return true, if dim < 0; false otherwise
     */
    boolean isNull()
    {
        return (this.getDim(false)<0);
    }

    /**
     * Checks, if this subspace is a point.
     * 
     * @return true, if dim = 0; false otherwise
     */
    private boolean isPoint()
    {
        return (this.getDim(false)==0);
    }

    /**
     * Checks, if this subspace is a line.
     * 
     * @return true, if dim = 1; false otherwise
     */
    private boolean isLine()
    {
        return (this.getDim(false)==1);
    }

    /**
     * Checks, if this subspace is a plane.
     * 
     * @return true, if dim = 2; false otherwise
     */
    private boolean isPlane()
    {
        return (this.getDim(false)==2);
    }

    /**
     * Checks, if this subspace is a hyperplane.
     * 
     * @return true, if dim = <i>n</i> - 1, where <i>n</i> is the dimension of the whole space; false otherwise
     */
    private boolean isHyperplane()
    {
        return (this.getDim(false)==this.DIM-1);
    }

    /**
     * Checks, if this subspace is the whole space.
     * 
     * @return true, if dim = <i>n</i>, where <i>n</i> is the dimension of the whole space; false otherwise
     */
    private boolean isWholeSpace()
    {
        return (this.getDim(false)==this.DIM);
    }

    /**
     * Properly describes the subspace.
     * 
     * @label label of the subspace
     * 
     * @return type of the subspace (null space, point, line, plane, <i>n</i>-dimensional space, hyperplane, whole space)
     */
    String describe(char label)
    {
        boolean verbose = true;

        if (!this.getOriginalForm() && this.getDim(false) == 0) {
            textOut("Protože parametrické zadání neobsahuje parametr, je okamžitě vidět, že se jedná o bod.");
            texOut("Protože parametrické zadání neobsahuje parametr, je okamžitě vidět, že se jedná o bod.");
            verbose = false; 
        }

        int dim = this.getDim(verbose, label);

        if (verbose) {
            textOut("Jelikož dimenze je " + String.valueOf(dim) + " tak ");
            texOut("Jelikož dimenze je " + String.valueOf(dim) + " tak ");
        }

        if (this.isNull()) {
            textOut("se nejedná o podprostor.");
            texOut("se nejedná o podprostor.");
            return "Nejedná se o podprostor.";
        }

        if (this.isWholeSpace()) {
            switch (this.dim) {
                case 0:  textOut("podprostor je bod (celý prostor je ovšem také jen bod).");
                texOut("podprostor je bod (celý prostor je ovšem také jen bod).");
                return "Bod (celý prostor je ovšem také jen bod).";
                case 1:  textOut("podprostor je přímka (tj. celý prostor).");
                texOut("podprostor je přímka (tj. celý prostor).");
                return "Celá přímka.";
                case 2:  textOut("podprostor je rovina (tj. celý prostor).");
                texOut("podprostor je rovina (tj. celý prostor).");
                return "Celá rovina.";
                case 3:  textOut("podprostor je celý trojrozměrný prostor.");
                texOut("podprostor je celý trojrozměrný prostor.");
                return "Celý trojrozměrný prostor.";
                default: textOut("podprostor je celý "+this.DIM+"-rozměrný prostor.");
                texOut("podprostor je celý "+this.DIM+"-rozměrný prostor.");
                return "Celý "+this.DIM+"-rozměrný prostor.";
            }
        }

        // Dále už tedy musí být dim < DIM => DIM >= 1.

        String superspaceDescription;
        switch (this.DIM) {
            case 1:  superspaceDescription = " na přímce.";
            break;
            case 2:  superspaceDescription = " v rovině.";
            break;
            case 3:  superspaceDescription = " v trojrozměrném prostoru.";
            break;
            default: superspaceDescription = " v "+this.DIM+"-rozměrném prostoru.";
            break;
        }

        if (this.isPoint()) {
            if (verbose) {
                textOut("se jedná o bod" + superspaceDescription);
                texOut("se jedná o bod" + superspaceDescription);       
            }
            return "Bod" + superspaceDescription;
        }
        if (this.isLine()) {
            textOut("se jedná o přímku" + superspaceDescription);
            texOut("se jedná o přímku" + superspaceDescription);             
            return "Přímka" + superspaceDescription;
        }
        if (this.isPlane()) {
            textOut("se jedná o rovinu" + superspaceDescription);
            texOut("se jedná o rovinu" + superspaceDescription);             
            return "Rovina" + superspaceDescription;
        }
        if (this.isHyperplane()) {
            textOut("se jedná o nadrovinu" + superspaceDescription);
            texOut("se jedná o nadrovinu" + superspaceDescription);             
            return "Nadrovina" + superspaceDescription;
        }

        textOut("se jedná o " + this.dim + "-rozměrný podprostor" + superspaceDescription);
        texOut("se jedná o " + this.dim + "-rozměrný podprostor" + superspaceDescription); 
        return this.dim + "-rozměrný podprostor" + superspaceDescription;
    }

    /**
     * Checks, if this subspace is parallel to the other. This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param W the other subspace
     * 
     * @throw UnsupportedOperationException Throws an exception if the subspaces have a different number of variables.
     * 
     * @return true, if the subspaces are parallel, false otherwise
     */

    boolean isParallel(Subspace W)
    {
        return isParallel(W, false);
    }

    /**
     * Checks, if this subspace is parallel to the other.
     * 
     * @param W the other subspace
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throw UnsupportedOperationException Throws an exception if the subspaces have a different number of variables.
     * 
     * @return true, if the subspaces are parallel, false otherwise
     */
    boolean isParallel(Subspace W, boolean verbose)
    {
        if (this.DIM != W.DIM) throw new UnsupportedOperationException("The subspaces are not subspaces of the same superspace - they have a different number of variables.");

        boolean generalOrParametric = false;
        if (verbose) {
            textOut("Při určování rovnoběžnosti potřebujeme, aby oba podprostory byly zadány stejně. V našem případě ");
            texOut("Při určování rovnoběžnosti potřebujeme, aby oba podprostory byly zadány stejně. V našem případě ");
            if (this.getOriginalForm() == W.getOriginalForm()) {
                textOut("jsou oba podprostory zadány " + (this.getOriginalForm() ? "obecně" : "parametricky") + ", a tak nemusíme žádný převádět na jiný typ vyjádření.");
                texOut("jsou oba podprostory zadány " + (this.getOriginalForm() ? "obecně" : "parametricky") + ", a tak nemusíme žádný převádět na jiný typ vyjádření.");
                generalOrParametric = this.getOriginalForm();
            }
            else {
                if (this.getOriginalForm()) {
                    textOut("tomu tak ovšem není; zvolíme tedy například parametrické vyjádření. Podprostor B již parametricky vyjádřen je; podprostor A na parametrické "
                        + "vyjádření převedeme.\r\n\r\npřevod A na parametrický tvar\r\n-----------------------------\r\n");
                    texOut("tomu tak ovšem není; zvolíme tedy například parametrické vyjádření. Podprostor $\\mathcal{B}$ již parametricky vyjádřen je; podprostor "
                        + "$\\mathcal{A}$ na parametrické vyjádření převedeme.\n\n\\subsection*{Převod $\\mathcal{A}$ na parametrický tvar}\n");
                    this.checkParametricForm('A', true);
                }
                else {
                    textOut("tomu tak ovšem není; zvolíme tedy například parametrické vyjádření. Podprostor A již parametricky vyjádřen je; podprostor B na parametrické "
                        + "vyjádření převedeme.\r\n\r\npřevod B na parametrický tvar\r\n-----------------------------\r\n");
                    texOut("tomu tak ovšem není; zvolíme tedy například parametrické vyjádření. Podprostor $\\mathcal{A}$ již parametricky vyjádřen je; podprostor "
                        + "$\\mathcal{B}$ na parametrické vyjádření převedeme.\n\n\\subsection*{Převod $\\mathcal{B}$ na parametrický tvar}\n");
                    W.checkParametricForm('B', true);
                }
            }
        }
        else {
            this.checkParametricForm();
            W.checkParametricForm();
        }

        int firstRank, secondRank, unionRank;
        if (generalOrParametric) {
            if (verbose) {
                textOut("\r\n\r\nPodprostory jsou rovnoběžné, jestliže platí\r\n\tdim (U+V) = max(dim(U), dim(V)),\r\nkde U je zaměření podprostoru A a V zaměření podprostoru B. "
                    + "K tomu stačí, aby\r\n\th(S) = max(h(M), h(N)),\r\nkde M je matice soustavy (bez pravých stran!), která zadává podprostor A, N je matice soustavy, která "
                    + "zadává podprostor B, a S je matice soustavy, která zadává podprostor A∩B (ta vznikne „slepením“ obou matic). Umístění podprostoru je ovšem v případě "
                    + "určování rovnoběžnosti irelevantní, a tak nás budou zajímat pouze matice soustav bez absolutních členů. Zjistíme nejprve, jaké hodnosti mají matice "
                    + "soustav rovnic, jimiž jsou podprostory zadány\r\n\r\nzjištění hodnosti matice M\r\n--------------------------\r\n");
                texOut("\n\nPodprostory jsou rovnoběžné, jestliže platí\n\\[\\dim(U+V) = \\max(\\dim(U), \\dim(V)),\\]\nkde $U$ je zaměření podprostoru $\\mathcal{A}$ a "
                    + "$V$ zaměření podprostoru $\\mathcal{B}$. Jinými slovy, musí být\n\\[h(S) = \\max(h(M), h(N)),\n\\]\nkde $M$ je matice soustavy (bez pravých stran!), "
                    + "která zadává podprostor $\\mathcal{A}$, $N$ je matice soustavy, která zadává podprostor $\\mathcal{B}$, a $S$ je matice soustavy, která zadává "
                    + "podprostor $\\mathcal{A}\\cap\\mathcal{B}$ (ta vznikne \\uv{slepením} obou matic). Umístění podprostoru je ovšem v případě určování rovnoběžnosti "
                    + "irelevantní, a tak nás budou zajímat pouze matice soustav bez absolutních členů. Zjistíme nejprve, jaké hodnosti mají matice soustav rovnic, jimiž jsou "
                    + "podprostory zadány.\n\\subsection*{zjištění hodnosti matice $M$}\n");
            }
            firstRank = this.augMatrix.getSystemCoeffMatrix().rank('M',verbose);
            if (verbose) {
                textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + "." + "\r\n\r\nzjištění hodnosti matice N"
                    + "\r\n--------------------------\r\n");
                texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + "." + "\n\\subsection*{zjištění hodnosti matice $N$}\n");
            }
            secondRank = W.augMatrix.getSystemCoeffMatrix().rank('N',verbose);
            Matrix U = new Matrix();
            U.copy(this.augMatrix.getSystemCoeffMatrix().getEchelonForm(false));
            U.mergeFromBottom(W.augMatrix.getSystemCoeffMatrix().getEchelonForm(false));
            if (verbose) {
                textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + "." + "\r\n\r\nzjištění hodnosti matice S\r\n--------------------------\r\n"
                    + "Nyní vytvoříme průnik podprostorů (tj. obě matice prostě napíšeme „pod sebe“) a zjistíme hodnost matice průniku.\r\n\r\n");
                texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + "." + "\n\\subsection*{zjištění hodnosti matice $S$}\n"
                    + "Nyní vytvoříme průnik podprostorů (tj. obě matice prostě napíšeme \\uv{pod sebe}) a zjistíme hodnost matice průniku.\n\n");
            }
            unionRank = U.rank('S',verbose);
            if (verbose) {
                textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + unionRank + ".");
                texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + unionRank + ".");
            }
        }
        else {
            if (this.isPoint()) {
                if (verbose) {
                    textOut("Podprostor A je bod. Bod je rovnoběžný s čímkoliv.");
                    texOut("Podprostor $\\mathcal{A}$ je bod. Bod je rovnoběžný s čímkoliv.");
                }
                return true;
            }
            if (W.isPoint()) {
                if (verbose) {
                    textOut("Podprostor B je bod. Bod je rovnoběžný s čímkoliv.");
                    texOut("Podprostor $\\mathcal{B}$ je bod. Bod je rovnoběžný s čímkoliv.");
                }
                return true;
            }
            if (verbose) {
                textOut("\r\n\r\nPodprostory jsou rovnoběžné, jestliže platí\r\n\tdim(U+V) = max(dim(U), dim(V)),\r\n kde U je zaměření podprostoru A a V zaměření podprostoru B. "
                    + "Jinými slovy, musí být\r\n\th(S) = max(h(M), h(N)),\r\nkde M je matice, jejíž řádky tvoří směrové vektory podprostoru A, N je matice, jejíž řádky "
                    + "tvoří směrové vektory podprostoru B, a S je matice, jejíž řádky tvoří směrové vektory podprostoru A+B (ta vznikne „slepením“ obou matic). Zjistíme "
                    + "nejprve, jaké hodnosti mají matice, jejichž řádky tvoří směrové vektory jednotlivých podprostorů."
                    + "\r\n\r\nzjištění hodnosti matice M\r\n--------------------------\r\n");
                texOut("\n\nPodprostory jsou rovnoběžné, jestliže platí\n\\[\\dim(U+V) = \\max(\\dim(U), \\dim(V)),\\]\n kde $U$ je zaměření podprostoru $\\mathcal{A}$ a "
                    + "$V$ zaměření podprostoru $\\mathcal{B}$. Jinými slovy, musí být\n\\[h(S) = \\max(h(M), h(N)),\n\\]\nkde $M$ je matice, jejíž řádky tvoří směrové vektory "
                    + "podprostoru $\\mathcal{A}$, $N$ je matice, jejíž řádky tvoří směrové vektory podprostoru $\\mathcal{B}$, a $S$ je matice, jejíž řádky tvoří směrové "
                    + "vektory podprostoru $\\mathcal{A}+\\mathcal{B}$ (ta vznikne \\uv{slepením} obou matic). Zjistíme nejprve, jaké hodnosti mají matice, jejichž řádky tvoří "
                    + "směrové vektory jednotlivých podprostorů.\n\\subsection*{zjištění hodnosti matice $M$}\n");
            }
            firstRank = this.vecMatrix.rank('M',verbose);
            if (verbose) {
                textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + "." + "\r\n\r\nzjištění hodnosti matice N\r\n--------------------------\r\n");
                texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + "." + "\n\\subsection*{zjištění hodnosti matice $N$}\n");
            }
            secondRank = W.vecMatrix.rank('N',verbose);
            Matrix U = new Matrix();
            U.copy(this.vecMatrix.getEchelonForm(false));
            U.mergeFromBottom(W.vecMatrix.getEchelonForm(false));
            if (verbose) {
                textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + "." + "\r\n\r\nzjištění hodnosti matice S\r\n--------------------------\r\n"
                    + "Nyní vytvoříme součet podprostorů (tj. obě matice prostě napíšeme „pod sebe“) a zjistíme hodnost matice součtu.\r\n\r\n");
                texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + "." + "\n\\subsection*{zjištění hodnosti matice $S$}\n"
                    + "Nyní vytvoříme součet podprostorů (tj. obě matice prostě napíšeme \\uv{pod sebe}) a zjistíme hodnost matice součtu.\n\n");
            }
            unionRank = U.rank('S',verbose);
            if (verbose) {
                textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + unionRank + ".");
                texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + unionRank + ".");
            }
        }

        boolean result = (unionRank == Math.max(firstRank, secondRank));
        if (verbose) {
            textOut("\r\n\r\nrozhodnutí o rovnoběžnosti\r\n--------------------------\r\n" + "Matice příslušné k jednotlivým podprostorům tedy mají hodnosti " + firstRank + " a "
                + secondRank + " a je tedy max(" + firstRank + ", " + secondRank + ") = " + Math.max(firstRank, secondRank) + ". Matice příslušná k součtu podprostorů má "
                + "hodnost " + unionRank + ", přičemž " + unionRank + (result ? " = " : " ≠ ") + Math.max(firstRank, secondRank) + ". Podprostory tedy "
                + (result ? "" : "ne") + "jsou rovnoběžné.");
            texOut("\n\n\\subsection*{rozhodnutí o rovnoběžnosti}\n" + "Matice příslušné k jednotlivým podprostorům tedy mají hodnosti " + firstRank + " a "
                + secondRank + " a je tedy $\\max(" + firstRank + ", " + secondRank + ") = " + Math.max(firstRank, secondRank) + "$. Matice příslušná k součtu podprostorů má "
                + "hodnost " + unionRank + ", přičemž $" + unionRank + (result ? " = " : " \\neq ") + Math.max(firstRank, secondRank) + "$. Podprostory tedy "
                + (result ? "" : "ne") + "jsou rovnoběžné.");
        }

        return result;
    }

    /**
     * Checks, if this subspace intersects with the other. This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param W the other subspace
     * 
     * @throw UnsupportedOperationException Throws an exception if the subspaces have a different number of variables.
     * 
     * @return true, if the subspaces intersect, false otherwise
     */
    boolean intersects(Subspace W)
    {
        return intersects(W,'X','Y',false);
    }

    /**
     * Checks, if this subspace intersects with the other.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throw UnsupportedOperationException Throws an exception if the subspaces have a different number of variables.
     * 
     * @return true, if the subspaces intersect, false otherwise
     */
    boolean intersects(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (this.DIM != W.DIM) throw new UnsupportedOperationException("The subspaces are not subspaces of the same superspace - they have a different number of variables.");

        if(W.isPoint()&&!W.getOriginalForm()&&this.isPoint()&&!this.getOriginalForm()){
            if (this.getRefPoint().isEqual(W.getRefPoint())) {
                if(verbose){
                    textOut("Na první pohled vidíme, že se jedná o dva stejné body. Podprostory se tedy protínají.");
                    texOut("Na první pohled vidíme, že se jedná o dva stejné body. Podprostory se tedy protínají.");
                }
                return true;
            }else{
                if(verbose){
                    textOut("Na první pohled vidíme, že se jedná o dva různé body. Podprostory se tedy protínají.");
                    texOut("Na první pohled vidíme, že se jedná o dva různé body. Podprostory se tedy protínají.");
                }
                return false;  
            }
        }

        if (this.generalFormAvailable && W.generalFormAvailable) {
            Matrix U = new Matrix();
            U.copy(this.augMatrix);
            U.mergeFromBottom(W.augMatrix);

            if (verbose) {
                textOut("Oba podprostory jsou zadány obecně. V takovém případě lze zjistit, zda se podprostory protínají jednoduše tak, že ověříme, zda je řešitelná "
                    + "soustava, tvořená všemi rovnicemi obou podprostorů. Matice příslušná podprostoru " + firstLabel + " je přitom:\r\n" +
                    this.augMatrix.toText() + "\r\na matice příslušná podprostoru " + secondLabel + " je:\r\n" + W.augMatrix.toText()
                    + "\r\nOvěříme tedy řešitelnost soustavy, jejíž matice je:\r\n" + U.toText());
                texOut("Oba podprostory jsou zadány obecně. V takovém případě lze zjistit, zda se podprostory protínají jednoduše tak, že ověříme, zda je řešitelná "
                    + "soustava, tvořená všemi rovnicemi obou podprostorů. Matice příslušná podprostoru $\\mathcal{" + firstLabel + "}$ je přitom:\n\\[\n" +
                    this.augMatrix.toTeX() + "\n\\]\na matice příslušná podprostoru $\\mathcal{" + secondLabel + "}$ je:\n\\[\n" + W.augMatrix.toTeX()
                    + "\n\\]\nOvěříme tedy řešitelnost soustavy, jejíž matice je:\n\\[\n" + U.toTeX() + "\n\\]\n");
            }

            return U.isSolvable(verbose);
        }

        if (this.generalFormAvailable && W.parametricFormAvailable) {
            Vector[] normVectors = this.augMatrix.getSystemCoeffMatrix().rowsToVectors();
            Vector[] dirVectors = W.vecMatrix.rowsToVectors();
            Vector point = new Vector(W.refPoint);

            Matrix U = new Matrix(normVectors.length,dirVectors.length + 1);

            String eqnArrayText = "Dostaneme:\r\n\t";
            String[] individualEqnsText = W.toTextParametric(true).split("\r\n");
            String eqnArrayTeX = "Dostaneme:\n\\begin{align*}\n";
            String[] individualEqnsTeX = W.toTeXParametric().split("\n");
            for (int i=0; i<individualEqnsText.length; i++) {
                individualEqnsText[i] = individualEqnsText[i].replaceFirst("\t", "");
                individualEqnsText[i] = individualEqnsText[i].replaceFirst("x_" + String.valueOf(i+1) + " = ", "");
                individualEqnsTeX[i+1] = individualEqnsTeX[i+1].replaceAll("&", "");
                individualEqnsTeX[i+1] = individualEqnsTeX[i+1].replaceAll("\\\\\\\\", "");
                individualEqnsTeX[i+1] = individualEqnsTeX[i+1].replaceFirst("x_\\{" + String.valueOf(i+1) + "\\}  \\{\\}=\\{\\} ", "");
            }
            for (int i=1; i<=U.getR(); i++) {
                for (int j=1; j<U.getC(); j++) {
                    U.set(normVectors[i-1].scalarProduct(dirVectors[j-1]), i, j);
                }

                U.set(normVectors[i-1].scalarProduct(point).add(this.augMatrix.get(i, this.augMatrix.getC())), i, U.getC());

                if (verbose) {
                    Fraction frac = new Fraction();
                    Subspace fake = new Subspace();

                    for (int k=1; k <= DIM; k++) {
                        frac = normVectors[i-1].get(k);
                        if (k > 1 && frac.compare(ZERO) != -1) {
                            eqnArrayText += "+" + frac.toText() + "*";
                            eqnArrayTeX += "+" + frac.toTeX() + "\\cdot";
                        }
                        else {
                            eqnArrayText += frac.toText() + "*";
                            eqnArrayTeX += frac.toTeX() + "\\cdot";
                        }

                        if (individualEqnsText[k-1].indexOf("+") >= 0 || individualEqnsText[k-1].indexOf("-") >= 0)
                            eqnArrayText += "(" + individualEqnsText[k-1] + ")";
                        else
                            eqnArrayText += individualEqnsText[k-1];

                        if (individualEqnsTeX[k].indexOf("+") >= 0 || individualEqnsTeX[k].indexOf("+") >= 0 )
                            eqnArrayTeX += "\\left(" + individualEqnsTeX[k] + "\\right)";
                        else
                            eqnArrayTeX += individualEqnsTeX[k];
                    }

                    frac = this.augMatrix.get(i, this.augMatrix.getC());
                    if (frac.compare(ZERO) == 1){
                        eqnArrayText += "+" + frac.toText();
                        eqnArrayTeX += "+" + frac.toTeX();
                    }
                    else if (frac.compare(ZERO) == -1) {
                        eqnArrayText += frac.toText();
                        eqnArrayTeX += frac.toTeX();
                    }

                    fake = new Subspace(U.submatrix(i, i, 1, U.getC()));
                    String[] result = fake.toTeXGeneral('t').split("\n");
                    eqnArrayText += " = " + fake.toTextGeneral(false,'t');
                    eqnArrayTeX += " &= \\\\ =" + result[1].replaceAll("&", "");

                    eqnArrayText += "\r\n" + (i == U.getR() ? "" : "\t");
                    eqnArrayTeX += (i == U.getR() ? "" : "\\\\");
                }
            }

            if (verbose) {
                eqnArrayTeX += "\n\\end{align*}\n";
                eqnArrayText = eqnArrayText.replaceAll("=0", " = 0");
                textOut("Zatímco podprostor " + firstLabel + " je zadán obecně, podprostor " + secondLabel + " je zadán parametricky. V tom případě jednoduše dosadíme "
                    + "z vyjádření " + secondLabel + " do vyjádření " + firstLabel + ".\r\n\r\n" + eqnArrayText);
                texOut("Zatímco podprostor $\\mathcal{" + firstLabel + "}$ je zadán obecně, podprostor $\\mathcal{" + secondLabel + "}$ je zadán parametricky. V tom "
                    + "případě jednoduše dosadíme z vyjádření $\\mathcal{" + secondLabel + "}$ do vyjádření $\\mathcal{" + firstLabel + "}$.\n\n"
                    + eqnArrayTeX);
            }
            if(U.rank(false)==0){
                if(this.getAugMatrix().getR()<2){
                    textOut("\r\nLevá strana rovnice je po dosazení rovna nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor " + secondLabel + " řešením rovnice určující podprostor " 
                        + firstLabel + ". Podprostor " + secondLabel + " je celý obsažen v podprostoru " + firstLabel + " a podprosory se tedy protínají. ");
                    texOut("\nLevá strana rovnice je po dosazení rovna nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor $\\mathcal{" + secondLabel + "}$ řešením rovnice určující podprostor $\\mathcal{" 
                        + firstLabel + "}$. Podprostor $\\mathcal{" + secondLabel + "}$ je celý obsažen v podprostoru $\\mathcal{" + firstLabel + "}$ a podprosory se tedy protínají. ");
                }else{
                    textOut("\r\nLevé strany rovnic jsou po dosazení rovny nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor " + secondLabel + " řešením soustavy rovnic určující podprostor " 
                        + firstLabel + ". Podprostor " + secondLabel + " je celý obsažen v podprostoru " + firstLabel + " a podprosory se tedy protínají. ");
                    texOut("\nLevé strany rovnic jsou po dosazení rovny nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor $\\mathcal{" + secondLabel + "}$ řešením soustavy rovnic určující podprostor $\\mathcal{" 
                        + firstLabel + "}$. Podprostor $\\mathcal{" + secondLabel + "}$ je celý obsažen v podprostoru $\\mathcal{" + firstLabel + "}$ a podprosory se tedy protínají. ");
                }
                return true;
            }
            if(U.getC()==1){
                if(this.getAugMatrix().getR()<2){
                    textOut("\r\nLevá strana rovnice je po dosazení rovna nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                    texOut("\nLevá strana rovnice je po dosazení rovna nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                }else{
                    textOut("\r\nLevé strany rovnice jsou po dosazení rovny nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                    texOut("\nLevé strany rovnice jsou po dosazení rovny nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                }
                return false;
            }
            return U.isSolvable(verbose);
        }

        if (W.generalFormAvailable && this.parametricFormAvailable)
            return W.intersects(this,secondLabel, firstLabel, verbose);

        if (this.parametricFormAvailable && this.parametricFormAvailable) {
            Matrix leftSideVectors = this.vecMatrix.transpose();
            Matrix rightSideVectors = W.vecMatrix.transpose();
            Vector pointsVector = new Vector(W.refPoint, this.refPoint);
            Matrix absoluteCoeffs = pointsVector.toMatrixColumn();

            int rows = leftSideVectors.getR();
            String[] individualEqnsText = new String[2*rows];
            String[] individualEqnsTeX = new String[2*rows];
            boolean firstTerm, absolutelyFirst;
            Fraction frac;
            for (int r = 1; r <= rows; r++) {
                if (verbose) {
                    firstTerm = true;
                    individualEqnsText[r-1] = "";
                    individualEqnsText[rows+r-1] = "";
                    individualEqnsTeX[r-1] = "";
                    individualEqnsTeX[rows+r-1] = "";

                    for (int c = 1; c <= leftSideVectors.getC(); c++) {
                        frac = leftSideVectors.get(r, c);
                        if (frac.compare(ZERO) == 1) {
                            individualEqnsText[r-1] += (!firstTerm ? "+" : "") + (frac.compare(ONE) == 0 ? "" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&") + (frac.compare(ONE) == 0 ? "" : frac.toTeX()) + "t_{" + c + "}";
                            individualEqnsText[rows+r-1] += (!firstTerm ? "+" : "") + (frac.compare(ONE) == 0 ? "" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[rows+r-1] += (!firstTerm ? "&{}+" : "&") + (frac.compare(ONE) == 0 ? "" : frac.toTeX()) + "t_{" + c + "}";
                            firstTerm = false;
                        }
                        else if (frac.compare(ZERO) == -1) {
                            individualEqnsText[r-1] += (frac.compare(NEG) == 0 ? "-" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[r-1] += (frac.compare(NEG) == 0 ? "&{}-" : "&{}" + frac.toTeX()) + "t_{" + c + "}";
                            individualEqnsText[rows+r-1] += (frac.compare(NEG) == 0 ? "-" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[rows+r-1] += (frac.compare(NEG) == 0 ? "&{}-" : "&{}" + frac.toTeX()) + "t_{" + c + "}";
                            firstTerm = false;
                        }
                        else {
                            individualEqnsTeX[r-1] += "&";
                            individualEqnsTeX[rows+r-1] += "&";
                        }
                    }
                    frac = this.refPoint.get(r);
                    if (frac.compare(ZERO) == 0) {
                        individualEqnsText[r-1] += (firstTerm ? "0" : "");
                        individualEqnsTeX[r-1] += (firstTerm ? "&0" : "&");
                    }
                    else if (frac.compare(ZERO) == 1) {
                        individualEqnsText[r-1] += (!firstTerm ? "+" : "") + frac.toText();
                        individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&") + frac.toTeX();
                    }
                    else {
                        individualEqnsText[r-1] += frac.toText();
                        individualEqnsTeX[r-1] += "&" + frac.toTeX();
                    }

                    individualEqnsText[r-1] += " = ";
                    individualEqnsTeX[r-1] += " &{}={} ";
                    absolutelyFirst = firstTerm;
                    firstTerm = true;

                    for (int c = 1; c <= rightSideVectors.getC(); c++) {
                        frac = rightSideVectors.get(r, c);
                        if (frac.compare(ZERO) == 1) {
                            individualEqnsText[r-1] += (!firstTerm ? "+" : "") + (frac.compare(ONE) == 0 ? "" : frac.toText()) + "u_" + c;
                            individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&{}") + (frac.compare(ONE) == 0 ? "" : frac.toTeX()) + "u_{" + c + "}";
                            individualEqnsText[rows+r-1] += (frac.compare(ONE) == 0 ? "-" : frac.multiply(NEG).toText()) + "u_" + c;
                            individualEqnsTeX[rows+r-1] += (frac.compare(ONE) == 0 ? "&{}-" : "&{}" + frac.multiply(NEG).toTeX()) + "u_{" + c + "}";
                            firstTerm = false;
                            absolutelyFirst = false;
                        }
                        else if (frac.compare(ZERO) == -1) {
                            individualEqnsText[r-1] += (frac.compare(NEG) == 0 ? "-" : frac.toText()) + "u_" + c;
                            individualEqnsTeX[r-1] += (frac.compare(NEG) == 0 ? "&{}-" : "&{}" + frac.toTeX()) + "u_{" + c + "}";
                            individualEqnsText[rows+r-1] += (!absolutelyFirst ? "+" : "") + (frac.compare(NEG) == 0 ? "" : frac.multiply(NEG).toText())
                            + "u_" + c;
                            individualEqnsTeX[rows+r-1] += (!absolutelyFirst ? "&{}+" : "&{}") + (frac.compare(NEG) == 0 ? "&{}" : "&{}" + frac.multiply(NEG).toTeX())
                            + "u_{" + c + "}";
                            firstTerm = false;
                            absolutelyFirst = false;
                        }
                        else {
                            individualEqnsTeX[r-1] += "&";
                            individualEqnsTeX[rows+r-1] += "&";
                        }
                    }
                    frac = W.refPoint.get(r);
                    if (frac.compare(ZERO) == 0) {
                        individualEqnsText[r-1] += (firstTerm ? "0" : "");
                        individualEqnsTeX[r-1] += (firstTerm ? "&{}0" : "&{}");
                    }
                    else if (frac.compare(ZERO) == 1) {
                        individualEqnsText[r-1] += (!firstTerm ? "+" : "") + frac.toText();
                        individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&{}") + frac.toTeX();
                    }
                    else {
                        individualEqnsText[r-1] += frac.toText();
                        individualEqnsTeX[r-1] += "&{}" + frac.toTeX();
                    }
                    frac = absoluteCoeffs.get(r, 1);
                    if (frac.compare(ZERO) == 0) {
                        individualEqnsText[rows+r-1] += (absolutelyFirst ? "0" : "");
                        individualEqnsTeX[rows+r-1] += (absolutelyFirst ? "&{}0" : "&{}");
                    }
                    else if (frac.compare(ZERO) == 1) {
                        individualEqnsText[rows+r-1] += (!absolutelyFirst ? "+" : "") + frac.toText();
                        individualEqnsTeX[rows+r-1] += (!absolutelyFirst ? "&{}+" : "&{}") + frac.toTeX();
                    }
                    else {
                        individualEqnsText[rows+r-1] += frac.toText();
                        individualEqnsTeX[rows+r-1] += "&{}" + frac.toTeX();
                    }

                    individualEqnsText[rows+r-1] += " = 0";
                    individualEqnsTeX[rows+r-1] += " &{}= 0";
                }

                rightSideVectors.multiplyRow(r, NEG);
            }

            Matrix U = new Matrix();
            U.copy(leftSideVectors);
            U.mergeFromRight(rightSideVectors);
            U.mergeFromRight(absoluteCoeffs);   

            if (verbose) {
                String allEquationsText = new String();
                String allEquationsTeX = "\n\\[\n\\begin{array}{*{" + String.valueOf(leftSideVectors.getC() + rightSideVectors.getC() + 4) + "}{r@{}}}";
                for (int i=0; i<rows; i++) {
                    allEquationsText += "\t" + individualEqnsText[i] + "\r\n";
                    allEquationsTeX += individualEqnsTeX[i] + "\\\\\n";
                }
                allEquationsTeX += "\\end{array}\n\\]\n";

                String allEquationsOnOneSideText = new String();
                String allEquationsOnOneSideTeX = "\n\\[\n\\begin{array}{*{" + String.valueOf(leftSideVectors.getC() + rightSideVectors.getC() + 5) + "}{r@{}}}";
                for (int i=rows; i<2*rows; i++) {
                    allEquationsOnOneSideText += "\t" + individualEqnsText[i] + "\r\n";
                    allEquationsOnOneSideTeX += individualEqnsTeX[i] + "\\\\\n";
                }
                allEquationsOnOneSideTeX += "\\end{array}\n\\]\n";

                textOut("Oba podprostory jsou zadány parametricky. V takovém případě lze zjistit, zda se podprostory protínají jednoduše tak, že položíme sobě rovné "
                    + "odpovídající si neznámé a ověříme tedy řešitelnost vzniklé soustavy rovnic. Každý podprostor ovšem bude mít „svůj“ parametr, a proto "
                    + " u podprostoru " + secondLabel + " píšeme místo parametru t parametr u.\r\n\r\nJednotlivé rovnice tedy jsou:\r\n"
                    + allEquationsText + "Po úpravě dostáváme soustavu:\r\n" + allEquationsOnOneSideText + "Budeme ověřovat řešitelnost této soustavy. ");
                texOut("Oba podprostory jsou zadány parametricky. V takovém případě lze zjistit, zda se podprostory protínají jednoduše tak, že položíme sobě rovné "
                    + "odpovídající si neznámé a ověříme tedy řešitelnost vzniklé soustavy rovnic. Každý podprostor ovšem bude mít \\uv{svůj} parametr, a proto "
                    + " u podprostoru $\\mathcal{" + secondLabel + "}$ píšeme místo parametru $t$ parametr $u$.\n\nJednotlivé rovnice tedy jsou:\n"
                    + allEquationsTeX + "Po úpravě dostáváme soustavu:\n" + allEquationsOnOneSideTeX + "Budeme ověřovat řešitelnost této soustavy. ");
            }

            return U.isSolvable(verbose);
        }

        return false;
    }

    /**
     * Checks, if this subspace intersects with the other. And find intersection of given subspaces.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throw UnsupportedOperationException Throws an exception if the subspaces have a different number of variables.
     * 
     * @return intersection subspace
     */
    String intersection(Subspace W, char firstLabel, char secondLabel, boolean verbose){
        if (this.DIM != W.DIM) throw new UnsupportedOperationException("The subspaces are not subspaces of the same superspace - they have a different number of variables.");

        if(W.isPoint()&&!W.getOriginalForm()&&this.isPoint()&&!this.getOriginalForm()){
            if (this.getRefPoint().isEqual(W.getRefPoint())) {
                if(verbose){
                    textOut("Na první pohled vidíme, že podprostry jsou totožné, jelikož se jedná o tentýž bod. Průsečíkem je tedy tento bod.");
                    texOut("Na první pohled vidíme, že podprostory jsou totožné, jelikož se jedná o tentýž bod. Průsečíkem je tedy tento bod.");
                }
                return "Zadané podprostory se neprotínají.";
            }else{
                if(verbose){
                    textOut("Na první pohled vidíme, že se jedná o dva různé body. Podprostory se tedy protínají.");
                    texOut("Na první pohled vidíme, že se jedná o dva různé body. Podprostory se tedy protínají.");
                }
                return "Zadané podprostory se neprotínají.";  
            }
        }

        if (this.generalFormAvailable && W.generalFormAvailable) {
            Matrix U = new Matrix();
            U.copy(this.augMatrix);
            U.mergeFromBottom(W.augMatrix);
            if (verbose) {
                textOut("Oba podprostory jsou zadány obecně. V takovém případě nalezneme průnik obou podprostorů tak, že vyřešíme "
                    + "soustavu, tvořenou všemi rovnicemi obou podprostorů. Matice příslušná podprostoru " + firstLabel + " je přitom:\r\n" +
                    this.augMatrix.toText() + "\r\na matice příslušná podprostoru " + secondLabel + " je:\r\n" + W.augMatrix.toText()
                    + "\r\nBudeme tedy řešit soustavu, jejíž matice je:\r\n" + U.toText() 
                    + "Nejprve matici soustavy převedeme na schodovitý tvar.\r\n\r\n" + "převod na schodovitý tvar\r\n-------------------------\r\n");
                texOut("Oba podprostory jsou zadány obecně. V takovém případě nalezneme průnik obou podprostorů tak, že vyřešíme "
                    + "soustavu, tvořenou všemi rovnicemi obou podprostorů. Matice příslušná podprostoru $\\mathcal{" + firstLabel + "}$ je přitom:\n\\[\n" +
                    this.augMatrix.toTeX() + "\n\\]\na matice příslušná podprostoru $\\mathcal{" + secondLabel + "}$ je:\n\\[\n" + W.augMatrix.toTeX()
                    + "\n\\]\nOvěříme tedy řešitelnost soustavy, jejíž matice je:\n\\[\n" + U.toTeX() + "\n\\]\n"
                    + "Nejprve matici soustavy převedeme na schodovitý tvar.\n\\subsection*{převod na schodovitý tvar}\n");
            }
            U.getEchelonForm();
            if (verbose) {
                textOut(" Nyní ověříme, zda se zadané podprostory protínají. Toto zjistíme jednoduše tak, že ověříme řešitelnost soustavy vyjádřené maticí M. ");
                texOut(" Nyní ověříme, zda se zadané podprostory protínají. Toto zjistíme jednoduše tak, že ověříme řešitelnost soustavy vyjádřené maticí M. ");
            }
            if(U.isSolvable(verbose, true)){
                if (verbose) {
                    textOut(" Nyní můžeme přistoupit k samotnému řešení soustavy rovnic. Využijeme toho, že matici M již máme převedenou na schodovitý tvar. ");
                    texOut(" Nyní můžeme přistoupit k samotnému řešení soustavy rovnic. Využijeme toho, že matici M již máme převedenou na schodovitý tvar. ");
                }
                U.solve('M',verbose, true);
                Subspace sub = new Subspace(U);
                sub.getParametricForm('M', false);
                if (verbose) {
                    textOut("Jedná se přitom současně o parametrické vyjádření průniku zadaných podprostorů.");
                    texOut("Jedná se přitom současně o parametrické vyjádření průniku zadaných podprostorů.");
                }
                return sub.toTextParametric();
            }else{
                return "Zadané podprostory se neprotínají.";
            }
        }

        if (this.generalFormAvailable && W.parametricFormAvailable) {
            Vector[] normVectors = this.augMatrix.getSystemCoeffMatrix().rowsToVectors();
            Vector[] dirVectors = W.vecMatrix.rowsToVectors();
            Vector point = new Vector(W.refPoint);

            Matrix U = new Matrix(normVectors.length,dirVectors.length + 1);

            String eqnArrayText = "Dostaneme:\r\n\t";
            String[] individualEqnsText = W.toTextParametric(true).split("\r\n");
            String eqnArrayTeX = "Dostaneme:\n\\begin{align*}\n";
            String[] individualEqnsTeX = W.toTeXParametric().split("\n");
            for (int i=0; i<individualEqnsText.length; i++) {
                individualEqnsText[i] = individualEqnsText[i].replaceFirst("\t", "");
                individualEqnsText[i] = individualEqnsText[i].replaceFirst("x_" + String.valueOf(i+1) + " = ", "");
                individualEqnsTeX[i+1] = individualEqnsTeX[i+1].replaceAll("&", "");
                individualEqnsTeX[i+1] = individualEqnsTeX[i+1].replaceAll("\\\\\\\\", "");
                individualEqnsTeX[i+1] = individualEqnsTeX[i+1].replaceFirst("x_\\{" + String.valueOf(i+1) + "\\}  \\{\\}=\\{\\} ", "");
            }
            for (int i=1; i<=U.getR(); i++) {
                for (int j=1; j<U.getC(); j++) {
                    U.set(normVectors[i-1].scalarProduct(dirVectors[j-1]), i, j);
                }

                U.set(normVectors[i-1].scalarProduct(point).add(this.augMatrix.get(i, this.augMatrix.getC())), i, U.getC());

                if (verbose) {
                    Fraction frac = new Fraction();
                    Subspace fake = new Subspace();

                    for (int k=1; k <= DIM; k++) {
                        frac = normVectors[i-1].get(k);
                        if (k > 1 && frac.compare(ZERO) != -1) {
                            eqnArrayText += "+" + frac.toText() + "*";
                            eqnArrayTeX += "+" + frac.toTeX() + "\\cdot";
                        }
                        else {
                            eqnArrayText += frac.toText() + "*";
                            eqnArrayTeX += frac.toTeX() + "\\cdot";
                        }

                        if (individualEqnsText[k-1].indexOf("+") >= 0 || individualEqnsText[k-1].indexOf("-") >= 0)
                            eqnArrayText += "(" + individualEqnsText[k-1] + ")";
                        else
                            eqnArrayText += individualEqnsText[k-1];

                        if (individualEqnsTeX[k].indexOf("+") >= 0 || individualEqnsTeX[k].indexOf("+") >= 0 )
                            eqnArrayTeX += "\\left(" + individualEqnsTeX[k] + "\\right)";
                        else
                            eqnArrayTeX += individualEqnsTeX[k];
                    }

                    frac = this.augMatrix.get(i, this.augMatrix.getC());
                    if (frac.compare(ZERO) == 1){
                        eqnArrayText += "+" + frac.toText();
                        eqnArrayTeX += "+" + frac.toTeX();
                    }
                    else if (frac.compare(ZERO) == -1) {
                        eqnArrayText += frac.toText();
                        eqnArrayTeX += frac.toTeX();
                    }

                    fake = new Subspace(U.submatrix(i, i, 1, U.getC()));
                    String[] result = fake.toTeXGeneral('t').split("\n");
                    eqnArrayText += " = " + fake.toTextGeneral(false,'t');
                    eqnArrayTeX += " &= \\\\ =" + result[1].replaceAll("&", "");

                    eqnArrayText += "\r\n" + (i == U.getR() ? "" : "\t");
                    eqnArrayTeX += (i == U.getR() ? "" : "\\\\");
                }
            }

            if (verbose) {
                eqnArrayTeX += "\n\\end{align*}\n";
                eqnArrayText = eqnArrayText.replaceAll("=0", " = 0");
                textOut("Zatímco podprostor " + firstLabel + " je zadán obecně, podprostor " + secondLabel + " je zadán parametricky. V tom případě jednoduše dosadíme z vyjádření " 
                    + secondLabel + " do vyjádření " + firstLabel + ".\r\n\r\n" + eqnArrayText);
                texOut("Zatímco podprostor $\\mathcal{" + firstLabel + "}$ je zadán obecně, podprostor $\\mathcal{" + secondLabel + "}$ je zadán parametricky. V tom "
                    + "případě jednoduše dosadíme z vyjádření $\\mathcal{" + secondLabel + "}$ do vyjádření $\\mathcal{" + firstLabel + "}$.\n\n"
                    + eqnArrayTeX);
            } 

            if(U.rank(false)==0){
                if(this.getAugMatrix().getR()<2){
                    textOut("\r\nLevá strana rovnice je po dosazení rovna nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor " + secondLabel + " řešením rovnice určující podprostor " 
                        + firstLabel + ". Podprostor " + secondLabel + " je celý obsažen v podprostoru " + firstLabel + " a podprosory se tedy protínají. ");
                    texOut("\nLevá strana rovnice je po dosazení rovna nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor $\\mathcal{" + secondLabel + "}$ řešením rovnice určující podprostor $\\mathcal{" 
                        + firstLabel + "}$. Podprostor $\\mathcal{" + secondLabel + "}$ je celý obsažen v podprostoru $\\mathcal{" + firstLabel + "}$ a podprosory se tedy protínají. ");
                }else{
                    textOut("\r\nLevé strany rovnic jsou po dosazení rovny nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor " + secondLabel + " řešením soustavy rovnic určující podprostor " 
                        + firstLabel + ". Podprostor " + secondLabel + " je celý obsažen v podprostoru " + firstLabel + " a podprosory se tedy protínají. ");
                    texOut("\nLevé strany rovnic jsou po dosazení rovny nule, což znamená, že pro libovolné parametry jsou rovnice určující podprostor $\\mathcal{" + secondLabel + "}$ řešením soustavy rovnic určující podprostor $\\mathcal{" 
                        + firstLabel + "}$. Podprostor $\\mathcal{" + secondLabel + "}$ je celý obsažen v podprostoru $\\mathcal{" + firstLabel + "}$ a podprosory se tedy protínají. ");
                }
                return W.toTextParametric();
            }
            if(U.getC()==1){
                if(this.getAugMatrix().getR()<2){
                    textOut("\r\nLevá strana rovnice je po dosazení rovna nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                    texOut("\nLevá strana rovnice je po dosazení rovna nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                }else{
                    textOut("\r\nLevé strany rovnice jsou po dosazení rovny nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                    texOut("\nLevé strany rovnice jsou po dosazení rovny nenulovému číslu. Pravá strana rovnice se ale rovná nule. Soustava tedy není řešitelná a podprostory se tedy neprotínají.");
                }
                return "Zadané podprostory se neprotínají.";
            }

            if (verbose) {        
                textOut("\r\nVyřešením této soustavy zjistíme hodnoty parametrů, které poté dosadíme do rovnic podprostoru " + secondLabel + ". Budeme tedy řešit soustavu, jejíž matice je:\r\n" 
                    + U.toText() + "Nejprve matici soustavy převedeme na schodovitý tvar.\r\n\r\n" + "převod na schodovitý tvar\r\n-------------------------\r\n");
                texOut("\nVyřešením této soustavy zjistíme hodnoty parametrů, které poté dosadíme do rovnic podprostoru  $\\mathcal{" + secondLabel + "}$ Budeme tedy řešit soustavu, jejíž matice je: "
                    + "\n\\[" + U.toTeX() + "\\]\n Nejprve matici soustavy převedeme na schodovitý tvar.\n\\subsection*{převod na schodovitý tvar}\n");
            }
            Matrix S = U.getEchelonForm();
            if (verbose) {
                textOut(" Nyní ověříme, zda se zadané podprostory protínají. Toto zjistíme jednoduše tak, že ověříme řešitelnost soustavy vyjádřené maticí M. ");
                texOut(" Nyní ověříme, zda se zadané podprostory protínají. Toto zjistíme jednoduše tak, že ověříme řešitelnost soustavy vyjádřené maticí M. ");
            }

            if(S.isSolvable(verbose, true)){
                if (verbose) {
                    textOut(" Nyní můžeme přistoupit k samotnému řešení soustavy rovnic. Využijeme toho, že matici M již máme převedenou na schodovitý tvar. ");
                    texOut(" Nyní můžeme přistoupit k samotnému řešení soustavy rovnic. Využijeme toho, že matici M již máme převedenou na schodovitý tvar. ");
                }
                Matrix solution = S.solve2('M',verbose, true, W.getVecMatrix().getR());

                Matrix B = solution.submatrix(1,solution.getR(), S.getR()+1,solution.getC());
                Matrix P = new Matrix(1,B.getC());
                P.set(ONE,1,B.getC());
                B.mergeFromBottom(P);

                

                Matrix A = new Matrix();
                A.copy(W.getVecMatrix());
                A.copy(A.transpose());
                P.copy(W.getRefPoint().toMatrixColumn());
                A.mergeFromRight(P);

               

                Matrix intersection = A.multiplyMatrix(B);

               

                Subspace sub = new Subspace();
                if(intersection.getC()==1){
                    Point[] pointField = new Point[1];
                    pointField[0] = new Point(intersection.transpose().rowsToVectors()[0]);
                    Vector[] vectorField = new Vector[0];
                    sub = new Subspace(pointField, vectorField);
                }else{
                    Point[] pointField = new Point[1];
                    pointField[0] = new Point(intersection.getConstTerms().transpose().rowsToVectors()[0]);
                    Vector[] vectorField = intersection.getSystemCoeffMatrix().transpose().rowsToVectors();
                    sub = new Subspace(pointField, vectorField);
                }

                if (verbose) {
                    textOut("Tímto jsme vyřešili soustavu rovnic a zjistili hodnoty parametrů, které nyní můžeme dosadit do rovnic podprostoru " + secondLabel + "."
                        + "\r\n\r\ndosazení do zadání\r\n------------------\r\nDosazením vypočtených hodnot do soustavy\r\n" + W.toTextParametric(true,'x','t')
                        + "dostáváme:\r\n" + sub.toTextParametric(true ,'x','p') + "což je parametrické vyjádření hledaného průniku podprostorů.");
                    texOut("Tímto jsme vyřešili soustavu rovnic a zjistili hodnoty parametrů, které nyní můžeme dosadit do rovnic podprostoru $\\mathcal{" + secondLabel + "}$."
                        + "\n\\subsection*{dosazení do zadání}\n Dosazením vypočtených hodnot do soustavy \n\\[" + W.toTeXParametric('x','t')
                        + "\\]\n dostáváme: \n\\[" + sub.toTeXParametric('x','p') + "\\]\n což je parametrické vyjádření hledaného průniku podprostorů.");
                }

                return sub.toTextParametric();
            }else{
                return "Zadané podprostory se neprotínají.";
            }
        }

        if (W.generalFormAvailable && this.parametricFormAvailable)
            return W.intersection(this,secondLabel, firstLabel, verbose);

        if (this.parametricFormAvailable && W.parametricFormAvailable && this.getVecMatrix().getR()<W.getVecMatrix().getR())   
            return W.intersection(this,secondLabel, firstLabel, verbose);

        if (this.parametricFormAvailable && W.parametricFormAvailable) {
            Matrix leftSideVectors = this.vecMatrix.transpose();
            Matrix rightSideVectors = W.vecMatrix.transpose();
            Vector pointsVector = new Vector(W.refPoint, this.refPoint);
            Matrix absoluteCoeffs = pointsVector.toMatrixColumn();

            int rows = leftSideVectors.getR();
            String[] individualEqnsText = new String[2*rows];
            String[] individualEqnsTeX = new String[2*rows];
            boolean firstTerm, absolutelyFirst;
            Fraction frac;
            for (int r = 1; r <= rows; r++) {
                if (verbose) {
                    firstTerm = true;
                    individualEqnsText[r-1] = "";
                    individualEqnsText[rows+r-1] = "";
                    individualEqnsTeX[r-1] = "";
                    individualEqnsTeX[rows+r-1] = "";

                    for (int c = 1; c <= leftSideVectors.getC(); c++) {
                        frac = leftSideVectors.get(r, c);
                        if (frac.compare(ZERO) == 1) {
                            individualEqnsText[r-1] += (!firstTerm ? "+" : "") + (frac.compare(ONE) == 0 ? "" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&") + (frac.compare(ONE) == 0 ? "" : frac.toTeX()) + "t_{" + c + "}";
                            individualEqnsText[rows+r-1] += (!firstTerm ? "+" : "") + (frac.compare(ONE) == 0 ? "" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[rows+r-1] += (!firstTerm ? "&{}+" : "&") + (frac.compare(ONE) == 0 ? "" : frac.toTeX()) + "t_{" + c + "}";
                            firstTerm = false;
                        }
                        else if (frac.compare(ZERO) == -1) {
                            individualEqnsText[r-1] += (frac.compare(NEG) == 0 ? "-" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[r-1] += (frac.compare(NEG) == 0 ? "&{}-" : "&{}" + frac.toTeX()) + "t_{" + c + "}";
                            individualEqnsText[rows+r-1] += (frac.compare(NEG) == 0 ? "-" : frac.toText()) + "t_" + c;
                            individualEqnsTeX[rows+r-1] += (frac.compare(NEG) == 0 ? "&{}-" : "&{}" + frac.toTeX()) + "t_{" + c + "}";
                            firstTerm = false;
                        }
                        else {
                            individualEqnsTeX[r-1] += "&";
                            individualEqnsTeX[rows+r-1] += "&";
                        }
                    }
                    frac = this.refPoint.get(r);
                    if (frac.compare(ZERO) == 0) {
                        individualEqnsText[r-1] += (firstTerm ? "0" : "");
                        individualEqnsTeX[r-1] += (firstTerm ? "&0" : "&");
                    }
                    else if (frac.compare(ZERO) == 1) {
                        individualEqnsText[r-1] += (!firstTerm ? "+" : "") + frac.toText();
                        individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&") + frac.toTeX();
                    }
                    else {
                        individualEqnsText[r-1] += frac.toText();
                        individualEqnsTeX[r-1] += "&" + frac.toTeX();
                    }

                    individualEqnsText[r-1] += " = ";
                    individualEqnsTeX[r-1] += " &{}={} ";
                    absolutelyFirst = firstTerm;
                    firstTerm = true;

                    for (int c = 1; c <= rightSideVectors.getC(); c++) {
                        frac = rightSideVectors.get(r, c);
                        if (frac.compare(ZERO) == 1) {
                            individualEqnsText[r-1] += (!firstTerm ? "+" : "") + (frac.compare(ONE) == 0 ? "" : frac.toText()) + "u_" + c;
                            individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&{}") + (frac.compare(ONE) == 0 ? "" : frac.toTeX()) + "u_{" + c + "}";
                            individualEqnsText[rows+r-1] += (frac.compare(ONE) == 0 ? "-" : frac.multiply(NEG).toText()) + "u_" + c;
                            individualEqnsTeX[rows+r-1] += (frac.compare(ONE) == 0 ? "&{}-" : "&{}" + frac.multiply(NEG).toTeX()) + "u_{" + c + "}";
                            firstTerm = false;
                            absolutelyFirst = false;
                        }
                        else if (frac.compare(ZERO) == -1) {
                            individualEqnsText[r-1] += (frac.compare(NEG) == 0 ? "-" : frac.toText()) + "u_" + c;
                            individualEqnsTeX[r-1] += (frac.compare(NEG) == 0 ? "&{}-" : "&{}" + frac.toTeX()) + "u_{" + c + "}";
                            individualEqnsText[rows+r-1] += (!absolutelyFirst ? "+" : "") + (frac.compare(NEG) == 0 ? "" : frac.multiply(NEG).toText())
                            + "u_" + c;
                            individualEqnsTeX[rows+r-1] += (!absolutelyFirst ? "&{}+" : "&{}") + (frac.compare(NEG) == 0 ? "&{}" : "&{}" + frac.multiply(NEG).toTeX())
                            + "u_{" + c + "}";
                            firstTerm = false;
                            absolutelyFirst = false;
                        }
                        else {
                            individualEqnsTeX[r-1] += "&";
                            individualEqnsTeX[rows+r-1] += "&";
                        }
                    }
                    frac = W.refPoint.get(r);
                    if (frac.compare(ZERO) == 0) {
                        individualEqnsText[r-1] += (firstTerm ? "0" : "");
                        individualEqnsTeX[r-1] += (firstTerm ? "&{}0" : "&{}");
                    }
                    else if (frac.compare(ZERO) == 1) {
                        individualEqnsText[r-1] += (!firstTerm ? "+" : "") + frac.toText();
                        individualEqnsTeX[r-1] += (!firstTerm ? "&{}+" : "&{}") + frac.toTeX();
                    }
                    else {
                        individualEqnsText[r-1] += frac.toText();
                        individualEqnsTeX[r-1] += "&{}" + frac.toTeX();
                    }
                    frac = absoluteCoeffs.get(r, 1);
                    if (frac.compare(ZERO) == 0) {
                        individualEqnsText[rows+r-1] += (absolutelyFirst ? "0" : "");
                        individualEqnsTeX[rows+r-1] += (absolutelyFirst ? "&{}0" : "&{}");
                    }
                    else if (frac.compare(ZERO) == 1) {
                        individualEqnsText[rows+r-1] += (!absolutelyFirst ? "+" : "") + frac.toText();
                        individualEqnsTeX[rows+r-1] += (!absolutelyFirst ? "&{}+" : "&{}") + frac.toTeX();
                    }
                    else {
                        individualEqnsText[rows+r-1] += frac.toText();
                        individualEqnsTeX[rows+r-1] += "&{}" + frac.toTeX();
                    }

                    individualEqnsText[rows+r-1] += " = 0";
                    individualEqnsTeX[rows+r-1] += " &{}= 0";
                }

                rightSideVectors.multiplyRow(r, NEG);
            }

            Matrix U = new Matrix();
            U.copy(leftSideVectors);
            U.mergeFromRight(rightSideVectors);
            U.mergeFromRight(absoluteCoeffs);   

            if (verbose) {
                String allEquationsText = new String();
                String allEquationsTeX = "\n\\[\n\\begin{array}{*{" + String.valueOf(leftSideVectors.getC() + rightSideVectors.getC() + 4) + "}{r@{}}}";
                for (int i=0; i<rows; i++) {
                    allEquationsText += "\t" + individualEqnsText[i] + "\r\n";
                    allEquationsTeX += individualEqnsTeX[i] + "\\\\\n";
                }
                allEquationsTeX += "\\end{array}\n\\]\n";

                String allEquationsOnOneSideText = new String();
                String allEquationsOnOneSideTeX = "\n\\[\n\\begin{array}{*{" + String.valueOf(leftSideVectors.getC() + rightSideVectors.getC() + 5) + "}{r@{}}}";
                for (int i=rows; i<2*rows; i++) {
                    allEquationsOnOneSideText += "\t" + individualEqnsText[i] + "\r\n";
                    allEquationsOnOneSideTeX += individualEqnsTeX[i] + "\\\\\n";
                }
                allEquationsOnOneSideTeX += "\\end{array}\n\\]\n";

                textOut("Oba podprostory jsou zadány parametricky. Průnik podprostorů nalezneme jednoduše tak, že položíme sobě rovné odpovídající si neznámé. "
                    + "Vzniklou soustavu rovnic vyřešíme a získáme hodnoty parametrů, pro které jsou si zadané podprostory rovny. "
                    + "Dosazením parametrů zpět do zadání získáme parametrickou rovnici průniku podprostorů. "
                    + "Abychom mohli rozlišit parametry jednotlivých podprostrů, upravíme podprostor " + secondLabel 
                    + " tak, že místo parametru t píšeme u, dostaneme:\r\n" + W.toTextParametric(true,'x','u')
                    + (this.getVecMatrix().getR()>W.getVecMatrix().getR() ? "úmyslně vybereme podprostor s menším počtem parametrů (brzy se dozvíme proč). " : "")
                    + "Nyní vytvoříme rovnice, podprostor " + firstLabel + " bude na levé straně, podprostor " + secondLabel + " na pravé straně. Jednotlivé rovnice tedy jsou:\r\n"
                    + allEquationsText + "Po úpravě dostáváme soustavu:\r\n" + allEquationsOnOneSideText + "Budeme tedy řešit soustavu, jejíž matice je:\r\n" 
                    + U.toText() + "Nejprve matici soustavy převedeme na schodovitý tvar.\r\n\r\n" + "převod na schodovitý tvar\r\n-------------------------\r\n");
                texOut("Oba podprostory jsou zadány parametricky. Průnik podprostorů nalezneme jednoduše tak, že položíme sobě rovné odpovídající si neznámé. "
                    + "Vzniklou soustavu rovnic vyřešíme a získáme hodnoty parametrů, pro které jsou si zadané podprostory rovny. "
                    + "Dosazením parametrů zpět do zadání získáme parametrickou rovnici průniku podprostorů. "
                    + "Abychom mohli rozlišit parametry jednotlivých podprostrů, upravíme podprostor  $\\mathcal{" + secondLabel + "}$ "
                    + "tak, že píšeme místo parametru $t$ parametr $u$. dostaneme \n\\[" + W.toTeXParametric('x','u') + "\\]\n"
                    + (this.getVecMatrix().getR()>W.getVecMatrix().getR() ? "úmyslně vybereme podprostor s menším počtem parametrů (brzy se dozvíme proč). " : "")
                    + "Nyní vytvoříme rovnice, podprostor $\\mathcal{" + firstLabel + "}$ bude na levé straně, podprostor $\\mathcal{" + secondLabel + "}$ na pravé straně. \n\nJednotlivé rovnice tedy jsou:\n"
                    + allEquationsTeX + "Po úpravě dostáváme soustavu:\n" + allEquationsOnOneSideTeX + "Budeme tedy řešit soustavu, jejíž matice je: \n "
                    + "\\[" + U.toTeX() + "\\] \n Nejprve matici soustavy převedeme na schodovitý tvar. \\subsection*{převod na schodovitý tvar}");
            }

            Matrix S = U.getEchelonForm();
            if (verbose) {
                textOut(" Nyní ověříme, zda se zadané podprostory protínají. Toto zjistíme jednoduše tak, že ověříme řešitelnost soustavy vyjádřené maticí M. ");
                texOut(" Nyní ověříme, zda se zadané podprostory protínají. Toto zjistíme jednoduše tak, že ověříme řešitelnost soustavy vyjádřené maticí $M$. ");
            }
            if(S.isSolvable(verbose)){
                if (verbose) {
                    textOut(" Nyní můžeme přistoupit k samotnému řešení soustavy rovnic. Využijeme toho, že matici M již máme převedenou na schodovitý tvar. ");
                    texOut(" Nyní můžeme přistoupit k samotnému řešení soustavy rovnic. Využijeme toho, že matici M již máme převedenou na schodovitý tvar. ");
                }
                Matrix solution = S.solve2('M',verbose, true, this.getVecMatrix().getR());

                Matrix B = solution.submatrix(this.getVecMatrix().getR()+1,solution.getR(), S.getR()+1,solution.getC());
                Matrix P = new Matrix(1,B.getC());
                P.set(ONE,1,B.getC());
                B.mergeFromBottom(P);

                Matrix A = new Matrix();
                A.copy(W.getVecMatrix());
                A.copy(A.transpose());
                P.copy(W.getRefPoint().toMatrixColumn());
                A.mergeFromRight(P);

                Matrix intersection = A.multiplyMatrix(B);

                Subspace sub = new Subspace();
                if(intersection.getC()==1){
                    Point[] pointField = new Point[1];
                    pointField[0] = new Point(intersection.transpose().rowsToVectors()[0]);
                    Vector[] vectorField = new Vector[0];
                    sub = new Subspace(pointField, vectorField);
                }else{
                    Point[] pointField = new Point[1];
                    pointField[0] = new Point(intersection.getConstTerms().transpose().rowsToVectors()[0]);
                    Vector[] vectorField = intersection.getSystemCoeffMatrix().transpose().rowsToVectors();
                    sub = new Subspace(pointField, vectorField);
                }

                if (verbose) {
                    textOut("Dále soustavu řešit nemusíme, protože už známe hodnoty všech parametrů podprostoru " + secondLabel
                        + (this.getVecMatrix().getR()>W.getVecMatrix().getR() ? " (proto je vhodné vybrat jako druhý podprostor ten s méně parametry)." : ".")
                        + "\r\n\r\ndosazení do zadání\r\n------------------\r\nDosazením vypočtených hodnot do rovnice\r\n" + W.toTextParametric(true,'x','u')
                        + "dostáváme:\r\n" + sub.toTextParametric(true,'x','p') + "což je parametrické vyjádření hledaného průniku podprostorů.");
                    texOut("Dále soustavu řešit nemusíme, protože už známe hodnoty všech parametrů podprostoru $\\mathcal{" + secondLabel + "}$"
                        + (this.getVecMatrix().getR()>W.getVecMatrix().getR() ? " (proto je vhodné vybrat jako druhý podprostor ten s méně parametry)." : ".")
                        + "\\subsection*{dosazení do zadání}\n Dosazením vypočtených hodnot do rovnice \n\\[" + W.toTeXParametric('x','u')
                        + "\\]\ndostáváme: \n\\[" + sub.toTeXParametric('x','p') + "\n\\]což je parametrické vyjádření hledaného průniku podprostorů.");
                }
                return  sub.toTextParametric();
            }else{
                return "Zadané podprostory se neprotínají.";
            }
        }
        return "chyba";
    }

    /**
     * Determine the relative position of given subspaces.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throw UnsupportedOperationException Throws an exception if the subspaces have a different number of variables.
     * 
     * @return description of relative position
     */
    String relativePosition(Subspace W, char firstLabel, char secondLabel, boolean verbose){
        if (this.DIM != W.DIM) throw new UnsupportedOperationException("The subspaces are not subspaces of the same superspace - they have a different number of variables.");

        if(W.isPoint()&&!W.getOriginalForm()&&this.isPoint()&&!this.getOriginalForm()){
            if (this.getRefPoint().isEqual(W.getRefPoint())) {
                if(verbose){
                    textOut("Na první pohled vidíme, že se jedná o dva stejné body. Podprostory jsou tedy rovnoběžné totožné.");
                    texOut("Na první pohled vidíme, že se jedná o dva stejné body. Podprostory jsou tedy rovnoběžné totožné.");
                }
                return "rovnoběžné totožné";
            }else{
                if(verbose){
                    textOut("Na první pohled vidíme, že se jedná o dva různé body. Podprostory jsou tedy rovnoběžné různé.");
                    texOut("Na první pohled vidíme, že se jedná o dva různé body. Podprostory jsou tedy rovnoběžné různé.");
                }
                return "rovnoběžné různé";  
            }
        }

        if(W.isPoint()&&!W.getOriginalForm()){
            return W.relativePosition(this, secondLabel, firstLabel, verbose);
        }
        if(this.isPoint()&&!this.getOriginalForm()){
            if(verbose){
                textOut("Na první pohled vidíme, že se podprostor "+firstLabel+" je bod. Podprostory tedy budou jistě rovnoběžné a stačí pouze určit, zda se protínají."
                    + "\r\n\r\nOvěření disjunktnosti podprostorů\r\n---------------------------------\r\n");
                texOut("Na první pohled vidíme, že se podprostor $\\mathcal{"+firstLabel+"}$ je bod. Podprostory tedy budou jistě rovnoběžné a stačí pouze určit, zda se protínají."
                    + "\n\\subsection*{Ověření disjunktnosti podprostorů}\n");
            }
            if(this.intersects(W, firstLabel, secondLabel, verbose)){
                if(verbose){
                    textOut(" Podprostory jsou tudíž rovnoběžné a " + firstLabel +  " ⊂ " + secondLabel + ".");
                    texOut(" Podprostory jsou tudíž rovnoběžné a $\\mathcal{" + firstLabel +  "} \\subset \\mathcal{" + secondLabel + "}$.");
                }
                return firstLabel +  " ⊂ " + secondLabel;
            }else{
                if(verbose){
                    textOut(" Podprostory jsou tudíž rovnoběžné různé.");
                    texOut(" Podprostory jsou tudíž rovnoběžné různé.");
                }
                return "rovnoběžné různé";
            }
        }

        if (this.parametricFormAvailable && W.parametricFormAvailable) {
            Matrix U = new Matrix();
            U.copy(this.vecMatrix.getEchelonForm(false));
            U.mergeFromBottom(W.vecMatrix.getEchelonForm(false));

            if (verbose) {
                textOut("Při určování vzájemné polohy budeme postupovat tak, že nejprve ověříme, zda jsou podprostory rovnoběžné a poté rozhodneme, "
                    + "zda se podprostory protínají.\r\n" + "\r\nOvěření rovnoběžnosti podprostorů\r\n---------------------------------\r\n");
                texOut("Při určování vzájemné polohy budeme postupovat tak, že nejprve ověříme, zda jsou podprostory rovnoběžné a poté rozhodneme, "
                    + "zda se podprostory protínají.\n" + "\\subsection*{Ověření rovnoběžnosti podprostorů}\n");
            }

            this.isParallel(W, verbose);

            int maxDim = (W.getDim()>this.getDim() ? W.getDim() : this.getDim());
            Matrix echelon = new Matrix();
            echelon.copy(U.getEchelonForm(false));
            boolean parallel;
            int echelonRank=echelon.rank(false);

            if(echelonRank==maxDim){
                parallel=true;
            }else{
                parallel=false;
            }

            Vector pointsVector = new Vector(this.refPoint, W.refPoint);
            Matrix absoluteCoeffs = pointsVector.toMatrixRow();
            echelon.mergeFromBottom(absoluteCoeffs);
            boolean intersects;
            int dimA = this.getDim();
            int dimB = W.getDim();

            if (verbose) {
                textOut("\r\nOvěření disjunktnosti podprostorů\r\n---------------------------------\r\n"
                    + "Nyní potřebujeme rozhodnout, zda se podprostory protínají. K tomu využijeme vektor, který vytvoříme z bodů obou podprostorů. "
                    + "Vzniklý vektor " + pointsVector.toText() + " přidáme jako poslední řádek do matice S. Pokud se tím hodnost matice nezmění, znamená to, že podprostory se protínají. "
                    + "Pokud se hodnost matice zvýší, pak se podprostory neprotínají. Zjistíme tedy hodnost upravené matice S.\r\n");
                texOut("\n\\subsection*{Ověření disjunktnosti podprostorů}\n"
                    + "Nyní potřebujeme rozhodnout, zda se podprostory protínají. K tomu využijeme vektor, který vytvoříme z bodů obou podprostorů. "
                    + "Vzniklý vektor $" + pointsVector.toTeX() + "$ přidáme jako poslední řádek do matice $S$. Pokud se tím hodnost matice nezmění, znamená to, že podprostory se protínají. "
                    + "Pokud se hodnost matice zvýší, pak se podprostory neprotínají. Zjistíme tedy hodnost upravené matice $S$.\n");
                echelon.rank('S' , true);
                textOut(" Hodnost upravené matice je " + echelon.rank(false) + ", přičemž původní hodnost byla " + echelonRank + ".");
                texOut(" Hodnost upravené matice je " + echelon.rank(false) + ", přičemž původní hodnost byla " + echelonRank + ".");
            }

            if(echelon.rank(false)==echelonRank){
                if (verbose) {
                    textOut(" Hodnost matice se nezměnila, takže podprostory se protínají. ");
                    texOut(" Hodnost matice se nezměnila, takže podprostory se protínají. ");
                }
                intersects = true;
            }else{
                if (verbose) {
                    textOut(" Hodnost matice se zvýšila, takže podprostory se neprotínají. ");
                    texOut(" Hodnost matice se zvýšila, takže podprostory se neprotínají. ");
                }
                intersects=false;
            }
            if(intersects&&parallel){
                if (verbose) {
                    textOut("Podprostory jsou rovnoběžné a protínají se, navíc podle hodností matic M a N je dim(A) = " + dimA + ", dim(B) = " + dimB + ". ");
                    texOut("Podprostory jsou rovnoběžné a protínají se, navíc podle hodností matic $M$ a $N$ je $\\dim\\mathcal{A} = " + dimA + ", \\dim\\mathcal{B} = " + dimB + "$. ");
                }
                if(dimA==dimB){
                    if (verbose) {
                        textOut("Podprostory jsou tedy rovnoběžné totožné.");
                        texOut("Podprostory jsou tedy rovnoběžné totožné.");
                    }
                    return "rovnoběžné totožné";
                }else{
                    if (verbose) {
                        textOut("Platí tedy " + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel) + ".");
                        texOut("Platí tedy $\\mathcal{" + (dimB < dimA ? secondLabel : firstLabel) + "} \\subset \\mathcal{" + (dimB < dimA ? firstLabel : secondLabel) + "}$.");
                    }
                    return "rovnoběžné, " + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel);
                }
            }
            if(!intersects&&parallel){
                if (verbose) {
                    textOut("Podprostory jsou rovnoběžné a neprotínají se, jsou tedy rovnoběžné různé.");
                    texOut("Podprostory jsou rovnoběžné a neprotínají se, jsou tedy rovnoběžné různé.");
                }
                return "rovnoběžné různé";
            }
            if(intersects&&!parallel){
                if (verbose) {
                    textOut("Podprostory nejsou rovnoběžné a protínají se, jsou tedy různoběžné.");
                    texOut("Podprostory nejsou rovnoběžné a protínají se, jsou tedy různoběžné.");
                }
                return "různoběžné";
            }
            if(!intersects&&!parallel){
                if (verbose) {
                    textOut("Podprostory nejsou rovnoběžné a neprotínají se, jsou tedy mimoběžné.");
                    texOut("Podprostory nejsou rovnoběžné a neprotínají se, jsou tedy mimoběžné.");
                }
                return "mimoběžné";
            }
        }

        if (this.generalFormAvailable && W.generalFormAvailable) {
            if (verbose) {
                textOut("Při určování vzájemné polohy budeme postupovat tak, že nejprve ověříme zda se podprostory protínají a poté rozhodneme, zda jsou podporostory rovnoběžné.\r\n"
                    + "\r\nOvěření disjunktnosti podprostorů\r\n---------------------------------\r\n");
                texOut("Při určování vzájemné polohy budeme postupovat tak, že nejprve ověříme zda se podprostory protínají a poté rozhodneme, zda jsou podporostory rovnoběžné.\r\n"
                    + "\n\\subsection*{Ověření disjunktnosti podprostorů}\n");
            }

            int dimA = this.getDim();
            int dimB = W.getDim();

            Matrix echelon = new Matrix();
            Matrix U = new Matrix();
            U.copy(this.augMatrix);
            U.mergeFromBottom(W.augMatrix);
            echelon = U.getEchelonForm(false);
            int unionRank=echelon.getSystemCoeffMatrix().rank(false);
            int maxRank = (dimB < dimA ? DIM-dimB : DIM-dimA);

            if(this.intersects(W, firstLabel, secondLabel, verbose)){
                int paramNum = echelon.getC()-echelon.getR()-1; 
                int minDim = (dimB < dimA ? dimB : dimA);

                if (verbose) {
                    textOut("\r\n\r\nověření rovnoběžnosti podprostorů\r\n---------------------------------\r\n"
                        + "Podprostory jsou rovnoběžné, jestliže platí\r\n\tdim (U∩V) = min(dim(U), dim(V)),\r\nkde U je zaměření podprostoru A a V zaměření podprostoru B. "
                        + "Jinými slovy, musí být\r\n\th(S) = max(h(M), h(N)),\r\nkde M je matice soustavy (bez absolutních členů), která zadává podprostor A, N je matice soustavy, která "
                        + "zadává podprostor B, a S je matice soustavy, která zadává podprostor A∩B. Umístění podprostoru je ovšem v případě "
                        + "určování rovnoběžnosti irelevantní, a tak nás budou zajímat pouze matice soustav bez absolutních členů. Matici S jsme vypočítali výše, jedná se o matici \r\n" 
                        + echelon.getSystemCoeffMatrix().getEchelonForm(false).toText() + "a její hodnost je " + unionRank + ". Nyní zjistíme, jaké hodnosti mají matice soustav rovnic, "
                        + "jimiž jsou podprostory zadány. \r\n\r\nzjištění hodnosti matice M\r\n--------------------------\r\n");
                    texOut("\n\\subsection*{ověření rovnoběžnosti podprostorů}\n"
                        + "\n\nPodprostory jsou rovnoběžné, jestliže platí\n\\[\\dim(U\\cap V) = \\min(\\dim(U), \\dim(V)),\\]\nkde $U$ je zaměření podprostoru $\\mathcal{A}$ a "
                        + "$V$ zaměření podprostoru $\\mathcal{B}$. Jinými slovy, musí být\n\\[h(S) = \\max(h(M), h(N)),\n\\]\nkde $M$ je matice soustavy (bez absolutních členů), "
                        + "která zadává podprostor $\\mathcal{A}$, $N$ je matice soustavy, která zadává podprostor $\\mathcal{B}$, a $S$ je matice soustavy, která zadává "
                        + "podprostor $\\mathcal{A}\\cap\\mathcal{B}$. Umístění podprostoru je ovšem v případě určování rovnoběžnosti "
                        + "irelevantní, a tak nás budou zajímat pouze matice soustav bez absolutních členů. Matici $S$ jsme vypočítali výše, jedná se o matici \n\\[" 
                        + echelon.getSystemCoeffMatrix().getEchelonForm(false).toTeX() + "\\]\na její hodnost je " + unionRank + ". Nyní zjistíme, jaké hodnosti mají matice soustav rovnic, jimiž jsou "
                        + "podprostory zadány.\n\\subsection*{zjištění hodnosti matice $M$}\n");
                }

                int firstRank = this.augMatrix.getSystemCoeffMatrix().rank('M',verbose);
                if (verbose) {
                    textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + ". ");
                    texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + ". ");
                }
                if (verbose) {
                    textOut("\r\n\r\nzjištění hodnosti matice N\r\n--------------------------\r\n");
                    texOut("\n\\subsection*{zjištění hodnosti matice $N$}\n");
                }
                int secondRank = W.augMatrix.getSystemCoeffMatrix().rank('N',verbose);
                if (verbose) {
                    textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + ". ");
                    texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + ". ");
                }     
                if (verbose) {
                    textOut("\r\n\r\nrozhodnutí o rovnoběžnosti\r\n--------------------------\r\n" + "Matice příslušné k jednotlivým podprostorům tedy mají hodnosti " + firstRank + " a "
                        + secondRank + " a je tedy max(" + firstRank + ", " + secondRank + ") = " + Math.max(firstRank, secondRank) + ". Matice příslušná k součtu podprostorů má "
                        + "hodnost " + unionRank + ", přičemž " + unionRank);
                    texOut("\n\n\\subsection*{rozhodnutí o rovnoběžnosti}\n" + "Matice příslušné k jednotlivým podprostorům tedy mají hodnosti " + firstRank + " a "
                        + secondRank + " a je tedy $\\max(" + firstRank + ", " + secondRank + ") = " + Math.max(firstRank, secondRank) + "$. Matice příslušná k součtu podprostorů má "
                        + "hodnost " + unionRank + ", přičemž $" + unionRank);
                }

                if(maxRank==unionRank){
                    if (verbose) {
                        textOut(" = " + maxRank + ". Podprostory tedy jsou rovnoběžné. Navíc pomocí hodností matic M a N snadno dopočítáme, že dim(A) = " + dimA +", dim(B) = " + dimB + ". Z toho vyplývá, že ");
                        texOut(" = " + maxRank + "$. Podprostory tedy jsou rovnoběžné. Navíc pomocí hodností matic $M$ a $N$ snadno dopočítáme, že $\\dim\\mathcal{A} = " + dimA +", \\dim\\mathcal{B) = " + dimB + "$. Z toho vyplývá, že ");
                    }
                    if(dimB==dimA){
                        if (verbose) {
                            textOut("podprostory jsou rovnoběžné totožné.");
                            texOut("podprostory jsou rovnoběžné totožné.");
                        }
                        return "rovnoběžné totožné";
                    }else{
                        if (verbose) {
                            textOut((dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel) + ".");
                            texOut("$\\mathcal{" + (dimB < dimA ? secondLabel : firstLabel) + "} \\subset \\mathcal{" + (dimB < dimA ? firstLabel : secondLabel) + "}$.");
                        }
                        return "rovnoběžné, " + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel);
                    }
                }else{
                    if (verbose) {
                        textOut(" ≠ " + maxRank + ". Podprostory se neprotínají a nejsou rovnoběžné, jsou tedy různoběžné.");
                        texOut(" \\neq " + maxRank + "$. Podprostory se neprotínají a nejsou rovnoběžné, jsou tedy různoběžné.");
                    }
                    return "různoběžné";
                }
            }else{

                if (verbose) {
                    textOut("\r\n\r\nověření rovnoběžnosti podprostorů\r\n---------------------------------\r\n"
                        + "Podprostory jsou rovnoběžné, jestliže platí\r\n\tdim (U∩V) = max(dim(U), dim(V)),\r\nkde U je zaměření podprostoru A a V zaměření podprostoru B. "
                        + "Jinými slovy, musí být\r\n\th(S) = max(h(M), h(N)),\r\nkde M je matice soustavy (bez absolutních členů), která zadává podprostor A, N je matice soustavy, která "
                        + "zadává podprostor B, a S je matice soustavy, která zadává podprostor A∩B (ta vznikne „slepením“ obou matic). Umístění podprostoru je ovšem v případě "
                        + "určování rovnoběžnosti irelevantní, a tak nás budou zajímat pouze matice soustav bez absolutních členů. Matici S jsme vypočítali výše, jedná se o matici \r\n" 
                        + echelon.getSystemCoeffMatrix().getEchelonForm(false).toText() + "a její hodnost je " + unionRank + ". Nyní zjistíme, jaké hodnosti mají matice soustav rovnic, "
                        + "jimiž jsou podprostory zadány (jelikož se podpostory neprotínají, tak bude o rovnoběžnosti rozhodnuto okamžitě, pokud hodnost matice alespoň jednoho podprostoru "
                        + "bude rovna jedné - mimoběžné podprostory nemohou být nadroviny) \r\n\r\nzjištění hodnosti matice M\r\n--------------------------\r\n");
                    texOut("\n\\subsection*{ověření rovnoběžnosti podprostorů}\n"
                        + "\n\nPodprostory jsou rovnoběžné, jestliže platí\n\\[\\dim(U\\cap V) = \\max(\\dim(U), \\dim(V)),\\]\nkde $U$ je zaměření podprostoru $\\mathcal{A}$ a "
                        + "$V$ zaměření podprostoru $\\mathcal{B}$. Jinými slovy, musí být\n\\[h(S) = \\max(h(M), h(N)),\n\\]\nkde $M$ je matice soustavy (bez absolutních členů), "
                        + "která zadává podprostor $\\mathcal{A}$, $N$ je matice soustavy, která zadává podprostor $\\mathcal{B}$, a $S$ je matice soustavy, která zadává "
                        + "podprostor $\\mathcal{A}\\cap\\mathcal{B}$. Umístění podprostoru je ovšem v případě určování rovnoběžnosti "
                        + "irelevantní, a tak nás budou zajímat pouze matice soustav bez absolutních členů. Matici $S$ jsme vypočítali výše, jedná se o matici \n\\[" 
                        + echelon.getSystemCoeffMatrix().getEchelonForm(false).toTeX() + "\\]\na její hodnost je " + unionRank + ". Nyní zjistíme, jaké hodnosti mají matice soustav rovnic, jimiž jsou "
                        + "podprostory zadány.\n\\subsection*{zjištění hodnosti matice $M$}\n");
                }
                int firstRank = this.augMatrix.getSystemCoeffMatrix().rank('M',verbose);
                if (verbose) {
                    textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + ". ");
                    texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + firstRank + ". ");
                }
                if(firstRank<2){
                    if (verbose) {
                        textOut("Jedná se tedy o nadrovinu, takže můžeme již nyní rozhodnout, že podprostory jsou rovnoběžné různé.");
                        texOut("Jedná se tedy o nadrovinu, takže můžeme již nyní rozhodnout, že podprostory jsou rovnoběžné různé.");
                        return "rovnoběžné různé";
                    }
                }else{
                    if (verbose) {
                        textOut("\r\n\r\nzjištění hodnosti matice N\r\n--------------------------\r\n");
                        texOut("\n\\subsection*{zjištění hodnosti matice $N$}\n");
                    }
                    int secondRank = W.augMatrix.getSystemCoeffMatrix().rank('N',verbose);
                    if (verbose) {
                        textOut("\r\n\r\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + ". ");
                        texOut("\n\nPočet nenulových řádků, a tedy i hodnost matice je " + secondRank + ". ");
                    }
                    if(secondRank<2){
                        if (verbose) {
                            textOut("Jedná se tedy o nadrovinu, takže můžeme již nyní rozhodnout, že podprostory jsou rovnoběžné různé.");
                            texOut("Jedná se tedy o nadrovinu, takže můžeme již nyní rozhodnout, že podprostory jsou rovnoběžné různé.");
                            return "rovnoběžné různé";
                        }
                    }else{
                        if (verbose) {
                            textOut("\r\n\r\nrozhodnutí o rovnoběžnosti\r\n--------------------------\r\n" + "Matice příslušné k jednotlivým podprostorům tedy mají hodnosti " + firstRank + " a "
                                + secondRank + " a je tedy max(" + firstRank + ", " + secondRank + ") = " + Math.max(firstRank, secondRank) + ". Matice příslušná k součtu podprostorů má "
                                + "hodnost " + unionRank + ", přičemž " + unionRank);
                            texOut("\n\n\\subsection*{rozhodnutí o rovnoběžnosti}\n" + "Matice příslušné k jednotlivým podprostorům tedy mají hodnosti " + firstRank + " a "
                                + secondRank + " a je tedy $\\max(" + firstRank + ", " + secondRank + ") = " + Math.max(firstRank, secondRank) + "$. Matice příslušná k součtu podprostorů má "
                                + "hodnost " + unionRank + ", přičemž $" + unionRank);
                        }
                    }
                }

                if(maxRank==unionRank){
                    if (verbose) {
                        textOut(" = " + maxRank + ". Podprostory tedy jsou rovnoběžné různé.");
                        texOut(" = " + maxRank + "$. Podprostory tedy jsou rovnoběžné různé.");
                    }
                    return "rovnoběžné různé";
                }else{
                    if (verbose) {
                        textOut(" ≠ " + maxRank + ". Podprostory tedy jsou mimoběžné.");
                        texOut(" \\neq " + maxRank + "$. Podprostory tedy jsou mimoběžné.");
                    }
                    return "mimoběžné";

                }
            }
        }

        if (this.parametricFormAvailable && W.generalFormAvailable) {
            return W.relativePosition(this , secondLabel, firstLabel, verbose);
        }

        if (this.generalFormAvailable && W.parametricFormAvailable) {
            if (verbose) {
                textOut("Při určování vzájemné polohy budeme postupovat tak, že nejprve zjistíme dimenze obou podprostorů, ověříme zda se podprostory protínají a poté rozhodneme, zda jsou podporostory rovnoběžné.\r\n"
                    + "\r\nVýpočet dimenze " + firstLabel + "\r\n-----------------\r\n");
                texOut("Při určování vzájemné polohy budeme postupovat tak, že nejprve zjistíme dimenze obou podprostorů, ověříme zda se podprostory protínají a poté rozhodneme, zda jsou podporostory rovnoběžné."
                    + "\n\\subsection*{Výpočet dimenze $\\mathcal{" + firstLabel + "}$}\n");
            }
            this.getDim(verbose, firstLabel);
            if (verbose) {
                textOut("\r\n\r\nVýpočet dimenze " + secondLabel + "\r\n-----------------\r\n");
                texOut("\n\\subsection*{Výpočet dimenze $\\mathcal{" + secondLabel + "}$}\n");
            }
            W.getDim(verbose, secondLabel);
            if (verbose) {
                textOut("\r\n\r\nOvěření disjunktnosti podprostorů\r\n---------------------------------\r\n");
                texOut("\n\\subsection*{Ověření disjunktnosti podprostorů}\n");
            }

            Matrix echelon = new Matrix();
            Vector[] normVectors = this.augMatrix.getSystemCoeffMatrix().rowsToVectors();
            Vector[] dirVectors = W.vecMatrix.rowsToVectors();
            Vector point = new Vector(W.refPoint);
            Matrix U = new Matrix(normVectors.length,dirVectors.length + 1);
            for (int i=1; i<=U.getR(); i++) {
                for (int j=1; j<U.getC(); j++) {
                    U.set(normVectors[i-1].scalarProduct(dirVectors[j-1]), i, j);
                }

                U.set(normVectors[i-1].scalarProduct(point).add(this.augMatrix.get(i, this.augMatrix.getC())), i, U.getC());
            }
            echelon = U.getEchelonForm(false);    

            int dimA = this.getDim();
            int dimB = W.getDim();

            int paramNum = echelon.getC()-echelon.getR()-1; 
            int minDim = (dimB < dimA ? dimB : dimA);

            if(this.intersects(W, firstLabel, secondLabel, verbose)){ 
                if(U.rank(false)==0){
                    if(dimB==dimA){
                        if (verbose) {
                            textOut("Navíc víme, že dim(A) = dim(B). Podprostory jsou tedy rovnoběžné totožné.");
                            texOut("Navíc víme, že $\\dim\\mathcal{A} = \\dim\\mathcal{B}$. Podprostory jsou tedy rovnoběžné totožné.");
                        }
                        return "rovnoběžné totožné";
                    }else{
                        if (verbose) {
                            textOut("Navíc víme, že dim(" + (dimB < dimA ? secondLabel : firstLabel) + ") < dim(" + (dimB < dimA ? firstLabel : secondLabel) + "). Podprostory jsou tedy rovnoběžné přičemž " 
                                + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel) + ".");
                            texOut("Navíc víme, že $\\dim\\mathcal{" + (dimB < dimA ? secondLabel : firstLabel) + "} < \\dim\\mathcal{" + (dimB < dimA ? firstLabel : secondLabel) + "}$. Podprostory jsou tedy rovnoběžné přičemž $\\mathcal{" 
                                + (dimB < dimA ? secondLabel : firstLabel) + "} \\subset \\mathcal{" + (dimB < dimA ? firstLabel : secondLabel) + "}$.");
                        }
                        return "rovnoběžné, " + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel);
                    }
                }

                String equations, unknowns, parText;
                if (echelon.getR() == 1) equations = "jedinou rovnici";
                else                          equations = "soustavu " + String.valueOf(echelon.getR()) + " rovnic";
                if (echelon.getC()-1 == 1)          unknowns = "jedné neznámé";
                else                          unknowns = String.valueOf(echelon.getC()-1) + " neznámých";
                if (paramNum == 0)              parText = "nebude obsahovat parametr.";
                else {
                    parText = "bude obsahovat " + String.valueOf(paramNum) + " parametr";
                    switch (paramNum) {
                        case 1:  break;
                        case 2:
                        case 3:
                        case 4:  parText += "y";
                        break;
                        default: parText += "ů";
                        break;
                    }
                    parText += ".";
                }
                if (verbose) { 
                    textOut("\r\nOvěření rovnoběžnosti podprostorů\r\n---------------------------------\r\n Vyřešením soustavy dané maticí\r\n" + echelon.toText() + "a dosazením do zadání podprostoru "
                        + secondLabel + ", bychom nalezli průnik zadaných podprostorů, nás ale zajímá pouze jeho dimenze. Jelikož matice reprezentuje " + equations +
                        " o " + unknowns + ", řešení " + parText + "Dimenze průniku zadaných podprostorů je tedy " 
                        + paramNum + ". My ale víme, že dim(" + firstLabel + ") = " + dimA + " a dim(" + secondLabel + ") = " + dimB);
                    texOut("\n\\subsection*{Ověření rovnoběžnosti podprostorů}\n Vyřešením soustavy dané maticí\n\\[" + echelon.toTeX() + "\\]\na dosazením do zadání podprostoru $\\mathcal{"
                        + secondLabel + "}$, bychom nalezli průnik zadaných podprostorů, nás ale zajímá pouze jeho dimenze. Jelikož matice reprezentuje " + equations +
                        " o " + unknowns + ", řešení " + parText + "Dimenze průniku zadaných podprostorů je tedy " 
                        + paramNum + ". My ale víme, že $\\dim\\mathcal{" + firstLabel + "} = " + dimA + "$ a $\\dim\\mathcal{" + secondLabel + "} = " + dimB + "$");
                }
                if(paramNum<minDim){
                    if (verbose) {
                        textOut(". Průnikem tedy jistě není ani jeden z podprostorů a zadané podprostory musí být různoběžné.");
                        texOut(". Průnikem tedy jistě není ani jeden z podprostorů a zadané podprostory musí být různoběžné.");
                    }
                    return "různoběžné";
                }else{
                    if(dimB==dimA){
                        if (verbose) {
                            textOut(". Z toho vyplývá, že podprostory jsou rovnoběžné totožné.");
                            texOut(". Z toho vyplývá, že podprostory jsou rovnoběžné totožné.");
                        }
                        return "rovnoběžné totožné";
                    }else{
                        if (verbose) {
                            textOut(". Průnikem je tedy celý podprostor " +  (dimB < dimA ? secondLabel : firstLabel) + ". Podprostory tedy jsou rovnoběžné přičemž " 
                                + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel) + ".");
                            texOut(". Průnikem je tedy celý podprostor $\\mathcal{" +  (dimB < dimA ? secondLabel : firstLabel) + "}$. Podprostory tedy jsou rovnoběžné přičemž $\\mathcal{" 
                                + (dimB < dimA ? secondLabel : firstLabel) + "} \\subset \\mathcal{" + (dimB < dimA ? firstLabel : secondLabel) + "}$.");
                        }
                        return "rovnoběžné, " + (dimB < dimA ? secondLabel : firstLabel) + " ⊂ " + (dimB < dimA ? firstLabel : secondLabel);
                    }
                }
            }else{
                if (verbose) {
                    textOut("\r\n\r\nOvěření rovnoběžnosti podprostorů\r\n---------------------------------\r\n");
                    texOut("\n\\subsection*{Ověření rovnoběžnosti podprostorů}\n");
                }
                if(W.getDim()==DIM-1 || this.getDim()==DIM-1){
                    if (verbose) {
                        textOut("Víme, že podprostory se neprotínají, jsou tedy buď rovnoběžné, nebo mimoběžné. "
                            + "V prostoru dimenze " + DIM + " ovšem mohou být mimoběžné maximálně " + (DIM-2) + "- rozměrné podprostory. My ale víme, že " 
                            + (dimA == dimB ? "dimenze obou podprostorů je " + dimA : (dimA>dimB ? "dim(" + firstLabel +") = " + dimA : "dim(" + secondLabel +") = " + dimB)) 
                            + ". Podporostory tedy musí být rovnoběžné různé.");
                        texOut("Víme, že podprostory se neprotínají, jsou tedy buď rovnoběžné, nebo mimoběžné. "
                            + "V prostoru dimenze " + DIM + " ovšem mohou být mimoběžné maximálně " + (DIM-2) + "-rozměrné podprostory. My ale víme, že " 
                            + (dimA == dimB ? "dimenze obou podprostorů je " + dimA : (dimA>dimB ? "$\\dim\\mathcal{" + firstLabel +"} = " + dimA + "$" : "$\\dim\\mathcal{" + secondLabel +"} = " + dimB + "$")) 
                            + ". Podporostory tedy musí být rovnoběžné různé.");
                    }
                    return "rovnoběžné různé";
                }else{
                    if (verbose) {
                        textOut("K rozhodnutí o rovnoběžnosti využijeme tvar výše vypočítané matice M bez absolutních členů. ");
                        texOut("K rozhodnutí o rovnoběžnosti využijeme tvar výše vypočítané matice M bez absolutních členů. ");
                    }
                    if(this.isParallel(W, false)){
                        if(dimA<dimB){

                            if (verbose) {
                                textOut("Tedy Matice \r\n" + echelon.getSystemCoeffMatrix().getEchelonForm(false).toText() 
                                    + "Rozdíl počtu sloupců a počtu řádků této matice určuje dimenzi průniku zaměření zadaných podprostorů, ta je tedy rovna " 
                                    + (echelon.getSystemCoeffMatrix().getEchelonForm(false).getC() - echelon.getSystemCoeffMatrix().getEchelonForm(false).getR())
                                    + ". Zároveň víme, že dim(" + firstLabel +") = " + dimA + " a dim(" + secondLabel +") = " + dimB + ". Podprostory se neprotínají a dimenze průniku zaměření"
                                    + " je rovna dimenzi podprostoru " + firstLabel + " . Podprostory jsou tedy rovnoběžné různé.");
                                texOut("Tedy Matice \n\\[" + echelon.getSystemCoeffMatrix().getEchelonForm(false).toText() 
                                    + "\\]\nRozdíl počtu sloupců a počtu řádků této matice určuje dimenzi průniku zaměření zadaných podprostorů, ta je tedy rovna " 
                                    + (echelon.getSystemCoeffMatrix().getEchelonForm(false).getC() - echelon.getSystemCoeffMatrix().getEchelonForm(false).getR())
                                    + ". Zároveň víme, že $\\dim\\mathcal{" + firstLabel +"} = " + dimA + "$ a $\\dim\\mathcal{" + secondLabel +"} = " + dimB + "$. Podprostory se neprotínají a dimenze průniku zaměření"
                                    + " je rovna dimenzi podprostoru $\\mathcal{" + firstLabel + "}$ . Podprostory jsou tedy rovnoběžné různé.");
                            }
                        }else{
                            if (verbose) {
                                textOut("Matice je nulová, z čehož můžeme usoudit, že zaměření podprostoru " + secondLabel + " je celé osaženo v zaměření podprostoru "
                                    + firstLabel + ". Podprostory tedy jsou rovnoběžné různé.");
                                texOut("Matice je nulová, z čehož můžeme usoudit, že zaměření podprostoru $\\mathcal{" + secondLabel + "}$ je celé osaženo v zaměření podprostoru $\\mathcal{"
                                    + firstLabel + "}$. Podprostory tedy jsou rovnoběžné různé.");
                            }
                        }
                        return "rovnoběžné různé";
                    }else{
                        if (verbose) {
                            textOut("Tedy Matice \r\n" + echelon.getSystemCoeffMatrix().getEchelonForm(false).toText() 
                                + "Rozdíl počtu sloupců a počtu řádků této matice určuje dimenzi průniku zaměření zadaných podprostorů, ta je tedy rovna " 
                                + (echelon.getSystemCoeffMatrix().getEchelonForm(false).getC() - echelon.getSystemCoeffMatrix().getEchelonForm(false).getR())
                                + ". Zároveň víme, že dim(" + firstLabel +") = " + dimA + " a dim(" + secondLabel +") = " + dimB + ". Podprostory se neprotínají a dimenze průniku zaměření"
                                + " je menší než dimenze samotných podprostorů. Podprostory jsou tedy mimoběžné.");
                            texOut("Tedy Matice \n\\[" + echelon.getSystemCoeffMatrix().getEchelonForm(false).toTeX() 
                                + "\\]\nRozdíl počtu sloupců a počtu řádků této matice určuje dimenzi průniku zaměření zadaných podprostorů, ta je tedy rovna " 
                                + (echelon.getSystemCoeffMatrix().getEchelonForm(false).getC() - echelon.getSystemCoeffMatrix().getEchelonForm(false).getR())
                                + ". Zároveň víme, že $\\dim\\mathcal{" + firstLabel +"} = " + dimA + "$ a $\\dim\\mathcal{" + secondLabel +"} = " + dimB + "$. Podprostory se neprotínají a dimenze průniku zaměření"
                                + " je menší než dimenze samotných podprostorů. Podprostory jsou tedy mimoběžné.");
                        }
                        return "jsou mimoběžné";
                    }
                }
            }
        }
        return "chyba";
    }

    /**
     * Calculates the distance of any two subspaces using the most efficient method. This method doesn't “talk” (creates no text nor TeX output).
     * 
     * @param W the other subspace
     * 
     * @return the distance of the given subspaces
     * 
     * @throws IllegalArgumentException Throws an exception if the two subspaces do not come from the same space, ie. they have a different <code>DIM</code>
     */
    SquareRootFraction distance(Subspace W)
    {
        return distance(W,'A','B',false);
    }

    /**
     * Calculates the distance of any two subspaces using the most efficient method.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return the distance of the given subspaces
     * 
     * @throws IllegalArgumentException Throws an exception if the two subspaces do not come from the same space, ie. they have a different <code>DIM</code>
     */
    SquareRootFraction distance(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (this.DIM != W.DIM) throw new IllegalArgumentException("The subspaces are not subspaces of the same space!");

        if (verbose) {
            textOut("Výpočet vzdálenosti podprostorů přímou metodou je poměrně pracný, a proto je dobré nejprve zjistit, zda není splněna některá z podmínek, která umožňuje "
                + "použití některého vzorce či pravidla pro snazší určení vzdálenosti. Zjistěme nejprve, o jaké podprostory se jedná.");
            texOut("Výpočet vzdálenosti podprostorů přímou metodou je poměrně pracný, a proto je dobré nejprve zjistit, zda není splněna některá z podmínek, která umožňuje "
                + "použití některého vzorce či pravidla pro snazší určení vzdálenosti. Zjistěme nejprve, o jaké podprostory se jedná.");

            textOut("\r\n\r\npopis " + firstLabel + "\r\n" + "-------\r\n");
            texOut("\n\\subsection*{Popis $\\mathcal{" + firstLabel + "}$}");
            this.describe(firstLabel);

            textOut("\r\n\r\npopis " + secondLabel + "\r\n" + "-------\r\n");
            texOut("\n\\subsection*{Popis $\\mathcal{" + secondLabel + "}$}");
            W.describe(secondLabel);
        }

        // a whole space and anything
        if (this.isWholeSpace() || W.isWholeSpace()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n" + "Celý prostor má od jakéhokoliv svého podprostoru vzdálenost rovnu nule.");
                texOut("\n\\section*{Určení vzdálenosti}" + "Celý prostor má od jakéhokoliv svého podprostoru vzdálenost rovnu nule.");
            }
            return new SquareRootFraction(0);
        }        

        // two points
        if (this.isPoint() && W.isPoint()) {
            if (verbose) {    
                textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n");
                texOut("\n\\section*{Určení vzdálenosti}");
            }
            return this.distancePointPoint(W, firstLabel, secondLabel, verbose);
        }

        // a point and a hyperplane
        if (this.isPoint() && W.isHyperplane()) {
            if (verbose) {    
                textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n");
                texOut("\n\\section*{Určení vzdálenosti}");
            }
            return this.distancePointHyperplane(W, firstLabel, secondLabel, verbose);
        }
        if (W.isPoint() && this.isHyperplane()) {
            if (verbose) {    
                textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n");
                texOut("\n\\section*{Určení vzdálenosti}");
            }
            return W.distancePointHyperplane(this, secondLabel, firstLabel, verbose);
        }

        // a point and any subspace
        if (this.isPoint()) {
            if (verbose) {    
                textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n");
                texOut("\n\\section*{Určení vzdálenosti}");
            }
            return this.distancePointAny(W, firstLabel, secondLabel, verbose);
        }
        if (W.isPoint()) {
            if (verbose) {    
                textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n");
                texOut("\n\\section*{Určení vzdálenosti}");
            }
            return W.distancePointAny(this, secondLabel, firstLabel, verbose);
        }

        if (verbose) {
            textOut("\r\n\r\nOd obtížných výpočtů nás může ještě zachránit vzájemná poloha podprostorů, jmenovitě jsou-li rovnoběžné nebo pokud se protínají.");
            texOut("\n\nOd obtížných výpočtů nás může ještě zachránit vzájemná poloha podprostorů, jmenovitě jsou\\discretionary{-}{-}{-}li rovnoběžné nebo pokud se protínají.");

            textOut("\r\n\r\nověření průniku\r\n" + "---------------\r\n");
            texOut("\n\\subsection*{ověření průniku}");
        }

        // two intersecting subspaces
        if (this.intersects(W, firstLabel, secondLabel, verbose)) {
            textOut(" Protínající se podprostory mají vzdálenost rovnu nule.");
            texOut(" Protínající se podprostory mají vzdálenost rovnu nule.");
            return new SquareRootFraction(0);
        }

        if (verbose) {
            textOut("\r\n\r\nověření rovnoběžnosti\r\n" + "---------------------\r\n");
            texOut("\n\\subsection*{ověření rovnoběžnosti}");
        }

        // two parallel subspaces
        if (this.isParallel(W, verbose)) {
            if (this.dim <= W.dim) return W.distanceFromParallelSubspace(this, firstLabel, secondLabel, verbose);
            else                   return this.distanceFromParallelSubspace(W, secondLabel, firstLabel, verbose);
        }

        if (verbose) {    
            textOut("\r\n\r\nURČENÍ VZDÁLENOSTI\r\n" + "==================\r\n");
            texOut("\n\\section*{Určení vzdálenosti}");
        }
        return this.distanceUniversalUsingGram(W, firstLabel, secondLabel, verbose);
    }

    /**
     * Calculates the distance of two points.
     * 
     * @param W the other point
     * @firstLabel label of this point
     * @secondLabel label of the point <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throws IllegalArgumentException Throws an exception if one of the spaces is not a point.
     * 
     * @return the distance of the points
     */
    SquareRootFraction distancePointPoint(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (!(this.isPoint() && W.isPoint())) throw new IllegalArgumentException("Both subspaces must be only points!");

        if (verbose && this.getOriginalForm()) {
            textOut("zjištění souřadnic bodu " + firstLabel + "\r\n-------------------------\r\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy.");
            texOut("\n\\subsection*{zjištění souřadnic bodu $" + firstLabel + "$}\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy.");
            this.augMatrix.solveOnly(firstLabel, true);
            textOut("\r\n\r\n");
        }
        this.checkParametricForm();

        if (verbose && W.getOriginalForm()) {
            textOut("\r\n\r\nzjištění souřadnic bodu " + secondLabel + "\r\n-------------------------\r\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy.");
            texOut("\n\\subsection*{zjištění souřadnic bodu $" + secondLabel + "$}\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy.");
            W.augMatrix.solveOnly(secondLabel, true);
            textOut("\r\n\r\n");
        }
        W.checkParametricForm();

        Vector V = new Vector(this.refPoint, W.refPoint);
        if (verbose) {
            textOut("\r\n\r\nvýpočet vzdálenosti\r\n-------------------\r\n" + "Vzdálenost dvou bodů určíme jako velikost vektoru, který určují. Platí tedy:\r\n\t"
                + "u = B - A = " + W.refPoint.toText() + " - " + this.refPoint.toText() + " = " + V.toText() + ".\r\n");
            texOut("\n\\subsection*{výpočet vzdálenosti}\n" + "Vzdálenost dvou bodů určíme jako velikost vektoru, který určují. Platí tedy:\n\\begin{center}$\\displaystyle"
                + "\\vec{u} = B - A = " + W.refPoint.toTeX() + " - " + this.refPoint.toTeX() + " = " + V.toTeX() + ".$\\end{center}\n");
        }

        return V.length('u', verbose);
    }

    /**
     * Calculates the distance of a point from a hyperplane.
     * 
     * @param W the hyperplane
     * @firstLabel label of this hyperplane
     * @secondLabel label of the hyperplane <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throws IllegalArgumentException Throws an exception if this subspace is not a point or W is not a hyperplane.
     * 
     * @return the distance of the point from the hyperplane
     */
    SquareRootFraction distancePointHyperplane(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (!(this.isPoint() && W.isHyperplane())) throw new IllegalArgumentException("This method only calculates distance of a point from a hyperplane!");

        if (verbose && this.getOriginalForm()) {
            textOut("zjištění souřadnic bodu " + firstLabel + "\r\n-------------------------\r\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
            texOut("\n\\subsection*{zjištění souřadnic bodu $" + firstLabel + "$}\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
            this.augMatrix.getEchelonForm(false).solve(firstLabel, true);
            this.checkParametricForm();
            textOut("a " + firstLabel + " = " + this.refPoint.toText() + ".\r\n\r\n");
            texOut("a $" + firstLabel + " = " + this.refPoint.toTeX() + "$.\n\n");
        }
        else this.checkParametricForm();

        if (verbose && !W.getOriginalForm()) {
            textOut("převod nadroviny " + secondLabel + " do obecného tvaru\r\n------------------------------------\r\n"
                + "Nadrovina musí být v obecném tvaru, a proto ji do něj převedeme. ");
            texOut("\n\\subsection*{převod nadroviny $\\mathcal{" + secondLabel + "}$ do obecného tvaru}\n"
                + "Nadrovina musí být v~obecném tvaru, a proto ji do něj převedeme. ");
            W.checkGeneralForm(secondLabel, true);
            textOut("\r\n\r\n");
            texOut("\n\n");
        }
        else W.checkGeneralForm();

        Fraction F = ZERO;

        String formula1Text = "";
        String formula1TeX = "";
        String formula2Text = "";
        String formula2TeX = "";
        boolean firstTerm = true;
        for (int i=1; i<=this.DIM; i++) {
            Fraction hyperplaneCoeff = W.augMatrix.get(1, i);
            Fraction pointCoeff = this.refPoint.get(i);
            F = F.add(hyperplaneCoeff.multiply(pointCoeff));
            if (verbose) {
                boolean negative = (hyperplaneCoeff.compare(ZERO) == -1);
                boolean negativePoint = (pointCoeff.compare(ZERO) == -1);
                formula1Text += (firstTerm ? "" : " + ") + (negative ? "(" + hyperplaneCoeff.toText() + ")" : hyperplaneCoeff.toText())
                + "*" + (negativePoint ? "(" + pointCoeff.toText() + ")" : pointCoeff.toText());
                formula1TeX += (firstTerm ? "" : " + ") + (negative ? "\\left(" + hyperplaneCoeff.toTeX() + "\\right)" : hyperplaneCoeff.toTeX())
                + "\\cdot" + (negativePoint ? "\\left(" + pointCoeff.toTeX() + "\\right)" : pointCoeff.toTeX());
                formula2Text += (firstTerm ? "" : " + ") + (negative ? "(" + hyperplaneCoeff.toText() + ")" : hyperplaneCoeff.toText()) + "^2";
                formula2TeX += (firstTerm ? "" : " + ") + (negative ? "(" + hyperplaneCoeff.toTeX() + ")" : hyperplaneCoeff.toTeX()) + "^2";
                firstTerm = false;
            }
        }
        F = F.add(W.augMatrix.get(1,this.DIM+1));
        if (verbose) {
            switch (W.augMatrix.get(1,this.DIM+1).compare(ZERO)) {
                case 1:  formula1Text += " + " + W.augMatrix.get(1,this.DIM+1).toText();
                formula1TeX += " + " + W.augMatrix.get(1,this.DIM+1).toTeX();
                break;
                case -1: formula1Text += W.augMatrix.get(1,this.DIM+1).toText();
                formula1TeX += W.augMatrix.get(1,this.DIM+1).toTeX();
                break;
                default: break;
            }
        }
        SquareRootFraction N = F.abs().convertToSRF();

        Vector[] coeffs = W.augMatrix.submatrix(1, 1, 1, this.DIM).rowsToVectors();
        SquareRootFraction D = coeffs[0].length();

        SquareRootFraction result = N.divide(D);

        if (verbose) {
            textOut("Vzdálenost bodu od nadroviny zjistíme podle vzorce:\r\n\t" + "d = |a_1p_1 + a_2p_2 + … + a_np_n + b|/sqrt(a_1^2 + a_2^2 + … + a_n^2),\r\nkde " 
                + "a_1x_1 + a_2x_2 + … + a_nx_n + b = 0 je rovnice nadroviny a bod má souřadnice [p_1, p_2, …, p_n].\r\n\r\n"
                + "V našem případě je konkrétně " + secondLabel + ": " + W.toTextGeneral(false) + " a " + firstLabel + " = " + this.refPoint.toText()
                + ", a tak \r\n\td = |" + formula1Text + "|/sqrt(" + formula2Text + ") = " + N.toText() + "/" + D.toText() + " = " + result.toText() + ".");
            texOut("Vzdálenost bodu od nadroviny zjistíme podle vzorce:\n\\[\n"
                + "d = \\frac{\\left|a_1p_1 + a_2p_2 + \\cdots + a_np_n + b\\right|}{\\sqrt{a_1^2 + a_2^2 + \\cdots + a_n^2}},\n\\]\n" 
                + "kde $a_1x_1 + a_2x_2 + \\cdots + a_nx_n + b = 0$ je rovnice nadroviny a bod má souřadnice [p_1, p_2, \\ldots, p_n]$.\n\n"
                + "V našem případě je konkrétně $\\mathcal{" + secondLabel + ": " + W.toTeXGeneral() + "}$ a $" + firstLabel + " = " + this.refPoint.toTeX() + "$, "
                + "a tak \n\\[\n" + "d = \\frac{\\left|" + formula1TeX + "\\right|}{\\sqrt{" + formula2TeX + "}} = \\frac{" + N.toTeX() + "}{" + D.toTeX() + "} = " 
                + result.toTeX() + ".\n\\]\n");
        }

        return result;
    }

    /**
     * Calculates the distance of a point from any subspace.
     * 
     * @param W any subspace
     * @firstLabel label of this point
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @throws IllegalArgumentException Throws an exception if this subspace is not a point.
     * 
     * @return the distance of the point from the given subspace
     */
    SquareRootFraction distancePointAny(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (!(this.isPoint())) throw new IllegalArgumentException("This method only calculates distance of a point from a subsapce!");

        if (verbose && this.getOriginalForm()) {
            textOut("zjištění souřadnic bodu " + firstLabel + "\r\n-------------------------\r\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
            texOut("\n\\subsection*{zjištění souřadnic bodu $" + firstLabel + "$}\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
            this.augMatrix.getEchelonForm(false).solve(firstLabel, true);
            this.checkParametricForm();
            textOut("a " + firstLabel + " = " + this.refPoint.toText() + ".\r\n\r\n");
            texOut("a $" + firstLabel + " = " + this.refPoint.toTeX() + "$.\r\n\r\n");
        }
        else this.checkParametricForm();

        if (verbose && !W.getOriginalForm()) {
            textOut("převod " + secondLabel + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Druhý podprostor musí být v parametrickém tvaru, a tak jej do něj převedeme. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ do parametrického tvaru}\n"
                + "Druhý podprostor musí být v~parametrickém tvaru, a tak jej do něj převedeme. ");
        }
        W.checkParametricForm(secondLabel, verbose);

        Vector V = new Vector(this.refPoint, W.refPoint);
        if (verbose) {
            Vector[] baseVectors = W.vecMatrix.rowsToVectors();
            String baseVectorsText = "";
            String baseVectorsTeX = "";
            for (int i = 0; i<baseVectors.length; i++) {
                baseVectorsText += (i == 0 ? "L(" : ", ") + baseVectors[i].toText();
                baseVectorsTeX += (i == 0 ? "L\\left(" : ", ") + baseVectors[i].toTeX();
            }
            baseVectorsText += ")";
            baseVectorsTeX += "\\right)";
            textOut("\r\n\r\nVzdálenost bodu od druhého podprostoru zjistíme tak, že najdeme ortogonální komponentu vektoru x = P - " + firstLabel
                + ", kde P je libovolný bod podprostoru " + secondLabel + ". V našem případě tedy x = " + W.refPoint.toText() + " - " + this.refPoint.toText()
                + " = " + V.toText() + ". Ortogonální komponentu hledáme vzhledem k zaměření podprostoru " + secondLabel + ", tj. " + baseVectorsText + ".");
            texOut("\n\nVzdálenost bodu od druhého podprostoru zjistíme tak, že najdeme ortogonální komponentu vektoru $\\vec{x} = P - " + firstLabel
                + "$, kde $P$ je libovolný bod podprostoru $\\mathcal{" + secondLabel + "}$. V našem případě tedy $\\vec{x}$ = " + W.refPoint.toTeX()
                + " - " + this.refPoint.toTeX() + " = " + V.toTeX() + "$. Ortogonální komponentu hledáme vzhledem k~zaměření podprostoru $\\mathcal{"
                + secondLabel + "}$, tj. $" + baseVectorsTeX + "$.");
        }
        Vector projection = V.orthogonalProjection(W.vecMatrix, true);
        Vector component = V.subtract(projection);
        if (verbose) {
            textOut("\r\n\r\nnalezení ortogonální komponenty" + "\r\n-------------------------------\r\n" + "Snadno již nyní nalezneme ortogonální komponentu:\r\n\t"
                + "z = x - y = " + V.toText() + " - " + projection.toText() + " = " + component.toText() + ".\r\n\r\n"
                + "Konečně vzdálenost spočítáme jako velikost této komponenty. ");
            texOut("\n\\subsection*{nalezení ortogonální komponenty}\n" + "Snadno již nyní nalezneme ortogonální komponentu:\n\\begin{center}$\\displaystyle"
                + "\\vec{z} = \\vec{x} - \\vec{y} = " + V.toTeX() + " - " + projection.toTeX() + " = " + component.toTeX() + ".$\\end{center}\\]\n\n"
                + "Konečně vzdálenost spočítáme jako velikost této komponenty. ");
        }

        return component.length('z', verbose);
    }

    /**
     * Calculates the distance of the subspace from a parallel subspace.
     * 
     * @param W the other subspace, parallel to this one
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return the distance of this subspace from the parallel subspace
     */
    SquareRootFraction distanceFromParallelSubspace(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (this.getDim(false) < W.getDim(false)) {
            if (verbose) {
                textOut("V případě rovnoběžných podprostorů je jejich vzdálenost rovna vzdálenosti libovolného bodu z podprostoru s menší nebo stejnou dimenzí od druhého "
                    + "podprostoru. Z předchozích výpočtů víme, že podprostor " + firstLabel + " má menší dimenzi; proto bod budeme vybírat z něj.");
                texOut("V~případě rovnoběžných podprostorů je jejich vzdálenost rovna vzdálenosti libovolného bodu z~podprostoru s~menší nebo stejnou dimenzí od druhého "
                    + "podprostoru. Z~předchozích výpočtů víme, že podprostor $\\mathcal{" + firstLabel + "}$ má menší dimenzi; proto bod budeme vybírat z~něj.");
                if (this.getOriginalForm()) {
                    textOut("zjištění souřadnic bodu " + firstLabel + "\r\n-------------------------\r\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
                    texOut("\n\\subsection*{zjištění souřadnic bodu $" + firstLabel + "$}\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
                    this.augMatrix.getEchelonForm(false).solve(firstLabel, true);
                    this.checkParametricForm();
                    textOut("a " + firstLabel + " = " + this.refPoint.toText() + ".\r\n\r\n");
                    texOut("a $" + firstLabel + " = " + this.refPoint.toTeX() + "$.\r\n\r\n");
                }
            }
            else this.checkParametricForm();
            Point[] points = new Point[1];
            points[0] = this.refPoint;
            Vector[] vectors = new Vector[0];
            Subspace point = new Subspace(points, vectors);
            if (W.isHyperplane()) return point.distancePointHyperplane(W, firstLabel, secondLabel, verbose);
            return point.distancePointAny(W, firstLabel, secondLabel, verbose);
        }

        if (this.getDim(false) > W.getDim(false)) return W.distanceFromParallelSubspace(this, secondLabel, firstLabel, verbose);

        if (verbose) {
            textOut("V případě rovnoběžných podprostorů je jejich vzdálenost rovna vzdálenosti libovolného bodu z podprostoru s menší nebo stejnou dimenzí od druhého "
                + "podprostoru. Z předchozích výpočtů víme, že podprostory mají dimenzi stejnou; proto bod budeme vybírat např. z podprostoru " + firstLabel + ".");
            texOut("V~případě rovnoběžných podprostorů je jejich vzdálenost rovna vzdálenosti libovolného bodu z~podprostoru s~menší nebo stejnou dimenzí od druhého "
                + "podprostoru. Z~předchozích výpočtů víme, že podprostory mají dimenzi stejnou; proto bod budeme vybírat např.~z~podprostoru  $\\mathcal{" + firstLabel
                + "}$.");
            if (this.getOriginalForm()) {
                textOut("zjištění souřadnic bodu " + firstLabel + "\r\n-------------------------\r\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
                texOut("\n\\subsection*{zjištění souřadnic bodu $" + firstLabel + "$}\n" + "Souřadnice bodu zjistíme jednoduše vyřešením soustavy. ");
                this.augMatrix.getEchelonForm(false).solve(firstLabel, true);
                this.checkParametricForm();
                textOut("a " + firstLabel + " = " + this.refPoint.toText() + ".\r\n\r\n");
                texOut("a $" + firstLabel + " = " + this.refPoint.toTeX() + "$.\r\n\r\n");
            }
        }
        else this.checkParametricForm();
        Point[] points = new Point[1];
        points[0] = this.refPoint;
        Vector[] vectors = new Vector[0];
        Subspace point = new Subspace(points, vectors);
        if (W.isHyperplane()) return point.distancePointHyperplane(W, firstLabel, secondLabel, verbose);
        return point.distancePointAny(W, firstLabel, secondLabel, verbose);
    }

    /**
     * Calculates the distance of any two subspaces using Gram determinant.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return the distance of the given subspaces
     */
    SquareRootFraction distanceUniversalUsingGram(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (verbose) {
            textOut("Nezbývá nám, než vzdálenost vypočítat obecným postupem, který využívá Gramova determinantu."
                + ((this.getOriginalForm() || W.getOriginalForm()) ? " K tomu potřebujeme znát zaměření obou podprostorů, tedy jejich parametrické vyjádření, které teď najdeme" : ""));
            texOut("Nezbývá nám, než vzdálenost vypočítat obecným postupem, který využívá Gramova determinantu."
                + ((this.getOriginalForm() || W.getOriginalForm()) ? " K tomu potřebujeme znát zaměření obou podprostorů, tedy jejich parametrické vyjádření, které teď najdeme." : ""));
            if (this.getOriginalForm()) {
                this.augMatrix.getEchelonForm(false).solveOnly(firstLabel, true);
                this.checkParametricForm();
                textOut("což je hledané parametrické vyjádření podprostoru " + firstLabel + ".\r\n\r\n");
                texOut("což je hledané parametrické vyjádření podprostoru $\\mathcal{" + firstLabel + "}$.\r\n\r\n");
            }
            if (W.getOriginalForm()) {
                W.augMatrix.getEchelonForm(false).solveOnly(secondLabel, true);
                W.checkParametricForm();
                textOut("což je hledané parametrické vyjádření podprostoru " + secondLabel + ".\r\n\r\n");
                texOut("což je hledané parametrické vyjádření podprostoru $\\mathcal{" + secondLabel + "}$.\r\n\r\n");
            }
        }
        else {
            this.checkParametricForm();
            W.checkParametricForm();
        }

        Matrix B = new Matrix();
        Matrix A = new Matrix();
        B.copy(this.vecMatrix);
        B.mergeFromBottom(W.vecMatrix);
        if (verbose) {
            textOut("výběr báze součtu podprostorů" + "\r\n-----------------------------\r\n"
                + "Nejprve vybereme ze směrových vektorů obou podprostorů bázi, tzn. převedeme na schodovitý tvar matici B, jejíž řádky tvoří jednotlivé směrové vektory.\r\n\r\n");
            texOut("\\subsection*{výběr báze součtu podprostorů}"
                + "Nejprve vybereme ze směrových vektorů obou podprostorů bázi, tzn.~převedeme na schodovitý tvar matici $B$, jejíž řádky tvoří jednotlivé směrové vektory.\r\n\r\n");
        }
        Matrix M = new Matrix();
        B.copy(B.getEchelonForm('B', Math.min(B.getR(), B.getC()), verbose));

        Vector V = new Vector(this.refPoint, W.refPoint);
        A.copy(B);
        A.mergeFromBottom(V.toMatrixRow());
        if (verbose) {
            textOut("\r\n\r\nNásledně vytvoříme matici A, která vznikne z matice B tak, že k ní přidáme vektor u = " + secondLabel + " - " + firstLabel + " = " + W.refPoint.toText()
                + " - " + this.refPoint.toText() + " = " + V.toText() + ". Matice A tedy je:\r\n" + A.toText() + "\r\nVzdálenost pak je:\r\n"
                + "d = sqrt(G(A)/G(B)),\r\n" + "kde G(M) je Gramův determinant matice M.\r\n\r\nvýpočet G(A)\r\n------------\r\n");
            texOut("\n\nNásledně vytvoříme matici $A$, která vznikne z~matice $B$ tak, že k~ní přidáme vektor $\\vec{u} = " + secondLabel + " - " + firstLabel + " = "
                + W.refPoint.toTeX() + " - " + this.refPoint.toTeX() + " = " + V.toTeX() + "$. Matice $A$ tedy je:\n\\[\n" + A.toTeX() + "\\]\nVzdálenost pak je:\n\\["
                + "d = \\sqrt{\\frac{G(A)}{G(B)}},\\]\n" + "kde $G(M)$ je Gramův determinant matice $M$.\n\n\\subsection*{výpočet $G(A)$}\n");
        }
        Fraction n = A.detGram('A', verbose);
        if (verbose) {
            textOut("\r\n\r\nvýpočet G(B)\r\n------------\r\n");
            texOut("\n\n\\subsection*{výpočet $G(B)$}\n");
        }
        Fraction d = B.detGram('B', verbose);
        SquareRootFraction N = new SquareRootFraction(n);
        SquareRootFraction D = new SquareRootFraction(d);
        if (verbose) {
            SquareRootFraction result = N.divide(D);
            textOut("\r\n\r\nvýpočet vzdálenosti\r\n-------------------\r\n" + "Konečně můžeme spočítat vzdálenost:\r\n\t" + "d = sqrt[(" + n.toText() + ") / (" + d.toText() + ")] = "
                + "sqrt(" + result.square().toText() +") = " + result.toText() + ".");
            texOut("\n\n\\subsection*{výpočet vzdálenosti}\n" + "Konečně můžeme spočítat vzdálenost:\n\\[" + "d = \\sqrt{\\frac{" + n.toTeX() + "}{" + d.toTeX() + "}} = "
                + "\\sqrt{" + result.square().toTeX() +"} = " + result.toTeX() + ".\\]");
            return result;
        }
        return N.divide(D);
    }

    /**
     * Finds intersection of difference spaces of two subspaces.
     * 
     * @param W the other subspace
     * 
     * @return the intersection of the difference spaces
     */
    Subspace intersectionDiffSpace(Subspace W, char firstLabel, char secondLabel, boolean verbose, boolean noPriorKnowledge)
    {
        boolean heading = false;
        if (verbose && !this.getOriginalForm()) {
            textOut("převod " + firstLabel + " na obecný tvar\r\n-----------------------\r\n");
            texOut("\n\\subsection*{převod $\\mathcal{" + firstLabel + "}$ na obecný tvar}\n");
            heading = true;
        }
        this.checkGeneralForm(firstLabel, verbose);
        if (verbose && !W.getOriginalForm()) {
            textOut("převod " + secondLabel + " na obecný tvar\r\n-----------------------\r\n");
            texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ na obecný tvar}\n");
            heading = true;
        }
        W.checkGeneralForm(secondLabel, verbose);

        Matrix M = new Matrix();
        M.copy(this.augMatrix);
        M.mergeFromBottom(W.augMatrix);
        if (verbose) {
            if (!heading && !noPriorKnowledge) {
                textOut("\r\nKdyž jsme zjišťovali rovnoběžnost podprostorů, zjistili jsme, že společná soustava rovnic obou podprostorů má matici:\r\n"
                    + M.getSystemCoeffMatrix().getEchelonForm(false).toText() + "Soustavu nyní vyřešíme.");
                texOut("\nKdyž jsme zjišťovali rovnoběžnost podprostorů, zjistili jsme, že společná soustava rovnic obou podprostorů má matici:\n\\["
                    + M.getSystemCoeffMatrix().getEchelonForm(false).toTeX() + "\\]\nSoustavu nyní vyřešíme.");
                for (int i=1; i<=M.getR(); i++) M.set(ZERO,i,M.getC());
                M.solveOnly('M', true);
            }
            else {
                textOut("\r\n\r\nzjištění průniku\r\n----------------\r\n" + "Matice soustavy příslušná podprostoru " + firstLabel + " je:\r\n"
                    + this.augMatrix.getSystemCoeffMatrix().toText() + "\r\nMatice soustavy příslušná podprostoru " + secondLabel + " je:\r\n"
                    + W.augMatrix.getSystemCoeffMatrix().toText() + "\r\nObě matice nyní napíšeme pod sebe a dostaneme:\r\n"
                    + M.getSystemCoeffMatrix().toText() + "Soustavu nyní vyřešíme.\r\n");
                texOut("\n\\subsection*{zjištění průniku}\n" + "Matice soustavy příslušná podprostoru $\\mathcal{" + firstLabel + "}$ je:\n\\["
                    + this.augMatrix.getSystemCoeffMatrix().toTeX() + "\\]\n\nMatice soustavy příslušná podprostoru $\\mathcal{" + secondLabel + "}$ je:\n\\["
                    + W.augMatrix.getSystemCoeffMatrix().toTeX() + "\\]\n\nObě matice nyní napíšeme pod sebe "
                    + "a dostaneme:\n\\[" + M.getSystemCoeffMatrix().toTeX() + "\\]\nSoustavu nyní vyřešíme.\n\n");
                for (int i=1; i<=M.getR(); i++) M.set(ZERO,i,M.getC());
                M.solve('M', true);  
            }
        }
        else {
            // we only care about difference spaces, so we discard absolute values and input zeroes instead
            for (int i=1; i<=M.getR(); i++) M.set(ZERO,i,M.getC());
        }

        Subspace S = new Subspace(M);
        S.checkParametricForm();

        return S;
    }

    /**
     * Finds orthogonal supplement to a subspace.
     * 
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return the orthogonal supplement to this subspace
     */
    Subspace orthogonalSupplement(char label, boolean verbose)
    {
        boolean heading = false;
        if (verbose && this.getOriginalForm()) {
            textOut("\r\npřevod " + label + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Potřebujeme znát zaměření podprostoru " + label + ", a tak jej převedeme na parametrický tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + label + "}$ do parametrického tvaru}\n"
                + "Potřebujeme znát zaměření podprostoru $\\mathcal{" + label + "}$, a tak jej převedeme na parametrický tvar. ");
            heading = true;
        }
        this.checkParametricForm(label, verbose);
        Matrix V = new Matrix();
        V.copy(this.vecMatrix);
        Matrix O = new Matrix(V.getR(), 1);
        for (int i = 1; i <= O.getR(); i++) O.set(ZERO,i,1);
        V.mergeFromRight(O);
        // we have now obtained a matrix (V|O), where V is the matrix of vectors and O is a 1-column zero matrix - that consitutes augmented matrix of orthogonal supplement

        Subspace result = new Subspace(V);
        if (verbose) {
            textOut((heading ? "\r\nnalezení ortogonálního doplňku\r\n------------------------------\r\n" : "\r\n")
                + "Ortogonální doplněk je podprostor zadaný obecně, přičemž koeficienty rovnic v jednotlivých rovnicích jsou souřadnice jednotlivých vektorů ze "
                + "zaměření. Ortogonální doplněk je tudíž:\r\n" + result.toTextGeneral(true));
            texOut((heading ? "\n\\subsection*{nalezení ortogonálního doplňku}\n" : "\n")
                + "Ortogonální doplněk je podprostor zadaný obecně, přičemž koeficienty rovnic v~jednotlivých rovnicích jsou souřadnice jednotlivých vektorů ze "
                + "zaměření. Ortogonální doplněk je tudíž:\n\\[" + result.toTeXGeneral() + ".\\]\n");
        }

        return result;
    }

    /**
     * Calculates the angle of any two subspaces using the most efficient method.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     * 
     * @throws IllegalArgumentException Throws an exception if the two subspaces do not come from the same space, ie. they have a different <code>DIM</code>.
     */
    Cyclometric angle(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (this.DIM != W.DIM) throw new IllegalArgumentException("The subspaces are not subspaces of the same space!");

        if (verbose) {
            textOut("Výpočet odchylky podprostorů přímou metodou je poměrně pracný, a proto je dobré nejprve zjistit, zda není splněna některá z podmínek, která umožňuje "
                + "použití některého vzorce či pravidla pro snazší určení vzdálenosti. Zjistěme nejprve, o jaké podprostory se jedná.");
            texOut("Výpočet odchylky podprostorů přímou metodou je poměrně pracný, a proto je dobré nejprve zjistit, zda není splněna některá z~podmínek, která umožňuje "
                + "použití některého vzorce či pravidla pro snazší určení vzdálenosti. Zjistěme nejprve, o~jaké podprostory se jedná.");

            textOut("\r\n\r\npopis " + firstLabel + "\r\n" + "-------\r\n");
            texOut("\n\\subsection*{Popis $\\mathcal{" + firstLabel + "}$}");
            this.describe(firstLabel);

            textOut("\r\n\r\npopis " + secondLabel + "\r\n" + "-------\r\n");
            texOut("\n\\subsection*{Popis $\\mathcal{" + secondLabel + "}$}");
            W.describe(secondLabel);
        }  

        // a trivial subspace
        if (this.isPoint() || W.isPoint()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n" + "Bod má od jakéhokoliv podprostoru odchylku rovnu nule.");
                texOut("\n\\section*{Určení odchylky}" + "Bod má od jakéhokoliv podprostoru odchylku rovnu nule.");
            }
            return new Cyclometric(SRF_ZERO,true);
        }
        if (this.isWholeSpace() || W.isWholeSpace()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n" + "Celý prostor má od jakéhokoliv svého podprostoru odchylku rovnu nule.");
                texOut("\n\\section*{Určení odchylky}" + "Celý prostor má od jakéhokoliv svého podprostoru odchylku rovnu nule.");
            }
            return new Cyclometric(SRF_ZERO,true);
        }

        // two lines
        if (this.isLine() && W.isLine()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
                texOut("\n\\section*{Určení odchylky}");
            }
            return this.angleLineLine(W, firstLabel, secondLabel, verbose);
        }

        // a line and a hyperplane
        if (this.isLine() && W.isHyperplane()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
                texOut("\n\\section*{Určení odchylky}");
            }
            return this.angleLineHyperplane(W, firstLabel, secondLabel, verbose);
        }
        if (this.isHyperplane() && W.isLine()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
                texOut("\n\\section*{Určení odchylky}");
            }
            return W.angleLineHyperplane(this, secondLabel, firstLabel, verbose);
        }

        // two hyperplanes
        if (this.isHyperplane() && W.isHyperplane()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
                texOut("\n\\section*{Určení odchylky}");
            }
            return this.angleHyperplaneHyperplane(W, firstLabel, secondLabel, verbose);
        }

        // a line and any subspace which is not trivial, line, nor hyperplane
        if (this.isLine()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
                texOut("\n\\section*{Určení odchylky}");
            }
            return this.angleLineAny(W, firstLabel, secondLabel, verbose);
        }
        if (W.isLine()) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
                texOut("\n\\section*{Určení odchylky}");
            }
            return W.angleLineAny(this, secondLabel, firstLabel, verbose);
        }

        if (verbose) {
            textOut("\r\n\r\nOd obtížných výpočtů nás může ještě zachránit vzájemná poloha podprostorů, jmenovitě jsou-li rovnoběžné.");
            texOut("\n\nOd obtížných výpočtů nás může ještě zachránit vzájemná poloha podprostorů, jmenovitě jsou\\discretionary{-}{-}{-}li rovnoběžné.");

            textOut("\r\n\r\nověření rovnoběžnosti\r\n" + "---------------\r\n");
            texOut("\n\\subsection*{ověření rovnoběžnosti}");
        }

        // two parallel subspaces
        if (this.isParallel(W, verbose)) {
            if (verbose) {
                textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n" + "Rovnoběžné podprostory mají odchylku rovnu nule.");
                texOut("\n\\section*{Určení odchylky}" + "Rovnoběžné podprostory mají odchylku rovnu nule.");
            }
            return new Cyclometric(SRF_ZERO,true);
        }            

        if (verbose) {
            textOut("\r\n\r\nURČENÍ ODCHYLKY\r\n" + "===============\r\n");
            texOut("\n\\section*{Určení odchylky}");
        }

        return this.angleUniversal(W, firstLabel, secondLabel, verbose);
    }    

    /**
     * Calculates the angle of two lines, ie. one-dimensional subspaces.
     * 
     * @param W the other line
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     * 
     * @throws IllegalArgumentException Throws an exception if any of the two subspaces is not a line in fact.
     */
    Cyclometric angleLineLine(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if ((this.dim != 1) || (W.dim != 1)) throw new IllegalArgumentException("One of the subspaces is not a line!");

        if (verbose && this.getOriginalForm() && this.getDIM() == 2) {
            if (W.getOriginalForm()) {
                Vector thisNormVector = this.augMatrix.getSystemCoeffMatrix().rowsToVectors()[0];
                Vector WNormVector = W.augMatrix.getSystemCoeffMatrix().rowsToVectors()[0];
                textOut("V rovině je přímka zadána obecně jedinou rovnicí, a má tak definovaný normálový vektor. Odchylka přímek zadaných obecně je pak rovna odchylce jejich "
                    + "normálových vektorů. Normálové vektory přitom jsou:\r\n\tn_" + firstLabel + " = " + thisNormVector.toText()
                    + ",\r\n\tn_" + secondLabel + " = " + WNormVector.toText() + ".\r\n\r\n");
                texOut("V~rovině je přímka zadána obecně jedinou rovnicí, a má tak definovaný normálový vektor. Odchylka přímek zadaných obecně je pak rovna odchylce jejich "
                    + "normálových vektorů. Normálové vektory přitom jsou:\n\\begin{align*}\n\\vec{n}_{\\mathcal{" + firstLabel + "}} &= " + thisNormVector.toTeX()
                    + ",\\\\\n\\vec{n}_{\\mathcal{" + secondLabel + "}} &= " + WNormVector.toTeX() + ".\n\\end{align*}");

                return thisNormVector.angleNormalVectors(WNormVector, firstLabel, secondLabel, true);
            }
            else {
                Vector thisNormVector = this.augMatrix.getSystemCoeffMatrix().rowsToVectors()[0];
                Vector WDirVector = W.vecMatrix.rowsToVectors()[0];
                textOut("V rovině je přímka zadána obecně jedinou rovnicí, a má tak definovaný normálový vektor. Odchylku přímek, z nichž jedna je zadána obecně, a má tak normálový "
                    + "vektor, a druhá je zadána parametricky, a má tak směrový vektor, spočítáme tak , že nejprve spočítáme odchylku obou zmíněných vektorů; odchylka "
                    + "původních přímek je pak doplněk do pravého úhlu. V praxi se však místo hledání doplňku do pravého úhlu využije místo funkce kosinus funkce sinus. "
                    + "Normálový vektor přitom je:\r\n\tn_" + firstLabel + " = " + thisNormVector.toText()
                    + "a směrový vektor je:\r\n\tv_" + secondLabel + " = " + WDirVector.toText() + ".\r\n\r\n");
                texOut("V~rovině je přímka zadána obecně jedinou rovnicí, a má tak definovaný normálový vektor. Odchylku přímek, z~nichž jedna je zadána obecně, a má tak normálový "
                    + "vektor, a druhá je zadána parametricky, a má tak směrový vektor, spočítáme tak , že nejprve spočítáme odchylku obou zmíněných vektorů; odchylka "
                    + "původních přímek je pak doplněk do pravého úhlu. V praxi se však místo hledání doplňku do pravého úhlu využije místo funkce kosinus funkce sinus. "
                    + "Normálový vektor přitom je:\n\\begin{align*}\n\\vec{n}_{\\mathcal{" + firstLabel + "}} &= " + thisNormVector.toTeX()
                    + ",\\intertext{a směrový vektor je:}\n\\vec{v}_{\\mathcal{" + secondLabel + "}} &= " + WDirVector.toTeX() + ".\n\\end{align*}");

                return thisNormVector.angleNormalVectorDirectionVector(WDirVector, firstLabel, secondLabel, true);
            }
        }
        if (verbose && W.getOriginalForm() && W.getDIM() == 2) return W.angleLineLine(this, secondLabel, firstLabel, verbose);

        boolean heading = false;
        if (verbose && this.getOriginalForm()) {
            textOut("převod " + firstLabel + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Potřebujeme znát směrový vektor přímky " + firstLabel + ", a tak ji převedeme na parametrický tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + firstLabel + "}$ do parametrického tvaru}\n"
                + "Potřebujeme znát směrový vektor přímky $\\mathcal{" + firstLabel + "}$, a tak ji převedeme na parametrický tvar. ");
            heading = true;
        }
        this.checkParametricForm(firstLabel, verbose);

        if (verbose && W.getOriginalForm()) {
            textOut("převod " + secondLabel + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Potřebujeme znát směrový vektor přímky " + secondLabel + ", a tak ji převedeme na parametrický tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ do parametrického tvaru}\n"
                + "Potřebujeme znát směrový vektor přímky $\\mathcal{" + secondLabel + "}$, a tak ji převedeme na parametrický tvar. ");
            heading = true;
        }
        W.checkParametricForm(secondLabel, verbose);

        Vector thisDirVector = this.vecMatrix.rowsToVectors()[0];
        Vector WDirVector = W.vecMatrix.rowsToVectors()[0];

        if (verbose) {
            textOut((heading ? "výpočet odchylky\r\n----------------\r\n" : "") + "Odchylka přímek je pak rovna odchylce jejich směrových vektorů. Směrové vektory přitom jsou:"
                + "\r\n\tv_" + firstLabel + " = " + thisDirVector.toText()
                + ",\r\n\tv_" + secondLabel + " = " + WDirVector.toText() + ".\r\n\r\n");
            texOut((heading ? "\n\\subsection*{výpočet odchylky}\n" : "") + "Odchylka přímek je pak rovna odchylce jejich směrových vektorů. Směrové vektory přitom jsou:"
                + "\n\\[\\begin{align*}\n\\vec{v}_{\\mathcal{" + firstLabel + "}} &= " + thisDirVector.toTeX()
                + ",\\\\\n\\vec{v}_{\\mathcal{" + secondLabel + "}} &= " + WDirVector.toTeX() + ".\n\\end{align*}\\]\n");
        }

        return thisDirVector.angleDirectionVectors(WDirVector, firstLabel, secondLabel, verbose);
    }

    /**
     * Calculates the angle of a line and a hyperplane.
     * 
     * @param W the hyperplane
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     * 
     * @throws IllegalArgumentException Throws an exception if this subspaces is not a line or <code>W</code> is not a hyperplane.
     */
    Cyclometric angleLineHyperplane(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if ((this.dim != 1) || (W.dim != W.DIM-1)) throw new IllegalArgumentException("This method only calculates angle of a line and a hyperplane!");

        boolean heading = false;
        String name = (W.getDIM() == 3 ? "roviny" : "nadroviny");

        if (verbose && this.getOriginalForm()) {
            textOut("převod " + firstLabel + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Potřebujeme znát směrový vektor přímky " + firstLabel + ", a tak ji převedeme na parametrický tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + firstLabel + "}$ do parametrického tvaru}\n"
                + "Potřebujeme znát směrový vektor přímky $\\mathcal{" + firstLabel + "}$, a tak ji převedeme na parametrický tvar. ");
            heading = true;
        }
        this.checkParametricForm(firstLabel, verbose);

        if (verbose && !W.getOriginalForm()) {
            textOut("převod " + secondLabel + " na obecný tvar\r\n-----------------------\r\n"
                + "Potřebujeme znát normálový vektor " + name + " " + secondLabel + ", a tak ji převedeme na obecný tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ na obecný tvar}\n"
                + "Potřebujeme znát normálový vektor " + name + " " + "$\\mathcal{" + secondLabel + "}$" + ", a tak ji převedeme na obecný tvar. ");
            heading = true;
        }
        W.checkGeneralForm(secondLabel, verbose);

        Vector lineDirVector = this.vecMatrix.rowsToVectors()[0];
        Vector hyperplaneNormVector = W.augMatrix.getSystemCoeffMatrix().rowsToVectors()[0];
        if (verbose) {
            textOut((heading ? "výpočet odchylky\r\n----------------\r\n" : "") + "Odchylku přímky a " + name + " spočítáme tak, že nejprve spočítáme odchylku "
                + "směrového vektoru přímky od normálového vektoru " + name + "; odchylka přímky a " + name + " je pak doplněk do pravého úhlu. "
                + "V praxi se však místo hledání doplňku do pravého úhlu využije místo funkce kosinus funkce sinus. "
                + "Směrový vektor přímky přitom je:\r\n\tv_" + firstLabel + " = " + lineDirVector.toText()
                + "a normálový vektor " + name + " je:\r\n\tn_" + secondLabel + " = " + hyperplaneNormVector.toText() + ".\r\n\r\n");
            texOut((heading ? "\n\\subsection*{výpočet odchylky}\n" : "") + "Odchylku přímky a " + name + " spočítáme tak, že nejprve spočítáme odchylku "
                + "směrového vektoru přímky od normálového vektoru " + name + "; odchylka přímky a " + name + " je pak doplněk do pravého úhlu. "
                + "V~praxi se však místo hledání doplňku do pravého úhlu využije místo funkce kosinus funkce sinus. "
                + "Směrový vektor přímky přitom je:\n\\begin{align*}\n\\vec{v}_{\\mathcal{" + firstLabel + "}} &= " + lineDirVector.toTeX()
                + "\\intertext{a normálový vektor " + name + " je:}\n\\vec{n}_{\\mathcal{" + secondLabel + "}} &= " + hyperplaneNormVector.toTeX() + ".\n\\end{align*}");
        }

        return hyperplaneNormVector.angleNormalVectorDirectionVector(lineDirVector, secondLabel, firstLabel, verbose);
    }

    /**
     * Calculates the angle of two hyperplanes.
     * 
     * @param W the other hyperplane
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     * 
     * @throws IllegalArgumentException Throws an exception if any of the two subspaces is not a hyperplane in fact.
     */
    Cyclometric angleHyperplaneHyperplane(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if ((this.dim != this.DIM-1) || (W.dim != W.DIM-1)) throw new IllegalArgumentException("One of the subspaces is not a hyperplane!");

        boolean heading = false;
        String name = (W.getDIM() == 3 ? "roviny" : "nadroviny");

        if (verbose && !this.getOriginalForm()) {
            textOut("převod " + firstLabel + " na obecný tvar\r\n-----------------------\r\n"
                + "Potřebujeme znát normálový vektor " + name + " " + firstLabel + ", a tak ji převedeme na obecný tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + firstLabel + "}$ na obecný tvar}\n"
                + "Potřebujeme znát normálový vektor " + name + " " + "$\\mathcal{" + firstLabel + "}$" + ", a tak ji převedeme na obecný tvar. ");
            heading = true;
        }
        this.checkGeneralForm(firstLabel, verbose);

        if (verbose && !W.getOriginalForm()) {
            textOut("převod " + secondLabel + " na obecný tvar\r\n-----------------------\r\n"
                + "Potřebujeme znát normálový vektor " + name + " " + secondLabel + ", a tak ji převedeme na obecný tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ na obecný tvar}\n"
                + "Potřebujeme znát normálový vektor " + name + " " + "$\\mathcal{" + secondLabel + "}$" + ", a tak ji převedeme na obecný tvar. ");
            heading = true;
        }
        W.checkGeneralForm(secondLabel, verbose);

        Vector firstNormVector = this.augMatrix.getSystemCoeffMatrix().rowsToVectors()[0];
        Vector secondNormVector = W.augMatrix.getSystemCoeffMatrix().rowsToVectors()[0];
        if (verbose) {
            textOut((heading ? "výpočet odchylky\r\n----------------\r\n" : "") + "Odchylka " + name.substring(0, name.length()-1) + " je pak rovna odchylce jejich normálových vektorů. "
                + "Normálové vektory přitom jsou:\r\n\tn_" + firstLabel + " = " + firstNormVector.toText()
                + ",\r\n\tn_" + secondLabel + " = " + secondNormVector.toText() + ".\r\n\r\n");
            texOut((heading ? "\n\\subsection*{výpočet odchylky}\n" : "") + "Odchylka " + name.substring(0, name.length()-1) + " je pak rovna odchylce jejich normálových vektorů. "
                + "Normálové vektory přitom jsou:\n\\[\\begin{align*}\n\\vec{v}_{\\mathcal{" + firstLabel + "}} &= " + firstNormVector.toTeX()
                + ",\\\\\n\\vec{v}_{\\mathcal{" + secondLabel + "}} &= " + secondNormVector.toTeX() + ".\n\\end{align*}\\]\n");
        }

        return firstNormVector.angleNormalVectors(secondNormVector, firstLabel, secondLabel, verbose);
    }

    /**
     * Calculates the angle of a line and any other subspace.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     * 
     * @throws IllegalArgumentException Throws an exception if this subspaces is not a line.
     */
    Cyclometric angleLineAny(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (this.dim != 1) throw new IllegalArgumentException("This is not a line!");

        boolean heading = false;

        if (verbose && this.getOriginalForm()) {
            textOut("\r\npřevod " + firstLabel + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Potřebujeme znát směrový vektor přímky " + firstLabel + ", a tak ji převedeme na parametrický tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + firstLabel + "}$ do parametrického tvaru}\n"
                + "Potřebujeme znát směrový vektor přímky $\\mathcal{" + firstLabel + "}$, a tak ji převedeme na parametrický tvar. ");
            heading = true;
        }
        this.checkParametricForm(firstLabel, verbose);

        if (verbose && W.getOriginalForm()) {
            textOut("\r\npřevod " + secondLabel + " do parametrického tvaru\r\n--------------------------------\r\n"
                + "Potřebujeme znát zaměření podprostoru " + secondLabel + ", a tak jej převedeme na parametrický tvar. ");
            texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ do parametrického tvaru}\n"
                + "Potřebujeme znát zaměření podprostoru $\\mathcal{" + secondLabel + "}$, a tak jej převedeme na parametrický tvar. ");
            heading = true;
        }
        W.checkParametricForm(secondLabel, verbose);

        Vector lineDirVector = this.vecMatrix.rowsToVectors()[0];
        if (verbose && heading) {
            textOut("\r\n\r\nnalezení ortogonální projekce směrového vektoru v_" + firstLabel + " na zaměření podprostoru " + secondLabel
                + "\r\n-----------------------------------------------------------------------------\r\n");
            texOut("\n\\subsection*{nalezení ortogonální projekce směrového vektoru $\\vec{v}_{\\mathcal{" + firstLabel + "}}$ na zaměření podprostoru $\\mathcal{" + secondLabel
                + "}$}\n");
        }
        Vector orthProjection = lineDirVector.orthogonalProjection(W.vecMatrix, verbose);

        // if line's directional vector is its own orthogonal component, the subspaces are orthogonal
        if (orthProjection.isZeroVector()) {
            Cyclometric result = new Cyclometric(new SquareRootFraction(0), false);
            if (verbose) {
                textOut("\r\n\r\nvýpočet odchylky\r\n----------------\r\n" + "Vidíme, že ortogonální projekce je nulový vektor, neboli směrový vektor v_" + firstLabel
                    + " je vzhledem k zaměření podprostoru " + secondLabel + " sám sobě ortogonální komponentou. To znamená, že leží v ortogonálním doplňku podprostoru "
                    + secondLabel + ", a to znamená, že oba podprostory jsou kolmé, tedy jejich odchylka je:\r\n\tphi = " + result.toText() + ".");
                texOut("\n\\subsection*{výpočet odchylky}\n" + "Vidíme, že ortogonální projekce je nulový vektor, neboli směrový vektor $\\vec{v}_{\\mathcal{" + firstLabel
                    + "}}$ je vzhledem k zaměření podprostoru $\\mathcal{" + secondLabel + "}$ sám sobě ortogonální komponentou. To znamená, že leží "
                    + "v~ortogonálním doplňku podprostoru $\\mathcal{" + secondLabel + "}$, a to znamená, že oba podprostory jsou kolmé, tedy jejich odchylka je:\n\\[\\phi = "
                    + result.toTeX() + ".\\]\n");
            }
            return new Cyclometric(new SquareRootFraction(0), false);
        }

        // otherwise, the angle is equal to the angle of this line with line with the orthogonal projection as direction vector
        Cyclometric result = new Cyclometric(new SquareRootFraction(0), false);
        if (verbose) {
            textOut("výpočet odchylky\r\n----------------\r\n" + "Odchylka přímky " + firstLabel + " od podprostoru " + secondLabel + " je pak rovna odchylce směrového vektoru "
                + "přímky " + firstLabel + " od její ortogonální projekce na zaměření podprostoru " + secondLabel + ". Tyto vektory přitom jsou:"
                + "\r\n\tv_" + firstLabel + " = " + lineDirVector.toText()
                + ",\r\n\tv_y = " + orthProjection.toText() + ".\r\n\r\n");
            texOut("\n\\subsection*{výpočet odchylky}\n" + "Odchylka přímky $\\mathcal{" + firstLabel + "}$ od podprostoru $\\mathcal{" + secondLabel + "}$ je pak rovna odchylce "
                + "směrového vektoru přímky $\\mathcal{" + firstLabel + "}$ od její ortogonální projekce na zaměření podprostoru $\\mathcal{" + secondLabel + "}$. "
                + "Tyto vektory přitom jsou:" + "\n\\[\\begin{align*}\n\\vec{v}_{\\mathcal{" + firstLabel + "}} &= " + lineDirVector.toTeX()
                + ",\\\\\n\\vec{v}_{\\mathcal{Y}} &= " + orthProjection.toTeX() + ".\n\\end{align*}\\]\n");
        }

        return lineDirVector.angleDirectionVectors(orthProjection, firstLabel, 'Y', verbose);
    }

    /**
     * Calculates the angle of two subspaces.
     * 
     * @param W the other subspace
     * @firstLabel label of this subspace
     * @secondLabel label of the subspace <code>W</code>
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * 
     * @return a {@link Cyclometric} that holds all information about the angle; most importantly, if the angle was any of 0, &pi;/6, &pi;/4, &pi;/3, &pi;/2, this value is marked
     * together with double value in radians
     */
    Cyclometric angleUniversal(Subspace W, char firstLabel, char secondLabel, boolean verbose)
    {
        if (verbose) {
            textOut("Bohužel nezbývá než se uchýlit k obecnému, ale pracnému postupu. Zjistíme nejprve, jaký je průnik zaměření obou podprostorů."
                + "\r\n\r\nstanovení průniku zaměření\r\n--------------------------\r\n");
            texOut("Bohužel nezbývá než se uchýlit k~obecnému, ale pracnému postupu. Zjistíme nejprve, jaký je průnik zaměření obou podprostorů."
                + "\n\\subsection*{stanovení průniku zaměření}\n");
        }
        Subspace intersection = this.intersectionDiffSpace(W, firstLabel, secondLabel, verbose, false);

        if (intersection.vecMatrix.rank(false) == 0) { // the difference spaces intersect at null space
            if (verbose) {
                textOut("\r\nPrůnikem zaměření je tedy triviální podprostor. V tom případě nejprve najdeme ortonormální báze obou podprostorů.");
                texOut("\n\nPrůnikem zaměření je tedy triviální podprostor. V~tom případě nejprve najdeme ortonormální báze obou podprostorů.");
            }

            // we find orthogonal base of both subspaces
            boolean heading = false;

            if (verbose && this.getOriginalForm()) {
                textOut("\r\n\r\npřevod " + firstLabel + " do parametrického tvaru\r\n--------------------------------\r\n");
                texOut("\n\\subsection*{převod $\\mathcal{" + firstLabel + "}$ do parametrického tvaru}\n");
                heading = true;
            }
            this.checkParametricForm(firstLabel, verbose);

            if (verbose && W.getOriginalForm()) {
                textOut("\r\n\r\npřevod " + secondLabel + " do parametrického tvaru\r\n--------------------------------\r\n");
                texOut("\n\\subsection*{převod $\\mathcal{" + secondLabel + "}$ do parametrického tvaru}\n");
                heading = true;
            }
            W.checkParametricForm(secondLabel, verbose);

            if (verbose) {
                textOut("\r\n\r\nnalezení ortonormální báze " + firstLabel + "\r\n----------------------------\r\n");
                texOut("\n\\subsection*{nalezení ortonormální báze $\\mathcal{" + firstLabel + "}$}\n");
            }
            this.vecMatrix = this.vecMatrix.orthogonalizeWithGramSchmidt(firstLabel, verbose);
            if (verbose) {
                textOut("\r\n\r\nnalezení ortonormální báze " + secondLabel + "\r\n----------------------------\r\n");
                texOut("\n\\subsection*{nalezení ortonormální báze $\\mathcal{" + secondLabel + "}$}\n");
            }
            W.vecMatrix = W.vecMatrix.orthogonalizeWithGramSchmidt(secondLabel, verbose);

            // we find all eigenvalues
            SquareRootMatrix AAT = this.vecMatrix.gramianMatrixProduct(W.vecMatrix);
            Jama.Matrix AATReal = AAT.toRealMatrix();
            Jama.EigenvalueDecomposition eigenvalues = AATReal.eig();

            // we find the largest eigenvalue
            double[] realEigenvalues = eigenvalues.getRealEigenvalues();
            double maxEigenvalue = realEigenvalues[0];
            for (int i=1; i<realEigenvalues.length; i++) maxEigenvalue = Math.max(realEigenvalues[i],maxEigenvalue);

            // we try to represent the eigenvalue as a fraction
            Fraction lambda = new Fraction(maxEigenvalue);
            // and now we check if we guessed right (we allow for error on 10th decimal place)
            boolean useFraction = (Math.abs((((double) lambda.n) / ((double) lambda.d)) - maxEigenvalue) <= 0.0000000001);

            if (verbose) {
                SquareRootMatrix G = this.vecMatrix.gramianMatrixForOutput(W.vecMatrix);
                SquareRootMatrix GT = W.vecMatrix.gramianMatrixForOutput(this.vecMatrix);
                int rowsNumber = Math.max(G.getR(), GT.getR());
                int GStartRow = (rowsNumber - G.getR())/2;
                int GTStartRow = (rowsNumber - GT.getR())/2;
                String GGT = "";
                int GRow = 1, GTRow = 1;
                for (int i = 0; i < rowsNumber; i++) {
                    boolean GRowWrite = (i >= GStartRow && GRow <= G.getR());
                    boolean GTRowWrite = (i >= GTStartRow && GTRow <= GT.getR());
                    GGT += (i == rowsNumber/2 ? "\tA = \t" : "\t\t") + (GRowWrite ? "(\t" : "\t");
                    for (int j = 1; j <= G.getC(); j++) GGT += (GRowWrite ? G.get(GRow, j).toText() + "\t" : "\t");
                    GGT += (GRowWrite ? ")" : "") + (i == rowsNumber/2 ? "\t×\t" : "\t\t") + (GTRowWrite ? "(\t" : "\t");
                    for (int j = 1; j <= GT.getC(); j++) GGT += (GTRowWrite ? GT.get(GTRow, j).toText() + "\t" : "\t");
                    GGT += (GTRowWrite ? ")\r\n" : "\r\n");
                    if (GRowWrite) GRow++;
                    if (GTRowWrite) GTRow++;
                }

                String AATlambdaText = "", AATlambdaTeX = "\\left(" + "\\begin{array}{*{" + String.valueOf(AAT.getC()) + "}r}";
                for (int i = 1; i<=AAT.getR(); i++) {
                    AATlambdaText += "\t(";
                    for (int j = 1; j<=AAT.getC(); j++) {
                        AATlambdaText += "\t" + AAT.get(i,j).toText() + (i == j ? "-x " : "") + " ";
                        if (j != AAT.getC())
                            AATlambdaTeX += AAT.get(i,j).toTeX() + (i == j ? "-\\lambda " : "") + " & ";
                        else
                            AATlambdaTeX += AAT.get(i,j).toTeX() + (i == j ? "-\\lambda " : "");
                    }
                    if (i != AAT.getR()) AATlambdaTeX += "\\\\";
                    AATlambdaText += "\t)\r\n";
                }
                AATlambdaTeX += "\\end{array}" + "\\right)";

                double[] polynomeCoeffs = AAT.getCharPolyCoeffs();
                int n = AAT.getR();
                String polynomeText = (n % 2 == 0 ? "x" : "-x") + (n == 1 ? "" : ("^" + n));
                String polynomeTeX = (n % 2 == 0 ? "\\lambda" : "-\\lambda") + (n == 1 ? "" : ("^{" + n + "}"));
                for (int i = 0; i < n; i++) {
                    boolean plusMinus = (n % 2 == 0);
                    if (polynomeCoeffs[i] < 0) plusMinus = !plusMinus;
                    if (polynomeCoeffs[i] == 0) plusMinus = true;
                    polynomeText += (plusMinus ? " + " : " - ") + String.format("%.2f", Math.abs(polynomeCoeffs[i]));
                    polynomeTeX += (plusMinus ? " + " : " - ") + String.format("%.2f", Math.abs(polynomeCoeffs[i])).replace(",","{,}");
                    switch (n-i-1) {
                        case 0:  break;
                        case 1:  polynomeText += "x";
                        polynomeTeX  += "\\lambda";
                        break;
                        default: polynomeText += "x^" + String.valueOf(n-i-1);
                        polynomeTeX  += "\\lambda^{" + String.valueOf(n-i-1) + "}";
                        break;
                    }
                }
                polynomeText += " = 0";
                polynomeTeX += " = 0";

                String solutionsText = "", solutionsTeX = "\\begin{align*}\n";
                for (int i = 0; i < realEigenvalues.length; i++) {
                    solutionsText += String.format("\tx_" + String.valueOf(i+1) + " = %.3f\r\n", realEigenvalues[i]);
                    solutionsTeX += String.format("\\lambda_{" + String.valueOf(i+1) + "} &= %.3f\\\\\n", realEigenvalues[i]).replace(",","{,}");
                }
                solutionsTeX += "\\end{align*}\n";

                SquareRootFraction result = new SquareRootFraction(lambda);
                textOut("\r\n\r\nnalezení Gramovy matice\r\n-----------------------\r\n" + "Máme nyní k dispozici ortonormální báze obou podprostorů. Vytvoříme nyní Gramovu matici G, "
                    + "jejíž prvky jsou dány pravidlem:\r\n\tG(i,j) = (v_i*w_j),\r\nkde v_i jsou vektory ortonormální báze podprostoru " + firstLabel + " a w_j vektory "
                    + "ortonormální báze podprostoru " + secondLabel + ". Tato matice tedy je:\r\n" + G.toText() + "\r\nZajímá nás ovšem zejména matice A = G×G^T, tedy:\r\n"
                    + GGT + "Matice A je tedy:\r\n" + AAT.toText() + "Hledáme nyní řešení rovnice:\r\n\t|A-x*E| = 0,\r\nkde E je jednotková matice vhodné dimenze. Máme "
                    + "tedy spočítat determinant matice:\r\n" + AATlambdaText
                    + "\r\n\r\nřešení charakteristického polynomu a výpočet odchylky\r\n-----------------------------------------------------\r\n"
                    + "\r\n(Pozn.: V dalším se odchýlíme od uvádění přesných výpočtů a budeme výsledky udávat jako desetinná čísla. Činíme tak z důvodu mnohonásobně "
                    + "obtížnějších výpočtů, které je zapotřebí provést.)\r\n\r\n"
                    + "Po výpočtu tohoto determinantu a úpravě dostaneme rovnici:\r\n\t" + polynomeText + ",\r\nkterá má řešení:\r\n" + solutionsText
                    + "\nZ nich je největší x_max = " + String.format("%.3f", maxEigenvalue)
                    + (useFraction ? ", což odpovídá zlomku " + lambda.toText() + ". " : ". ") + "Odchylku pak spočítáme jako arccos(sqrt(x_max)) = "
                    + (useFraction ? "arccos(" + result.toText() + ")" : String.format("arccos(%.4f)", Math.sqrt(maxEigenvalue))) + "; odchylka je tedy:"
                    + "\r\n\tphi = " + new Cyclometric(result, false).toText() + ".");
                texOut("\n\\subsection*{nalezení Gramovy matice}\n" + "Máme nyní k~dispozici ortonormální báze obou podprostorů. Vytvoříme nyní Gramovu matici $G$, "
                    + "jejíž prvky jsou dány pravidlem:\n\\[G(i,j) = (\\vec{v}_i\\cdot\\vec{w}_j),\\]\nkde $\\vec{v}_i$ jsou vektory ortonormální báze podprostoru $\\mathcal{"
                    + firstLabel + "}$ a $\\vec{w}_j$ vektory ortonormální báze podprostoru $\\mathcal{" + secondLabel + "}$. Tato matice tedy je:\n\\[" + G.toTeX() + ".\\]\n"
                    + "Zajímá nás ovšem zejména matice $A = G\\times G^T$, tedy:\n\\[A = " + G.toTeX() + "\\times" + GT.toTeX() + ".\\]\n"
                    + "Matice $A$ je tedy:\n\\[" + AAT.toTeX() + "\\]\n" + "Hledáme nyní řešení rovnice:\n\\[\\abs{A-\\lambda E} = 0,\\]\nkde $E$ je jednotková matice vhodné "
                    + "dimenze. Máme tedy spočítat determinant matice:\n\\[" + AATlambdaTeX + ".\\]\n"
                    + "\n\\subsection*{řešení charakteristického polynomu a výpočet odchylky}\n"
                    + "\n\n\\begin{small}\\itshape (Pozn.: V~dalším se odchýlíme od uvádění přesných výpočtů a budeme výsledky udávat jako desetinná čísla. "
                    + "Činíme tak z důvodu mnohonásobně obtížnějších výpočtů, které je zapotřebí provést.)\\end{small}\n\n"
                    + "Po výpočtu tohoto determinantu a úpravě dostaneme rovnici:\n\\[" + polynomeTeX + ",\\]\nkterá má řešení:\n" + solutionsTeX
                    + "\nZ~nich je největší $x_{\\text{max}} = " + String.format("%.3f", maxEigenvalue).replace(",","{,}") + "$"
                    + (useFraction ? ", což odpovídá zlomku $" + lambda.toTeX() + "$. " : ". ") + "Odchylku pak spočítáme jako $\\arccos\\left(\\sqrt{x_{\\text{max}}}\\right) = "
                    + (useFraction ? "\\arccos\\left(" + result.toTeX() + "\\right)" : String.format("\\arccos(%.4f)", Math.sqrt(maxEigenvalue)).replace(",","{,}")) + "$; "
                    + "odchylka je tedy:\n\\[\\phi = " + new Cyclometric(result, false).toTeX() + ".\\]\n");
            }

            if (useFraction) return new Cyclometric(new SquareRootFraction(lambda), false);
            else             return new Cyclometric(Math.sqrt(maxEigenvalue), false);
        }

        else {                                    // the difference spaces do not intersect at null space
            if (verbose) {
                textOut("\r\nPrůnikem zaměření je tedy netriviální podprostor U. V tom případě najdeme nejprve ortogonální doplněk k nalezenému průniku.\r\n");
                texOut("\n\nPrůnikem zaměření je tedy netriviální podprostor $\\mathcal{U}$. V~tom případě najdeme nejprve ortogonální doplněk k~nalezenému průniku.\r\n");
            }

            intersection.inputInGeneralForm = false;
            Subspace orthSupp = intersection.orthogonalSupplement('U', verbose);
            /* Now we find intersections of the orthogonal supplement with this subspace, or the other subspace, respectively.
             * For practical reasons, we abbreviate them P and Q. */
            if (verbose) {
                textOut("\r\nNyní najdeme podprostory P a Q, pro něž platí, že Z(P) = Z(" + firstLabel + ") ∩ Z(U), resp. Z(Q) = Z(" + secondLabel + ") ∩ Z(U). "
                    + "Odchylka podprostorů " + firstLabel + " a " + secondLabel + " je pak stejná jako odchylka podprostorů P a Q."
                    + "\r\n\r\nnalezení podprostoru P\r\n----------------------\r\n");
                texOut("\r\nNyní najdeme podprostory $\\mathcal{P}$ a $\\mathcal{Q}$, pro něž platí, že $Z(\\mathcal{P}) = Z(\\mathcal{" + firstLabel + "}) \\cap Z(\\mathcal{U})$, "
                    + "resp.~$Z(\\mathcal{Q}) = Z(\\mathcal{" + secondLabel + "}) \\cap Z(\\mathcal{U})$. Odchylka podprostorů $\\mathcal{" + firstLabel + "}$ a "
                    + "$\\mathcal{" + secondLabel + "}$ je pak stejná jako odchylka podprostorů $\\mathcal{P}$ a $\\mathcal{Q}$."
                    + "\n\\subsection*{nalezení podprostoru $\\mathcal{P}$}\n");
            }

            Subspace P = this.intersectionDiffSpace(orthSupp, firstLabel, 'U', verbose, true);
            if (verbose) {
                textOut("\r\n\r\nnalezení podprostoru Q\r\n----------------------\r\n");
                texOut("\n\\subsection*{nalezení podprostoru $\\mathcal{Q}$}\n");
            }
            P.inputInGeneralForm = false;

            Subspace Q = W.intersectionDiffSpace(orthSupp, secondLabel, 'U', verbose, true);
            if (verbose) {
                textOut("\r\n\r\nVÝPOČET ODCHYLKY\r\n");
                textOut("================\r\n");
                texOut("\\section*{Výpočet odchylky}\n");
            }
            Q.inputInGeneralForm = false;

            return P.angle(Q,'P','Q',true);
        }
    }

    /**
     * Generates a random hyperplane in general form.
     * 
     * @param DIM dimension of the superspace (ie. a number of variables)
     */
    static Subspace generateRandomHyperplane(int DIM)
    {
        Fraction[] coeffs = new Fraction[DIM+1];

        for (int i=0; i<=DIM; i++)
            coeffs[i] = new Fraction(Utility.randInt(-20,20),1);

        return new Subspace(new Matrix(coeffs,1,DIM+1));
    }

    /**
     * Generates a random hyperplane in general form. The length of the normal vector of the hyperplane will be an integer.
     * 
     * @param DIM dimension of the superspace (ie. a number of variables)
     */
    static Subspace generateRandomHyperplaneWithIntegerNorm(int DIM)
    {
        int[] params = Utility.randNTupleUniform(0,3,DIM);
        Fraction[] coeffs = new Fraction[DIM+1];

        coeffs[0] = new Fraction(params[0]*params[0]);
        for (int i=1; i<=DIM-1; i++) {
            coeffs[0] = coeffs[0].subtract(new Fraction(params[i]*params[i]));
            coeffs[i] = new Fraction(2*params[0]*params[i]);
        }
        coeffs[DIM] = new Fraction(Utility.randInt(-8,8),1);

        return new Subspace(new Matrix(coeffs,1,DIM+1));
    }

    /**
     * Generates a random subspace in general form.
     * 
     * @param dim dimension of the subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     * 
     * @throws IndexOutOfBoundsException throws an exception if <code>dim</code> is not from [0, <code>DIM</code>)
     */
    static Subspace generateRandomGeneralSubspace(int dim, int DIM)
    {
        if ((dim<0) || (dim>=DIM)) throw new IndexOutOfBoundsException("Dimension of a subspace must be at least zero and at most DIM-1!");

        Subspace randomSubspace = generateRandomHyperplane(DIM);
        Matrix aM = new Matrix();

        // now we generate hyperplanes, until we have a subspace of dimension dim (this isn't necessarily DIM-dim hyperplanes, as some hyperplanes might be dependent)
        while (randomSubspace.getDim(false) != dim) {
            aM = randomSubspace.getAugMatrix();
            aM.mergeFromBottom(generateRandomHyperplane(DIM).getAugMatrix());
            randomSubspace = new Subspace(aM);
        }
        return randomSubspace;
    }

    /**
     * Generates a random subspace in parametric form.
     * 
     * @param dim dimension of the subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     * 
     * @throws IndexOutOfBoundsException throws an exception if <code>dim</code> is not from [0, <code>DIM</code>)
     */
    static Subspace generateRandomParametricSubspace(int dim, int DIM)
    {
        if ((dim<0) || (dim>DIM)) throw new IndexOutOfBoundsException("Dimension of a subspace must be at least zero and at most DIM!");

        Point randomPoint = Point.generateRandomPoint(DIM);
        Point[] points = new Point[1];
        points[0] = randomPoint;
        Vector[] vectors = new Vector[0];
        Subspace randomSubspace = new Subspace(points, vectors);

        // now we generate vectors, until we have a subspace of dimension dim (this isn't necessarily dim vectors, as some vectors might be dependent)
        while (randomSubspace.getDim(false) != dim) {
            randomSubspace.vecMatrix.mergeFromBottom(generateRandomHyperplane(DIM-1).getAugMatrix());
        }   

        return randomSubspace;
    }

    /**
     * Generate two random subspaces in parametric form with given dimension of intersection.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     * @param intersectionDim dimension of the intersection 
     */
    static Subspace[] generateRandomSubspacesWithGivenIntersectionDimension(int dim1, int dim2, int DIM, int intersectionDim) {
        Subspace intersection = generateRandomParametricSubspace(intersectionDim, DIM);
        return generateRandomSubspacesWithGivenIntersection(dim1, dim2, DIM, intersection);
    }

    /**
     * Generate two random subspaces in parametric form with given intersection.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     * @param intersection intersection of subspaces 
     */
    static Subspace[] generateRandomSubspacesWithGivenIntersection(int dim1, int dim2, int DIM, Subspace intersection) {
        int intersectionDim = intersection.getDim();
        intersection.checkParametricForm();

        Vector[] vectorField = intersection.getVecMatrix().generateRandomLinearyIndependentVectors(dim1+dim2-2*intersectionDim, DIM);

        Subspace alpha = new Subspace(), beta = new Subspace();
        Vector[] alphaVecField = new Vector[dim1];
        Point[] alphaPointField = new Point[1];
        alphaPointField[0]=intersection.getRefPoint();
        Vector[] betaVecField = new Vector[dim2];
        Point[] betaPointField = new Point[1];
        betaPointField[0] = intersection.getRefPoint();
        for(int i=0; i < intersectionDim; i++){
            alphaVecField[i]=intersection.getVecMatrix().rowsToVectors()[i];
            betaVecField[i]=intersection.getVecMatrix().rowsToVectors()[i];
        }

        for(int i=intersectionDim; i < dim1; i++){
            alphaVecField[i]=vectorField[i-intersectionDim];
        }

        for(int i=intersectionDim; i < dim2; i++){
            betaVecField[i]=vectorField[i+dim1-2*intersectionDim];
        }

        alpha=new Subspace(alphaPointField,alphaVecField);
        beta=new Subspace(betaPointField,betaVecField);
        alphaPointField[0]= alpha.getRandomPoint();
        betaPointField[0] = beta.getRandomPoint();
        alpha=new Subspace(alphaPointField,alphaVecField);
        beta=new Subspace(betaPointField,betaVecField);

        alpha.shuffle2();
        beta.shuffle2();

        Subspace[] output = new Subspace[2];
        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Generates two random subspaces in parmetric form. The subspaces are parallel and disjiont.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     */
    static Subspace[] generateRandomParallelDisjointSubspaces(int dim1, int dim2, int DIM) {
        Subspace beta = new Subspace();
        Subspace alpha = new Subspace();

        if(dim1<dim2){
            alpha = generateRandomParametricSubspace(dim1, DIM);
            Vector[] vectorField = alpha.getVecMatrix().generateRandomLinearyIndependentVectors(1+dim2-dim1, DIM);

            Vector[] betaVecField = new Vector[dim2];
            Point[] betaPointField = new Point[1];
            betaPointField[0]= new Point(alpha.getRefPoint(), vectorField[0]);

            for(int i=0; i < dim1; i++){
                betaVecField[i]=alpha.getVecMatrix().rowsToVectors()[i];
            }

            for(int i=0; i < dim2-dim1; i++){
                betaVecField[i+dim1]=vectorField[i+1];
            }
            beta=new Subspace(betaPointField,betaVecField);
        }else{
            beta = generateRandomParametricSubspace(dim2, DIM);
            Vector[] vectorField = beta.getVecMatrix().generateRandomLinearyIndependentVectors(1+dim1-dim2, DIM);

            Vector[] alphaVecField = new Vector[dim1];
            Point[] alphaPointField = new Point[1];
            alphaPointField[0]= new Point(beta.getRefPoint(), vectorField[0]);

            for(int i=0; i < dim2; i++){
                alphaVecField[i]=beta.getVecMatrix().rowsToVectors()[i];
            }

            for(int i=0; i < dim1-dim2; i++){
                alphaVecField[i+dim2]=vectorField[i+1];
            }
            alpha=new Subspace(alphaPointField,alphaVecField);
        }

        alpha.shuffle2();
        beta.shuffle2();

        Subspace[] output = new Subspace[2];
        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Generate two random skew subspaces.
     * 
     * @param dim1 dimension of the first subspace (must be from [1, <code>DIM-2</code>])
     * @param dim2 dimension of the second subspace (must be from [1, <code>DIM-2</code>])
     * @param DIM dimension of the superspace
     * @param dimInt dimension of the intersection of difference spaces
     */
    static Subspace[] generateRandomSkewSubspaces(int dim1, int dim2, int DIM, int dimInt) {
        Subspace intersection = generateRandomParametricSubspace(dimInt, DIM);

        Vector[] vectorField = intersection.getVecMatrix().generateRandomLinearyIndependentVectors(dim1+dim2+1-2*dimInt, DIM);

        Vector[] betaVecField = new Vector[dim2];
        Point[] betaPointField = new Point[1];
        betaPointField[0]= new Point(intersection.getRefPoint(), vectorField[0]);

        Vector[] alphaVecField = new Vector[dim1];
        Point[] alphaPointField = new Point[1];
        alphaPointField[0] = intersection.getRefPoint();

        for(int i=0; i < dimInt; i++){
            betaVecField[i]=intersection.getVecMatrix().rowsToVectors()[i];
            alphaVecField[i]=intersection.getVecMatrix().rowsToVectors()[i];
        }

        for(int i=0; i < dim1-dimInt; i++){
            alphaVecField[dimInt+i]=vectorField[i+1];
        }

        for(int i=0; i < dim2-dimInt; i++){
            betaVecField[dimInt+i]=vectorField[i+1+dim1-dimInt];
        }

        Subspace alpha=new Subspace(alphaPointField,alphaVecField);
        Subspace beta=new Subspace(betaPointField,betaVecField);

        alpha.shuffle2();
        beta.shuffle2();

        Subspace[] output = new Subspace[2];
        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Generates two random subspaces in general form. The subspaces satisfy that their distance is a given number.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     * @param dist distance of the subspaces
     * 
     * @throws IndexOutOfBoundsException throws an exception if <code>dim1</code> or <code>dim2</code> is not from [0, <code>DIM</code>)
     */
    static Subspace[] generateRandomSubspacesWithGivenDistance(int dim1, int dim2, int DIM, Fraction dist)
    {
        if ((dim1<0) || (dim1>=DIM) || (dim2<0) || (dim2>=DIM)) throw new IndexOutOfBoundsException("Dimension of a subspace must be at least zero and at most DIM-1!");

        // first, we generate a hyperplane with normal vector with integer length - that will allow us to select their position so that their distance is given integer
        Subspace tempHyperplane = generateRandomHyperplaneWithIntegerNorm(DIM);
        Vector[] nField = tempHyperplane.getAugMatrix().getSystemCoeffMatrix().rowsToVectors();
        Vector n = new Vector(DIM);
        n.copy(nField[0]);
        SquareRootFraction normSRF = n.length();
        Fraction norm = normSRF.convertToFraction();
        boolean plusMinus = Utility.randBool();
        if (!plusMinus) norm = norm.multiply(NEG);

        Point A = Point.generateRandomPoint(DIM);
        Fraction t = dist.divide(norm);
        Vector tn = n.multiply(t);
        Point B = new Point(A, tn);

        Subspace alpha = new Subspace();
        Subspace beta = new Subspace();
        alpha.copy(tempHyperplane);
        beta.copy(tempHyperplane);

        Fraction absValA = new Fraction(0);
        Fraction absValB = new Fraction(0);
        for (int i=1; i<=DIM; i++) {
            absValA = absValA.subtract(tempHyperplane.getAugMatrix().get(1,i).multiply(A.get(i)));
            absValB = absValB.subtract(tempHyperplane.getAugMatrix().get(1,i).multiply(B.get(i)));
        }
        alpha.augMatrix.set(absValA, 1, DIM+1);
        beta.augMatrix.set(absValB, 1, DIM+1);

        /* Now we generate hyperplanes, until we have a subspace of dimension dim (this isn't necessarily DIM-dim hyperplanes, as some hyperplanes might be dependent).
         * The hyperplanes are required to go through point A, or B respectively.
         */
        Matrix aM = new Matrix();      
        if (dim1 == 0) {
            Point[] points = new Point[1];
            points[0] = new Point();
            points[0].copy(A);
            Vector[] vectors = new Vector[0];
            alpha = new Subspace(points, vectors);
        }
        else {
            while (alpha.getDim() != dim1) {
                aM = alpha.getAugMatrix();
                tempHyperplane = generateRandomHyperplane(DIM);
                absValA = new Fraction(0);
                for (int i=1; i<=DIM; i++) absValA = absValA.subtract(tempHyperplane.getAugMatrix().get(1,i).multiply(A.get(i)));
                tempHyperplane.augMatrix.set(absValA, 1, DIM+1);
                aM.mergeFromBottom(tempHyperplane.getAugMatrix());
                alpha = new Subspace(aM);
            }
            alpha.shuffle();
        }

        if (dim2 == 0) {
            Point[] points = new Point[1];
            points[0] = new Point();
            points[0].copy(B);
            Vector[] vectors = new Vector[0];
            beta = new Subspace(points, vectors);
        }
        else {
            while (beta.getDim() != dim2) {
                aM = beta.getAugMatrix();
                tempHyperplane = generateRandomHyperplane(DIM);
                absValB = new Fraction(0);
                for (int i=1; i<=DIM; i++) absValB = absValB.subtract(tempHyperplane.getAugMatrix().get(1,i).multiply(B.get(i)));
                tempHyperplane.augMatrix.set(absValB, 1, DIM+1);
                aM.mergeFromBottom(tempHyperplane.getAugMatrix());
                beta = new Subspace(aM);
            }
            beta.shuffle();
        }

        Subspace[] output = new Subspace[2];
        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Generates two random subspaces in parametric form. The subspaces satisfy that their angle is a given number.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     * @param angle angle of the subspaces
     * 
     * @throws IndexOutOfBoundsException throws an exception if <code>dim1</code> or <code>dim2</code> is not from [0, <code>DIM</code>)
     */
    static Subspace[] generateRandomSubspacesWithGivenAngle(int dim1, int dim2, int DIM, PiFraction angle)
    {
        if ((dim1<=0) || (dim1>=DIM) || (dim2<=0) || (dim2>=DIM)) throw new IndexOutOfBoundsException("Dimension of a subspace must be at least one and at most DIM-1!");
        if (angle.isEqual(new PiFraction(0))) return generateRandomParallelSubspaces(dim1, dim2, DIM);
        if (angle.isEqual(new PiFraction(1,2))) return generateRandomPerpendicularSubspaces(dim1, dim2, DIM);

        Vector[] originalLines = Vector.generateRandomVectorsWithGivenAngle(angle, DIM);

        int howManyVectors = dim1 + dim2 - DIM;
        howManyVectors = Math.max(dim1 + dim2 - DIM,0);
        int howManyRandomVectors;
        if (Utility.randIntUniform(1, 3) == 1) {
            howManyRandomVectors = Utility.randIntUniform(0, Math.min(dim1, dim2) - howManyVectors - 1);
        }
        else howManyRandomVectors = 0;
        howManyVectors += howManyRandomVectors;

        Point[] randomPoints = new Point[1];
        randomPoints[0] = new Point(DIM);
        Vector[] availableVectors, tempA, tempB;
        availableVectors = new Vector[2];
        availableVectors[0] = new Vector();
        availableVectors[0].copy(originalLines[0]);
        availableVectors[1] = new Vector();
        availableVectors[1].copy(originalLines[1]);
        Subspace spaceUnion, orthCompl;
        Vector tempVector;
        for (int i = 1; i <= howManyVectors; i++) {
            spaceUnion = new Subspace(randomPoints, availableVectors);
            orthCompl = spaceUnion.orthogonalSupplement('V',false);
            orthCompl.checkParametricForm();
            tempVector = new Vector();
            tempVector = orthCompl.vecMatrix.rowsToVectors()[0];
            tempA = new Vector[1+i];
            for (int j = 0; j < 1+i; j++) {
                tempA[j] = new Vector();
                tempA[j].copy(availableVectors[j]);
            }
            availableVectors = new Vector[2+i];
            for (int j = 0; j < 1+i; j++) {
                availableVectors[j] = new Vector();
                availableVectors[j].copy(tempA[j]);
            }
            availableVectors[1+i] = new Vector();
            availableVectors[1+i].copy(tempVector);
        }

        randomPoints = new Point[1];
        randomPoints[0] = Point.generateRandomPoint(DIM);
        Vector[] alphaVectors = new Vector[1 + howManyVectors];
        alphaVectors[0] = new Vector();
        alphaVectors[0].copy(availableVectors[0]);
        for (int i = 2; i <= 1 + howManyVectors; i++) {
            alphaVectors[i-1] = new Vector();
            alphaVectors[i-1].copy(availableVectors[i]);
        }
        Subspace alpha = new Subspace(randomPoints, alphaVectors);

        int next = alpha.getDim();
        int allVectors = 2 + howManyVectors;
        while (next < dim1) {
            tempA = new Vector[next];
            tempB = new Vector[allVectors];
            for (int i = 0; i < next; i++) {
                tempA[i] = new Vector();
                tempA[i].copy(alphaVectors[i]);
            }
            for (int i = 0; i < allVectors; i++) {
                tempB[i] = new Vector();
                tempB[i].copy(availableVectors[i]);
            }

            spaceUnion = new Subspace(randomPoints, availableVectors);
            orthCompl = spaceUnion.orthogonalSupplement('V',false);
            orthCompl.checkParametricForm();
            tempVector = new Vector();
            tempVector = orthCompl.vecMatrix.rowsToVectors()[0];

            next++;
            allVectors++;
            alphaVectors = new Vector[next];
            availableVectors = new Vector[allVectors];
            for (int i = 0; i < next-1; i++) {
                alphaVectors[i] = new Vector();
                alphaVectors[i].copy(tempA[i]);
            }
            for (int i = 0; i < allVectors-1; i++) {
                availableVectors[i] = new Vector();
                availableVectors[i].copy(tempB[i]);
            }
            alphaVectors[next-1] = new Vector();
            alphaVectors[next-1].copy(tempVector);
            availableVectors[allVectors-1] = new Vector();
            availableVectors[allVectors-1].copy(tempVector);
        }
        alpha = new Subspace(randomPoints, alphaVectors);

        randomPoints[0] = Point.generateRandomPoint(DIM);
        Vector[] betaVectors = new Vector[1 + howManyVectors];
        betaVectors[0] = new Vector();
        betaVectors[0].copy(availableVectors[1]);
        for (int i = 2; i <= 1 + howManyVectors; i++) {
            betaVectors[i-1] = new Vector();
            betaVectors[i-1].copy(availableVectors[i]);
        }
        Subspace beta = new Subspace(randomPoints, betaVectors);

        next = beta.getDim();
        while (next < dim2) {
            tempA = new Vector[next];
            tempB = new Vector[allVectors];
            for (int i = 0; i < next; i++) {
                tempA[i] = new Vector();
                tempA[i].copy(betaVectors[i]);
            }
            for (int i = 0; i < allVectors; i++) {
                tempB[i] = new Vector();
                tempB[i].copy(availableVectors[i]);
            }

            spaceUnion = new Subspace(randomPoints, availableVectors);
            orthCompl = spaceUnion.orthogonalSupplement('V',false);
            orthCompl.checkParametricForm();
            tempVector = new Vector();
            tempVector = orthCompl.vecMatrix.rowsToVectors()[0];

            next++;
            allVectors++;
            betaVectors = new Vector[next];
            availableVectors = new Vector[allVectors];
            for (int i = 0; i < next-1; i++) {
                betaVectors[i] = new Vector();
                betaVectors[i].copy(tempA[i]);
            }
            for (int i = 0; i < allVectors-1; i++) {
                availableVectors[i] = new Vector();
                availableVectors[i].copy(tempB[i]);
            }
            betaVectors[next-1] = new Vector();
            betaVectors[next-1].copy(tempVector);
            availableVectors[allVectors-1] = new Vector();
            availableVectors[allVectors-1].copy(tempVector);
        }
        beta = new Subspace(randomPoints, betaVectors);

        alpha.shuffle();
        beta.shuffle();

        Subspace[] output = new Subspace[2];
        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Generates two random subspaces in general form. The subspaces are parallel.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     */
    private static Subspace[] generateRandomParallelSubspaces(int dim1, int dim2, int DIM) {
        Subspace alpha = new Subspace(), beta = new Subspace();
        if (dim1 > dim2) {
            beta = generateRandomGeneralSubspace(dim2, DIM);
            alpha.copy(beta);
            alpha.augMatrix.copy(alpha.augMatrix.submatrix(1, DIM-dim1, 1, DIM+1));
            for (int i = 1; i <= DIM-dim1; i++) {
                Fraction tempFrac = new Fraction(Utility.randInt(-20, 20));
                alpha.augMatrix.set(tempFrac, i, DIM+1);
            }
        }
        else {
            alpha = generateRandomGeneralSubspace(dim1, DIM);
            beta.copy(alpha);
            beta.augMatrix.copy(beta.augMatrix.submatrix(1, DIM-dim2, 1, DIM+1));
            for (int i = 1; i <= DIM-dim2; i++) {
                Fraction tempFrac = new Fraction(Utility.randInt(-20, 20));
                beta.augMatrix.set(tempFrac, i, DIM+1);
            }
        }

        alpha.shuffle();
        beta.shuffle();

        Subspace[] output = new Subspace[2];
        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Generates two random subspaces in general form. The subspaces are perpendicular.
     * 
     * @param dim1 dimension of the first subspace (must be from [0, <code>DIM</code>))
     * @param dim2 dimension of the second subspace (must be from [0, <code>DIM</code>))
     * @param DIM dimension of the superspace
     */
    private static Subspace[] generateRandomPerpendicularSubspaces(int dim1, int dim2, int DIM) {
        Subspace alpha = new Subspace(), beta = new Subspace();
        Subspace[] output = new Subspace[2];

        if (dim1 + dim2 > DIM) {
            int dimUnion = dim1 + dim2 - DIM;
            output = generateRandomPerpendicularSubspaces(dim1 - dimUnion, dim2 - dimUnion, DIM);
            alpha.copy(output[0]);
            beta.copy(output[1]);

            Point[] randomPoints = new Point[1];
            randomPoints[0] = new Point();
            Point[] alphaPoints = new Point[1];
            alphaPoints[0] = new Point();
            alphaPoints[0].copy(alpha.refPoint);
            Point[] betaPoints = new Point[1];
            betaPoints[0] = new Point();
            betaPoints[0].copy(beta.refPoint);
            int howManyVectors = dim1 + dim2 - 2*dimUnion;
            Vector[] availableVectors = new Vector[howManyVectors];
            Vector[] alphaVectors = alpha.vecMatrix.rowsToVectors();
            Vector[] betaVectors = beta.vecMatrix.rowsToVectors();
            for (int i = 1; i <= dim1 - dimUnion; i++) {
                availableVectors[i-1] = alphaVectors[i-1];
            }
            for (int i = dim1 - dimUnion + 1, j = 1; i <= howManyVectors; i++, j++) {
                availableVectors[i-1] = betaVectors[j-1];
            }

            for (int i = 1; i <= dimUnion; i++) {
                Subspace spaceUnion = new Subspace(randomPoints, availableVectors);
                Subspace orthCompl = spaceUnion.orthogonalSupplement('V',false);
                orthCompl.checkParametricForm();
                Vector tempVector = new Vector();
                tempVector = orthCompl.vecMatrix.rowsToVectors()[0];
                Vector[] tempA = new Vector[availableVectors.length];
                for (int j = 0; j < tempA.length; j++) {
                    tempA[j] = new Vector();
                    tempA[j].copy(availableVectors[j]);
                }
                availableVectors = new Vector[availableVectors.length+1];
                for (int j = 0; j < tempA.length; j++) {
                    availableVectors[j] = new Vector();
                    availableVectors[j].copy(tempA[j]);
                }
                availableVectors[availableVectors.length-1] = new Vector();
                availableVectors[availableVectors.length-1].copy(tempVector);

                tempA = new Vector[alphaVectors.length];
                for (int j = 0; j < tempA.length; j++) {
                    tempA[j] = new Vector();
                    tempA[j].copy(alphaVectors[j]);
                }
                alphaVectors = new Vector[alphaVectors.length+1];
                for (int j = 0; j < tempA.length; j++) {
                    alphaVectors[j] = new Vector();
                    alphaVectors[j].copy(tempA[j]);
                }
                alphaVectors[alphaVectors.length-1] = new Vector();
                alphaVectors[alphaVectors.length-1].copy(tempVector);

                tempA = new Vector[betaVectors.length];
                for (int j = 0; j < tempA.length; j++) {
                    tempA[j] = new Vector();
                    tempA[j].copy(betaVectors[j]);
                }
                betaVectors = new Vector[betaVectors.length+1];
                for (int j = 0; j < tempA.length; j++) {
                    betaVectors[j] = new Vector();
                    betaVectors[j].copy(tempA[j]);
                }
                betaVectors[betaVectors.length-1] = new Vector();
                betaVectors[betaVectors.length-1].copy(tempVector);

                alpha = new Subspace(alphaPoints, alphaVectors);
                beta = new Subspace(betaPoints, betaVectors);
            }
        }
        else {
            if (dim1 > dim2) {
                beta = generateRandomParametricSubspace(dim2, DIM);
                alpha = beta.orthogonalSupplement('V',false);
                alpha.checkParametricForm();
                alpha.refPoint = Point.generateRandomPoint(DIM);
                alpha.vecMatrix.copy(alpha.vecMatrix.submatrix(1, dim1, 1, DIM));
                alpha.generalFormAvailable = false;
                alpha.inputInGeneralForm = false;
            }
            else {
                alpha = generateRandomParametricSubspace(dim1, DIM);
                beta = alpha.orthogonalSupplement('V',false);
                beta.checkParametricForm();
                beta.refPoint = Point.generateRandomPoint(DIM);
                beta.vecMatrix.copy(beta.vecMatrix.submatrix(1, dim2, 1, DIM));
                beta.generalFormAvailable = false;
                beta.inputInGeneralForm = false;
            }
        }

        alpha.shuffle();
        beta.shuffle();

        output[0] = new Subspace();
        output[0].copy(alpha);
        output[1] = new Subspace();
        output[1].copy(beta);
        return output;
    }

    /**
     * Changes the appearance of the subspace without modifying what the subspace is. That is, to some rows of governing matrices this will add multiple of other rows and then
     * reorder the rows randomly.
     */
    private void shuffle() {
        if (this.getOriginalForm()) this.shuffleGeneral();
        else                        this.shuffleParametric();
    }

    /**
     * Changes the appearance of the subspace in general form without modifying what the subspace is. That is, to some rows of augmented matrix this will add multiple of another row
     * and then reorder the rows randomly.
     */
    private void shuffleGeneral() {
        int randRow, randMultiple, dim = this.getDim(), DIM = this.getDIM();
        boolean plusMinus = Utility.randBool();

        // we cannot modify a hyperplane
        if (DIM - dim == 1) return;

        // we always modify the first row
        randRow = Utility.randIntUniform(2,DIM-dim);
        randMultiple = plusMinus ? Utility.randIntUniform(1,3) : Utility.randIntUniform(-3,-1);
        this.augMatrix.addRow(1, randRow, new Fraction(randMultiple));

        // we reorder the rows randomly
        Fraction[] tempFracArray = new Fraction[DIM-dim];
        int[] randomPermutation = Utility.generatePermutation(DIM-dim);
        for (int i = 1; i<=DIM-dim; i++) tempFracArray[i-1] = new Fraction(randomPermutation[i-1]);
        Matrix P = new Matrix(tempFracArray, 1, DIM-dim); 

        for (int i = 1; i<P.getC(); i++) {
            int j = (int) P.get(1,i).n;
            if (j != i) {
                P.swapColumns(i,j);
                this.augMatrix.swapRows(i,j);
                i--;
            }
        }

        // then maybe modify some more rows
        for (int i = 1; i < DIM-dim; i++) {
            randRow = Utility.randIntUniform(i+1,DIM-dim);
            randMultiple = Utility.randInt(-4,4);
            this.augMatrix.addRow(i, randRow, new Fraction(randMultiple));
        }

        this.beautifyGeneral();
    }

    /**
     * Changes the appearance of the subspace in parametric form without modifying what the subspace is. That is, to some rows of matrix of vectors this will add multiple of another
     * row and then reorder the rows randomly.
     */
    private void shuffleParametric() {
        int randRow, randMultiple, dim = this.getDim(), DIM = this.getDIM();
        boolean plusMinus = Utility.randBool();

        // we cannot modify a line or point
        if (dim <= 1) return;

        // we always modify the first row
        randRow = Utility.randIntUniform(2,dim);
        randMultiple = plusMinus ? Utility.randIntUniform(1,3) : Utility.randIntUniform(-3,-1);
        this.vecMatrix.addRow(1, randRow, new Fraction(randMultiple));

        // we reorder the rows randomly
        Fraction[] tempFracArray = new Fraction[dim];
        int[] randomPermutation = Utility.generatePermutation(dim);
        for (int i = 1; i<=dim; i++) tempFracArray[i-1] = new Fraction(randomPermutation[i-1]);
        Matrix P = new Matrix(tempFracArray, 1, dim); 

        for (int i = 1; i<P.getC(); i++) {
            int j = (int) P.get(1,i).n;
            if (j != i) {
                P.swapColumns(i,j);
                this.vecMatrix.swapRows(i,j);
                i--;
            }
        }

        // then maybe modify some more rows
        for (int i = 1; i < dim; i++) {
            randRow = Utility.randIntUniform(i+1,dim);
            randMultiple = Utility.randInt(-4,4);
            this.vecMatrix.addRow(i, randRow, new Fraction(randMultiple));
        }

        this.beautifyParametric();
    }

    /**
     * Changes the appearance of the subspace in parametric form without modifying what the subspace is. That is, to some rows of matrix of vectors this will add multiple of another
     * row and then reorder the rows randomly.
     */
    private void shuffle2() {
        int randRow, randMultiple, dim = this.getDim(), DIM = this.getDIM();
        boolean plusMinus = Utility.randBool();

        // we cannot modify a line or point
        if (dim == 0) return;
       
        randMultiple = plusMinus ? Utility.randIntUniform(1,3) : Utility.randIntUniform(-3,-1);
        if(dim==1){
            this.vecMatrix.multiplyRow(1, new Fraction(randMultiple));
            return;
        }
        
        // we always modify the first row
        randRow = Utility.randIntUniform(2,dim);
        randMultiple = plusMinus ? Utility.randIntUniform(1,3) : Utility.randIntUniform(-3,-1);
        this.vecMatrix.addRow(1, randRow, new Fraction(randMultiple));

        
        
        // we reorder the rows randomly
        Fraction[] tempFracArray = new Fraction[dim];
        int[] randomPermutation = Utility.generatePermutation(dim);
        for (int i = 1; i<=dim; i++) tempFracArray[i-1] = new Fraction(randomPermutation[i-1]);
        Matrix P = new Matrix(tempFracArray, 1, dim); 

        for (int i = 1; i<P.getC(); i++) {
            int j = (int) P.get(1,i).n;
            if (j != i) {
                P.swapColumns(i,j);
                this.vecMatrix.swapRows(i,j);
                i--;
            }
        }

        // then maybe modify some more rows
        for (int i = 1; i < dim; i++) {
            randRow = Utility.randIntUniform(i+1,dim);
            randMultiple = Utility.randInt(-4,4);
            this.vecMatrix.addRow(i, randRow, new Fraction(randMultiple));
        }

        this.beautifyParametric();
    }

    /**
     * Prints general form of this subspace formatted as text.
     * 
     * @return indented string formatted to general form
     */

    String toTextGeneral() {
        return toTextGeneral(true, 'x');
    }

    /**
     * Prints general form of this subspace formatted as text.
     * 
     * @param verbose determines if the output will be indented
     * 
     * @return string formatted to general form
     */

    String toTextGeneral(boolean verbose) {
        return toTextGeneral(verbose, 'x');
    }

    /**
     * Prints general form of this subspace formatted as text.
     * 
     * @param verbose determines if the output will be indented
     * @param variable label of the variables
     * 
     * @return string formatted to general form
     */
    String toTextGeneral(boolean verbose, char variable) {
        String out = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        for(int i=0; i<augMatrix.getR(); i++){
            if (verbose) out += "\t";
            firstTerm = true;
            for(int j=0; j<(augMatrix.getC() - 1) ; j++){
                temp = augMatrix.get(i+1,j+1);
                if (temp.compare(0)==-1) {
                    if (temp.isEqual(-1))
                        out += "-" + variable + "_" + String.valueOf(j+1);
                    else
                        out += temp.toText() + variable + "_" + String.valueOf(j+1);
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) out += "+";
                    if (temp.isEqual(1))
                        out += variable + "_" + String.valueOf(j+1);
                    else
                        out += temp.toText() + variable + "_" + String.valueOf(j+1);
                    firstTerm = false;
                }
            }

            temp = augMatrix.get(i+1, augMatrix.getC());
            if ((temp.compare(0)==-1) || firstTerm) {
                out += temp.toText();
            }
            else if ((temp.compare(0)==1)) {
                out += "+" + temp.toText();
            }

            if (!firstTerm) out += "=0";

            if (verbose) out += "\r\n";
            else         out += "\n";
        }

        return out;
    }

    /**
     * Prints general form of this subspace formatted as text.
     * 
     * @param verbose determines if the output will be indented
     * @param variable label of the variables
     * 
     * @return string formatted to general form
     */
    String toTextGeneralOut(boolean verbose, char variable) {
        String out = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        for(int i=0; i<augMatrix.getR(); i++){
            if (verbose) out += "\t";
            firstTerm = true;
            for(int j=0; j<(augMatrix.getC() - 1) ; j++){
                temp = augMatrix.get(i+1,j+1);
                if (temp.compare(0)==-1) {
                    if (temp.isEqual(-1))
                        out += "-" + variable + "_" + String.valueOf(j+1);
                    else
                        out += temp.toText() + variable + "_" + String.valueOf(j+1);
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) out += "+";
                    if (temp.isEqual(1))
                        out += variable + "_" + String.valueOf(j+1);
                    else
                        out += temp.toText() + variable + "_" + String.valueOf(j+1);
                    firstTerm = false;
                }
            }

            if(this.getAugMatrix().transpose().rowsToVectors()[DIM-1].isZeroVector()){
                out += "+0x_"+String.valueOf(DIM);
            }
            
            temp = augMatrix.get(i+1, augMatrix.getC());
            if ((temp.compare(0)==-1) || firstTerm) {
                out += temp.toText();
            }
            else if ((temp.compare(0)==1)) {
                out += "+" + temp.toText();
            }
            
            if (!firstTerm) out += "=0";

            if (verbose) out += "\r\n";
            else         out += "\n";
        }

        return out;
    }
    
    /**
     * Alternative metod for toTexGeneral with multiple wariable. Prints general form of this subspace formatted as TeX code.
     * 
     * @param variable label of the variables
     * @param varChange number of elements with variable t
     * 
     * @return string formatted to general form
     */
    String toTextGeneral2(boolean verbose, char variable, int varChange) {
        String out = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        for(int i=0; i<augMatrix.getR(); i++){
            if (verbose) out += "\t";
            firstTerm = true;
            for(int j=0; j<(augMatrix.getC() - 1) ; j++){
                temp = augMatrix.get(i+1,j+1);
                if (temp.compare(0)==-1) {
                    if (temp.isEqual(-1))
                        out += "-" + (j+1>varChange ? variable : "t") + "_" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1);
                    else
                        out += temp.toText() + (j+1>varChange ? variable : "t") + "_" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1);
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) out += "+";
                    if (temp.isEqual(1))
                        out += (j+1>varChange ? variable : "t") + "_" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1);
                    else
                        out += temp.toText() + (j+1>varChange ? variable : "t") + "_" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1);
                    firstTerm = false;
                }
            }

            temp = augMatrix.get(i+1, augMatrix.getC());
            if ((temp.compare(0)==-1) || firstTerm) {
                out += temp.toText();
            }
            else if ((temp.compare(0)==1)) {
                out += "+" + temp.toText();
            }

            if (!firstTerm) out += "=0";

            if (verbose) out += "\r\n";
            else         out += "\n";
        }

        return out;
    }

    /**
     * Prints general form of this subspace formatted as TeX code.
     * 
     * @return string formatted to general form
     */

    String toTeXGeneral(){
        return toTeXGeneral('x');
    }

    /**
     * Prints general form of this subspace formatted as TeX code.
     * 
     * @param variable label of the variables
     * 
     * @return string formatted to general form
     */

    String toTeXGeneral(char variable){
        String out = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        out += "\\begin{array}{*{" + String.valueOf(augMatrix.getC() + 1) + "}{r@{}}}\n";
        for(int i=0; i<augMatrix.getR(); i++){
            firstTerm = true;
            for(int j=0; j<(augMatrix.getC() - 1) ; j++){
                temp = augMatrix.get(i+1,j+1);
                if (temp.compare(0)==-1) {
                    if (!firstTerm) out += "{}";
                    if (temp.isEqual(-1))
                        out += "-" + variable + "_{" + String.valueOf(j+1) + "}";
                    else
                        out += temp.toTeX() + variable + "_{" + String.valueOf(j+1) + "}";
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) out += "{}+";
                    if (temp.isEqual(1))
                        out += variable + "_{" + String.valueOf(j+1) + "}";
                    else
                        out += temp.toTeX() + variable + "_{" + String.valueOf(j+1) + "}";
                    firstTerm = false;
                }
                out += " & ";
            }

            temp = augMatrix.get(i+1, augMatrix.getC());
            if ((temp.compare(0)==-1) || firstTerm) {
                out += "{}" + temp.toTeX();
            }
            else if ((temp.compare(0)==1)) {
                out += "{}+" + temp.toTeX();
            }

            if (!firstTerm)
                out += " & {}=0\\\\\n";
            else
                out += " \\\\\n";
        }
        out += "\\end{array}\n";

        return out;
    }

    /**
     * Alternative metod for toTexGeneral with multiple wariable. Prints general form of this subspace formatted as TeX code.
     * 
     * @param variable label of the variables
     * @param varChange number of elements with variable t
     * 
     * @return string formatted to general form
     */

    String toTeXGeneral2(char variable, int varChange){
        String out = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        out += "\\begin{array}{*{" + String.valueOf(augMatrix.getC() + 1) + "}{r@{}}}\n";
        for(int i=0; i<augMatrix.getR(); i++){
            firstTerm = true;
            for(int j=0; j<(augMatrix.getC() - 1) ; j++){
                temp = augMatrix.get(i+1,j+1);
                if (temp.compare(0)==-1) {
                    if (!firstTerm) out += "{}";
                    if (temp.isEqual(-1))
                        out += "-" + (j+1>varChange ? variable : "t") + "_{" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1) + "}";
                    else
                        out += temp.toTeX() + (j+1>varChange ? variable : "t") + "_{" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1) + "}";
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) out += "{}+";
                    if (temp.isEqual(1))
                        out += (j+1>varChange ? variable : "t") + "_{" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1) + "}";
                    else
                        out += temp.toTeX() + (j+1>varChange ? variable : "t") + "_{" + String.valueOf(j+1 > varChange ? j+1-varChange : j+1) + "}";
                    firstTerm = false;
                }
                out += " & ";
            }

            temp = augMatrix.get(i+1, augMatrix.getC());
            if ((temp.compare(0)==-1) || firstTerm) {
                out += "{}" + temp.toTeX();
            }
            else if ((temp.compare(0)==1)) {
                out += "{}+" + temp.toTeX();
            }

            if (!firstTerm)
                out += " & {}=0\\\\\n";
            else
                out += " \\\\\n";
        }
        out += "\\end{array}\n";

        return out;
    }

    /**
     * Prints parametric form of this subspace formatted as text.
     * 
     * @return string formatted to parametric form containing also “<i>x<sub>i</sub></i> = ”
     */
    String toTextParametric() {
        return toTextParametric(true, 'x', 't');
    }

    /**
     * Prints parametric form of this subspace formatted as text.
     * 
     * @param x name of variable
     * @param t name of variable
     * 
     * @return string formatted to parametric form containing also “<i>x<sub>i</sub></i> = ”
     */
    String toTextParametric(boolean verbose) {
        return toTextParametric(verbose, 'x', 't');
    }

    /**
     * Prints parametric form of this subspace formatted as text.
     * 
     * @param verbose determines if the result shall contain full output (containing “<i>x<sub>i</sub></i> = ”) = true; or short output (omitting “<i>x<sub>i</sub></i> = ”) = false
     * @param x name of variable
     * @param t name of variable
     * 
     * @return string formatted to parametric form
     */
    String toTextParametric(boolean verbose, char x, char t) {
        // a point in input form:
        this.vecMatrix.deleteZeroRows();
        if (this.vecMatrix.getR() < 1 && !verbose) return this.refPoint.toText();

        // anything else:
        String out = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        for(int i=1; i<=DIM; i++){
            out += (verbose ? "\t" + x + "_" + i + " = " : "") ;
            firstTerm = true;

            temp = refPoint.get(i);
            if (!(temp.isEqual(0))) {
                out += temp.toText();
                firstTerm = false;
            }

            for(int j=1; j<=vecMatrix.getR(); j++){
                temp = vecMatrix.get(j,i);
                if (temp.compare(0)==-1) {
                    if (temp.isEqual(-1))
                        out += "-"+t+"_" + String.valueOf(j);
                    else
                        out += temp.toText() + t + "_" + String.valueOf(j);
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) out += "+";
                    if (temp.isEqual(1))
                        out += t + "_" + String.valueOf(j);
                    else
                        out += temp.toText() + t + "_" + String.valueOf(j);
                    firstTerm = false;
                }
            }

            if (firstTerm) out += "0";
            if (verbose) out += "\r\n";
            else         out += "\n";
        }

        return out;
    }

    /**
     * Prints parametric form of this subspace formatted as TeX code.
     * 
     * @return string formatted to parametric form
     */

    String toTeXParametric(){
        return toTeXParametric('x', 't');
    }

    /**
     * Prints parametric form of this subspace formatted as TeX code.
     * 
     * @param x name of variable
     * @param t name of variable
     * 
     * @return string formatted to parametric form
     */
    String toTeXParametric(char x, char t){
        return toTeXParametric(true, x, t);
    }

    /**
     * Prints parametric form of this subspace formatted as TeX code.
     * 
     * @param verbose determines if the result shall contain full output (containing “<i>x<sub>i</sub></i> = ”) = true; or short output (omitting “<i>x<sub>i</sub></i> = ”) = false
     * @param x name of variable
     * @param t name of variable
     * 
     * @return string formatted to parametric form
     */

    String toTeXParametric(boolean verbose, char x, char t){
        String out, rowBuffer = new String();
        Fraction temp = new Fraction();
        boolean firstTerm;

        out = "\\begin{array}{*{" + String.valueOf(vecMatrix.getR()+3) + "}{r@{}}}\n";
        for(int i=1; i<=DIM; i++){
            firstTerm = true;
            out += (verbose ? x + "_{" + String.valueOf(i) + "} & {}={} &" : "");
            //out += x + "_{" + String.valueOf(i) + "} & {}={} &";
            rowBuffer = "";

            temp = refPoint.get(i);
            if (!(temp.isEqual(0))) {
                rowBuffer += temp.toTeX();
                firstTerm = false;
            }

            for(int j=1; j<=vecMatrix.getR(); j++){
                rowBuffer += " & ";
                temp = vecMatrix.get(j,i);
                if (temp.compare(0)==-1) {
                    if (!firstTerm) rowBuffer += "{}";
                    if (temp.isEqual(-1))
                        rowBuffer += "-" + t + "_" + String.valueOf(j);
                    else
                        rowBuffer += "" + temp.toTeX() + t + "_{" + String.valueOf(j) + "}";
                    firstTerm = false; 
                }
                else if (temp.compare(0)==1) {
                    if (!firstTerm) rowBuffer += "{}+";
                    if (temp.isEqual(1))
                        rowBuffer += t + "_" + String.valueOf(j);
                    else
                        rowBuffer += temp.toTeX() + t + "_{" + String.valueOf(j) + "}";
                    firstTerm = false;
                }
            }

            if (firstTerm) rowBuffer = "0" + (vecMatrix.getR() < 1 ? "" : " & ");
            out += rowBuffer + "\\\\\n";
        }
        out += "\\end{array}\n";

        return out;
    }

    /**
     * Prints parametric form of this subspace formatted as TeX code.
     * 
     * @return string formatted to parametric form
     */
    /*
    String toTeXParametric(){
    String out, rowBuffer = new String();
    Fraction temp = new Fraction();
    boolean firstTerm;

    out = "\\begin{array}{*{" + String.valueOf(vecMatrix.getR()+3) + "}{r@{}}}\n";
    for(int i=1; i<=DIM; i++){
    firstTerm = true;
    out += "x_{" + String.valueOf(i) + "} & {}={} &";
    rowBuffer = "";

    temp = refPoint.get(i);
    if (!(temp.isEqual(0))) {
    rowBuffer += temp.toTeX();
    firstTerm = false;
    }

    for(int j=1; j<=vecMatrix.getR(); j++){
    rowBuffer += " & ";
    temp = vecMatrix.get(j,i);
    if (temp.compare(0)==-1) {
    if (!firstTerm) rowBuffer += "{}";
    if (temp.isEqual(-1))
    rowBuffer += "-t_" + String.valueOf(j);
    else
    rowBuffer += "" + temp.toTeX() + "t_{" + String.valueOf(j) + "}";
    firstTerm = false; 
    }
    else if (temp.compare(0)==1) {
    if (!firstTerm) rowBuffer += "{}+";
    if (temp.isEqual(1))
    rowBuffer += "t_" + String.valueOf(j);
    else
    rowBuffer += temp.toTeX() + "t_{" + String.valueOf(j) + "}";
    firstTerm = false;
    }
    }

    if (firstTerm) rowBuffer = "0" + (vecMatrix.getR() < 1 ? "" : " & ");
    out += rowBuffer + "\\\\\n";
    }
    out += "\\end{array}\n";

    return out;
    }
     */

    /**
     * Imports {@link Utility#textOut(str) textOut(str)}.
     * 
     * @see {@link Utility#textOut(str) textOut(str)}
     */
    private static void textOut(String str)
    {
        Utility.textOut(str);
    }

    /**
     * Imports {@link Utility#texOut(str) texOut(str)}.
     * 
     * @see {@link Utility#texOut(str) texOut(str)}
     */
    private static void texOut(String str)
    {
        Utility.texOut(str);
    }
}