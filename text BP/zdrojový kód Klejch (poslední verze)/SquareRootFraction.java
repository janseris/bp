/**
 * This class introduces all square roots of fractions (SRF) and features basic necessary operations with them.
 * {@link SquareRootFraction}s are numbers in form (<i>n</i>*&radic;<i>nSqrt</i>)/<i>d</i>, where all <i>n</i>, <i>nSqrt</i>, and <i>d</i> are integers (<i>nSqrt</i> is a non-negative integer).
 * 
 * @author  Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since   2014-04-01
 */
class SquareRootFraction
{
    /** whole part of the numerator */
    long n;
    /** square-root-part of the numerator */
    long nSqrt;
    /** denominator */
    long d;

    /**
     * Constructs a {@link SquareRootFraction} from its numerator and denominator. For example, <code>SquareRootFraction(1,2)</code> yields &radic;2/2.
     * 
     * @param nn nominator
     * @param dd denominator
     * 
     * @throws IllegalArgumentException Throws an exception if the fraction is negative.
     * @throws IllegalArgumentException Throws an exception if the denominator would be zero.
     */
    SquareRootFraction(long nn, long dd)
    {
        if      ((nn < 0) && (dd < 0)) {
            nn *= -1;
            dd *= -1;
        }
        else if ((nn < 0) || (dd < 0)) {
            if (nn != 0) throw new IllegalArgumentException("I cannot take a square root of a negative number!");
        }
        else if (dd == 0) {
            throw new IllegalArgumentException("The denominator of a fraction must not be zero!");
        }
        
        // taking a partial square root
        long N = 1;
        long NSqrt = nn*dd;
        long i = (long) Math.sqrt(NSqrt);
        
        if (i>0) {
            do {
                long ii = i*i;
                if (NSqrt % ii == 0) {
                    NSqrt /= ii;
                    N *= i;
                    i = (long) Math.sqrt(NSqrt);
                }
                else i--;
            } while (i >= 2);

            long g = Utility.gcd(N,dd);
            n = N / g;
            nSqrt = NSqrt;
            d = dd / g;
        }
        
        else {
            n = 0;
            nSqrt = 1;
            d = 1;
        }
    }

    /**
     * Constructs a {@link SquareRootFraction} from a {@link Fraction}. For example, <code>SquareRootFraction(new Fraction(1,2))</code> yields &radic;2/2.
     * 
     * @param F fraction to be taken the square root of
     * 
     * @throws IllegalArgumentException Throws an exception if the fraction is negative.
     * @throws IllegalArgumentException Throws an exception if the denominator would be zero.
     */
    SquareRootFraction(Fraction F)
    {
        long nn = F.n;
        long dd = F.d;
        
        if       ((nn < 0) && (dd < 0)) {
            nn *= -1;
            dd *= -1;
        }
        
        else if ((nn < 0) || (dd < 0)) {
            if (nn !=0) throw new IllegalArgumentException("I cannot take a square root of a negative number!");
        }
        
        else if (dd == 0) {
            throw new IllegalArgumentException("The denominator of a fraction must not be zero!");
        }
        
        // taking a partial square root
        long N = 1;
        long NSqrt = nn*dd;
        long i = (long) Math.sqrt(NSqrt);
        
        if (i>0) {
            do {
                long ii = i*i;
                if (NSqrt % ii == 0) {
                    NSqrt /= ii;
                    N *= i;
                    i = (long) Math.sqrt(NSqrt);
                }
                else i--;
            } while (i >= 2);

            long g = Utility.gcd(N,dd);
            n = N / g;
            nSqrt = NSqrt;
            d = dd / g;
        }
        
        else {
            n = 0;
            nSqrt = 1;
            d = 1;
        }
    } 

    /**
     * Constructs a {@link SquareRootFraction} from an integer. For example, <code>SquareRootFraction(2)</code> yields &radic;2.
     * 
     * @param nn integer to be taken a square root of
     * 
     * @throws IllegalArgumentException Throws an exception if the number is negative.
     */
    SquareRootFraction(long nn)
    {
        if      (nn < 0) {
            throw new IllegalArgumentException("I cannot take a square root of a negative number!");
        }
        
        else if (nn == 0) {
            n = 0;
            nSqrt = 1;
            d = 1;
        }
        
        else {
            // taking a partial square root
            long N = 1;
            long NSqrt = nn;
            long i = (long) Math.sqrt(NSqrt);
            do {
                long ii = i*i;
                if (NSqrt % ii == 0) {
                    NSqrt /= ii;
                    N *= i;
                    i = (long) Math.sqrt(NSqrt);
                }
                else i--;
            } while (i >= 2);
            
            n = N;
            nSqrt = NSqrt;
            d = 1;
        }
    }
    
    
    /**
     * Simplifies this {@link SquareRootFraction}, so that denominator and numerator are coprime and denominator is positive.
     */
    private void simplify()
    {
        long g = Utility.gcd(this.n, this.d);
        this.n /= g;
        this.d /= g;
        
        if (d<0) {
            n *= -1;
            d *= -1;
        }
    }
    
    /**
     * Multiplies this {@link SquareRootFraction} by another {@link SquareRootFraction}.
     * 
     * @param f factor
     * 
     * @return the product
     */    
    SquareRootFraction multiply(SquareRootFraction f)
    {
        SquareRootFraction prod = new SquareRootFraction(this.nSqrt * f.nSqrt);
        
        prod.n *= this.n * f.n;
        prod.d = this.d * f.d;
        prod.simplify();
        
        return prod;
    }
    
    /**
     * Divides this {@link SquareRootFraction} by another {@link SquareRootFraction}.
     * 
     * @param d divisor
     */       
    SquareRootFraction divide(SquareRootFraction d)
    {
        SquareRootFraction ratio = new SquareRootFraction(this.nSqrt, d.nSqrt);
        
        ratio.n *= this.n * d.d;
        ratio.d *= this.d * d.n;
        ratio.simplify();
        
        return ratio;
    }
    
    /**
     * Checks if two {@link SquareRootFraction}s are equal.
     * 
     * @param c comparand
     * 
     * @return true if this {@link SquareRootFraction} is equal to the comparand, false otherwise
     */
    boolean isEqual(SquareRootFraction c)
    {
        return ((this.n == c.n) && (this.nSqrt == c.nSqrt) && (this.d == c.d));
    }
    
    /**
     * Squares {@link SquareRootFraction} producing a simple fraction.
     * 
     * @return the square of this {@link SquareRootFraction}
     */    
    Fraction square()
    {
        long nn = this.n * this.n * this.nSqrt;
        long dd = this.d * this.d;
        
        return new Fraction(nn,dd);
    }
    
    /**
     * Converts this {@link SquareRootFraction} to a {@link Fraction} if it indeed is a rational number.
     * 
     * @return the {@link SquareRootFraction} re-typed to {@link Fraction} with preserved value
     */
    Fraction convertToFraction()
    {
        if (this.nSqrt != 1) throw new UnsupportedOperationException("This number is not a rational number!");
        return new Fraction(this.n,this.d);
    }
    
    /**
     * Prints this {@link SquareRootFraction} formatted as text.
     * 
     * @return plain text description of this {@link SquareRootFraction}; for example, -3*&radic;7/2 yields <code>-3*sqrt(7)/2</code>
     */
    String toText()
    {
        this.simplify();
        
        if ((this.n * this.nSqrt) == 0) return "0";
        
        String numerator = "";
        
        if      (this.nSqrt == 1) numerator = this.n + "";
        else if (this.n == 1)     numerator = "sqrt(" + this.nSqrt + ")";
        else if (this.n == -1)    numerator = "-sqrt(" + this.nSqrt + ")";
        else                      numerator = this.n + "*sqrt(" + this.nSqrt + ")";
        
        if (this.d == 1) return numerator;
        
        return numerator + "/" + this.d;
    }
    
    /**
     * Prints this {@link SquareRootFraction} formatted as TeX code.
     * 
     * @return TeX code for this {@link SquareRootFraction}; for example, -3*&radic;7/2 yields <code>-\frac{3\sqrt{7}}{2}</code>
     */
    String toTeX()
    {
        this.simplify();
        
        if ((this.n * this.nSqrt) == 0) return "0";
        
        String numerator = "";
        
        if      (this.nSqrt == 1)   numerator = this.n + "";
        else if (this.n == 1 || this.n == -1)
                                    numerator = "\\sqrt{" + this.nSqrt + "}";
        else if (this.n < 1)        numerator = (-1*this.n) + "\\sqrt{" + this.nSqrt + "}";
        else                        numerator = this.n + "\\sqrt{" + this.nSqrt + "}";
        
        
        if (this.n < -1) {
            if (this.d == 1) return "-" + numerator;
            return "-\\frac{" + numerator + "}{" + this.d + "}";
        }
        
        else {
            if (this.d == 1) return numerator;
            return "\\frac{" + numerator + "}{" + this.d + "}";
        }
    }
}
