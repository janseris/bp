/**
 * This class introduces matrices of square roots of fractions ({@link SquareRootFraction} or SRF). It also features basic necessary operations with them.
 * 
 * @author  Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since   2014-04-01
 */

class SquareRootMatrix
{
    /** number of rows */
    private int rows;
    /** number of columns */
    private int cols;
    /** elements of the matrix */
    private SquareRootFraction[][] data;
    /** {@link SquareRootFraction} 0. */
    private static final SquareRootFraction ZERO = new SquareRootFraction(0);
    
    /**
     * Constructs a matrix from a field of {@link Fraction}s. The {@link Fraction}s will be converted to {@link SquareRootFraction}s without changing their
     * value (ie. the root will not be extracted).
     * 
     * @param M field of {@link Fraction}s, containing the matrix's elements
     * @param r number of matrix's rows
     * @param c number of matrix's columns
     */
    SquareRootMatrix(Fraction[] M, int r, int c)
    {
        this.rows = r;
        this.cols = c;
        this.data = new SquareRootFraction[r][c];
        for (int i = 0; i<=r-1; i++) {
            for (int j = 0; j<=c-1; j++) {
                if ((M[c*i+j] == null) || (c*i+j >= M.length))
                    this.data[i][j]=ZERO;
                else {
                    this.data[i][j]=M[this.cols*i+j].convertToSRF();
                }
            }
        }
    }
    
    /**
     * Constructs a zero matrix of given dimensions.
     * 
     * @param r number of matrix's rows
     * @param c number of matrix's columns
     */
    SquareRootMatrix(int r, int c)
    {
        this.rows = r;
        this.cols = c;
        this.data = new SquareRootFraction[r][c];
        for (int i = 0; i<=r-1; i++) {
            for (int j = 0; j<=c-1; j++) {
                this.data[i][j]=ZERO;
            }
        }
    }
    
    /**
     * Constructs a null matrix of dimension 0.
     */ 
    SquareRootMatrix()
    {
        this.rows = 0;
        this.cols = 0;
        this.data = new SquareRootFraction[0][0];
    }
    
    /**
     * Number of rows of this Matrix.
     * 
     * @return a number of rows
     */
    int getR()
    {
        return this.rows;
    }
    
    /**
     * Number of columns of this Matrix.
     * 
     * @return a number of columns
     */
    int getC()
    {
        return this.cols;
    }
    
    /**
     * The getter of this class - returns the value of a given element <i>a</i><sub><i>r</i>,<i>c</i></sub>.
     * 
     * @param r the row index of the desired element
     * @param c the column index of the desired element
     * 
     * @return the value (a {@link SquareRootFraction}) of the element on the <tt>r</tt>th row and the <tt>c</tt>th column
     * 
     * @throws IndexOutOfBoundsException Throws an exception if the row index isn't of &lt;1,<i>number of rows</i>&gt;.
     * @throws IndexOutOfBoundsException Throws an exception if the column index isn't of &lt;1,<i>number of columns</i>&gt;.
     */
    SquareRootFraction get(int r, int c)
    {
        if (r>this.rows) throw new IndexOutOfBoundsException("Matrix only has "+this.rows+" rows!");
        if (c>this.cols) throw new IndexOutOfBoundsException("Matrix only has "+this.cols+" columns!");
        if ((r<=0) || (c<=0)) throw new IndexOutOfBoundsException("Row and column indices ought to be positive!");
        return this.data[r-1][c-1];
    }

    /**
     * The setter of this class - sets given element <i>a</i><sub><i>r</i>,<i>c</i></sub> to a given value.
     * 
     * @param F the desired value (a {@link SquareRootFraction}) of the element on the <tt>r</tt>th row and the <tt>c</tt>th column
     * @param r the row index of the desired element
     * @param c the column index of the desired element
     * 
     * @throws IndexOutOfBoundsException Throws an exception if the row index isn't of &lt;1,<i>number of rows</i>&gt;.
     * @throws IndexOutOfBoundsException Throws an exception if the column index isn't of &lt;1,<i>number of columns</i>&gt;.
     */
    void set(SquareRootFraction F, int r, int c)
    {
        if (r>this.rows) throw new IndexOutOfBoundsException("Matrix only has "+this.rows+" rows.");
        if (c>this.cols) throw new IndexOutOfBoundsException("Matrix only has "+this.cols+" columns.");
        if ((r<=0) || (c<=0)) throw new IndexOutOfBoundsException("Row and column indices ought to be positive.");
        this.data[r-1][c-1] = F;
    }
    
    /**
     * Copies another matrix onto this matrix. This is the correct way of assigning matrices, not by <code>=</code>.
     * 
     * @param I the image matrix
     */
    void copy(SquareRootMatrix I)
    {
        this.rows = I.rows;
        this.cols = I.cols;
        this.data = new SquareRootFraction[I.rows][I.cols];
        for (int i = 1; i<=I.rows; i++) {
            for (int j = 1; j<=I.cols; j++) {
                this.set(I.get(i,j),i,j);
            }
        }
    }
    
    /**
     * Converts all elements to real numbers.
     * 
     * @return the copy of this matrix with elements represented as real numbers (<code>double</code>) instead {@link SquareRootFraction}s
     */
    Jama.Matrix toRealMatrix() {
        double[][] elements = new double[this.rows][this.cols];
        
        for (int i = 1; i<=this.rows; i++) {
            for (int j = 1; j<=this.cols; j++) {
                double nn = (double) this.get(i,j).n;
                double dd = (double) this.get(i,j).d;
                double ns = (double) Math.sqrt(this.get(i,j).nSqrt);
                double value = nn / dd * ns;
                elements[i-1][j-1] = value;
            }
        }
        
        return new Jama.Matrix(elements);
    }
    
    /**
     * Calculates coefficients of characteristic polynome of the matrix.
     * 
     * @return a field of real numbers, where on the first position is the absolute coefficient and on the last position the leading coefficient
     */
    double[] getCharPolyCoeffs() {
        int n = this.rows;
        double[] result = new double[this.rows];
        double[] aux = new double[this.rows];
        Jama.Matrix realMatrix = this.toRealMatrix();
        
        aux[0] = realMatrix.trace();
        for (int i = 1;  i < n-1; i++) {
            realMatrix = realMatrix.times(realMatrix).copy();
            aux[i] = realMatrix.trace();
        }
        aux[n-1] = 0;
        for (int i = 0;  i < n; i++) {
            for (int j = 0;  j < n; j++) {
                aux[n-1] += realMatrix.get(i, j)*realMatrix.get(j, i);
            }
        }
        
        result[0] = aux[0];
        for (int i = 1;  i < n; i++) {
            result[i] = aux[i];
            for (int j = 0; j < i; j++) {
                result[i] -= aux[j]*result[i-j-1];
            }
            result[i] /= i+1;
        }
        
        return result;
    }
    
    /**
     * Prints this matrix formatted as text.
     * 
     * @return plain text description of this matrix; the rows will begin with <code>(</code> and end with <code>)</code>.
     */
    String toText()
    {
        String S = "";
        
        for (int i = 1; i<=this.rows; i++) {
           S += "\t(";
           for (int j = 1; j<=this.cols; j++) {
               S += "\t" + this.get(i,j).toText() + " ";
           }
           S += "\t)\r\n";
        }
        
        return S;
    }

    /**
     * Prints this matrix formatted as TeX code.
     * 
     * @return TeX code for this matrix
     */
    String toTeX()
    {
        String S = "";
        
        S += "\\left(\\begin{array}{";
        for (int j = 1; j<=this.cols; j++) S += "r";
        S += "}";
        
        for (int i = 1; i<=this.rows; i++) {
           for (int j = 1; j<=this.cols; j++) {
               if (j != this.cols)
               S += this.get(i,j).toTeX() + " & ";
               else
               S += this.get(i,j).toTeX() + " \\\\\n";
           }
        }
        
        S += "\\end{array}\\right)";
        
        return S;
    }
}