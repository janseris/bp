<Query Kind="Program">
  <Connection>
    <ID>a645cd38-9158-49fe-b0ce-2c0e5c1d6b8e</ID>
    <Persist>true</Persist>
    <Server>localhost\SQLEXPRESS</Server>
    <SqlSecurity>true</SqlSecurity>
    <UserName>user</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA9LD4MZ7l/EyybBXYWFL1vgAAAAACAAAAAAAQZgAAAAEAACAAAABB1VDSMb/YxBUg3Gy2SYMYiMyTGSVgb8GzVtfVLf0m8gAAAAAOgAAAAAIAACAAAAAmKnkopRB+m7uoGifUq9gsaDxuVIndJJjh2kK123xoSxAAAABxDJXzv7TnpG2nMALnNk5VQAAAAEqA3Ozpw3df6CtVxJbKrjqZt31tG4t6kdyWyQ5anB0iMuycHvzhT1aQdlJ6E/J3rJTIp7c672N4RNfMM8gutc0=</Password>
    <Database>AnalyticGeometryOnline</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

/*
This LinqPad script saves data stored in a VARBINARY field to the specified folder.
1. Connect to SQL server and select the correct database in the connection dropdown (top right)
2. Change the Language to C# Program
3. Change "Attachments" to the name of your table that holds the VARBINARY data
4. Change "AttachmentBuffer" to the name of the field that holds the data
5. Change "Id" to the unique identifier field name
6. Change "1090" to the identity of the record you want to save
7. Change the path to where you want to save the file. Make sure you choose the right extension.

Notes: Windows 10 may give you "Access Denied" error when trying to save directly to C:\. Rather save to a subfolder.
*/

void Main()
{
	string name = "+ěščřžýáíé knihovna";
    var context = this;
    var query = 
        from ci in context.Libraries
        where ci.Name == name
        select ci.Contents
    ;
    byte[] result = query.Single().ToArray();
    File.WriteAllBytes(@"C:\Users\Knuckles\Desktop\skola\jaro2020\bp_projekt\bp\linqpadScripts\" + name + ".txt", result);
    Console.WriteLine("Done");
}