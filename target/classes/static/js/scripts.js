//if uploaded file size exceeds 100kB, the submit button will be disabled
function checkUploadedFileSize(thisElement) {
    if(document.getElementById('contentSourceTypeField') != null){
        selectFileInputType();
    }
    var input, file;

    input = document.getElementById('libraryFileInput');
    file = input.files[0];
    //bodyAppend("p", "File " + file.name + " is " + file.size + " bytes in size");
    var submitButton = document.getElementById('formSubmitButton');
    if(file.size > 32*1024){ //file too big
        submitButton.disabled = true; //do not allow form submit
        showFileUploadWarning(thisElement,"p", "Velikost souboru přesahuje 32kB! Prosím, vyberte menší soubor."); //show warning message
    } else { //file ok
        if(submitButton.disabled == true){
            submitButton.disabled = false; //if it was disabled, enable
        }
        var fileUploadWarningParagraph = document.getElementById('fileUploadWarning');
        if(fileUploadWarningParagraph != null){
            fileUploadWarningParagraph.remove();
        }
    }
}

//add an html element into webpage
function showFileUploadWarning(where, tagName, innerHTML) {
    var elem;
    elem = document.createElement(tagName);
    elem.innerHTML = innerHTML; //the html code contained inside this element (for paragraph, this results in text attribute if no html tag detected)
    elem.setAttribute("class", "errorMessage"); //for class style applied from css
    elem.setAttribute("id", "fileUploadWarning"); //for identification while checking content
    insertAfter(elem, where);
}

//insert a new html element
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function toggleLibraryContentsVisibility(callerElem){
    var parentTable = callerElem.closest("table");  //closest parent table
    var libraryTextDiv = parentTable.getElementsByClassName("libraryContents")[0]; //first

    if (libraryTextDiv.style.display === 'none') { //toto se udělá, když se zobrazí div
        libraryTextDiv.style.display = 'inline';
        callerElem.innerHTML = "Schovat obsah";
    } else { //když se schová div
        libraryTextDiv.style.display = 'none';
        callerElem.innerHTML = "Ukázat obsah";
        if(navigator.appCodeName == 'Mozilla'){
            var gridContainer = parentTable.closest("div");
            reloadHtmlElement(gridContainer);
            console.log("grid container div reloaded");
        }
    }
}

//disables listeners though.
function reloadHtmlElement(element){
    var content = element.innerHTML;
    element.innerHTML = content;
}

function showLibraryDeleteConfirmation(){
    document.getElementById("libraryConfirmDeleteButton").style.display = 'inline';
    document.getElementById("libraryCancelDeleteButton").style.display = 'inline';
}
function hideLibraryDeleteConfirmation(){
    document.getElementById("libraryConfirmDeleteButton").style.display = 'none';
    document.getElementById("libraryCancelDeleteButton").style.display = 'none';
}

//https://stackoverflow.com/questions/20982683/spring-mvc-3-2-thymeleaf-ajax-fragments
function loadPublicLibrariesWithCustomOrdering() {
    var url = '/publicLibraries';
    document.getElementById('publicLibraries').load(url);
}

function selectFileInputType(){
    document.getElementById('contentSourceTypeField').selectedIndex = 1;
}

function selectTextInputType(){
    document.getElementById('contentSourceTypeField').selectedIndex = 0;
}

function setTextFileAccessedFlag() {
    document.getElementById('textFieldAccessed').setAttribute('value', 'true');
    selectTextInputType();
}



/* REGION program.html functions */

//used for automated selection when page is loaded from redirect in program.html <select> elements
function setSelectElementSelectedOption(selectElementId, index){
    document.getElementById(selectElementId).options.selectedIndex = index;
}


/* subREGION program form 1 functions */

//used for manual selection handling
function assignmentTypeOptionSelected(selectElement){
    assignmentTypeOptionSelectedByIndex(selectElement.selectedIndex);
}

//used for automated selection handling when page is loaded from redirect in program.html
function assignmentTypeOptionSelectedByIndex(index){
    if(index == null){
        index = 0; //first visit of /program
    }
    var id = "assignmentType";
    var disableSubmitButton = (index == 0);
    var solveButton = document.getElementById("solveTaskSubmitButton");
    if(solveButton != null){
        solveButton.disabled = disableSubmitButton;
    }
    setSelectElementSelectedOption(id, index); //for automated select remembered option value saved in redirect attributes on page load
}



/* END subREGION program form 1 functions */








/* subREGION program form 2 functions */


//used for switching content when when user activates an <option> in program.html main <select>
function generateSubspacesGenerateTaskTypeOptionSelected(selectElement){
    generateSubspacesGenerateTaskTypeOptionSelectedByIndex(selectElement.selectedIndex);
}

//used for automated selection when page is loaded from redirect in program.html main <select>
function generateSubspacesGenerateTaskTypeOptionSelectedByIndex(index){
    var id = "generateType";
    if(index == null){
        index = 0; //first visit of /program
    }
    var disableSubmitButton = (index == 0);
    document.getElementById("generateTaskSubmitButton").disabled = disableSubmitButton;

    setSelectElementSelectedOption(id, index); //for automated select remembered option value saved in redirect attributes on page load
    if(index == 0){
        generateSubspacesGenerateTaskTypeOption1Selected();
    } else if (index == 1){
        generateSubspacesGenerateTaskTypeOption1Selected();
    }  else if (index == 2){
        generateSubspacesGenerateTaskTypeOption2Selected();
    }  else if (index == 3){
        generateSubspacesGenerateTaskTypeOption3Selected();
    }  else if (index == 4){
        generateSubspacesGenerateTaskTypeOption4Selected();
    }  else if (index == 5){
        generateSubspacesGenerateTaskTypeOption5Selected();
    }
}

//used for switching content when when user activates an <option> in program.html child <select>
function generateSubspacesGenerateTaskRelationTypeOptionSelected(selectElement){
    generateSubspacesGenerateTaskRelationTypeOptionSelectedByIndex(selectElement.selectedIndex);
}

//used for automated selection when page is loaded from redirect in program.html child <select>
function generateSubspacesGenerateTaskRelationTypeOptionSelectedByIndex(index){
    var id = "relationType";
    setSelectElementSelectedOption(id, index); //for automated select remembered option value saved in redirect attributes on page load
    if(index == 0){
        generateSubspacesGenerateTaskTypeOption2aSelected();
    } else if (index == 1){
        generateSubspacesGenerateTaskTypeOption2bSelected();
    }  else if (index == 2){
        generateSubspacesGenerateTaskTypeOption2cSelected();
    }
}


//used for automated selection when page is loaded from redirect in a program.html child child <select>
function generateSubspacesGenerateTaskSubspacesAngleOptionSelectedByIndex(index){
    var id = "subspacesAngle";
    setSelectElementSelectedOption(id, index); //for automated select remembered option value saved in redirect attributes on page load
}

//used for automated selection when page is loaded from redirect in a program.html child child <select>
function generateSubspacesGenerateTaskSubspacesDisjointBoolOptionSelectedByIndex(index){
    var id = "generatedSubspacesDisjointBool";
    setSelectElementSelectedOption(id, index); //for automated select remembered option value saved in redirect attributes on page load
}


function generateSubspacesGenerateTaskTypeOption1Selected(){
    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2Selected(){
    console.log("option 2 selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2aSelected(){
    console.log("option 2a selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2bSelected(){
    console.log("option 2b selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption2cSelected(){
    console.log("option 2c selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'inline';
}

function generateSubspacesGenerateTaskTypeOption3Selected(){
    console.log("option 3 selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption4Selected(){
    console.log("option 4 selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'inline';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'none';
}

function generateSubspacesGenerateTaskTypeOption5Selected(){
    console.log("option 5 selected");

    document.getElementById("generateSubspaceGenerateTaskTypeOption2").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2a").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2b").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption2c").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption3").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption4").style.display = 'none';
    document.getElementById("generateSubspaceGenerateTaskTypeOption5").style.display = 'inline';
}

function setNumberInputValue(id, value){
    document.getElementById(id).value = value;
}

function onlyAllowNumbers(elem){
    elem.value=elem.value.replace(/[^0-9]/g,'');
}

/* END subREGION program form 2 functions */

function toggleInputHelp(){
    var button = document.getElementById("taskInputHelpToggleButton");
    var programHelpWindow = document.getElementById("taskInputHelpWindow");
    console.log(programHelpWindow.style.display);

    if (programHelpWindow.style.display === '' || programHelpWindow.style.display === 'none') { //toto se udělá, když má zobrazit element
        programHelpWindow.style.display = 'inline';
        button.innerHTML = "Schovat";
    } else { //když se schová div
        programHelpWindow.style.display = 'none';
        button.innerHTML = "Nápověda - zadávání podprostorů";
    }
}

function toggleLibraryHelp(){
    var button = document.getElementById("libraryHelpToggleButton");
    var programHelpWindow = document.getElementById("libraryHelpWindow");
    console.log(programHelpWindow.style.display);

    if (programHelpWindow.style.display === '' || programHelpWindow.style.display === 'none') { //toto se udělá, když má zobrazit element
        programHelpWindow.style.display = 'inline';
        button.innerHTML = "Schovat";
    } else { //když se schová div
        programHelpWindow.style.display = 'none';
        button.innerHTML = "Nápověda - knihovna příkladů";
    }
}

function toggleSolveHelp(){
    var button = document.getElementById("solveHelpToggleButton");
    var programHelpWindow = document.getElementById("solveTaskHelpWindow");
    console.log(programHelpWindow.style.display);

    if (programHelpWindow.style.display === '' || programHelpWindow.style.display === 'none') { //toto se udělá, když má zobrazit element
        programHelpWindow.style.display = 'inline';
        button.innerHTML = "Schovat";
    } else { //když se schová div
        programHelpWindow.style.display = 'none';
        button.innerHTML = "Nápověda - výpočet příkladu";
    }
}

function toggleGenerateHelp(){
    var button = document.getElementById("generateHelpToggleButton");
    var programHelpWindow = document.getElementById("generateTaskHelpWindow");
    console.log(programHelpWindow.style.display);

    if (programHelpWindow.style.display === '' || programHelpWindow.style.display === 'none') { //toto se udělá, když má zobrazit element
        programHelpWindow.style.display = 'inline';
        button.innerHTML = "Schovat";
    } else { //když se schová div
        programHelpWindow.style.display = 'none';
        button.innerHTML = "Nápověda - generování příkladu";
    }
}

function loadAssignment(){
    //use assignmentTypeOptionSelectedByIndex
    var selectedAssignmentIndex = document.getElementById("libraryAssignmentsList").selectedIndex;
    var infoDiv = document.getElementById("assignment" + selectedAssignmentIndex);
    var assignmentTypeIndex = infoDiv.children[0].innerHTML;
    var subspace1Text = infoDiv.children[1].innerHTML;
    var subspace2Text = infoDiv.children[2].innerHTML;

    assignmentTypeOptionSelectedByIndex(assignmentTypeIndex); //trigger selection of assignment type to solve
    var subspace1InputTextArea = document.getElementById("subspace1Input");
    var subspace2InputTextArea = document.getElementById("subspace2Input");
    subspace1InputTextArea.textContent = subspace1Text;
    subspace2InputTextArea.textContent = subspace2Text;
    console.log(subspace1Text);
    console.log(subspace2Text);
    console.log(selectedAssignmentIndex)
}


/* END REGION program.html functions */

function showOnlineUserNames(event){
    var table = document.getElementById("onlineUserNamesContainer");
    table.style.left = event.pageX + 'px';
    table.style.top = event.pageY + 'px';
    table.style.display = 'block';
}

function hideOnlineUserNames(){
    var table = document.getElementById("onlineUserNamesContainer");
    table.style.display = 'none';
}