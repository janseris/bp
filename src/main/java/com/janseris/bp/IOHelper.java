package com.janseris.bp;

import com.janseris.bp.model.AssignmentValidityCheckDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import static com.janseris.bp.Constants.LIBRARY_FILE_PATH;
import static com.janseris.bp.Constants.LIBRARY_HELPFILE_PATH;

/**
 * Class used to handle file-related input and output
 * Instantiated for every program user (1 instance for GUI, 1 instance for every web application user)
 * After any library file operation, the line endings are converted \r\n to avoid further complications
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-02-22
 */

public final class IOHelper {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    static final String UNIX_LINE_SEPARATOR_STRING = "\n";
    public static final String WINDOWS_LINE_SEPARATOR_STRING = "\r\n";
    static final String APPLE_LINE_SEPARATOR_STRING = "\r";

    //if library has not been loaded yet, new library will be created in UTF-8
    //if library has been already loaded, its encoding will be used from now on
    //ICU4j library has poor support for detecting czech language in Win-1250 encoding, but it detects UTF-8 perfectly


    /**
     * Used only in desktop app
     * Initialized to UTF-8 because null would prevent creating sample library or library help file if library hadn't already been accessed by that time
     * Set to detected library encoding to be able to write back in that encoding
     */
    private Charset detectedLibraryCharset = StandardCharsets.UTF_8;

    public Charset getDetectedLibraryCharset() {
        return detectedLibraryCharset;
    }

    private String outputFolderPath;
    private String outputTxtFileName;
    private String outputTeXFileName;
    private String outputPDFFileName;

    private StringBuilder textOutputBuilder = new StringBuilder();
    private StringBuilder LaTeXOutputBuilder = new StringBuilder();

    public IOHelper(String outputFolderPath, String outputTxtFileName, String outputTeXFileName, String outputPDFFileName) {
        this.outputFolderPath = outputFolderPath;
        this.outputTxtFileName = outputTxtFileName;
        this.outputTeXFileName = outputTeXFileName;
        this.outputPDFFileName = outputPDFFileName;
    }

    public String getOutputFolderPath() {
        return outputFolderPath;
    }

    public String getOutputTxtFileName() {
        return outputTxtFileName;
    }

    public String getOutputTeXFileName() {
        return outputTeXFileName;
    }

    public String getOutputPDFFileName() {
        return outputPDFFileName;
    }

    /**
     * Prepare for output
     */
    public void clearOutputBuffers(){
        textOutputBuilder = new StringBuilder();
        LaTeXOutputBuilder = new StringBuilder();
    }

    /**
     * Get the complete details of the computation after assignment is solved
     * @return details in plain text
     */
    public String getCurrentTextOutputBufferContents(){
        return textOutputBuilder.toString();
    }

    /**
     * Get the complete details of the computation after assignment is solved
     * @return details formatted in LaTeX text format
     */
    public String getCurrentLaTeXOutputBufferContents(){
        return LaTeXOutputBuilder.toString();
    }

    public void writeBuffersToOutputFiles() throws IOException {
        String outputTxtFilePath = Paths.get(getOutputFolderPath(), getOutputTxtFileName()).toString();

        //write txt
        String textOutput = getCurrentTextOutputBufferContents();

        try (OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(outputTxtFilePath, true), StandardCharsets.UTF_8.newEncoder())){
            textFile.write(textOutput);
        } catch (IOException ex) {
            log.warn("I/O error while writing results to text file '" + outputTxtFilePath + ": " + ex.getLocalizedMessage());
            throw ex;
        }

        //write latex
        String LaTeXOutput = getCurrentLaTeXOutputBufferContents();

        String outputLaTeXFilePath = Paths.get(getOutputFolderPath(), getOutputTeXFileName()).toString();
        try (OutputStreamWriter texFile = new OutputStreamWriter(new FileOutputStream(outputLaTeXFilePath, true), StandardCharsets.UTF_8.newEncoder())){
            texFile.write(LaTeXOutput);
        } catch (IOException ex) {
            log.warn("I/O error while writing results to Tex file '" + outputLaTeXFilePath + ": " + ex.getLocalizedMessage());
            throw ex;
        }
    }


    /**
     * Only used in desktop app. (web app has guaranteed UTF-8 content storing in database)<br>
     * Builds string library contents from bytes, the string is then directly used in the program.<br>
     * The only accepted charsets are UTF-8 and Windows-1250.<br>
     * All the line endings are converted to \r\n (Windows line separator) to avoid further disorder.
     * @param bytes library text as byte array
     * @return library text as String
     * @throws IOException if the encoding is not supported
     */
    private String loadLibraryInternal(byte[] bytes) throws IOException {
        String library;
        if (isUTF8(bytes)) {
            detectedLibraryCharset = StandardCharsets.UTF_8;
            library = new String(bytes, StandardCharsets.UTF_8); //decode from UTF-8
            library = convertToWindowsLineSeparator(library);
            return library;
        }

        if (isWindows1250(bytes)) {
            detectedLibraryCharset = Charset.forName("windows-1250");
            library = new String(bytes, "Cp1250"); //decode from windows-1250
        } else {
            throw new IOException("Unsupported library file enconding");
        }

        library = convertToWindowsLineSeparator(library);
        return library;
    }


    /**
     * Reads the contents of library file into string.<br>
     * The string is then directly used in program.<br>
     * Only used in desktop app.<br>
     * The only accepted charsets are UTF-8 and Windows-1250<br>
     * All line endings are converted into \r\n (Windows line separator) to avoid further complications<br>
     * Throws exceptions with messages to be used in GUI
     */
    public String loadLibraryForGUIDesktopApp() throws IOException{
        byte[] bytes;
        try {
            bytes = Files.readAllBytes(Paths.get(LIBRARY_FILE_PATH));
        } catch (IOException ex) {
            throw new IOException("Knihovna " + LIBRARY_FILE_PATH + " nenalezena, nebo z ní nelze číst.", ex);
        }

        String library = null;
        try{
            library = loadLibraryInternal(bytes);
        } catch (IOException ex){
            throw new IOException("Knihovna " + LIBRARY_FILE_PATH + " není ve správném kódování. Akceptovaná kódování: UTF-8 a windows-1250.");
        }
        return library;
    }

    /**
     * Converts all line separators to \r\n if they differ from \r\n<br>
     * Has no effect on files with \r\n line separator<br>
     * @param libraryFile string which contains line endings
     */
    public static String convertToWindowsLineSeparator(String libraryFile){
        switch(getLineSeparatorType(libraryFile)){
            case WINDOWS_LINE_SEPARATOR:
                return libraryFile;
            case UNIX_LINE_SEPARATOR:
                return libraryFile.replaceAll(UNIX_LINE_SEPARATOR_STRING, WINDOWS_LINE_SEPARATOR_STRING);
            case APPLE_LINE_SEPARATOR:
                return libraryFile.replaceAll(APPLE_LINE_SEPARATOR_STRING, WINDOWS_LINE_SEPARATOR_STRING);
            case NO_LINE_SEPARATOR:
                return libraryFile;
            default:
                return libraryFile;
        }
    }

    /**
     * Determines which line separator is used in given text file contents<br>
     * The file separators check order is important because Apple and Unix line endings are substrings of Windows line separator<br>
     * @param libraryFile library file loaded into a string
     * @return type of line separator used in the file
     */
    private static LineSeparatorType getLineSeparatorType(String libraryFile){
        if(libraryFile.contains((WINDOWS_LINE_SEPARATOR_STRING))){ // \r\n
            return LineSeparatorType.WINDOWS_LINE_SEPARATOR;
        }
        if(libraryFile.contains(UNIX_LINE_SEPARATOR_STRING)){ // \n
            return LineSeparatorType.UNIX_LINE_SEPARATOR;
        }
        if(libraryFile.contains(APPLE_LINE_SEPARATOR_STRING)){
            return LineSeparatorType.APPLE_LINE_SEPARATOR;
        }
        return LineSeparatorType.NO_LINE_SEPARATOR; //file only has 1 line
    }


    static boolean libraryExists() {
        File f = new File(LIBRARY_FILE_PATH);
        return (f.exists() && !f.isDirectory());
    }

    static boolean libraryHelpFileExists() {
        File f = new File(LIBRARY_HELPFILE_PATH);
        return (f.exists() && !f.isDirectory());
    }


    /**
     * Writes single computation step details into output text file (actually, to a string buffer, much faster)
     * @param computationDetails string to be written
     */
    public void textOut(String computationDetails) {
        textOutputBuilder.append(computationDetails);
    }

    /**
     * Writes single computation step details into output TeX file (actually, to a string buffer, much faster)
     * @param computationDetails string to be written
     */
    public void LaTeXOut(String computationDetails) {
        LaTeXOutputBuilder.append(computationDetails);
    }

    /**
     * Write LaTeX file heading to LaTeX output
     */
    public void initializeTeXFile() {
        LaTeXOut("\\documentclass[12pt,a4paper]{article}\n\n\\usepackage{cmap}\n\\usepackage[utf8]{inputenc}\n\\usepackage[czech]{babel}\n\\usepackage[T1]{fontenc}\n\n"
                + "\\usepackage{amsmath}\n\\usepackage{amsfonts}\n\\usepackage{amssymb}\n\\usepackage{textcomp}\n\\usepackage{mathtools}\n\n\\usepackage{array}\n\n"
                + "\\newcommand{\\abs}[1]{\\left|#1\\right|}\n\\let\\phi\\varphi\n\n"
                + "\\setlength{\\parindent}{0pt}\n\\setlength{\\parskip}{3mm plus 2mm minus 1mm}\n\n"
                + "\\begin{document}\n");
    }

    /**
     * Append ending sequence to the LaTeX output so it can be now treated as valid LaTeX file (in-memory file)
     */
    public void finalizeTeXOutput() {
        LaTeXOut("\n\\end{document}");
    }


    /**
     * Creates a directory if it doesn't already exist
     * @throws SecurityException upon access denied
     * @param directoryPath
     */
    public static void guaranteeDirectoryExistence(String directoryPath) throws SecurityException {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    /**
     * Only used in desktop app. Web application uses bulk load approach.<br>
     * Loads an assignment by selected index in combobox (combobox shows assignment names).<br>
     * Any input before first assignment (defined by {@link Constants#ASSIGNMENT_DISTINGUISHING_WORD}) is ignored (space for comments).
     * @param assignmentIndex selected index in combobox (combobox shows assignment names)
     * @param libraryContents to obtain selected assignment from
     * @return a valid assignment object which can be directly used as program input
     * @throws LibrarySyntaxErrorException with error message if any syntax error within parsing any assignment is encountered
     * @throws LibraryLogicalErrorException with error message if any logical error is encountered while parsing given assignment
     */
    public static Assignment loadAssignmentByIndex(int assignmentIndex, String libraryContents) throws LibrarySyntaxErrorException {
        String[] assignments = libraryContents.split(Constants.ASSIGNMENT_DISTINGUISHING_WORD);
        assignments = Arrays.copyOfRange(assignments, 1, assignments.length); //ignore string before first PŘÍKLAD occurence (usually empty)

        ArrayList<Assignment> assignmentList = Assignment.buildAssignments(assignments);
        Assignment chosenAssignment = assignmentList.get(assignmentIndex);

        return chosenAssignment;
    }

    /**
     * Appends a new assignment entered by user into library file using the previously detected encoding and uniform line endings.<br>
     * The current library file is overwritten to ensure that it contains uniform \r\n line endings.<br>
     * The assignment validity shall be checked before this method is called.<br>
     * Only used in desktop application
     * @param newAssignment a validated program input
     * @throws IOException if an error occurs during writing appending to the library file
     */
    public void addAssignmentToLibrary(Assignment newAssignment) throws IOException{
        String outputFileName = LIBRARY_FILE_PATH;
        String libraryContents = loadLibraryForGUIDesktopApp();
        try (OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(outputFileName, false), detectedLibraryCharset.newEncoder())){
            textFile.write(libraryContents + newAssignment.toString());
        }
    }

    /**
     * Deletes selected assignment from the library file (cuts its text from the library contents)
     * @param assignmentIndex a valid assignment index in library, 0 represents the first assignment
     * @param libraryContents to be edited
     */
    public static String removeAssignmentFromLibraryByIndex(int assignmentIndex, String libraryContents) {
        String[] assignments = libraryContents.split(Constants.ASSIGNMENT_DISTINGUISHING_WORD);
        ArrayList<String> assignmentsList = new ArrayList<>(Arrays.asList(assignments));

        String libraryHeader = assignments[0]; //anything before first occurrence of PŘÍKLAD

        assignmentsList.remove(assignmentIndex + 1); //remove assignment (index + 1 because [0] is never an assignment but a header (can be empty))

        StringBuilder newLibraryBuilder = new StringBuilder();
        newLibraryBuilder.append(libraryHeader);

        for(int i = 1; i < assignmentsList.size(); i++){
            newLibraryBuilder.append(Constants.ASSIGNMENT_DISTINGUISHING_WORD).append(assignmentsList.get(i)).append(IOHelper.WINDOWS_LINE_SEPARATOR_STRING);
        }
        return newLibraryBuilder.toString();
    }

    /**
     * Get the names of assignments in library.
     * Only used in desktop app (to fill combobox items list without building Assignment objects).
     * Any input before first assignment (defined by {@link Constants#ASSIGNMENT_DISTINGUISHING_WORD}) is ignored (space for any comments)
     * Line endings must be in uniform \r\n format
     * @param libraryContents
     * @return
     */
    public static ArrayList<String> getAssignmentNamesFromLibrary(String libraryContents){
        String[] assignments = libraryContents.split(Constants.ASSIGNMENT_DISTINGUISHING_WORD);
        assignments = Arrays.copyOfRange(assignments, 1, assignments.length); //delete first empty string (delimiter PŘÍKLAD creates first empty result)

        ArrayList<String> assignmentNames = new ArrayList<>(assignments.length); //allocate n
        for (String assignment : assignments) {
            String name = assignment.split(IOHelper.WINDOWS_LINE_SEPARATOR_STRING)[0].trim();
            assignmentNames.add(name); //name = the rest of text found on the line containing PŘÍKLAD
        }

        return assignmentNames;
    }


    /**
     * Only used in desktop app<br>
     * Overwrites the current library text file with a sample library (a few sample assignments)
     * If a library hasn't been opened since application start, the file is written in UTF-8, otherwise, the detected charset is used
     * @see Constants#SAMPLE_LIBRARY_CONTENTS
     */
    public void libraryCreate() throws IOException{
        String filePath = LIBRARY_FILE_PATH;
        String contents = Constants.SAMPLE_LIBRARY_CONTENTS;
        try (OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(filePath), detectedLibraryCharset.newEncoder())){
            textFile.write(contents);
        }
    }

    /**
     * Creates a text file containing a manual to the accepted library file structure
     * If a library hasn't been opened since application start, the file is written in UTF-8, otherwise, the detected charset is used
     * @see Constants#LIBRARY_HELPFILE_CONTENTS
     */
    public void libraryHelpCreate() throws IOException{
        String filePath = LIBRARY_HELPFILE_PATH;
        String contents = Constants.LIBRARY_HELPFILE_CONTENTS;
        try(OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(filePath), detectedLibraryCharset.newEncoder())){
            textFile.write(contents);
        }
    }

    /**
     * Checks if the given byte array is a valid UTF-8 encoded string
     * @param inputBytes
     * @return
     */
    public static boolean isUTF8(byte[] inputBytes) {
        final String converted = new String(inputBytes, StandardCharsets.UTF_8);
        final byte[] outputBytes = converted.getBytes(StandardCharsets.UTF_8);
        return Arrays.equals(inputBytes, outputBytes);
    }

    /**
     * Checks if the given byte array is a valid windows-1250 encoded string<br>
     * Does not necessarily state that the encoding is windows-1250 and no other,
     * only checks if all given values are valid for windows-1250 chars
     * @param inputBytes
     * @return
     */
    public static boolean isWindows1250(byte[] inputBytes) {
        final String converted = new String(inputBytes, Charset.forName("windows-1250"));
        final byte[] outputBytes = converted.getBytes(Charset.forName("windows-1250"));
        return Arrays.equals(inputBytes, outputBytes);
    }

    //source: https://stackoverflow.com/questions/29667977/converting-string-from-one-charset-to-another?noredirect=1&lq=1
    public static byte[] transcodeField(byte[] source, Charset from, Charset to) {
        return new String(source, from).getBytes(to);
    }


    /**
     * Converts given library text to UTF-8 if it's in Windows-1250 without using online charset detection
     * This shall be used to ensure that only UTF-8 content is stored in database.
     * If source is UTF-8, does nothing
     * @param source library text file bytes
     * @return library text in UTF-8
     */
    public static byte[] convertToUTF8(byte[] source) throws IllegalArgumentException {
        Charset from = Charset.forName("windows-1250");
        Charset to = StandardCharsets.UTF_8;

        if(IOHelper.isUTF8(source)){
            return source;
        }
        if(IOHelper.isWindows1250(source)){
            return new String(source, from).getBytes(to);
        } else {
            throw new IllegalArgumentException("Invalid encoding");
        }
    }

    /**
     * Clears anything inside specified file (cut contents to 0 bytes) (truncate)
     * File is created if it doesn't exist
     * @param filePath path to file
     */
    public static void clearFile(String filePath) throws FileNotFoundException {
        try(PrintWriter ignored = new PrintWriter(filePath)){
            //doNothing();
        }
    }

    /**
     * Tries to create an output PDF file
     * @throws InterruptedException when the current thread was interrupted while waiting for results
     * @throws IOException if pdflatex program was not found
     */
    public void generateOutputPDF() throws InterruptedException, IOException {
        String sourceTexFilePath = Paths.get(getOutputFolderPath(), getOutputTeXFileName()).toString(); //"results/resultTeX.tex"
        Process pdfLatex = Runtime.getRuntime().exec( Constants.PDFLATEX_EXECUTABLE_NAME + " -interaction=batchmode -output-directory=" + getOutputFolderPath()
                    + " " + sourceTexFilePath);
        pdfLatex.waitFor();
    }

}
