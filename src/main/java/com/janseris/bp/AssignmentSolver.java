package com.janseris.bp;

import com.janseris.bp.model.SolveInputDTO;
import com.janseris.bp.model.SolveResultDTO;

/**
 * A class used to solve assignments and validate input to solve
 * Service-like
 * @author Jan Seris <456435@mail.muni.cz>
 * @version 1.0
 * @since 2020-05-16
 */

public final class AssignmentSolver {


    /**
     * Clears ioHelper string buffers (txt and TeX output) and initializes a new TeX output<br>
     * Solves an assignment<br>
     * Meanwhile, builds short text output and writes results to ioHelper output buffers
     * @param input validated input (returned by {@link AssignmentSolver#loadInputToSolve(String, String, String, InputReader)}
     * @param ioHelper used to store detailed build txt and tex output to be later prepared to be written into output files
     * @return short text output
     */
    public static SolveResultDTO solve(SolveInputDTO input, IOHelper ioHelper) {
        if(input == null || input.containsErrorMessage()){
            throw new IllegalArgumentException("Only a valid assignment shall be solved!");
        }
        ioHelper.clearOutputBuffers();
        ioHelper.initializeTeXFile();

        Subspace first = input.getFirst();
        Subspace second = input.getSecond();
        String task = input.getTask();
        boolean firstNull = first.isNull();
        boolean secondNull = second.isNull();

        //short text output is built directly, detailed output is built in ioHelper buffers
        String shortTextOutput = new String();

        ioHelper.textOut("ZADÁNÍ\r\n");
        ioHelper.textOut("======\r\n");
        ioHelper.LaTeXOut("\\section*{Zadání}");

        if (!firstNull) {
            ioHelper.textOut("Máme podprostor A zadaný " + (first.inputWasInGeneralForm() ? "obecně" : "parametricky") + ":\r\n\r\n");
            ioHelper.LaTeXOut("\\noindent Máme podprostor $\\mathcal{A}$ zadaný " + (first.inputWasInGeneralForm() ? "obecně" : "parametricky") + ":\n\\[");
            if (first.inputWasInGeneralForm()) {
                ioHelper.textOut(first.toTextGeneral());
                ioHelper.LaTeXOut(first.toTeXGeneral());
            } else {
                ioHelper.textOut(first.toTextParametric());
                ioHelper.LaTeXOut(first.toTeXParametric());
            }

            if (!secondNull) {
                ioHelper.textOut("\r\na podprostor B zadaný " + ((first.inputWasInGeneralForm() == second.inputWasInGeneralForm()) ? "také " : "")
                        + (second.inputWasInGeneralForm() ? "obecně" : "parametricky") + ":\r\n\r\n");
                ioHelper.LaTeXOut("\\]\na podprostor $\\mathcal{B}$ zadaný " + ((first.inputWasInGeneralForm() == second.inputWasInGeneralForm()) ? "také " : "")
                        + (second.inputWasInGeneralForm() ? "obecně" : "parametricky") + ":\n\\[");
                if (second.inputWasInGeneralForm()) {
                    ioHelper.textOut(second.toTextGeneral());
                    ioHelper.LaTeXOut(second.toTeXGeneral());
                } else {
                    ioHelper.textOut(second.toTextParametric());
                    ioHelper.LaTeXOut(second.toTeXParametric());
                }
            }

            ioHelper.textOut("\r\n");
            ioHelper.LaTeXOut("\\]\n");
        } else {
            ioHelper.textOut("Máme podprostor B zadaný " + (second.inputWasInGeneralForm() ? "obecně" : "parametricky") + ":\r\n\r\n");
            ioHelper.LaTeXOut("\\noindent Máme podprostor $\\mathcal{B}$ zadaný " + (second.inputWasInGeneralForm() ? "obecně" : "parametricky") + ":\n\\[");
            if (second.inputWasInGeneralForm()) {
                ioHelper.textOut(second.toTextGeneral());
                ioHelper.LaTeXOut(second.toTeXGeneral());
            } else {
                ioHelper.textOut(second.toTextParametric());
                ioHelper.LaTeXOut(second.toTeXParametric());
            }

            ioHelper.textOut("\r\n");
            ioHelper.LaTeXOut("\\]\n");
        }
        ioHelper.textOut("Naším úkolem je ");
        ioHelper.LaTeXOut("Naším úkolem je ");

        if (task.equals(Constants.solveTaskList[1])) {
            if (firstNull == secondNull) {
                ioHelper.textOut("popsat oba podprostory.");
                ioHelper.LaTeXOut("popsat oba podprostory.");
            } else {
                ioHelper.textOut("podprostor popsat.");
                ioHelper.LaTeXOut("podprostor popsat.");
            }

            if (!firstNull) {
                ioHelper.textOut("\r\n\r\nPOPIS A\r\n");
                ioHelper.textOut("=======\r\n");
                ioHelper.LaTeXOut("\n\\section*{Popis $\\mathcal{A}$}");
                shortTextOutput = "A: " + first.describe('A') + "\n";
            }
            if (!secondNull) {
                ioHelper.textOut("\r\n\r\nPOPIS B\r\n");
                ioHelper.textOut("=======\r\n");
                ioHelper.LaTeXOut("\n\\section*{Popis $\\mathcal{B}$}");
                shortTextOutput += "B: " + second.describe('B');
            }
        }

        if (task.equals(Constants.solveTaskList[2])) {
            if (firstNull == secondNull) {
                if (first.inputWasInGeneralForm() == second.inputWasInGeneralForm()) {
                    ioHelper.textOut("oba podprostory převést na " + (first.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar."));
                    ioHelper.LaTeXOut("oba podprostory převést na " + (first.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar."));
                } else {
                    ioHelper.textOut("podprostor A převést na " + (first.inputWasInGeneralForm() ? "parametrický " : "obecný ")
                            + "podprostor B na " + (second.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar."));
                    ioHelper.LaTeXOut("podprostor A převést na " + (first.inputWasInGeneralForm() ? "parametrický " : "obecný ")
                            + "podprostor B na " + (second.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar."));
                }
            } else {
                ioHelper.textOut("podprostor převést na ");
                if (firstNull) ioHelper.textOut(second.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar.");
                else ioHelper.textOut(first.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar.");
                ioHelper.LaTeXOut("podprostor převést na ");
                if (firstNull) ioHelper.LaTeXOut(second.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar.");
                else ioHelper.LaTeXOut(first.inputWasInGeneralForm() ? "parametrický tvar." : "obecný tvar.");
            }

            if (!firstNull) {
                ioHelper.textOut("\r\n\r\nPŘEVOD A NA " + (first.inputWasInGeneralForm() ? "PARAMETRICKÝ TVAR" : "OBECNÝ TVAR") + "\r\n");
                ioHelper.textOut("============" + (first.inputWasInGeneralForm() ? "=================" : "===========") + "\r\n");
                ioHelper.LaTeXOut("\\section*{Převod $\\mathcal{A}$ na " + (first.inputWasInGeneralForm() ? "parametrický tvar" : "obecný tvar") + "}\n");
                if (first.inputWasInGeneralForm()) {
                    first.checkParametricForm('A', true);
                    shortTextOutput = "A: " + first.toTextParametric() + "\n";
                } else {
                    first.checkGeneralForm('A', true);
                    if (first.isGeneralFormAvailable())
                        shortTextOutput = "A: " + first.toTextGeneral() + "\n";
                    else
                        shortTextOutput = "A: celý prostor nemá obecné vyjádření\n";
                }
            }
            if (!secondNull) {
                ioHelper.textOut("\r\n\r\nPŘEVOD B NA " + (second.inputWasInGeneralForm() ? "PARAMETRICKÝ TVAR" : "OBECNÝ TVAR") + "\r\n");
                ioHelper.textOut("============" + (second.inputWasInGeneralForm() ? "=================" : "===========") + "\r\n");
                ioHelper.LaTeXOut("\\section*{Převod $\\mathcal{B}$ na " + (second.inputWasInGeneralForm() ? "parametrický tvar" : "obecný tvar") + "}\n");
                if (second.inputWasInGeneralForm()) {
                    second.checkParametricForm('B', true);
                    shortTextOutput += "B: " + second.toTextParametric() + "\n";
                } else {
                    second.checkGeneralForm('B', true);
                    if (second.isGeneralFormAvailable())
                        shortTextOutput += "B: " + second.toTextGeneral() + "\n";
                    else
                        shortTextOutput += "B: celý prostor nemá obecné vyjádření\n";
                }
            }
        }

        if (task.equals(Constants.solveTaskList[5])) {
            ioHelper.textOut("určit vzájemnou polohu podprostorů.");
            ioHelper.LaTeXOut("určit vzájemnou polohu podprostorů.");
            ioHelper.textOut("\r\n\r\nURČENÍ VZÁJEMNÉ POLOHY PODPROSTORŮ\r\n");
            ioHelper.textOut("==================================\r\n");
            ioHelper.LaTeXOut("\\section*{URČENÍ VZÁJEMNÉ POLOHY PODPROSTORŮ}\n");
            shortTextOutput = "Podprostory jsou " + first.relativePosition(second, 'A', 'B', true) + ".\n";
        }

        if (task.equals(Constants.solveTaskList[4])) {
            ioHelper.textOut("najít průnik podprostorů.");
            ioHelper.LaTeXOut("najít průnik podprostorů.");
            ioHelper.textOut("\r\n\r\nHLEDÁNÍ PRŮNIKU\r\n");
            ioHelper.textOut("===============\r\n");
            ioHelper.LaTeXOut("\\section*{HLEDÁNÍ PRŮNIKU}\n");
            shortTextOutput = "Průnik: " + first.intersection(second, 'A', 'B', true) + "\n";
        }

        if (task.equals("Jsou podprostory rovnoběžné?")) {
            ioHelper.textOut("zjistit, jestli jsou oba podprostory rovnoběžné.");
            ioHelper.LaTeXOut("zjistit, jestli jsou oba podprostory rovnoběžné.");
            ioHelper.textOut("\r\n\r\nOVĚŘENÍ ROVNOBĚŽNOSTI PODPROSTORŮ\r\n");
            ioHelper.textOut("=================================\r\n");
            ioHelper.LaTeXOut("\\section*{Ověření rovnoběžnosti podprostorů}\n");
            shortTextOutput = first.isParallel(second, true) ? "Podprostory jsou rovnoběžné." : "Podprostory nejsou rovnoběžné.";
        }

        if (task.equals(Constants.solveTaskList[3])) {
            ioHelper.textOut("zjistit, jestli se podprostory protínají.");
            ioHelper.LaTeXOut("zjistit, jestli se podprostory protínají.");
            ioHelper.textOut("\r\n\r\nOVĚŘENÍ DISJUNKTNOSTI PODPROSTORŮ\r\n");
            ioHelper.textOut("=================================\r\n");
            ioHelper.LaTeXOut("\\section*{Ověření disjunktnosti podprostorů}\n");
            shortTextOutput = first.intersects(second, 'A', 'B', true) ? "Podprostory se protínají." : "Podprostory se neprotínají.";
        }

        if (task.equals(Constants.solveTaskList[6])) {
            ioHelper.textOut("spočítat vzdálenost těchto podprostorů.");
            ioHelper.LaTeXOut("spočítat vzdálenost těchto podprostorů.");
            ioHelper.textOut("\r\n\r\nVÝPOČET VZDÁLENOSTI\r\n");
            ioHelper.textOut("===================\r\n");
            ioHelper.LaTeXOut("\\section*{Výpočet vzdálenosti}\n");
            shortTextOutput = "Vzdálenost podprostorů je: " + first.distance(second, 'A', 'B', true).toText() + ".";
        }

        if (task.equals(Constants.solveTaskList[7])) {
            ioHelper.textOut("spočítat odchylku těchto podprostorů.");
            ioHelper.LaTeXOut("spočítat odchylku těchto podprostorů.");
            ioHelper.textOut("\r\n\r\nVÝPOČET ODCHYLKY\r\n");
            ioHelper.textOut("================\r\n");
            ioHelper.LaTeXOut("\\section*{Výpočet odchylky}\n");
            shortTextOutput = "Odchylka podprostorů je: " + first.angle(second, 'A', 'B', true).toText() + ".";
        }

        if (task.equals(Constants.solveTaskList[8])) {
            if (firstNull == secondNull) {
                ioHelper.textOut("najít ortogonální doplněk k oběma podprostorům.");
                ioHelper.LaTeXOut("najít ortogonální doplněk k oběma podprostorům.");
            } else {
                ioHelper.textOut("najít ortogonální doplněk k tomuto podprostoru.");
                ioHelper.LaTeXOut("najít ortogonální doplněk k tomuto podprostoru.");
            }

            if (!firstNull) {
                ioHelper.textOut("\r\n\r\nORTOGONÁLNÍ DOPLNĚK K A\r\n");
                ioHelper.textOut("=======================\r\n");
                ioHelper.LaTeXOut("\n\\section*{Ortogonální doplněk k~$\\mathcal{A}$}");
                shortTextOutput = "Ort. dopl. k A: " + first.orthogonalSupplement('A', true).toTextGeneral() + "\n";
            }
            if (!secondNull) {
                ioHelper.textOut("\r\n\r\nORTOGONÁLNÍ DOPLNĚK K B\r\n");
                ioHelper.textOut("=======================\r\n");
                ioHelper.LaTeXOut("\n\\section*{Ortogonální doplněk k~$\\mathcal{B}$}");
                shortTextOutput = "Ort. dopl. k B: " + second.orthogonalSupplement('B', true).toTextGeneral() + "\n";
            }
        }

        ioHelper.finalizeTeXOutput();
        return new SolveResultDTO(shortTextOutput);
    }


    /**
     * Loads input from subspace input text fields for assignment solving<br>
     * If input is invalid or any error occurs, returns an empty DTO with error message set to be handled in presentation layer<br>
     * If input is valid, returns valid DTO to be used in solve()
     * @param task selected task type
     * @param input1 subspace 1 input
     * @param input2 subspace 2 input
     * @param inputReader required to construct Subspaces because ioHelper must be passed to subspaces to store txt and TeX output generated during assigment solution
     */
    public static SolveInputDTO loadInputToSolve(String task, String input1, String input2, InputReader inputReader){
        boolean firstNull, secondNull; //subspace
        boolean passedFirst = false;
        Subspace first, second;

        try {
            first = inputReader.getSubspace(input1);
            firstNull = first.isNull();
            passedFirst = true;

            second = inputReader.getSubspace(input2);
            secondNull = second.isNull();
        } catch (Exception e) { //exception raised during subspace construction in InputReader.getSubspace()
            if (e.getMessage().equals("The matrix is not solvable!")) {
                if (!passedFirst)
                    return new SolveInputDTO(null, null, null, "Soustava rovnic, kterou je zadán první podprostor, není řešitelná.\n Nejde tak o podprostor a požadovanou operaci "
                            + "tedy nelze provést. Zadejte prosím existující podprostor.");
                else
                    return new SolveInputDTO(null, null, null, "Soustava rovnic, kterou je zadán druhý podprostor, není řešitelná.\n Nejde tak o podprostor a požadovanou operaci "
                            + "tedy nelze provést. Zadejte prosím existující podprostor.");
            } else
                return new SolveInputDTO(null, null, null, "Některý z podprostorů nebyl zadán správně. Opravte, prosím, zadání.\n Pokud si nejste jistí, jak má zadání vypadat, "
                        + "konzultujte nápovědu k zadávání podprostorů.");
        }

        if (firstNull && secondNull) {
            return new SolveInputDTO(null, null, null, "Je nutné zadat alespoň jeden podprostor.");
        }
        if ((firstNull || secondNull) && !(task.equals(Constants.solveTaskList[1]) || task.equals(Constants.solveTaskList[2])
                || task.equals(Constants.solveTaskList[8]))) {
            return new SolveInputDTO(null, null, null, "K vybranému typu úlohy je nutné zadat oba podprostory.");
        }
        if (!(task.equals(Constants.solveTaskList[1]) || task.equals(Constants.solveTaskList[2]) || task.equals(Constants.solveTaskList[8])) &&
                (first.getDIM() != second.getDIM())) {
            return new SolveInputDTO(null, null, null,"Podprostory nejsou podprostory téhož prostoru (mají jiný počet proměnných)!");
        }

        return new SolveInputDTO(first, second, task,null);
    }
}
