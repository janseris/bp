package com.janseris.bp;

import static com.janseris.bp.IOHelper.WINDOWS_LINE_SEPARATOR_STRING;

/**
 * A static class responsible for holding all (primarily text) constants reused in various classes
 * @author Jan Seris <456435@mail.muni.cz>
 * @version 1.0
 * @since 2020-02-26
 */

public final class Constants {
    private Constants(){

    }

    public static final String DESKTOP_VERSION_OUTPUT_DIRECTORY_PATH = "results";
    public static final String DESKTOP_VERSION_OUTPUT_PDF_NAME = "resultTeX.pdf";
    public static final String DESKTOP_VERSION_OUTPUT_TEX_NAME = "resultTeX.tex";
    public static final String DESKTOP_VERSION_OUTPUT_TXT_NAME = "resultText.txt";
    public static String PDFLATEX_EXECUTABLE_NAME = "pdflatex"; //must be accessible through PATH environment variable
    public static final String LIBRARY_FILE_PATH = "library.txt";
    public static final String LIBRARY_HELPFILE_PATH = "libraryHowTo.txt";

    public static final String ASSIGNMENT_DISTINGUISHING_WORD = "PŘÍKLAD";
    public static final String LIBRARY_HELPFILE_CONTENTS = "Toto je návod ke knihovně příkladů pro CVIČEBNICI ANALYTICKÉ GEOMETRIE." + WINDOWS_LINE_SEPARATOR_STRING
            + "Knihovna musí být uložena v kódování windows-1250 nebo UTF-8." + WINDOWS_LINE_SEPARATOR_STRING
            + "Příklady jsou rozpoznávány podle klíčového slova " + ASSIGNMENT_DISTINGUISHING_WORD + "." + WINDOWS_LINE_SEPARATOR_STRING
            + "Před prvním příkladem lze uvést libovolný text popisující obsah knihovny (neobsahující slovo " + ASSIGNMENT_DISTINGUISHING_WORD + ")." + WINDOWS_LINE_SEPARATOR_STRING
            + "Pro vkládání nových úloh do knihovny dodržujte následující strukturu: " + WINDOWS_LINE_SEPARATOR_STRING
            + "- každý nový záznam musí být uvozen slovem \"" + ASSIGNMENT_DISTINGUISHING_WORD + "\", poté musí na stejném řádku následovat neprázdný název/popis příkladu, pro lepší orientaci doporučujeme číslovat)" + WINDOWS_LINE_SEPARATOR_STRING
            + "- oddělení jedním prázdným řádkem" + WINDOWS_LINE_SEPARATOR_STRING
            + "- číslo popisující zadání úkolu (viz očíslování úkolů)." + WINDOWS_LINE_SEPARATOR_STRING
            + "- oddělení jedním prázdným řádkem" + WINDOWS_LINE_SEPARATOR_STRING
            + "- rovnice prvního podprostoru (dle syntaxe aplikace)" + WINDOWS_LINE_SEPARATOR_STRING
            + "- oddělení jedním prázdným řádkem" + WINDOWS_LINE_SEPARATOR_STRING
            + "- rovnice druhého podprostoru" + WINDOWS_LINE_SEPARATOR_STRING
            + "(u úloh 1, 2, 8 lze zadat i jen jeden podprostor)" + WINDOWS_LINE_SEPARATOR_STRING
            + "Očíslování úkolů:" + WINDOWS_LINE_SEPARATOR_STRING
            + "1 - Popiš podprostory." + WINDOWS_LINE_SEPARATOR_STRING
            + "2 - Převeď na obecný/parametrický tvar." + WINDOWS_LINE_SEPARATOR_STRING
            + "3 - Protínají se oba podprostory?" + WINDOWS_LINE_SEPARATOR_STRING
            + "4 - Najdi průnik podprostorů." + WINDOWS_LINE_SEPARATOR_STRING
            + "5 - Urči vzájemnou polohu podprostorů." + WINDOWS_LINE_SEPARATOR_STRING
            + "6 - Spočítej vzdálenost podprostorů." + WINDOWS_LINE_SEPARATOR_STRING
            + "7 - Spočítej odchylku podprostorů." + WINDOWS_LINE_SEPARATOR_STRING
            + "8 - Spočítej ortogonální doplněk k podprostoru." + WINDOWS_LINE_SEPARATOR_STRING;

    public static final String SAMPLE_LIBRARY_NAME = "Ukázková knihovna příkladů";
    public static final String SAMPLE_LIBRARY_CONTENTS = ASSIGNMENT_DISTINGUISHING_WORD + " 1 - vzájemná poloha" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "5" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-5+6t_1-t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "6-t_1" + WINDOWS_LINE_SEPARATOR_STRING +
            "-2+2t_1-t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-2x_2-x_3+10=0" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            ASSIGNMENT_DISTINGUISHING_WORD + " 2 - průnik" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "4" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "3+t_1-t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "-1-2t_1+2t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "-5t_1+t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "2-2t_1-t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-23x_1+3x_3-19x_4+17=0" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            ASSIGNMENT_DISTINGUISHING_WORD + " 3 - vzájemná poloha" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "5" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "4+10t_1-6t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "-8-13t_1+9t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "4+t_1-2t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "-2+14t_1-8t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "12x_1+4x_2+2x_3-5x_4-34=0" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            ASSIGNMENT_DISTINGUISHING_WORD + " 4 - odchylka" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "7" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-1-4t_1+t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "-4-4t_1+t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            "1-t_1+t_2" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "x_2-x_3-2=0" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            ASSIGNMENT_DISTINGUISHING_WORD + " 5 - protínají se?" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "3" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-x_1+7x_2-10x_3+4=0" + WINDOWS_LINE_SEPARATOR_STRING +
            "-7x_2+11x_3-x_4-6=0" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-7x_1+9x_2-5x_3-10x_4-2=0" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            ASSIGNMENT_DISTINGUISHING_WORD + " 6 - popiš podprostory" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "1" + WINDOWS_LINE_SEPARATOR_STRING +
            WINDOWS_LINE_SEPARATOR_STRING +
            "-x_1+7x_2-10x_3+4=0" + WINDOWS_LINE_SEPARATOR_STRING +
            "-7x_2+11x_3-x_4-6=0";

    public static final String ONLINE_LIBRARY_HELPTEXT = "Toto je návod ke knihovnám příkladů pro online aplikaci CVIČEBNICE ANALYTICKÉ GEOMETRIE.\n" +
            "Knihovna musí být strukturována následujícím způsobem: \n" +
            "Příklady jsou rozpoznávány podle klíčového slova " + ASSIGNMENT_DISTINGUISHING_WORD + ".\n" +
            "Před prvním příkladem může být uveden libovolný text popisující obsah knihovny (neobsahující slovo " + ASSIGNMENT_DISTINGUISHING_WORD + ").\n" +
            "Poté knihovna musí obsahovat alespoň jeden příklad s dodržením následujících pravidel:\n" +
            "- každý nový příklad musí být uvozen slovem \"" + ASSIGNMENT_DISTINGUISHING_WORD + "\" , poté musí na stejném řádku následovat neprázdný název/popis příkladu, pro lepší orientaci doporučujeme číslovat)\n" +
            "- >>oddělení jedním prázdným řádkem<<\n" +
            "- číslo popisující zadání úkolu (viz. \"Očíslování typů úloh\").\n" +
            "- >>oddělení jedním prázdným řádkem<<\n" +
            "- rovnice prvního podprostoru (dle syntaxe aplikace)\n" +
            "- >>oddělení jedním prázdným řádkem<<\n" +
            "- rovnice druhého podprostoru (dle syntaxe aplikace)\n" +
            "(u úloh typu 1, 2, 8 lze zadat i jen jeden z podprostorů)\n" +
            "Očíslování typů úloh:\n" +
            "1 - Popiš podprostor(y).\n" +
            "2 - Převeď na obecný/parametrický tvar.\n" +
            "3 - Protínají se oba podprostory?\n" +
            "4 - Najdi průnik podprostorů.\n" +
            "5 - Urči vzájemnou polohu podprostorů.\n" +
            "6 - Spočítej vzdálenost podprostorů.\n" +
            "7 - Spočítej odchylku podprostorů.\n" +
            "8 - Spočítej ortogonální doplněk k podprostoru.\n" +
            "např.:\n" +
            "PŘÍKLAD 5 - protínají se?\n" +
            "\n" +
            "3\n" +
            "\n" +
            "-x_1+7x_2-10x_3+4=0\n" +
            "-7x_2+11x_3-x_4-6=0\n" +
            "\n" +
            "-7x_1+9x_2-5x_3-10x_4-2=0\n" +
            "Kontrola syntaxe knihovny i validity každého z příkladů přprobíhá automaticky při nahrání nebo upravení knihovny. \n";

    public static final String ONLINE_SOLVE_HELPTEXT = "Pro řešení úloh je nutno zadat alespoň jeden podprostor některého ze vstupních polí.\n" +
            "Pro různé typy úloh je nutno zadat buď jeden nebo oba podprostory.\n" +
            "Pokud není vstup v pořádku nebo počet/typ podprostorů neodpovídá zvolenému typu úlohy, vypíše se informační zpráva.\n" +
            "Zkontrolovat vstup lze tlačítkem \"Zkontrolovat vstupní formát podprostoru A/B.\"\n" +
            "Po zkontrolování vstupu se objeví matematická reprezentace vstupu formátovaná programem TeX nebo se vypíše chyba.\n" +
            "Po zadání podprostorů vyberte typ úlohy, kterou následně vyřešíte stistknutím tlačítka \"Vyřeš\", které se po vybrání typu úlohy aktivuje.\n" +
            "Po vyřešení příkladu se aktivují tlačítka pro zobrazení textového postupu a PDF postupu, které obsahují detailní postup řešení.\n" +
            "Tyto soubory se nahradí novými po vypočítání jiné úlohy.\n" +
            "Předpřipravená zadání úloh lze načíst z jakékoliv veřejně dostupné knihovny nebo z vlastní knihovny (po přihlášení) tlačítkem \"Použít\" u dané knihovny.\n" +
            "Pro načtení příkladu přímo z ukázkové knihovny lze použít tlačítko \"Použít ukázkovou knihovnu přííkladů\" na této stránce.\n" +
            "Platné zadání lze uložit do vlastní knihovny příkladů (přihlášení uživatelé).\n";

    public static final String ONLINE_GENERATE_HELPTEXT = "Zadání úloh lze kromě načítání z knihoven příkladů také náhodně generovat.\n" +
            "Pod částí stránky řešící úlohy lze zadat parametry pro generování.\n" +
            "Lze zadat podmínky, které generované podprostory musí splňovat (dimenze, typ rovnic, dimenze, vzájemná poloha, apod.).\n" +
            "Lze také zadat podprostor definující průnik generovaných podprostorů, jehož vstup se kontroluje obdobně jako vstup pro řešení úloh.\n" +
            "Po vygenerování zadání se zadání přenese do vstupních polí pro zahájení řešení úlohy.\n" +
            "Vygenerované zadání lze stejně jako jiné zadání úlohy uložit do vlastní knihovny příkladů (přihlášení uživatelé).\n";

    public static final int JLATEXMATH_IMAGE_FONT_SIZE = 20;
    public static final int JLATEXMATH_IMAGE_PADDING_PIXELS = 5;

    public static final String piSymbol = "\u03C0";
    public static final String[] solveTaskList = {"Vyber úlohu:", "Popiš podprostor(y).", "Převeď na obecný/parametrický tvar.", "Protínají se oba podprostory?", "Najdi průnik podprostorů."/*,"Jsou podprostory rovnoběžné?"*/,
            "Urči vzájemnou polohu podprostorů.", "Spočítej vzdálenost podprostorů.", "Spočítej odchylku podprostorů.", "Spočítej ortogonální doplněk k podprostoru."}; //moved from GUI
    public static final String[] generateTaskList = {"Vyber úlohu:", "náhodné podprostory", "podprostory s danou vzájemnou polohou", "podprostory s daným průnikem", "podprostory s danou vzdáleností", "podprostory s danou odchylkou"};
    public static final String[] angleList = {"0 rad (tj. 0°)", piSymbol + "/6 rad (tj. 30°)", piSymbol + "/4 rad (tj. 45°)", piSymbol + "/3 rad (tj. 60°)", piSymbol + "/2 rad (tj. 90°)"};
    public static final String[] relationList = {"různoběžné", "mimoběžné", "rovnoběžné"};
    public static final String[] generatedSubspacesDisjointOptionList = {"Ano", "Ne"};

    public static final String[] generateSubspaceErrorMessages = {"Pro tento typ úlohy jsou zapotřebí oba podprostory; zadejte dimenze obou podprostorů.",
            "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
            "Do políčka pro požadovanou vzdálenost je nutné vepsat nezáporné číslo nebo zlomek (s použitím symbolu lomítka).",
            "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou ani bod, ani celý prostor.",
            "Nelze vygenerovat přímky v rovině s celočíselnými koeficienty tak, aby svíraly úhel 30° nebo 60°. Vyberte, prosím, jinou odchylku nebo dimenzi.",
            "Různoběžné podprostory nemohou být dimenze 0.",
            "Dimenze průniku podprostorů má neplatnou hodnotu.",
            "Mimoběžné podprostory nemohou být dimenze 0.",
            "Podprostory zadaných dimenzí nemohou být mimoběžné (dimenze podprostoru může být maximálně dimenze celého prostoru - 2).",
            "Dimenze průniku zaměření má nesprávnou nebo neplatnou hodnotu.",
            "Soustava rovnic, kterou je zadán průnik podprostorů, není řešitelná. Nejde tak o podprostor a požadovanou operaci tedy nelze provést. Zadejte prosím existující podprostor.",
            "Průnik podprostorů nebyl zadán správně. Opravte, prosím, zadání. Pokud si nejste jistí, jak má zadání vypadat, konzultujte nápovědu k zadávání podprostorů (v horní části okna).",
            "Je nutné zadat platný průnik podprostorů.",
            "Zadaný průnik není podprostor zadaného n-rozměrného prostoru.",
            "Dimenze průniku podprostorů se nenachází v povolených mezích."
    };

    public static final String INPUT_HELP_WINDOW_TEXT = "Podprostor můžete zadat:\nA) OBECNĚ\n=========\n"
            + "\n1. varianta - rovnicemi (příklad):\n" + "2x_1 - 3/2x_2 + x_4 - 3 = 0\n2/3x_2 - 3x_3 = 0\n"
            + "(Pozn.: „= 0“ je nepovinné a lze jej vynechat.)\n"
            + "\n2. varianta - maticí (příklad):\n" + "2, -3/2, 0, 1, -3\n0, 2/3, -3, 0, 0\n"
            + "\nB) PARAMETRICKY\n===============\n"
            + "\n1. varianta - rovnicemi (příklad):\n" + "x_1 = t_1 - 1/3t_2\nx_2 = 1\nx_3 = -2 + 3/2t_2\n"
            + "(Pozn.: „x_1 =“, „x_2 =“ apod. je nepovinné a lze jej vynechat.)\n"
            + "\n2. varianta - body a vektory (příklad):\n" + "[0, 1, -2]\n(1, 0, 0)\n(-1/3, 0, 3/2)\n"
            + "(Pozn.: Bodů může být i více, naopak vektor nemusí být zadán žádný.\nVždy ale alespoň jeden bod musí být zadán!)";
}
