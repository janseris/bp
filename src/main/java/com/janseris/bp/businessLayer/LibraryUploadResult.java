package com.janseris.bp.businessLayer;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-26
 */

public enum LibraryUploadResult {
    SUCCESS_OFFLINE_CHARSET_DETECTION,
    SUCCESS_ONLINE_CHARSET_DETECTION,
    ERROR,
    INVALID_ENCODING
}
