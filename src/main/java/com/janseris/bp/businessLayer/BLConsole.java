package com.janseris.bp.businessLayer;

import com.janseris.bp.IOHelper;
import com.janseris.bp.dataAccessLayer.LibraryDAO;
import com.janseris.bp.dataAccessLayer.UserDAO;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserRegisterDTO;
import com.janseris.bp.model.UserLoginDTO;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.UUID;

/**
 * An alternate runnable main class for testing purposes
 */
public class BLConsole {
    /*
    db query execution sample:
    try (Connection connection = dataSource.getConnection();
    Statement statement = connection.createStatement()) {
        try (ResultSet resultSet = statement.executeQuery("some query")) {
             // Do stuff with the result set.
        }
        try (ResultSet resultSet = statement.executeQuery("some query")) {
            // Do more stuff with the second result set.
        }
    }
     */

    public static void simulateLogIn(String email, String password, LogInService loginService){
        System.out.println("Logging in with credentials: " + email + " " + password);
        UserLoginDTO userInput = new UserLoginDTO(email, password);
        AuthenticationResult result = loginService.validateCredentials(userInput);
        if(result == AuthenticationResult.SUCCESS){
            System.out.println("Logging in: SUCCESS (email and password match)");
        } else if (result == AuthenticationResult.UNKNOWN_EMAIL){
            System.out.println("Logging in: unknown email");
        } else if (result == AuthenticationResult.WRONG_PASSWORD){
            System.out.println("Logging in: wrong password");
        } else if (result == AuthenticationResult.FAILURE){
            System.out.println("Logging in: db error");
        }
    }

    /**
     * Registers a user if user doesn't already exist
     * @param name user name
     * @param email user email
     * @param password user password
     * @param registerService service
     * @param userDAO database access object
     * @throws SQLException on db error while checking user existence
     * @return userId used for sample library insert
     */
    public static UUID simulateRegistration(String name, String email, String password, RegisterService registerService, UserDAO userDAO) throws SQLException {
        UserDTO maybeSampleUser = userDAO.getUserByEmail(email);
        if(maybeSampleUser != null){
            return maybeSampleUser.getId();
        }

        System.out.println("Registrating: " + email + " " + password);
        RegistrationResult res1 = registerService.registerUser(new UserRegisterDTO(name, email, password));
        if(res1 == RegistrationResult.SUCCESS){
            System.out.println("Registration successful");
        } else if (res1 == RegistrationResult.EMAIL_ALREADY_USED){
            System.out.println("User already exists");
        } else if (res1 == RegistrationResult.ERROR){
            System.out.println("Registration threw SQL error");
        }
        return userDAO.getUserByEmail(email).getId();
    }

    public static byte[] Win1250ToUTF8(byte[] source) {
        Charset from = Charset.forName("windows-1250");
        Charset to = StandardCharsets.UTF_8;
        if(IOHelper.isWindows1250(source)){
            return new String(source, from).getBytes(to);
        } else if (IOHelper.isUTF8(source)){
            return source;
        } else {
            throw new IllegalArgumentException("Invalid encoding");
        }
    }

    public static void main(String[] args) throws SQLException, IOException {

        byte[] bytes = Files.readAllBytes(Paths.get("library.txt"));

        UserDAO userDAO = new UserDAO();
        RegisterService registerService = new RegisterService(userDAO);
        LogInService logInService = new LogInService(userDAO);
        LibraryDAO libraryDAO = new LibraryDAO();

        //UserDTO userDTO = new UserDTO();
        //userDTO.setId(UUID.fromString("7B9126E9-5801-48BE-9DB2-0EFD90ED83F5")); //simulates "Loaded user from db and his id is ..."

        //libraryDAO.addLibrary(userDTO, "user1Library1UTF8", bytes);
        //libraryDAO.updateLastUsedDate(UUID.fromString("D05BCDED-8CC2-451B-B73D-1E3FF87EDF30"));

        /*try (Connection connection = ConnectionFactory.getConnection()){
            String schema = connection.getSchema();
            System.out.println("Successful connection - Schema: " + schema);

            System.out.println("Query data example:");
            System.out.println("=========================================");

            // Create and execute a SELECT SQL statement.
            String selectSql = "SELECT TOP 20 usr.Name as Username, library.name as LibraryName "
                    + "FROM [dbo].[Users] usr "
                    + "JOIN [dbo].[Libraries] library ON usr.ID = library.UserID";

            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(selectSql)) {

                // Print results from select statement
                System.out.println("first 20 libraries:");
                while (resultSet.next())
                {
                    System.out.println(resultSet.getString(1) + " : "
                            + resultSet.getString(2)); //POZOR index 0 out of range
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }*/

        System.out.println();

        //simulateRegistration("Uživatel s", "s", "s", registerService);
        //simulateLogIn("magdička1", "1234", logInService);
    }
}
