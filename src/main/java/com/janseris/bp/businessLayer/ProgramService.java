package com.janseris.bp.businessLayer;

import com.janseris.bp.*;
import com.janseris.bp.webApp.model.CheckSubspaceWithImageDTO;
import org.scilab.forge.jlatexmath.ParseException;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static com.janseris.bp.Constants.*;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-05
 */

@Service
public class ProgramService {

    public static final String ONLINE_VERSION_OUTPUT_PDF_NAME = Constants.DESKTOP_VERSION_OUTPUT_PDF_NAME;
    public static final String ONLINE_VERSION_OUTPUT_TXT_NAME = Constants.DESKTOP_VERSION_OUTPUT_TXT_NAME;
    public static final String ONLINE_VERSION_OUTPUT_TEX_NAME = Constants.DESKTOP_VERSION_OUTPUT_TEX_NAME;

    public static final String CHECK_SUBSPACE_OK_MESSAGE = "Formát podprostoru je v pořádku.";

    /**
     * Gets position of string in a given string array<br>
     * Used for automated HTML select & option tags selection on page reload
     * @param value
     * @param elements
     * @return
     */
    public static int getElementIndex(String value, String[] elements){
        if(value == null){
            return 0; //not selected -> select first option
        }
        ArrayList<String> values = new ArrayList<>(Arrays.asList(elements));
        return values.indexOf(value);
    }

    //every "solve" or "show results" request requires a valid SID which defines IOHelper which remembers computation output for each computation and defines a unique files folder

    public IOHelper createIOHelperInstance(String sid /* folder name for current user */){
        return new IOHelper(sid, ONLINE_VERSION_OUTPUT_TXT_NAME, ONLINE_VERSION_OUTPUT_TEX_NAME, ONLINE_VERSION_OUTPUT_PDF_NAME);
    }

    public InputReader createInputReaderInstance(IOHelper iohelper){
        return new InputReader(iohelper);
    }

    //pdf is generated directly using IOHelper

    public byte[] getTxtSolution(String sid) throws IOException {
        Path textFilePath = Paths.get(sid, ONLINE_VERSION_OUTPUT_TXT_NAME); //.NET Path.Combine
        return Files.readAllBytes(textFilePath);
    }

    public byte[] getPdfSolution(String sid) throws IOException {
        Path pdfFilePath = Paths.get(sid, ONLINE_VERSION_OUTPUT_PDF_NAME); //.NET Path.Combine
        return Files.readAllBytes(pdfFilePath);
    }


    public CheckSubspaceWithImageDTO checkSubspace(IOHelper ioHelper, String input, boolean createImage){
        if(input == null || input.isEmpty()){
            return new CheckSubspaceWithImageDTO("Prosím, zadejte podprostor ke kontrole.", null);
        }

        Subspace subspace;
        try{
            subspace = createInputReaderInstance(ioHelper).getSubspace(input); //new ioHelper instance can be created, txt/tex output files are not affected
        } catch (NumberFormatException ex){
            return new CheckSubspaceWithImageDTO("Zadaný formát podprostoru není platný!", null);
        } catch (UnsupportedOperationException ex){
            return new CheckSubspaceWithImageDTO("Zadaná soustava rovnic nemá řešení!", null);
        } catch (Exception ex){
            return new CheckSubspaceWithImageDTO("Zadaný formát podprostoru není platný!", null); //other unspecified error
        }

        //empty input or other invalid case
        if(subspace.isNull()){
            return new CheckSubspaceWithImageDTO("Zadaný formát podprostoru není platný!", null);
        }

        if(!createImage){
            return new CheckSubspaceWithImageDTO(CHECK_SUBSPACE_OK_MESSAGE, null);
        }

        //create image

        BufferedImage image = null;
        try{
            image = subspace.getTeXFormulaAsImage();
        } catch (ParseException ex){
            return new CheckSubspaceWithImageDTO("Zadaný formát podprostoru není platný.", null);
        }

        byte[] imageBytes = null;
        try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream()){
            ImageIO.write(image, "png", outputStream);
            imageBytes = outputStream.toByteArray();
        } catch (IOException e) {
            return new CheckSubspaceWithImageDTO("Formát podprostoru je platný, ale při generování obrázku nastala chyba.", null);
        }
        return new CheckSubspaceWithImageDTO(CHECK_SUBSPACE_OK_MESSAGE, imageBytes);
    }

    /**
     * Prepares output .txt and .tex files for output (clears them) and creates the user's folder if it doesn't exist
     * @param ioHelper ioHelper which defines current user (sid = output folder path)
     * @throws SecurityException if program is not permitted to create folders/files
     * @throws FileNotFoundException if there was an I/O error while clearing output files
     */
    public void clearOutputFiles(IOHelper ioHelper) throws SecurityException, FileNotFoundException {
        IOHelper.guaranteeDirectoryExistence(ioHelper.getOutputFolderPath());
        IOHelper.clearFile(Paths.get(ioHelper.getOutputFolderPath(), ioHelper.getOutputTxtFileName()).toString());
        IOHelper.clearFile(Paths.get(ioHelper.getOutputFolderPath(), ioHelper.getOutputTeXFileName()).toString());
    }

    /**
     * Constructs an Assignment object from previously validated input<br>
     * The assignment name is empty
     * @param task selected task name
     * @param subspace1Text valid input 1 lines if any
     * @param subspace2Text valid input 2 lines if any
     * @return valid assignment object ready for inserting into library
     */
    public Assignment getAssignmentFromProgramInput(String task, String subspace1Text, String subspace2Text){
        ArrayList<String> subspace1Lines = new ArrayList<>();
        ArrayList<String> subspace2Lines = new ArrayList<>();

        try(Scanner scanner = new Scanner(subspace1Text)){
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                subspace1Lines.add(line);
            }
        }

        try(Scanner scanner = new Scanner(subspace2Text)){
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                subspace2Lines.add(line);
            }
        }

        int taskIndex = getElementIndex(task, solveTaskList);

        return new Assignment("", taskIndex, subspace1Lines, subspace2Lines);
    }

    public AssignmentGenerator createAssignmentGeneratorInstance(IOHelper ioHelper, InputReader inputReader) {
        return new AssignmentGenerator(ioHelper, inputReader);
    }
}
