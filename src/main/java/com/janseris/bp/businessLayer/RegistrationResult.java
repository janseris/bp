package com.janseris.bp.businessLayer;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-01
 */

public enum RegistrationResult {
    SUCCESS,
    ERROR,
    EMAIL_ALREADY_USED,
    INVALID_EMAIL,
    INVALID_NAME,
    INVALID_PASSWORD
}
