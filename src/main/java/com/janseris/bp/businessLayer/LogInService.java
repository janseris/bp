package com.janseris.bp.businessLayer;

import com.janseris.bp.dataAccessLayer.UserDAO;

import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserLoginDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

import static com.janseris.bp.dataAccessLayer.Utilities.getPasswordHash;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-20
 */

@Service
public class LogInService {

    private final UserDAO userDAO;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public LogInService(UserDAO userDAO){
        this.userDAO = userDAO;
    }

    /**
     * Used to show user's details in view
     * @param email for email (passed in during login)
     * @return User's details
     * @throws SQLException
     */
    public UserDTO loadUserDataAfterLogin(String email) throws SQLException {
        return userDAO.getUserByEmail(email);
    }

    /**
     * Checks log-in input<br>
     * If successful, update last log-in time
     * @param userInput email and password
     * @return INVALID_EMAIL if user email is unknown<br>
     *         INVALID_PASSWORD if password does not match<br>
     *         SUCCESS if log in was successful<br>
     *         FAILURE if db communication error was encountered during validation process
     */
    public AuthenticationResult validateCredentials(UserLoginDTO userInput){
        try {
            UserDTO user = userDAO.getUserByEmail(userInput.getEmail());
            if (user == null) {
                return AuthenticationResult.UNKNOWN_EMAIL;
            }
            String comparedPasswordHash = getPasswordHash(userInput.getPassword(), user.getPasswordSalt());
            if (!user.getPasswordHash().equals(comparedPasswordHash)) {
                return AuthenticationResult.WRONG_PASSWORD;
            }
            userDAO.updateLastLoginDate(user.getId());
            return AuthenticationResult.SUCCESS;
        } catch (SQLException ex){
            log.warn("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during login for user " + userInput.getEmail());
            return AuthenticationResult.FAILURE;
        }
    }

}
