package com.janseris.bp.businessLayer;

import com.janseris.bp.dataAccessLayer.UserDAO;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserShowDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-02-29
 */

@Service
public class UserService {

    private final UserDAO userDAO;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public UserService(UserDAO userDAO){
        this.userDAO = userDAO;
    }

    private static int RANDOM_PASSWORD_LENGTH = 16;

    /**
     * Gets selected user data for public access.<br>
     * Used when a user is showing details about other user.
     * @param userId
     * @return user's name and login,registration date
     * @throws SQLException if db error is encountered, handled in controller
     */
    public UserShowDTO showUserForPublic(UUID userId) throws SQLException {
        UserDTO userData = userDAO.getUserById(userId);
        UserShowDTO userPublicData = new UserShowDTO();
        userPublicData.setName(userData.getName());
        userPublicData.setLastLogInDate(userData.getLastLogInDate());
        userPublicData.setRegistrationDate(userData.getRegistrationDate());
        return userPublicData;
    }

    /**
     * Sets user's password to a randomly generated string of defined length
     * @see UserService#RANDOM_PASSWORD_LENGTH
     * @param userId user Id
     * @return password which is then sent to user via email
     * @throws SQLException if update fails or db error is encountered, handled in controller
     */
    public String resetPassword(UUID userId) throws SQLException {
        final String alphaNumericCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder( RANDOM_PASSWORD_LENGTH );
        for( int i = 0; i < RANDOM_PASSWORD_LENGTH; i++ ) {
            sb.append(alphaNumericCharacters.charAt(rnd.nextInt(alphaNumericCharacters.length())));
        }

        String password = sb.toString();
        userDAO.updateUserPassword(userId, password);
        return password;
    }

    /**
     * Sets user's password to a string given by user
     * @param userId user Id
     * @throws SQLException if update fails or db error is encountered, handled in controller
     */
    public void changePassword(UUID userId, String newPassword) throws SQLException {
        userDAO.updateUserPassword(userId, newPassword);
    }

    /**
     * Checks if an user with given email exists
     * @param email email
     * @return user with this email exists
     * @throws SQLException on db error
     */
    public UserDTO checkUserExistence(String email) throws SQLException {
        return userDAO.getUserByEmail(email);
    }
}
