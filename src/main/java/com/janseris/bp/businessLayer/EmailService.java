package com.janseris.bp.businessLayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.Session;
import javax.mail.Transport;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-23
 */

@Service
public class EmailService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Used to receive emails about application runtime errors, either manually reported by users or automatically sent by the application (if error is of unexpected type)
     * e.g. automatic email is generated in:
     * @see #sendComputationErrorEmail
     */
    private static String applicationMaintainerEmail = null;

    /**
     * Used to send emails to users or to application maintainer
     */
    private static String applicationSourceEmail = null; /* xseris@fi.muni.cz */
    private static String applicationSourceEmailPassword = null; /* secondary password */
    private static String applicationSourceEmailHost = null; /* relay.fi.muni.cz */
    private static String applicationSourceEmailPort = null; /* 465 */
    private static String applicationSourceEmailSSLEnableBoolean = null; /* true */
    private static String applicationSourceEmailAuthEnableBoolean = null; /* true */

    public static void setApplicationMaintainerEmail(String applicationMaintainerEmail) {
        EmailService.applicationMaintainerEmail = applicationMaintainerEmail;
    }

    public static void setApplicationSourceEmail(String applicationSourceEmail) {
        EmailService.applicationSourceEmail = applicationSourceEmail;
    }

    public static void setApplicationSourceEmailPassword(String applicationSourceEmailPassword) {
        EmailService.applicationSourceEmailPassword = applicationSourceEmailPassword;
    }

    public static void setApplicationSourceEmailHost(String applicationSourceEmailHost) {
        EmailService.applicationSourceEmailHost = applicationSourceEmailHost;
    }

    public static void setApplicationSourceEmailPort(String applicationSourceEmailPort) {
        EmailService.applicationSourceEmailPort = applicationSourceEmailPort;
    }

    public static void setApplicationSourceEmailSSLEnableBoolean(String applicationSourceEmailSSLEnableBoolean) {
        EmailService.applicationSourceEmailSSLEnableBoolean = applicationSourceEmailSSLEnableBoolean;
    }

    public static void setApplicationSourceEmailAuthEnableBoolean(String applicationSourceEmailAuthEnableBoolean) {
        EmailService.applicationSourceEmailAuthEnableBoolean = applicationSourceEmailAuthEnableBoolean;
    }

    /**
     * Parses "solve form" values (only "solve" input) if error happens to a format for email to the application maintainer
     * @param request current request
     */
    private String programInputToEmailText(HttpServletRequest request, Exception ex){
        StringBuilder builder = new StringBuilder();
        //String requestPath = request.getServletPath().replace("/", "");
        builder.append("Unexpected problem occured during solving an assignment: \"solve assignment\"").append("\n");
        builder.append("subspace1Input: ").append(request.getParameter("subspace1Input")).append("\n");
        builder.append("subspace2Input: ").append(request.getParameter("subspace2Input")).append("\n");
        builder.append("selected assignment type: ").append(request.getParameter("assignmentType")).append("\n");
        builder.append("Exception: ").append(ex.getClass()).append(" with message: ").append(ex.getLocalizedMessage()).append("\n");
        return builder.toString();
    }

    public void sendComputationErrorEmail(HttpServletRequest request, Exception unexpectedProblem){
        String emailText = programInputToEmailText(request, unexpectedProblem);
        sendEmailToMaintainer("Analytic Geometry Online unexpected solve error", emailText);
        log.info("Unexpected solve error (different than Stackoverflow.class) email report sent");
    }

    public void sendPasswordResetEmail(String newPassword, String recipientEmail){
        String emailText = "Dobrý den,\n\n" +
                "Vaše přístupové heslo do aplikace Analytic Geometry Online bylo obnoveno.\n" +
                "Vaše nové automaticky vygenerované heslo je: " + newPassword + "\n" +
                "Po přihlášení s tímto heslem si můžete heslo změnit v sekci \"Stránka uživatele\".\n" +
                "Přejeme příjemný den\n";
        sendEmail(recipientEmail, "Analytic Geometry Online - zapomenuté heslo", emailText);
    }


    /**
     * Sends an email to a person who is responsible for maintaining this web application
     * @param subject
     * @param text
     */
    public void sendEmailToMaintainer(String subject, String text){
        sendEmail(applicationMaintainerEmail, subject, text);
    }

    /**
     * Sends an email to application creator
     * @param sender filled by user (optional)
     * @param errorReport filled by user
     */
    public void sendErrorReportEmail(String errorReport, String sender){
        StringBuilder emailText = new StringBuilder();
        emailText.append("Odesílatel: ").append(sender).append("\n");
        emailText.append("Popis problému: ").append(errorReport).append("\n");
        sendEmailToMaintainer("Analytická Geometrie Online - nahlášení chyby uživatelem", emailText.toString());
    }

    /**
     * Sends an email from application to destination
     * @see #applicationSourceEmail
     * @param recipient
     * @param subject
     * @param text
     */
    public void sendEmail(String recipient, String subject, String text){
        // how to set up properties -> https://www.fi.muni.cz/tech/unix/mail.html.cs

        // sender email
        String sender = applicationSourceEmail;

        // using host as localhost
        String host = applicationSourceEmailHost; /* relay.fi.muni.cz */

        // Getting system properties
        Properties properties = System.getProperties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", applicationSourceEmailPort); /* 465 */
        properties.put("mail.smtp.ssl.enable", applicationSourceEmailSSLEnableBoolean); /* true */
        properties.put("mail.smtp.auth", applicationSourceEmailAuthEnableBoolean); /* true */

        // Setting up mail server
        //properties.setProperty("mail.smtp.host", host);

        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(applicationSourceEmail, applicationSourceEmailPassword);
            }
        });

        try
        {
            // MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From Field: adding senders email to from field.
            message.setFrom(new InternetAddress(sender));

            // Set To Field: adding recipient's email to from field.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));

            // Set Subject: subject of the email
            message.setSubject(subject);

            // set body of the email.
            message.setText(text);

            // Send email.
            Transport.send(message);
            log.trace("Email to " + recipient + " successfully sent");
        }
        catch (MessagingException ex)
        {
            log.warn("An error occurred while sending email to " + recipient + ", subject: " + subject + ": " + ex.getClass() + " " + ex.getLocalizedMessage());
        }
    }
}

