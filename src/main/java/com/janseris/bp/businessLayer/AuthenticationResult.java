package com.janseris.bp.businessLayer;

/**
 * Used to define logging in attempt result
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-01
 */

public enum AuthenticationResult {
    SUCCESS,
    FAILURE,
    UNKNOWN_EMAIL,
    WRONG_PASSWORD
}
