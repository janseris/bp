package com.janseris.bp.businessLayer;

import com.janseris.bp.dataAccessLayer.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.janseris.bp.model.UserRegisterDTO;
import com.janseris.bp.model.UserDTO;

import java.sql.SQLException;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-24
 */

@Service
public class RegisterService {

    public static final int MIN_PASSWORD_LENGTH = 8;
    public static final String INSUFFICIENT_PASSWORD_LENGTH_ERROR_MESSAGE = "Zadané heslo je příliš krátké, musí být dlouhé alespoň " + RegisterService.MIN_PASSWORD_LENGTH + " znaků.";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private  UserDAO userDAO;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public RegisterService(UserDAO userDAO){
        this.userDAO = userDAO;
    }

    public static boolean isPasswordValid(String password){
        return password.length() >= MIN_PASSWORD_LENGTH;
    }

    private boolean isEmailAlreadyUsed(String email)  throws SQLException {
        UserDTO user = userDAO.getUserByEmail(email);
        return user != null;
    }

    private static boolean isEmailValid(String email)  {
        return email.length() > 0 && email.contains("@");
    }

    private static boolean isNameValid(String name){
        return name.length() > 0;
    }

    public RegistrationResult registerUser(UserRegisterDTO registrationData){
        if(!isNameValid(registrationData.getName())){
            return RegistrationResult.INVALID_NAME;
        }
        if(!isEmailValid(registrationData.getEmail())){
            return RegistrationResult.INVALID_EMAIL;
        }
        if(!isPasswordValid(registrationData.getPassword())){
            return RegistrationResult.INVALID_PASSWORD;
        }
        try{
            if(isEmailAlreadyUsed(registrationData.getEmail())){
                return RegistrationResult.EMAIL_ALREADY_USED;
            }
            boolean success = userDAO.addUser(registrationData);
            if(!success){
                return RegistrationResult.ERROR;
            }
        } catch (SQLException ex){
            log.warn("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during user " + registrationData.getEmail() + " registration");
            return RegistrationResult.ERROR;
        }
        return RegistrationResult.SUCCESS;
    }
}
