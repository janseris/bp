package com.janseris.bp.businessLayer;

import com.janseris.bp.*;
import com.janseris.bp.dataAccessLayer.LibraryDAO;
import com.janseris.bp.model.SQLResultOrderingMode;
import com.janseris.bp.model.LibraryOrderingColumn;
import com.janseris.bp.dataAccessLayer.UserDAO;
import com.janseris.bp.model.*;
import com.janseris.bp.webApp.model.AssignmentShowDTO;
import com.janseris.bp.webApp.model.LibraryUseDTO;
import com.janseris.bp.webApp.model.PublicLibraryShowDTO;
import com.janseris.bp.webApp.model.UserLibrariesSimpleListDTO;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-02-29
 */


@Service
public class LibraryService {

    private final LibraryDAO libraryDAO;
    private final UserDAO userDAO;

    private final static Logger log = LoggerFactory.getLogger(LibraryService.class);

    /**
     * Set on application startup
     * @see com.janseris.bp.webApp.DatabaseSeeder
     */
    private UUID SAMPLE_LIBRARY_ID = null;

    /**
     * Set on application startup & maybe updated during application life
     * @see com.janseris.bp.webApp.DatabaseSeeder
     */
    private LibraryUseDTO cachedSampleLibraryForUse = null;
    private LibraryDTO cachedSampleLibrary = null;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public LibraryService(LibraryDAO libraryDAO, UserDAO userDAO){
        this.libraryDAO = libraryDAO;
        this.userDAO = userDAO;
    }

    public void setSampleLibraryId(UUID id) {
        if(SAMPLE_LIBRARY_ID != null){
            throw new IllegalStateException("Sample library ID already set in LibraryDAO");
        }
        this.SAMPLE_LIBRARY_ID = id;
    }

    public void updateCachedSampleLibrary(LibraryDTO library, LibraryUseDTO libraryForUse){
        cachedSampleLibrary = library;
        cachedSampleLibraryForUse = libraryForUse;
    }

    /**
     * Returns the cached version of sample library ready to be used
     * Performs an asynchronous call to database updating the last used date
     * If updating last used date fails, the error is logged.
     * @return sample library ready to be used
     */
    public LibraryUseDTO loadSampleLibraryForUse(boolean updateLastUsedDate) {
        if(updateLastUsedDate){
            try{
                libraryDAO.updateSampleLibraryLastUsedDateAsync(SAMPLE_LIBRARY_ID);
            } catch (SQLException ex){
                log.warn(ex.getLocalizedMessage());
            }
        }
        return cachedSampleLibraryForUse;
    }

    public LibraryDTO getSampleLibrary(){
        return cachedSampleLibrary;
    }



    /**
     * Transforms text file bytes encoded in UTF-8 from database table Libraries into a String
     * @param bytes text file from db
     * @return string containing text from source text file
     */
    public static String getLibraryContentsString(byte[] bytes){
        return new String(bytes, StandardCharsets.UTF_8);
    }

    /**
     * Detects charset using nlp.fi.muni.cz tool "Chared"
     * @see <a href="https://nlp.fi.muni.cz/projects/chared/">https://nlp.fi.muni.cz/projects/chared/</a>
     * @param source text file bytes
     * @return
     * @throws IOException on communication error with nlp.fi.muni.cz
     * @throws IllegalArgumentException if the charset name returned from nlp.fi.muni.cz was not recognized
     * @throws UnsupportedOperationException if nlp.fi.muni.cz request does not succeed or their response body changes -> switch to regular detection
     */
    public static Charset detectCharsetOnline(byte[] source) throws IOException {
        String url = "https://nlp.fi.muni.cz/projects/chared/";
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpEntity entity = MultipartEntityBuilder
                .create()
                .addTextBody("language", "czech")
                .addBinaryBody("file", source, ContentType.create("application/octet-stream"), "filename")
                .addTextBody("submit_file", "Submit Query")
                .build();
        HttpPost request = new HttpPost(url);
        request.setEntity(entity);
        HttpResponse response = null;
        try{
            response = httpclient.execute(request);
        } catch (Exception ex) { //communication error
            throw new IOException(ex);
        }
        return getCharsetStrFromResponse(response);
    }

    /**
     * Gets charset from nlp.fi.muni.cz http response
     * @param response
     * @return
     * @throws IOException
     * @throws IllegalArgumentException if the charset name returned from nlp.fi.muni.cz was not recognized
     * @throws UnsupportedOperationException if nlp.fi.muni.cz request does not succeed or their response body changes -> switch to regular detection
     */
    private static Charset getCharsetStrFromResponse(HttpResponse response) throws IllegalArgumentException, UnsupportedOperationException{
        int responseStatusCode = response.getStatusLine().getStatusCode();
        if(responseStatusCode!= 200){
            throw new UnsupportedOperationException("Unexpected nlp.fi.muni charset detection tool response status code " + responseStatusCode);
        }
        String responseString;
        try{
            responseString = new BasicResponseHandler().handleResponse(response);
        } catch (IOException ex){
            return StandardCharsets.UTF_8;
        }
        String searched = "Detected by Chared: ";
        int searchedIndex = responseString.indexOf("Detected by Chared: ");
        int startIndex = responseString.indexOf("Detected by Chared: ") + searched.length();
        int endH3Index = responseString.indexOf("</h3>", startIndex);
        if(searchedIndex == -1 || endH3Index == -1){
            throw new UnsupportedOperationException("nlp.fi.muni charset detection tool response body pattern changed");
        }
        String charsetStr = responseString.substring(startIndex, endH3Index);
        return parseCharsetStr(charsetStr);
    }

    /**
     * Tries to recognize the charset by charset name returned from nlp.fi.muni.cz
     * @param charsetStr charset name returned from nlp.fi.muni.cz
     * @return
     * @throws IllegalArgumentException if the charset name returned from nlp.fi.muni.cz was not recognized
     */
    private static Charset parseCharsetStr(String charsetStr) throws IllegalArgumentException{
        if(charsetStr != null && charsetStr.contains("_")){
            charsetStr = charsetStr.replace('_', '-'); //nlp.fi.muni.cz returns "utf_8" etc.
        }
        try{
            return Charset.forName(charsetStr);
        } catch (Exception ex){
            throw new IllegalArgumentException(charsetStr); //charset name not recognized (null or invalid charset name)
        }
    }

    /**
     * Gets column name to be used for ordering libraries in a SELECT query
     * @param columnName enum value
     * @return column name for database query
     */
    public LibraryOrderingColumn getSelectedOrderingColumn(String columnName){
        switch(columnName){
            case "Library name":
                return LibraryOrderingColumn.NAME;
            case "Creation date":
                return LibraryOrderingColumn.DATE_CREATED;
            case "Last modification date":
                return LibraryOrderingColumn.DATE_MODIFIED;
            case "Last usage date":
                return LibraryOrderingColumn.DATE_USED;
            case "Usage count":
                return LibraryOrderingColumn.USAGE_COUNT;
            default:
                throw new IllegalArgumentException(
                        "Zvolený způsob řazení není platný.");
        }
    }

    /**
     * Gets ordering mode for ordering libraries in a SELECT query
     * @param orderingMode enum value
     * @return ordering mode for database query
     */
    public SQLResultOrderingMode getSelectedOrderingMode(String orderingMode){
        switch(orderingMode){
            case "Ascending":
                return SQLResultOrderingMode.ASCENDING;
            case "Descending":
                return SQLResultOrderingMode.DESCENDING;
            default:
                throw new IllegalArgumentException("Zvolený způsob výpisu seřazených knihoven není platný.");
        }
    }





    /**
     * Loads all libraries created by user with given Id (used to show user's own library collection)
     * @param userId
     * @return
     * @throws SQLException
     */
    public List<LibraryDTO> getAllUserLibraries(UUID userId) throws SQLException {
        return libraryDAO.getLibrariesCreatedByUser(userId);
    }

    /**
     * Saves the library file into database.<br>
     * File is saved in UTF-8 text enconding.<br>
     * Only a valid library is successfully saved into database.<br>
     * If online encoding detection is available, any encoding is converted into UTF-8<br>
     * If online encoding detection is not available, only Windows-1250 and UTF-8 encoding is supported<br>
     * Line endings are converted to \r\n<br>
     * If any unknown encoding is detected, INVALID ENCODING is returned.
     * @param user
     * @param name
     * @param bytes a library text file of any encoding
     * @return SUCCESS_ONLINE_CHARSET_DETECTION/SUCCESS_OFFLINE_CHARSET_DETECTION on success, INVALID ENCODING upon unknown encoding detected, ERROR on SQLException
     * @throws LibrarySyntaxErrorException with error message if any syntax error within parsing any assignment is encountered
     * @throws LibraryLogicalErrorException with error message if any logical error within parsing any assignment is encountered
     */
    public LibraryUploadResult addLibrary(UserDTO user, String name, byte[] bytes) throws LibrarySyntaxErrorException, LibraryLogicalErrorException {
        boolean onlineCharsetDetectionUsed = false;
        try{
            byte[] utf8bytes;
            try{
                utf8bytes = tryGetUTF8(bytes); //online detection, more reliable
                onlineCharsetDetectionUsed = true;
            } catch (Exception ex){
                log.warn("Online charset detection error: " + ex.getLocalizedMessage() + " (" + ex.getClass() + ")");
                utf8bytes = IOHelper.convertToUTF8(bytes); //old offline way, less reliable
            }

            String libraryWithUniformLineSeparator = IOHelper.convertToWindowsLineSeparator(new String(utf8bytes, StandardCharsets.UTF_8)); //convert to \r\n EOL

            List<Assignment> parsedAssignments = getAssignmentsFromLibrary(libraryWithUniformLineSeparator); //check syntax
            validateAllAssignments(parsedAssignments); //check assignments logic

            utf8bytes = libraryWithUniformLineSeparator.getBytes(StandardCharsets.UTF_8);

            boolean success = libraryDAO.addLibrary(user.getId(), name, utf8bytes);
            if(success){
                if(onlineCharsetDetectionUsed){
                    return LibraryUploadResult.SUCCESS_ONLINE_CHARSET_DETECTION;
                }
                return LibraryUploadResult.SUCCESS_OFFLINE_CHARSET_DETECTION;
            }
        } catch (SQLException ex){
            return LibraryUploadResult.ERROR;
        } catch (IllegalArgumentException ex){
            return LibraryUploadResult.INVALID_ENCODING;
        }
        return LibraryUploadResult.ERROR;
    }

    /**
     * Clones the current version of the sample library as a private library with the same name and contents into user's libraries
     * @param user who clones the sample library
     * @throws SQLException if an error occurred during insertion
     */
    public void addSampleLibraryForUser(UserDTO user) throws SQLException {
        LibraryDTO sampleLibrary = cachedSampleLibrary;
        boolean success = libraryDAO.addLibrary(user.getId(), sampleLibrary.getName(), sampleLibrary.getContents().getBytes(StandardCharsets.UTF_8));
        if(!success){
            throw new SQLException("An error occurred while trying to add the sample library to user " + user.getId() + "'s libraries.");
        }
    }

    /**
     * Updates selected library with new data
     * Accepted content encoding is UTF-8 and Windows-1250
     * Content is automatically converted to UTF-8, line endings are not modified and every assignment is validated
     * Last modify date is automatically updated upon successful db query
     * @param library
     * @return
     */
    public LibraryUploadResult updateLibrary(LibraryUpdateDTO library) throws LibrarySyntaxErrorException, LibraryLogicalErrorException{
        boolean success = false;
        boolean onlineCharsetDetectionUsed = false;
        try{
            if(!library.hasContents()){ //only name or public accessibility was edited
                success = libraryDAO.updateLibrary(library.getId(), library.getName(),  library.getPublicAccessibility());
            } else {
                if(library.getContentsFile() != null){ //source = text file uploaded by user
                    byte[] utf8bytes;
                    byte[] sourceBytes = library.getContentsFile();
                    try{
                        utf8bytes = tryGetUTF8(sourceBytes); //online detection, more reliable
                        onlineCharsetDetectionUsed = true;
                    } catch (Exception ex){
                        log.warn("Online charset detection error: " + ex.getLocalizedMessage() + " (" + ex.getClass() + ")");
                        utf8bytes = IOHelper.convertToUTF8(sourceBytes); //old offline GUI way, less reliable
                    }

                    String libraryWithUniformLineSeparator = IOHelper.convertToWindowsLineSeparator(new String(utf8bytes, StandardCharsets.UTF_8)); //convert to \r\n EOL

                    List<Assignment> parsedAssignments = getAssignmentsFromLibrary(libraryWithUniformLineSeparator); //check syntax
                    validateAllAssignments(parsedAssignments); //check assignments logic

                    success = libraryDAO.updateLibrary(library.getId(), library.getName(), utf8bytes, library.getPublicAccessibility());
                } else { //source = html textarea -> always UTF-8
                    String libraryWithUniformLineSeparator = IOHelper.convertToWindowsLineSeparator(library.getTextContents()); //convert to \r\n EOL

                    List<Assignment> parsedAssignments = getAssignmentsFromLibrary(libraryWithUniformLineSeparator); //check syntax
                    validateAllAssignments(parsedAssignments); //check assignments logic

                    success = libraryDAO.updateLibrary(library.getId(), library.getName(), libraryWithUniformLineSeparator.getBytes(StandardCharsets.UTF_8), library.getPublicAccessibility());
                }

            }
            if(success){
                if(library.getId().equals(SAMPLE_LIBRARY_ID)){ //if the edited library was the sample library, update the cached objects
                    LibraryDTO updatedSampleLibrary = getLibraryDetails(library.getId(), true);
                    LibraryUseDTO parsedUpdatedSampleLibrary = loadLibraryForUse(updatedSampleLibrary, false, true);
                    updateCachedSampleLibrary(updatedSampleLibrary, parsedUpdatedSampleLibrary);
                }
                if(onlineCharsetDetectionUsed){
                    return LibraryUploadResult.SUCCESS_ONLINE_CHARSET_DETECTION;
                }
                return LibraryUploadResult.SUCCESS_OFFLINE_CHARSET_DETECTION;
            }
        } catch (SQLException ex){
            return LibraryUploadResult.ERROR;
        } catch (IllegalArgumentException ex){
            return LibraryUploadResult.INVALID_ENCODING;
        }

        log.error("Library " + library.getId() + " update: no rows updated");
        return LibraryUploadResult.ERROR;
    }

    /**
     * Checks if the selected user has permission to edit the library
     * @param userId source user
     * @param libraryId target library
     * @return user has the permission to edit the library
     * @throws SQLException on db communication error
     */
    public boolean isOwner(UUID userId, UUID libraryId) throws SQLException {
        return libraryDAO.isOwner(userId, libraryId);
    }

    /**
     * Gets library data from db
     * @param libraryId
     * @return library or null if no match
     * @throws SQLException on db communication error
     */
    public LibraryDTO getLibraryDetails(UUID libraryId) throws SQLException {
        return getLibraryDetails(libraryId, false);
    }

    /**
     * Gets library data from db
     * @param libraryId
     * @return library or null if no match
     * @throws SQLException on db communication error
     */
    private LibraryDTO getLibraryDetails(UUID libraryId, boolean forceSampleLibraryDBCall) throws SQLException {
        if(!forceSampleLibraryDBCall && libraryId.equals(SAMPLE_LIBRARY_ID)){
            return getSampleLibrary();
        }
        return libraryDAO.getLibraryById(libraryId);
    }

    /**
     * Gets all publicly accessible libraries with user names loaded from db
     * Default ordering: last date used, lastest first
     * @return
     * @throws SQLException on db communication error
     */
    public List<PublicLibraryShowDTO> getAllPublicLibraries() throws SQLException {
        List<LibraryDTO> plainLibraries = libraryDAO.getAllPublicLibraries();
        return loadUserInfos(plainLibraries);
    }

    /**
     * Gets all publicly accessible libraries with user names loaded from db
     * With custom ordering
     * @return
     * @throws SQLException on db communication error
     */
    public List<PublicLibraryShowDTO> getAllPublicLibraries(LibraryOrderingColumn column, SQLResultOrderingMode orderingMode) throws SQLException {
        List<LibraryDTO> plainLibraries = libraryDAO.getAllPublicLibraries(column, orderingMode);
        return loadUserInfos(plainLibraries);
    }

    private List<PublicLibraryShowDTO> loadUserInfos(List<LibraryDTO> plainLibraries) throws SQLException {
        List<PublicLibraryShowDTO> librariesWithUserInfo = new ArrayList<>();
        for(LibraryDTO library : plainLibraries){
            UserDTO owner = getOwner(library);
            PublicLibraryShowDTO withUserInfo = new PublicLibraryShowDTO(library);
            withUserInfo.setCreatorName(owner.getName());
            librariesWithUserInfo.add(withUserInfo);
        }
        return librariesWithUserInfo;
    }

    public void deleteLibrary(UUID id) throws SQLException {
        libraryDAO.deleteLibraryById(id);
    }

    public UserDTO getOwner(LibraryDTO library) throws SQLException {
        return userDAO.getUserById(library.getCreatorUserId());
    }

    //java UUID ordering is hex-number based but db UUID ordering is different

    /**
     * Tries to obtain a UTF-8 version of text content using online charset detection
     * @param textFile text content
     * @return text content converted to UTF-8 or text content if encoding was UTF-8
     * @throws Exception if online charset detection fails
     */
    public static byte[] tryGetUTF8(byte[] textFile) throws Exception {
        Charset onlineDetectedCharset;
        onlineDetectedCharset = detectCharsetOnline(textFile);
        if(onlineDetectedCharset == StandardCharsets.UTF_8){
            return textFile;
        }
        log.trace("Online charset detection detected a different encoding than UTF-8: " + onlineDetectedCharset);
        return new String(textFile, onlineDetectedCharset).getBytes(StandardCharsets.UTF_8);
    }

    public LibraryUseDTO loadLibraryForUse(LibraryDTO library, boolean updateLastUsedDate) throws SQLException, LibrarySyntaxErrorException {
        return loadLibraryForUse(library, updateLastUsedDate, false);
    }

        /**
         * Loads library and parses all library assignments to be displayed and used in web GUI
         * @param library library previously loaded from db on access right check
         * @return object for displaying library assignments in /program view
         * @throws SQLException if library could not be loaded from DB
         * @throws LibrarySyntaxErrorException on library syntax error (shall not happen because libraries are checked before saved into the database)
         */
    public LibraryUseDTO loadLibraryForUse(LibraryDTO library, boolean updateLastUsedDate, boolean forceParseSampleLibrary) throws SQLException, LibrarySyntaxErrorException {
        if(!forceParseSampleLibrary && library.getId().equals(SAMPLE_LIBRARY_ID)){
            return loadSampleLibraryForUse(updateLastUsedDate);
        }

        UUID id = library.getId();
        String name = library.getName();
        String contents = library.getContents();
        if(updateLastUsedDate){
            libraryDAO.updateLastUsedDate(id); //the only db call within this function
        }

        LibraryUseDTO libraryForProgram = new LibraryUseDTO();
        libraryForProgram.setId(id);
        libraryForProgram.setName(name);
        //parse contents into Assignment objects
        List<Assignment> assignments = getAssignmentsFromLibrary(contents);
        //transform assignments into a better format for /program view
        libraryForProgram.setAssignments(buildAssignmentsForProgram(assignments));
        return libraryForProgram;
    }

    /**
     * Builds list of Assignment objects from given library text file contents
     * Uniform line endings (\r\n) are required to successfully parse assignments
     * @param libraryContents
     * @return assignments
     * @throws LibrarySyntaxErrorException with error message if any syntax error within parsing any assignment is encountered
     */
    public List<Assignment> getAssignmentsFromLibrary(String libraryContents) throws LibrarySyntaxErrorException {
        String[] assignments = libraryContents.split(Constants.ASSIGNMENT_DISTINGUISHING_WORD);
        if(assignments.length < 2){ //any occurrence of Constants#ASSIGNMENT_DISTINGUISHING_WORD
            throw new LibrarySyntaxErrorException("Knihovna příkladů neobsahuje ani jeden příklad!");
        }
        assignments = Arrays.copyOfRange(assignments, 1, assignments.length); //ignore string before first Constants#ASSIGNMENT_DISTINGUISHING_WORD occurrence (can contain random text)
        List<Assignment> parsedAssignments = Assignment.buildAssignments(assignments);
        return Assignment.buildAssignments(assignments);
    }

    /**
     * Calculates assignments count in a valid library (thus, no parsing is required)
     * @param libraryContents
     * @return assignments count
     */
    private int getAssignmentsCount(String libraryContents){
        String[] assignments = libraryContents.split(Constants.ASSIGNMENT_DISTINGUISHING_WORD);
        return assignments.length - 1;
    }

    /**
     * Parses assignments loaded from library into different objects which are used when a library is loaded in /program
     * @param assignments
     * @return assignment objects
     */
    private List<AssignmentShowDTO> buildAssignmentsForProgram(List<Assignment> assignments){
        List<AssignmentShowDTO> assignmentsForProgram = new ArrayList<>(assignments.size()); //set initial capacity

        int index = 0; //used to identify an assignment in the library
        for(Assignment assignment : assignments){
            String subspace1 = String.join(IOHelper.WINDOWS_LINE_SEPARATOR_STRING, assignment.getSubspace1Lines());
            String subspace2 = String.join(IOHelper.WINDOWS_LINE_SEPARATOR_STRING, assignment.getSubspace2Lines());
            assignmentsForProgram.add(new AssignmentShowDTO(index, assignment.getName(), subspace1, subspace2, assignment.getType()));
            index++;
        }
        return assignmentsForProgram;
    }

    public LibraryDownloadDTO downloadLibrary(UUID id) throws SQLException {
        return libraryDAO.downloadLibrary(id);
    }

    /**
     * Loads list of user's library names to be displayed to choose a library to save an assignment to in program page
     * @param id user Id
     * @return list of user's library names
     * @throws SQLException on db communication error
     */
    public UserLibrariesSimpleListDTO loadUserLibraryNames(UUID id) throws SQLException {
        List<LibraryDTO> userLibraries = libraryDAO.getLibrariesCreatedByUser(id);
        List<String> libraryNames = new ArrayList<>(userLibraries.size());
        List<String> libraryIds = new ArrayList<>(userLibraries.size());
        for(LibraryDTO library : userLibraries){
            libraryNames.add(library.getName());
            libraryIds.add(library.getId().toString());
        }

        return new UserLibrariesSimpleListDTO(null, libraryNames, libraryIds);
    }

    /**
     * Adds an assignment to given library (appends to the end)
     * @param id library id
     * @return true if database was updated
     * @throws SQLException on db communication error
     */
    public boolean addAssignmentToLibrary(UUID id, Assignment assignment) throws SQLException {
        LibraryDTO library = getLibraryDetails(id);
        String updatedContents = library.getContents() + assignment.toString();
        byte[] utf8Contents = updatedContents.getBytes(StandardCharsets.UTF_8);
        return libraryDAO.updateLibrary(id, library.getName(), utf8Contents,  library.getPublicAccessibility());
    }

    /**
     * Deletes selected assignment from library
     * @param library
     * @param deletedIndex
     * @return true if deleted, false if the index was not valid (library was in invalid state caused by user editing meanwhile)
     * @throws SQLException on db communication error
     */
    public boolean deleteAssignmentByIndex(LibraryDTO library, int deletedIndex) throws SQLException {
        String oldContents = library.getContents();
        int oldContentsAssignmentCount = getAssignmentsCount(oldContents); //get count
        if(deletedIndex < 0 || deletedIndex >= oldContentsAssignmentCount){
            //prevent illegal index usage
            return false;
        }
        String updatedContents = IOHelper.removeAssignmentFromLibraryByIndex(deletedIndex, oldContents);
        byte[] utf8Contents = updatedContents.getBytes(StandardCharsets.UTF_8);
        return libraryDAO.updateLibrary(library.getId(), library.getName(), utf8Contents, library.getPublicAccessibility());
    }

    /**
     * Validates all assignments if they can be directly used to solve
     * @param parsedAssignments
     * @throws LibraryLogicalErrorException on any logical error in any assignment
     */
    private void validateAllAssignments(List<Assignment> parsedAssignments) throws LibraryLogicalErrorException {
        int index = 0;
        for(Assignment assignment : parsedAssignments){
            AssignmentValidityCheckDTO validityCheck = assignment.checkLogicalValidity();
            if(validityCheck.generatedAnyErrorMessage()){
                throw new LibraryLogicalErrorException((index + 1) + ". příklad v pořadí: " + validityCheck.getErrorMessage());
            }
            index++;
        }
    }
}
