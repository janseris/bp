package com.janseris.bp.dataAccessLayer;

import java.sql.*;

/**
 * Used to establish a connection to a SQL Server
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-02-29
 */

public final class ConnectionFactory {

    private ConnectionFactory(){

    }

    private static String azureHostName = "janseris.database.windows.net";
    private static String azureDbName = "AnalyticGeometryOnline";
    private static String azureUser = "knuckles";
    private static String azurePassword = "" /* secret */;
    private static String azureUrl = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;encrypt=true;"
            + "hostNameInCertificate=*.database.windows.net;loginTimeout=30;", azureHostName, azureDbName, azureUser, azurePassword);

    //loaded from configuration file
    private static String hostName = null; //localhost or localhost\\SQLEXPRESS for local SQL Server instance
    private static String dbName = null;
    private static String user = null;
    private static String password = null;
    private static String url = null;

    public static void setHostName(String hostName) {
        ConnectionFactory.hostName = hostName;
    }

    public static void setDbName(String dbName) {
        ConnectionFactory.dbName = dbName;
    }

    public static void setUser(String user) {
        ConnectionFactory.user = user;
    }

    public static void setPassword(String password) {
        ConnectionFactory.password = password;
    }

    public static void buildUrl(){
        ConnectionFactory.url = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;"
                + "loginTimeout=30;", hostName, dbName, user, password);
    }

    /**
     * Establishes a connection to database
     * @return connection
     */
    public static Connection getConnection() {
        try{
            return DriverManager.getConnection(url);
        } catch (SQLException ex){
            throw new RuntimeException("Connection to database could not be estabilished", ex);
        }
    }

    /**
     * Establishes a connection to database in Microsoft Azure cloud
     * @return connection
     */
    public static Connection getAzureConnection() {
        try{
            return DriverManager.getConnection(azureUrl);
        } catch (SQLException ex){
            throw new RuntimeException("Connection to database could not be estabilished", ex);
        }
    }
}
