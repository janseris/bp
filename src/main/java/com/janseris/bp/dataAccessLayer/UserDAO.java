package com.janseris.bp.dataAccessLayer;

import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserRegisterDTO;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.sql.Timestamp;

import static com.janseris.bp.dataAccessLayer.Utilities.getPasswordHash;
import static com.janseris.bp.dataAccessLayer.Utilities.getRandomPasswordSalt;

/**
 * A class used to create abstraction to database access for users
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-02-29
 */

@Repository
public class UserDAO {

    /**
     * Inserts a user into database<br>
     * https://security.stackexchange.com/questions/69421/is-it-a-good-idea-to-use-the-users-username-as-a-salt-when-hashing-a-password-l<br>
     * Password salt is randomly generated and must be saved to check passwords upon login<br>
     * User name or email is not used as salt because it couldn't be changed if it was used as salt<br>
     * @param user
     * @return true on success, false on failure
     * @throws SQLException if db error occurs
     */
    public boolean addUser(UserRegisterDTO user) throws SQLException {
        String sqlString = "INSERT INTO [Users] (ID, Name, Email," +
                " PasswordHash, PasswordSalt, RegistrationDate, LastLogInDate)"
                + " values (?, ?, ?, ?, ?, ?, ?)";
        byte[] passwordSalt = getRandomPasswordSalt();
        String passwordHash = getPasswordHash(user.getPassword(), passwordSalt);
        UUID ID = UUID.randomUUID();
        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, ID);
            ps.setNString(2, user.getName());
            ps.setNString(3, user.getEmail());
            ps.setNString(4, passwordHash);
            ps.setBytes(5, passwordSalt);
            ps.setTimestamp(6, timeNow);
            ps.setTimestamp(7, timeNow);
            int rowsAffected = ps.executeUpdate();
            if(rowsAffected == 1){
                return true;
            }
        }
        return false;
    }


    /**
     * Retrieves a user with corresponding Id
     * @param id id to find a matching user
     * @return User or null if no match for Id
     * @throws SQLException on data format mismatch
     */
    public UserDTO getUserById(UUID id) throws SQLException {
        String sqlString = "SELECT * FROM [dbo].[Users] WHERE ID = ?"; //TOP 1 requires all column names in query
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getUserFromResultSet(resultSet);
            }
        }
    }

    /**
     * Updates the user's last login time<br>
     * To be called upon successful login
     * @param ID user ID
     * @throws SQLException if update fails or db error is encountered
     */
    public void updateLastLoginDate(UUID ID) throws SQLException {
        String sqlString = "UPDATE [Users] SET LastLogInDate = ? WHERE ID = ?";

        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setTimestamp(1, timeNow);
            ps.setObject(2, ID);
            int rowsAffected = ps.executeUpdate();
            if(rowsAffected != 1){
                throw new SQLException("Last Login date update failure for user ID " + ID);
            }
        }
    }

    /**
     * Updates the user's password<br>
     * To be called upon<br>
     *  1) manual user's password change<br>
     *  2) automated password change on "forgotten password -> send email"
     * @param ID user ID
     * @throws SQLException if update fails or db error is encountered
     */
    public void updateUserPassword(UUID ID, String password) throws SQLException {
        String sqlString = "UPDATE [Users] SET PasswordHash = ?, PasswordSalt = ? WHERE ID = ?";

        byte[] passwordSalt = getRandomPasswordSalt();
        String passwordHash = getPasswordHash(password, passwordSalt);

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setNString(1, passwordHash);
            ps.setBytes(2, passwordSalt);
            ps.setObject(3, ID);
            int rowsAffected = ps.executeUpdate();
            if(rowsAffected != 1){
                throw new SQLException("Password update failure for user ID " + ID);
            }
        }
    }

    /**
     * Retrieves a user with corresponding email
     * @param email email to find a matching user
     * @return User or null if no match for entered email
     * @throws SQLException on data format mismatch
     */
    public UserDTO getUserByEmail(String email) throws SQLException {
        String sqlString = "SELECT * FROM [dbo].[Users] WHERE Email = ?"; //TOP 1 requires all column names in query
        try (Connection conn = ConnectionFactory.getConnection();
        PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setNString(1, email);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getUserFromResultSet(resultSet);
            }
        }
    }

    /**
     * Gets single user data from the current resultSet row
     * @param resultSet result from db
     * @return first entry or null if no entries
     * @throws SQLException upon expected data format mismatch
     */
    private UserDTO getUserFromResultSet(ResultSet resultSet) throws SQLException {
        UserDTO user = new UserDTO();
        if(resultSet.next()){
            user.setId( resultSet.getObject("ID", java.util.UUID.class));
            user.setEmail( resultSet.getString("Email") );
            user.setName( resultSet.getString("Name") );
            user.setPasswordHash( resultSet.getString("PasswordHash") );
            user.setPasswordSalt( resultSet.getBytes("PasswordSalt") );
            user.setRegistrationDate( resultSet.getTimestamp("RegistrationDate") );
            user.setLastLogInDate( resultSet.getTimestamp("LastLogInDate") );
            return user;
        }
        return null;
    }

    /**
     * Extracts all users from result set obtained as a response to an executed database query
     * @param resultSet set of rows returned by the database
     * @return a set of n users depending on n = resultSet size
     * @throws SQLException upon expected data format mismatch
     */
    private Set<UserDTO> getUsersFromResultSet(ResultSet resultSet) throws SQLException {
        Set<UserDTO> users = new HashSet<>();

        UserDTO user = getUserFromResultSet(resultSet);
        while(user != null){
            users.add(user);
            user = getUserFromResultSet(resultSet);
        }
        return users;
    }
}
