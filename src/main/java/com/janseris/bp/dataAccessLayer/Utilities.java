package com.janseris.bp.dataAccessLayer;

import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;


/**
 * A static library providing helper functions for the data access layer
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-01
 */

public final class Utilities {
    private Utilities(){

    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Utilities.class);


    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return "0x" + new String(hexChars);
    }

    public static byte[] getRandomPasswordSalt(){
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

    public static String getPasswordHash(String password, byte[] salt){
        try{
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = factory.generateSecret(spec).getEncoded();
            Base64.Encoder enc = Base64.getEncoder();
            return enc.encodeToString(hash);
        } catch (Exception ex){
            log.error("Error generating password hash: " + ex.getClass() + " " + ex.getLocalizedMessage());
            throw new RuntimeException("Error generating password hash", ex);
        }
    }


}
