package com.janseris.bp.dataAccessLayer;

import com.janseris.bp.businessLayer.LibraryService;
import com.janseris.bp.model.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

/**
 * A class used to create abstraction to database access for libraries
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-02-29
 */

@Repository
public class LibraryDAO {

    /**
     * Checks, if the selected user has permission to edit the library
     * @param userId
     * @param libraryId
     * @return
     */
    public boolean isOwner(UUID userId, UUID libraryId) throws SQLException {
        String sqlString = "SELECT count(*) FROM [dbo].[Libraries] WHERE ID = ? AND UserID = ?";
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, libraryId);
            ps.setObject(2, userId);

            try (ResultSet resultSet = ps.executeQuery()) {
                resultSet.next();
                return resultSet.getInt(1) == 1;
            }
        }
    }

    /**
     * Gets column name to be used for ordering libraries in a SELECT query
     * @param column enum value
     * @return column name for database query
     */
    private String getOrderingColumnName(LibraryOrderingColumn column){
        switch(column){
            case DATE_CREATED:
                return "DateCreated";
            case DATE_MODIFIED:
                return "LastDateModified";
            case DATE_USED:
                return "LastDateUsed";
            case USAGE_COUNT:
                return "UsageCount";
            default:
                return "Name";
        }
    }

    /**
     * Gets ordering mode SQL string for ordering results in a SELECT query
     * @param ordering ascending/descending
     * @return ordering mode for database query
     */
    private String getOrderingModeName(SQLResultOrderingMode ordering){
        if (ordering == SQLResultOrderingMode.DESCENDING) {
            return "DESC";
        }
        return "ASC";
    }

    /**
     * Inserts a new library into database, with private accessibility mode<br>
     * UTF-8 encoded content with uniform \r\n line endings is required
     * @param creatorId Id of the user who creates this library
     * @param name library name
     * @param UTF8bytes library string contents which will be saved in UTF-8
     * @return library was inserted
     * @throws SQLException if db error occurs
     * @throws IllegalArgumentException if the source enconding is not supported (only UTF-8 and Windows-1250 is supported)
     */
    public boolean addLibrary(UUID creatorId, String name, byte[] UTF8bytes) throws SQLException, IllegalArgumentException {
        return addLibrary(creatorId, name, UTF8bytes, PublicAccessibility.PRIVATE);
    }

    /**
     * Inserts a new library into database<br>
     * UTF-8 encoded content with uniform \r\n line endings is required
     * @param creatorId Id of the user who creates this library
     * @param name library name
     * @param UTF8bytes library string contents in UTF-8
     * @param accessibility defines accessibility for all other/anonymous users
     * @return library was inserted
     * @throws SQLException if db error occurs
     */
    public boolean addLibrary(UUID creatorId, String name, byte[] UTF8bytes, PublicAccessibility accessibility) throws SQLException {
        String sqlString = "INSERT INTO [Libraries] (UserID, ID, Name, Contents, DateCreated, LastDateModified, LastDateUsed, PublicAccessibility)"
                + " values (?, ?, ?, ?, ?, ?, ?, ?)";

        UUID ID = UUID.randomUUID();
        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)) {
            ps.setObject(1, creatorId);
            ps.setObject(2, ID);
            ps.setNString(3, name);
            ps.setBytes(4, UTF8bytes);
            ps.setTimestamp(5, timeNow);
            ps.setTimestamp(6, timeNow);
            ps.setTimestamp(7, timeNow);
            ps.setInt(8, accessibility.ordinal());
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 1) {
                return true;
            }
        }
        return false;
    }



    /**
     * Updates the whole library record<br>
     * Last modify time is automatically updated<br>
     * 1 rows are affected even if the command is executed successfully (even on no change)<br>
     * UTF-8 encoded content with uniform \r\n line endings is required
     * @param libraryId
     * @param name new library name
     * @param UTF8bytes text in UTF-8
     * @param accessibility new accessibility
     * @return true -> 1 rows affected
     * @throws SQLException if db error occurs
     */
    public boolean updateLibrary(UUID libraryId, String name, byte[] UTF8bytes, PublicAccessibility accessibility) throws SQLException, IllegalArgumentException {
        String sqlString = "UPDATE [Libraries] SET Name = ?, Contents = ?, LastDateModified = ?, PublicAccessibility = ? WHERE ID = ?";

        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)) {
            ps.setNString(1, name);
            ps.setBytes(2, UTF8bytes);
            ps.setTimestamp(3, timeNow);
            ps.setInt(4, accessibility.ordinal());
            ps.setObject(5, libraryId);
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Updates only library name and public accessibility<br>
     * To be called if library content is not provided (faster update)<br>
     * Last modify time is automatically updated<br>
     * 1 rows are affected even if the command is executed successfully (even on no change)
     * @param libraryId
     * @param name
     * @param accessibility
     * @return
     * @throws SQLException if db error occurs
     */
    public boolean updateLibrary(UUID libraryId, String name, PublicAccessibility accessibility) throws SQLException {
        String sqlString = "UPDATE [Libraries] SET Name = ?, LastDateModified = ?, PublicAccessibility = ? WHERE ID = ?";

        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)) {
            ps.setNString(1, name);
            ps.setTimestamp(2, timeNow);
            ps.setInt(3, accessibility.ordinal());
            ps.setObject(4, libraryId);
            int rowsAffected = ps.executeUpdate();
            if (rowsAffected == 1) {
                return true;
            }
        }
        return false;
    }


    /**
     * Updates the library last used date and usage count<br>
     * This command is triggered when user loads the library during online computing
     * @param ID library ID
     * @throws SQLException if update fails or db error is encountered
     */
    public void updateLastUsedDate(UUID ID) throws SQLException {
        String sqlString = "UPDATE [Libraries] SET LastDateUsed = ?, UsageCount = UsageCount + 1 WHERE ID = ?";

        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setTimestamp(1, timeNow);
            ps.setObject(2, ID);
            int rowsAffected = ps.executeUpdate();
            if(rowsAffected != 1){
                throw new SQLException("Last used date update failure for library ID " + ID);
            }
        }
    }

    /**
     * Updates the sample library last used date and usage count asynchronously<br>
     * This command is triggered when user loads the sample library to be used for online computing
     * @param ID sample library ID
     * @throws SQLException if update fails or db error is encountered
     */
    @Async
    public void updateSampleLibraryLastUsedDateAsync(UUID ID) throws SQLException {
        String sqlString = "UPDATE [Libraries] SET LastDateUsed = ?, UsageCount = UsageCount + 1 WHERE ID = ?";

        Timestamp timeNow = new java.sql.Timestamp(new java.util.Date().getTime());

        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setTimestamp(1, timeNow);
            ps.setObject(2, ID);
            int rowsAffected = ps.executeUpdate();
            if(rowsAffected != 1){
                throw new SQLException("Last used date update failure for sample library (ID =  " + ID + ")");
            }
        }
    }


    /**
     * Retrieves libraries created by user, ordered by a specific column
     * @param userId creator id
     * @param column order by which column
     * @param orderingMode ascending/descending
     * @return a list of all libraries created by user
     * @throws SQLException on data format mismatch
     */
    public List<LibraryDTO> getLibrariesCreatedByUser(UUID userId, LibraryOrderingColumn column, SQLResultOrderingMode orderingMode) throws SQLException {
        String sqlString = "SELECT * FROM [dbo].[Libraries] WHERE UserID = ? ORDER BY "; //order by ? is not possible as it allows SQLInjection
        sqlString += getOrderingColumnName(column); //allowing only predefined values does not allow SQLInjection either even if using direct string concatenation
        sqlString += " " + getOrderingModeName(orderingMode); //ascending/descending
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, userId);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getLibrariesFromResultSet(resultSet);
            }
        }
    }

    /**
     * Retrieves libraries created by user, ordered by name, ascending
     * @param userId creator id
     * @return a list of all libraries created by user
     * @throws SQLException on data format mismatch
     */
    public List<LibraryDTO> getLibrariesCreatedByUser(UUID userId) throws SQLException {
        return getLibrariesCreatedByUser(userId, LibraryOrderingColumn.NAME, SQLResultOrderingMode.ASCENDING);
    }

    /**
     * Retrieves libraries created by user with selected ordering
     * @param column order by which column
     * @param orderingMode ascending/descending
     * @return a list of all libraries created by user
     * @throws SQLException on data format mismatch
     */
    public List<LibraryDTO> getAllPublicLibraries(LibraryOrderingColumn column, SQLResultOrderingMode orderingMode) throws SQLException {
        String sqlString = "SELECT * FROM [dbo].[Libraries] WHERE PublicAccessibility = 1 ORDER BY "; //order by ? is not possible as it allows SQLInjection
        //allowing only predefined values does not allow SQLInjection either even if using direct string concatenation
        sqlString += getOrderingColumnName(column);
        sqlString += " " + getOrderingModeName(orderingMode); //ascending/descending
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            try (ResultSet resultSet = ps.executeQuery()) {
                return getLibrariesFromResultSet(resultSet);
            }
        }
    }

    /**
     * Retrieves all public libraries, order by last date used
     * @return a list of all libraries created by user
     * @throws SQLException on data format mismatch
     */
    public List<LibraryDTO> getAllPublicLibraries() throws SQLException {
        return getAllPublicLibraries(LibraryOrderingColumn.DATE_USED, SQLResultOrderingMode.DESCENDING);
    }


    /**
     * Gets the library for the ID
     * @param libraryId library Id
     * @return library data or null if no match
     * @throws SQLException on db error
     */
    public LibraryDTO getLibraryById(UUID libraryId) throws SQLException {
        String sqlString = "SELECT * FROM [dbo].[Libraries] WHERE ID = ?"; //TOP 1 requires all column names in query
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, libraryId);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getLibraryFromResultSet(resultSet);
            }
        }
    }

    /**
     * Deletes selected library
     * @param id id
     * @throws SQLException
     */
    public void deleteLibraryById(UUID id) throws SQLException {
        String sqlString = "DELETE FROM [dbo].[Libraries] WHERE ID = ?";
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, id);
            ps.execute();
        }
    }

    /**
     * Gets single library data (first result) from the current resultSet row
     * @param resultSet result from db
     * @return first entry or null if no entries
     * @throws SQLException upon expected data format mismatch
     */
    private LibraryDTO getLibraryFromResultSet(ResultSet resultSet) throws SQLException {
        LibraryDTO library = new LibraryDTO();
        if(resultSet.next()){
            library.setCreatorUserId( resultSet.getObject("UserID", java.util.UUID.class));
            library.setId( resultSet.getObject("ID", java.util.UUID.class));
            library.setName( resultSet.getString("Name") );
            library.setContents( LibraryService.getLibraryContentsString(resultSet.getBytes("Contents")) );
            library.setDateCreated( resultSet.getTimestamp("DateCreated") );
            library.setLastDateModified( resultSet.getTimestamp("LastDateModified") );
            library.setLastDateUsed(resultSet.getTimestamp("LastDateUsed") );
            library.setPublicAccessibility(PublicAccessibility.fromInt(resultSet.getInt("PublicAccessibility")) );
            library.setUsageCount(resultSet.getInt("UsageCount"));
            return library;
        }
        return null;
    }

    /**
     * Extracts all libraries from result set obtained as a response to an executed database query
     * @param resultSet set of rows returned by the database
     * @return a set of n users depending on n = resultSet size
     * @throws SQLException upon expected data format mismatch
     */
    private List<LibraryDTO> getLibrariesFromResultSet(ResultSet resultSet) throws SQLException {
        List<LibraryDTO> libraries = new LinkedList<>();

        LibraryDTO library = getLibraryFromResultSet(resultSet);
        while(library != null){
            libraries.add(library);
            library = getLibraryFromResultSet(resultSet);
        }
        return libraries;
    }

    /**
     * Gets library name and byte contents for downloading
     * @param libraryId
     * @return
     * @throws SQLException on db error
     */
    public LibraryDownloadDTO downloadLibrary(UUID libraryId) throws SQLException {
        String sqlString = "SELECT Name, Contents FROM [dbo].[Libraries] WHERE ID = ?";
        try (Connection conn = ConnectionFactory.getConnection();
             PreparedStatement ps = conn.prepareStatement(sqlString)){
            ps.setObject(1, libraryId);
            try (ResultSet resultSet = ps.executeQuery()) {
                LibraryDownloadDTO library = new LibraryDownloadDTO();
                if(resultSet.next()){ //only first row
                    library.setName( resultSet.getString("Name") );
                    library.setContents(resultSet.getBytes("Contents"));
                    return library;
                }
                return null;
            }
        }
    }
}
