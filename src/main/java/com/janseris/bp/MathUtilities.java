package com.janseris.bp;
/**
 * This class is a utility class. It contains methods that are often re-used and common to more classes.
 *
 * @author Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since 2014-10-28
 */

import java.util.Random;

final class MathUtilities {

    /**
     * This class will never have any instances, therefore private and empty constructor.
     */
    private MathUtilities() {

    }

    /**
     * Calculates the greatest common divisor of two given integers using Euclid's algorithm.
     *
     * @param a the first number
     * @param b the other number
     *
     * @return greatest common divisor of the arguments (gcd is always non-negative, gcd(x,0) returns |x|)
     */
    static long gcd(long a, long b) {
        //System.out.println("a = " + a + ", b = " + b); //demonstrate long int overflow
        if (a < 0) return gcd(-a, b);
        if (b < 0) return gcd(a, -b);
        if (b == 0) return a;
        return gcd(b, a % b);
    }

    /**
     * Calculates the least common multiple of two given integers using formula <code>lcm(a,b)=(a*b)/gcd(a,b)</code>.
     *
     * @param a the first number
     * @param b the other number
     *
     * @return least common multiple of the arguments (lcm is always non-negative)
     */
    static long lcm(long a, long b) {
        if (a == 0 || b == 0) return 0;
        return (a * b) / gcd(a, b);
    }

    /**
     * Generates a random number from interval (inclusive).
     *
     * @param min minimum value
     * @param max maximum value (must be at least <code>min</code>; if <code>min</code> is greater than <code>max</code>, we switch the values)
     */
    static int randIntUniform(int min, int max) {
        if (max < min) return randIntUniform(max, min);
        if (max == min) return min;

        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    /**
     * Generates a random number from interval (inclusive), while the numbers closer to the center of the interval are more probable.
     *
     * @param min minimum value
     * @param max maximum value (must be at least <code>min</code>; if <code>min</code> is greater than <code>max</code>, we switch the values)
     */
    static int randInt(int min, int max) {
        if (max < min) return randInt(max, min);
        if (max == min) return min;

        Random rand = new Random();

        int r = min;
        for (int i = 0; i < max - min; i++)
            if (rand.nextDouble() < 0.5) r++;

        return r;
    }

    /**
     * Generates an <i>n</i>-tuple of random numbers from interval (inclusive).
     *
     * @param min minimum value
     * @param max maximum value (must be greater than <code>min</code>; if <code>min</code> is greater than <code>max</code>, we switch the values)
     */
    static int[] randNTupleUniform(int min, int max, int n) {
        if (n < 1) throw new IndexOutOfBoundsException("n has to be positive.");

        int[] nTuple = new int[n];

        for (int i = 0; i < n; i++)
            nTuple[i] = randIntUniform(min, max);

        return nTuple;
    }

    /**
     * Generates an <i>n</i>-tuple of random numbers from interval (inclusive), while the numbers closer to the center of the interval are more probable.
     *
     * @param min minimum value
     * @param max maximum value (must be greater than <code>min</code>; if <code>min</code> is greater than <code>max</code>, we switch the values)
     */
    static int[] randNTuple(int min, int max, int n) {
        if (n < 1) throw new IndexOutOfBoundsException("n has to be positive.");

        int[] nTuple = new int[n];

        for (int i = 0; i < n; i++)
            nTuple[i] = randInt(min, max);

        return nTuple;
    }

    /**
     * Generates a random boolean.
     */
    static boolean randBool() {
        Random rand = new Random();

        return rand.nextBoolean();
    }

    /**
     * Generates a permutation of integers 1 to <code>n</code>.
     *
     * @param n number of integers to permutate
     */
    static int[] generatePermutation(int n) {
        boolean[] alreadyUsed = new boolean[n];
        for (int i = 0; i < n; i++) alreadyUsed[i] = false;

        Random rand = new Random();
        int entriesLeft = n;
        int nextEntry, index, i;
        int[] permutation = new int[n];
        while (entriesLeft > 0) {
            index = 0;
            i = 1;
            nextEntry = rand.nextInt(entriesLeft) + 1;
            while (i <= nextEntry) {
                index++;
                if (!(alreadyUsed[index - 1])) i++;
            }
            permutation[entriesLeft - 1] = index;
            alreadyUsed[index - 1] = true;
            entriesLeft--;
        }

        return permutation;
    }

}
