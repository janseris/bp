package com.janseris.bp;

/**
 * This class introduces matrices and enables basic operations with them.
 * The elements are stored as a field of {@link Fraction}s.
 *
 * @author Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since 2014-04-01
 */

class Matrix {
    /**
     * number of rows
     */
    private int rows;
    /**
     * number of columns
     */
    private int cols;
    /**
     * elements of the matrix
     */
    private Fraction[][] data;
    /**
     * {@link Fraction} 0.
     */
    private static final Fraction ZERO = new Fraction(0);
    /**
     * {@link Fraction} 1.
     */
    private static final Fraction ONE = new Fraction(1);
    /**
     * {@link Fraction} -1.
     */
    private static final Fraction NEG_ONE = new Fraction(-1);

    private IOHelper ioHelper;

    /**
     * Constructs an empty matrix of dimension 0.
     */
    Matrix(IOHelper ioHelper) {
        this.ioHelper = ioHelper;
        this.rows = 0;
        this.cols = 0;
        this.data = new Fraction[0][0];
    }

    /**
     * Constructs a rectangular identity matrix of a given dimension.
     *
     * @param n the dimension of the matrix, i.e. number of its rows and columns
     */
    Matrix(int n, IOHelper ioHelper) {
        this.ioHelper = ioHelper;
        this.rows = n;
        this.cols = n;
        this.data = new Fraction[n][n];
        for (int i = 0; i <= n - 1; i++) {
            for (int j = 0; j <= n - 1; j++) {
                if (i == j)
                    this.data[i][j] = ONE;
                else
                    this.data[i][j] = ZERO;
            }
        }
    }

    /**
     * Constructs a zero-filled matrix of given dimensions.
     *
     * @param r number of matrix's rows
     * @param c number of matrix's columns
     */
    Matrix(int r, int c, IOHelper ioHelper) {
        this.ioHelper = ioHelper;
        this.rows = r;
        this.cols = c;
        this.data = new Fraction[r][c];
        for (int i = 0; i <= r - 1; i++) {
            for (int j = 0; j <= c - 1; j++) {
                this.data[i][j] = ZERO;
            }
        }
    }

    /**
     * Constructs a matrix from an array of {@link Fraction}s.
     *
     * @param M array of {@link Fraction}s, containing the matrix's elements
     * @param r number of matrix's rows
     * @param c number of matrix's columns
     */
    Matrix(Fraction[] M, int r, int c, IOHelper ioHelper) {
        this.ioHelper = ioHelper;
        this.rows = r;
        this.cols = c;
        this.data = new Fraction[r][c];
        for (int i = 0; i <= r - 1; i++) {
            for (int j = 0; j <= c - 1; j++) {
                if ((M[c * i + j] == null) || (c * i + j >= M.length))
                    this.data[i][j] = ZERO;
                else
                    this.data[i][j] = M[c * i + j];
            }
        }
    }

    /**
     * Number of rows of this Matrix.
     *
     * @return a number of rows
     */
    int getR() {
        return this.rows;
    }

    /**
     * Number of columns of this Matrix.
     *
     * @return a number of columns
     */
    int getC() {
        return this.cols;
    }

    /**
     * The getter of this class - returns the value of a given element <i>a</i><sub><i>r</i>,<i>c</i></sub>.
     *
     * @param r the row index of the desired element
     * @param c the column index of the desired element
     * @return the value (a {@link Fraction}) of the element on the <tt>r</tt>th row and the <tt>c</tt>th column
     * @throws IndexOutOfBoundsException Throws an exception if the row index isn't of &lt;1,<i>number of rows</i>&gt;.
     * @throws IndexOutOfBoundsException Throws an exception if the column index isn't of &lt;1,<i>number of columns</i>&gt;.
     */
    Fraction get(int r, int c) {
        if (r > this.rows) throw new IndexOutOfBoundsException("Matrix only has " + this.rows + " rows!");
        if (c > this.cols) throw new IndexOutOfBoundsException("Matrix only has " + this.cols + " columns!");
        if ((r <= 0) || (c <= 0)) throw new IndexOutOfBoundsException("Row and column indices ought to be positive!");
        return this.data[r - 1][c - 1];
    }

    /**
     * The setter of this class - sets given element <i>a</i><sub><i>r</i>,<i>c</i></sub> to a given value.
     *
     * @param F the desired value (a {@link Fraction}) of the element on the <tt>r</tt>th row and the <tt>c</tt>th column
     * @param r the row index of the desired element
     * @param c the column index of the desired element
     * @throws IndexOutOfBoundsException Throws an exception if the row index isn't of &lt;1,<i>number of rows</i>&gt;.
     * @throws IndexOutOfBoundsException Throws an exception if the column index isn't of &lt;1,<i>number of columns</i>&gt;.
     */
    void set(Fraction F, int r, int c) {
        if (r > this.rows) throw new IndexOutOfBoundsException("Matrix only has " + this.rows + " rows.");
        if (c > this.cols) throw new IndexOutOfBoundsException("Matrix only has " + this.cols + " columns.");
        if ((r <= 0) || (c <= 0)) throw new IndexOutOfBoundsException("Row and column indices ought to be positive.");
        this.data[r - 1][c - 1] = F;
    }

    /**
     * Copies another matrix into this matrix. This is the correct way of assigning matrices, not by <code>=</code>.
     *
     * @param I the image matrix
     * @throws NullPointerException Throws an exception if the image matrix has never been initiated.
     */
    void copy(Matrix I) {
        if (I == null) throw new NullPointerException("The image matrix does not exist!");

        this.rows = I.rows;
        this.cols = I.cols;
        this.data = new Fraction[I.rows][I.cols];
        for (int i = 1; i <= I.rows; i++) {
            for (int j = 1; j <= I.cols; j++) {
                this.set(I.get(i, j), i, j);
            }
        }
    }

    /**
     * Crops a submatrix from this matrix.
     *
     * @param rowStart the initial row of the submatrix (its row index in the original matrix)
     * @param rowEnd   the terminal row of the submatrix (its row index in the original matrix)
     * @param colStart the initial column of the submatrix (its column index in the original matrix)
     * @param colEnd   the terminal column of the submatrix (its column index in the original matrix)
     * @throws UnsupportedOperationException Throws an exception if the index of initial row (or column) is larger than the index of terminal row (or column).
     * @throws UnsupportedOperationException Throws an exception if the row indices aren't of &lt;1,<i>number of rows</i>&gt;.
     * @throws UnsupportedOperationException Throws an exception if the column indices aren't of &lt;1,<i>number of columns</i>&gt;.
     */
    Matrix submatrix(int rowStart, int rowEnd, int colStart, int colEnd) {
        if (rowStart > rowEnd)
            throw new UnsupportedOperationException("The index of initial row may be at most the index of terminal row!");
        if (colStart > colEnd)
            throw new UnsupportedOperationException("The index of initial column may be at most the index of terminal column!");
        if ((rowStart < 1) || (rowEnd > this.rows))
            throw new UnsupportedOperationException("The row indices must be from 1 to " + this.rows + "!");
        if ((colStart < 1) || (colEnd > this.cols))
            throw new UnsupportedOperationException("The column indices must be from 1 to " + this.cols + "!");

        Matrix M = new Matrix(rowEnd - rowStart + 1, colEnd - colStart + 1, ioHelper);
        for (int i = rowStart, I = 1; i <= rowEnd; i++, I++) {
            for (int j = colStart, J = 1; j <= colEnd; j++, J++) {
                M.set(this.get(i, j), I, J);
            }
        }

        return M;
    }

    /**
     * Crops the matrix of equation system coefficients, ie.&nbsp;all columns but the last.
     *
     * @return the matrix of equation system coefficients, ie.&nbsp;all columns but the last
     * @throws IllegalArgumentException Throws an exception if the matrices has less than two columns, and therefore isn't a matrix of any equation system.
     */
    Matrix getSystemCoeffMatrix() {
        if (this.cols < 2)
            throw new IllegalArgumentException("This is not a matrix of equation system, as it has less than 2 columns!");

        return this.submatrix(1, this.rows, 1, this.cols - 1);
    }

    /**
     * Crops the matrix of constant terms of equation system, ie.&nbsp;the last column.
     *
     * @return the matrix of constant terms of equation system, ie.&nbsp;the last column
     * @throws IllegalArgumentException Throws an exception if the matrices has less than two columns, and therefore isn't a matrix of any equation system.
     */
    Matrix getConstTerms() {
        if (this.cols < 2)
            throw new IllegalArgumentException("This is not a matrix of equation system, as it has less than 2 columns!");

        return this.submatrix(1, this.rows, this.cols, this.cols);
    }

    /**
     * Converts rows of this matrix into a field of vectors.
     *
     * @return a field of vectors; each vector represents one row of this matrix (if this matrix is null, <code>new Vector[0]</code> will be returned)
     */
    Vector[] rowsToVectors() {
        Vector[] vectors = new Vector[this.rows];
        for (int i = 1; i <= this.rows; i++) {
            Fraction[] coeffs = new Fraction[this.cols];
            for (int j = 1; j <= this.cols; j++) {
                coeffs[j - 1] = this.get(i, j);
            }
            vectors[i - 1] = new Vector(coeffs, this.cols, ioHelper);
        }

        return vectors;
    }

    /**
     * Merges another matrix to this matrix from right.
     *
     * @param N the matrix to be merged from right
     * @throws UnsupportedOperationException Throws an exception if the matrices don't have the same number of rows.
     */
    void mergeFromRight(Matrix N) {
        if ((N.rows == 0) || (N.cols == 0)) return;
        if ((this.rows == 0) || (this.cols == 0)) {
            this.copy(N);
            return;
        }
        if (this.rows != N.rows)
            throw new UnsupportedOperationException("The matrices don't have the same number of rows!");

        Matrix M = new Matrix(this.rows, this.cols + N.cols, ioHelper);
        for (int i = 1; i <= M.rows; i++) {
            for (int j = 1; j <= M.cols; j++) {
                if (j <= this.cols) M.set(this.get(i, j), i, j);
                else M.set(N.get(i, j - this.cols), i, j);
            }
        }

        this.copy(M);
    }

    /**
     * Merges another matrix to this matrix from bottom.
     *
     * @param N the matrix to be merged from bottom
     * @throws UnsupportedOperationException Throws an exception if the matrices don't have the same number of columns.
     */
    void mergeFromBottom(Matrix N) {
        if ((N.rows == 0) || (N.cols == 0)) return;
        if ((this.rows == 0) || (this.cols == 0)) {
            this.copy(N);
            return;
        }
        if (this.cols != N.cols)
            throw new UnsupportedOperationException("The matrices don't have the same number of columns!");

        Matrix M = new Matrix(this.rows + N.rows, this.cols, ioHelper);
        for (int i = 1; i <= M.rows; i++) {
            for (int j = 1; j <= M.cols; j++) {
                if (i <= this.rows) M.set(this.get(i, j), i, j);
                else M.set(N.get(i - this.rows, j), i, j);
            }
        }

        this.copy(M);
    }

    /**
     * Transposes this matrix.
     *
     * @return transpose of this matrix
     */

    Matrix transpose() {
        Matrix M = new Matrix(this.cols, this.rows, ioHelper);
        for (int i = 1; i <= M.rows; i++) {
            for (int j = 1; j <= M.cols; j++) {
                M.set(this.get(j, i), i, j);
            }
        }

        return M;
    }

    /**
     * Swaps two rows of this matrix.
     *
     * @param r1 the first swapped row
     * @param r2 the other swapped row
     */
    void swapRows(int r1, int r2) {
        Fraction tempFrac;

        for (int i = 1; i <= this.cols; i++) {
            tempFrac = this.get(r1, i);
            this.set(this.get(r2, i), r1, i);
            this.set(tempFrac, r2, i);
        }
    }

    /**
     * Swaps two columns of this matrix.
     *
     * @param c1 the first swapped column
     * @param c2 the other swapped column
     */
    void swapColumns(int c1, int c2) {
        Fraction tempFrac;

        for (int i = 1; i <= this.rows; i++) {
            tempFrac = this.get(i, c1);
            this.set(this.get(i, c2), i, c1);
            this.set(tempFrac, i, c2);
        }
    }

    /**
     * Adds a multiple of one row of this matrix to another row.
     *
     * @param toWhich the row which the other row is added to
     * @param which   the row which is added to the other row
     * @param coeff   the coefficient by which the added row is multiplied before it is added
     */
    void addRow(int toWhich, int which, Fraction coeff) {
        Fraction tempFrac;

        for (int i = 1; i <= this.cols; i++) {
            tempFrac = this.get(toWhich, i).add(coeff.multiply(this.get(which, i)));
            this.set(tempFrac, toWhich, i);
        }
    }

    /**
     * Adds a multiple of one column of this matrix to another column.
     *
     * @param toWhich the column which the other column is added to
     * @param which   the column which is added to the other column
     * @param coeff   the coefficient by which the added column is multiplied before it is added
     */
    void addColumn(int toWhich, int which, Fraction coeff) {
        Fraction tempFrac;

        for (int i = 1; i <= this.rows; i++) {
            tempFrac = this.get(i, toWhich).add(coeff.multiply(this.get(i, which)));
            this.set(tempFrac, i, toWhich);
        }
    }

    /**
     * Multiplies a row of this matrix by a given coefficient.
     *
     * @param which the row to be multiplied
     * @param coeff the coefficient by which the row is multiplied
     */
    void multiplyRow(int which, Fraction coeff) {
        // r*coeff = r + r*(coeff-1)
        this.addRow(which, which, coeff.add(NEG_ONE));
    }

    /**
     * Multiplies a column of this matrix by a given coefficient.
     *
     * @param which the column to be multiplied
     */
    void multiplyColumn(int which, Fraction coeff) {
        // s*coeff = s + s*(coeff-1)
        this.addColumn(which, which, coeff.add(NEG_ONE));
    }

    /**
     * Calculates Matrix product of this matrix and matrix B.
     *
     * @param Matrix B
     * @return product of this matrix and matrix B
     * @throws UnsupportedOperationException Throws an exception if matrices cannot be multiplied.
     */
    Matrix multiplyMatrix(Matrix B) {
        int a = this.getR(), b = this.getC(), c = B.getR(), d = B.getC();
        if (b != c) throw new UnsupportedOperationException("Those matrices cannot be multiplied!");
        Matrix product = new Matrix(a, d, ioHelper);

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < d; j++) {
                Fraction fract = ZERO;
                for (int k = 0; k < b; k++) {
                    fract = fract.add(this.get(i + 1, k + 1).multiply(B.get(k + 1, j + 1)));
                }
                product.set(fract, i + 1, j + 1);
            }
        }

        return product;
    }

    /**
     * Computes the determinant of this matrix.
     *
     * @param label   label of the matrix
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * @return determinant of this matrix
     * @throws UnsupportedOperationException Throws an exception if the matrix is not rectangular.
     * @throws UnsupportedOperationException Throws an exception if the matrix is a null matrix.
     */
    Fraction det(String label, boolean verbose) {
        class Printer {
            private String printText(Matrix M, int i, int j, boolean[][] parentheses) {
                return (parentheses[i - 1][j - 1] ? "(" + M.get(i, j).toText() + ")" : M.get(i, j).toText());
            }

            private String printTeX(Matrix M, int i, int j, boolean[][] parentheses) {
                return (parentheses[i - 1][j - 1] ? "\\left(" + M.get(i, j).toTeX() + "\\right)" : M.get(i, j).toTeX());
            }
        }

        if (this.rows != this.cols)
            throw new UnsupportedOperationException("Determinant can only be computed of rectangular matrices!");

        switch (this.rows) {
            case 0: // null matrix => throw an exception
                throw new UnsupportedOperationException("Determinant can only be computed from matrices with any data!");

            case 1: // 1-by-1 matrix => determinant is equal to the only element
                if (verbose) {
                    textOut("U matice typu 1×1 je determinant roven jedinému jejímu prvku, tj. det(" + label + ") = " + this.get(1, 1).toText() + ".");
                    LaTeXOut("U~matice typu $1\\times 1$ je determinant roven jedinému jejímu prvku, tj.~$\\det(" + label + ") = " + this.get(1, 1).toTeX() + "$.");
                }
                return this.get(1, 1);

            case 2: // 2-by-2 matrix => determinant is a11*a22-a12*a21
                if (verbose) {
                    Printer out = new Printer();
                    boolean[][] parentheses = new boolean[2][2];
                    for (int i = 1; i <= 2; i++) {
                        for (int j = 1; j <= 2; j++) {
                            parentheses[i - 1][j - 1] = (this.get(i, j).compare(ZERO) == -1);
                        }
                    }
                    Fraction result = this.get(1, 1).multiply(this.get(2, 2)).subtract(this.get(1, 2).multiply(this.get(2, 1)));
                    textOut("U matice typu 2×2 je determinant roven rozdílu součinů prvků na hlavní a na vedlejší diagonále, tj. a11*a22-a12*a21. "
                            + "V našem případě je tedy:\\r\n\tdet(" + label + ") = " + this.get(1, 1).toText() + "*" + out.printText(this, 2, 2, parentheses) + " - "
                            + out.printText(this, 1, 2, parentheses) + "*" + out.printText(this, 2, 1, parentheses) + " = " + result.toText() + ".");
                    LaTeXOut("U~matice typu $2\\times 2$ je determinant roven rozdílu součinů prvků na hlavní a na vedlejší diagonále, tj.~$a_{11}a_{22}-a_{12}a_{21}$. "
                            + "V~našem případě je tedy:\n\\[\\textstyle\\det(" + label + ") = " + this.get(1, 1).toTeX() + "\\cdot" + out.printTeX(this, 2, 2, parentheses) + " - "
                            + out.printTeX(this, 1, 2, parentheses) + "\\cdot" + out.printTeX(this, 2, 1, parentheses) + " = " + result.toTeX() + ".\\]\n");
                }
                return (this.get(1, 1).multiply(this.get(2, 2)).subtract(this.get(1, 2).multiply(this.get(2, 1))));

            case 3: // 3-by-3 matrix => determinant is computed using the Rule of Sarrus
                Fraction positive = ZERO;
                Fraction negative = ZERO;
                positive = positive.add(this.get(1, 1).multiply(this.get(2, 2)).multiply(this.get(3, 3)));
                positive = positive.add(this.get(2, 1).multiply(this.get(3, 2)).multiply(this.get(1, 3)));
                positive = positive.add(this.get(3, 1).multiply(this.get(1, 2)).multiply(this.get(2, 3)));
                negative = negative.add(this.get(1, 3).multiply(this.get(2, 2)).multiply(this.get(3, 1)));
                negative = negative.add(this.get(2, 3).multiply(this.get(3, 2)).multiply(this.get(1, 1)));
                negative = negative.add(this.get(3, 3).multiply(this.get(1, 2)).multiply(this.get(2, 1)));
                if (verbose) {
                    Printer out = new Printer();
                    boolean[][] parentheses = new boolean[3][3];
                    for (int i = 1; i <= 3; i++) {
                        for (int j = 1; j <= 3; j++) {
                            parentheses[i - 1][j - 1] = (this.get(i, j).compare(ZERO) == -1);
                        }
                    }
                    textOut("U matice typu 3×3 determinant spočítáme pomocí Sarrusova pravidla. V našem případě je tedy:\r\n\t"
                            + "det(" + label + ") = [" + this.get(1, 1).toText() + "*" + out.printText(this, 2, 2, parentheses) + "*" + out.printText(this, 3, 3, parentheses) + " + "
                            + out.printText(this, 2, 1, parentheses) + "*" + out.printText(this, 3, 2, parentheses) + "*" + out.printText(this, 1, 3, parentheses) + " + "
                            + out.printText(this, 3, 1, parentheses) + "*" + out.printText(this, 1, 2, parentheses) + "*" + out.printText(this, 2, 3, parentheses) + "] - ["
                            + this.get(1, 3).toText() + "*" + out.printText(this, 2, 2, parentheses) + "*" + out.printText(this, 3, 1, parentheses) + " + "
                            + out.printText(this, 2, 3, parentheses) + "*" + out.printText(this, 3, 2, parentheses) + "*" + out.printText(this, 1, 1, parentheses) + " + "
                            + out.printText(this, 3, 3, parentheses) + "*" + out.printText(this, 1, 2, parentheses) + "*" + out.printText(this, 2, 1, parentheses) + "] =\r\n\t    = "
                            + positive.toText() + " - " + (negative.compare(ZERO) == -1 ? "(" + negative.toText() + ")" : negative.toText()) + " = "
                            + positive.subtract(negative).toText() + ".");
                    LaTeXOut("U~matice typu $3\\times 3$ determinant spočítáme pomocí Sarrusova pravidla. V~našem případě je tedy:\n\\begin{align*}\\textstyle\n"
                            + "\\det(" + label + ") &= \\left[" + this.get(1, 1).toTeX() + "\\cdot" + out.printTeX(this, 2, 2, parentheses) + "\\cdot" + out.printTeX(this, 3, 3, parentheses)
                            + " + " + out.printTeX(this, 2, 1, parentheses) + "\\cdot" + out.printTeX(this, 3, 2, parentheses) + "\\cdot" + out.printTeX(this, 1, 3, parentheses) + " + "
                            + out.printTeX(this, 3, 1, parentheses) + "\\cdot" + out.printTeX(this, 1, 2, parentheses) + "\\cdot" + out.printTeX(this, 2, 3, parentheses) + "\\right] -\\\\\n&- \\left["
                            + this.get(1, 3).toTeX() + "\\cdot" + out.printTeX(this, 2, 2, parentheses) + "\\cdot" + out.printTeX(this, 3, 1, parentheses) + " + "
                            + out.printTeX(this, 2, 3, parentheses) + "\\cdot" + out.printTeX(this, 3, 2, parentheses) + "\\cdot" + out.printTeX(this, 1, 1, parentheses) + " + "
                            + out.printTeX(this, 3, 3, parentheses) + "\\cdot" + out.printTeX(this, 1, 2, parentheses) + "\\cdot" + out.printTeX(this, 2, 1, parentheses) + "\\right]=\\\\\n&= "
                            + positive.toTeX() + " - " + (negative.compare(ZERO) == -1 ? "\\left(" + negative.toTeX() + "\\right)" : negative.toTeX()) + " = "
                            + positive.subtract(negative).toTeX() + ".\\end{align*}\n");
                }
                return positive.subtract(negative);

            default: // 3+-by-3+ matrix => determinant is computed using the Laplace Expansion
            /* We will want to modify the matrix such that there will be a row or a column that will only have one non-zero element. We will expand by this row or column.
            In order to minimize the number of computations, we will first select a row or column that already has maximum number of zeroes. */
                int mostZerosRow = 0;
                int mostZerosColumn = 0;
                int mostZerosRowIndex = 1;
                int mostZerosColumnIndex = 1;
                int mostZeros = 0;

                if (verbose) {
                    textOut("U matice větší než 3×3 determinant spočítáme pomocí Laplaceova rozvoje. Budeme chtít některý řádek nebo sloupec upravit tak, aby v něm byl jediný nenulový "
                            + "prvek; rozvoj pak provedeme podle tohoto řádku nebo sloupce, protože pak budeme muset počítat jen jeden determinant nižšího stupně. Abychom zmenšili "
                            + "nezbytný počet výpočtů, podíváme se nejprve, zda již nemáme k dispozici sloupec nebo řádek s větším počtem nul. ");
                    LaTeXOut("U~matice větší než $3\\times 3$ determinant spočítáme pomocí Laplaceova rozvoje. Budeme chtít některý řádek nebo sloupec upravit tak, aby v~něm byl jediný nenulový "
                            + "prvek; rozvoj pak provedeme podle tohoto řádku nebo sloupce, protože pak budeme muset počítat jen jeden determinant nižšího stupně. Abychom zmenšili "
                            + "nezbytný počet výpočtů, podíváme se nejprve, zda již nemáme k~dispozici sloupec nebo řádek s~větším počtem nul. ");
                }

                for (int i = 1; i <= this.rows; i++) {
                    mostZeros = 0;
                    for (int j = 1; j <= this.cols; j++) {
                        if (this.get(i, j).isEqual(ZERO)) mostZeros++;
                    }
                    if (mostZeros > mostZerosRow) {
                        mostZerosRow = mostZeros;
                        mostZerosRowIndex = i;
                    }
                }

                for (int j = 1; j <= this.cols; j++) {
                    mostZeros = 0;
                    for (int i = 1; i <= this.rows; i++) {
                        if (this.get(i, j).isEqual(ZERO)) mostZeros++;
                    }
                    if (mostZeros > mostZerosColumn) {
                        mostZerosColumn = mostZeros;
                        mostZerosColumnIndex = j;
                    }
                }

                if (mostZerosRow == this.rows) {
                    if (verbose) {
                        textOut("Vidíme, že v " + mostZerosRowIndex + ". řádku jsou dokonce samé nuly. Determinant je v takovém případě roven nule.");
                        LaTeXOut("Vidíme, že v~" + mostZerosRowIndex + ".~řádku jsou dokonce samé nuly. Determinant je v~takovém případě roven nule.");
                    }
                    return ZERO; // there was a row full of zeroes => deterimnant is zero
                }

                if (mostZerosColumn == this.cols) {
                    if (verbose) {
                        textOut("Vidíme, že v " + mostZerosColumnIndex + ". sloupci jsou dokonce samé nuly. Determinant je v takovém případě roven nule.");
                        LaTeXOut("Vidíme, že v~" + mostZerosColumnIndex + ".~sloupci jsou dokonce samé nuly. Determinant je v~takovém případě roven nule.");
                    }
                    return ZERO; // there was a row full of zeroes => deterimnant is zero
                }

                Matrix M = new Matrix(ioHelper);
                M.copy(this);
                if (verbose) {
                    if (mostZerosRow == 0 && mostZerosColumn == 0) {
                        textOut("V žádném řádku ani sloupci ale bohužel žádná nula není.");
                        LaTeXOut("V~žádném řádku ani sloupci ale bohužel žádná nula není.");
                    } else if (mostZerosColumn > mostZerosRow) {
                        String howMany = "";
                        switch (mostZerosColumn) {
                            case 1:
                                howMany = "je jedna nula";
                                break;
                            case 2:
                            case 3:
                            case 4:
                                howMany = "jsou " + mostZerosColumn + " nuly";
                            default:
                                howMany = "je " + mostZerosColumn + " nul";
                        }
                        textOut("Vidíme, že v " + mostZerosColumnIndex + ". sloupci " + howMany + ". Až na jeden nyní vynulujeme i další prvky v tomto sloupci.\r\n\r\n");
                        LaTeXOut("Vidíme, že v~" + mostZerosColumnIndex + ".~sloupci " + howMany + ". Až na jeden nyní vynulujeme i další prvky v~tomto sloupci.\n\n");
                    } else {
                        String howMany = "";
                        switch (mostZerosColumn) {
                            case 1:
                                howMany = "je jedna nula";
                                break;
                            case 2:
                            case 3:
                            case 4:
                                howMany = "jsou " + mostZerosColumn + " nuly";
                            default:
                                howMany = "je " + mostZerosColumn + " nul";
                        }
                        textOut("Vidíme, že v " + mostZerosRowIndex + ". řádku " + howMany + ". Až na jeden nyní vynulujeme i další prvky v tomto řádku.\r\n\r\n");
                        LaTeXOut("Vidíme, že v~" + mostZerosRowIndex + ".~řádku " + howMany + ". Až na jeden nyní vynulujeme i další prvky v~tomto řádku.\n\n");
                    }
                }
                if (mostZerosColumn > mostZerosRow) { // we have found a column with many zeroes, and will create zeroes on all rows but one in this column
                    int l = 0; // index of a row with +-1
                    int m = 0; // index of a row with non-zero number
                    for (int i = M.rows; i >= 1; i--) { // scanning through the rows from the bottom
                        if (M.get(i, mostZerosColumnIndex).isEqual(ONE) || M.get(i, mostZerosColumnIndex).isEqual(NEG_ONE))
                            l = i;
                        if (!(M.get(i, mostZerosColumnIndex).isEqual(ZERO))) m = i;
                    }
                    // we have found either a row with +-1, or a row with a non-zero coefficient in our column
                    if (l > 0) {
                        mostZerosRowIndex = l;
                        if (verbose) {
                            textOut("V " + mostZerosRowIndex + ". řádku máme v " + mostZerosColumnIndex + ". sloupci " + M.get(mostZerosRowIndex, mostZerosColumnIndex).toText()
                                    + ", se kterou se dobře počítá; tuto pozici tedy necháme nenulovou. ");
                            LaTeXOut("V~" + mostZerosRowIndex + ".~řádku máme v~" + mostZerosColumnIndex + ".~sloupci $" + M.get(mostZerosRowIndex, mostZerosColumnIndex).toText()
                                    + "$, se kterou se dobře počítá; tuto pozici tedy necháme nenulovou. ");
                        }
                    } else {
                        mostZerosRowIndex = m;
                        if (verbose) {
                            textOut("Nenulový necháme například řádek " + mostZerosRowIndex + ". ");
                            LaTeXOut("Nenulový necháme například řádek " + mostZerosRowIndex + ". ");
                        }
                    }
                    // we add appropriate multiples of this row to other rows, creating zeroes there
                    boolean capitalK = true;
                    for (int i = 1; i <= M.rows; i++) {
                        if (i != mostZerosRowIndex) {
                            Fraction frac = NEG_ONE.multiply(M.get(i, mostZerosColumnIndex).divide(M.get(mostZerosRowIndex, mostZerosColumnIndex)));
                            if (verbose && !frac.isEqual(ZERO)) {
                                if (capitalK) {
                                    textOut("K ");
                                    LaTeXOut("K~");
                                    capitalK = false;
                                } else {
                                    textOut(", k ");
                                    LaTeXOut(", k~");
                                }
                                textOut(i + ". řádku přičteme " + frac.toText() + "-násobek " + mostZerosRowIndex + ". řádku");
                                LaTeXOut(i + ".~řádku přičteme $" + frac.toTeX() + "$\\discretionary{-}{-}{-}násobek " + mostZerosRowIndex + ".~řádku");
                            }
                            M.addRow(i, mostZerosRowIndex, frac);
                            if (verbose) {
                                textOut("Dostáváme tak matici:\r\n" + M.toText() + "\r\n");
                                LaTeXOut("Dostáváme tak matici:\n" + M.toTeX() + "\n");
                            }
                        }
                    }
                    if (verbose && !capitalK) {
                        textOut(". Dostáváme tak matici:\r\n" + M.toText() + "\r\n");
                        LaTeXOut(". Dostáváme tak matici:\n" + M.toTeX() + "\n");
                    }
                } else { // we have found a row with many zeroes, and will create zeroes on all columns but one in this row
                    int l = 0; // index of a column with +-1
                    int m = 0; // index of a column with non-zero number
                    for (int i = M.cols; i >= 1; i--) { // scanning through the columns from the bottom
                        if (M.get(mostZerosRowIndex, i).isEqual(ONE) || M.get(mostZerosRowIndex, i).isEqual(NEG_ONE))
                            l = i;
                        if (!(M.get(mostZerosRowIndex, i).isEqual(ZERO))) m = i;
                    }
                    // we have found either a column with +-1, or a column with a non-zero coefficient in our row
                    if (l > 0) {
                        mostZerosColumnIndex = l;
                        if (verbose) {
                            textOut("V " + mostZerosColumnIndex + ". sloupci máme v " + mostZerosRowIndex + ". řádku " + M.get(mostZerosRowIndex, mostZerosColumnIndex).toText()
                                    + ", se kterou se dobře počítá; tuto pozici tedy necháme nenulovou. ");
                            LaTeXOut("V~" + mostZerosColumnIndex + ".~sloupci máme v~" + mostZerosRowIndex + ".~řádku $" + M.get(mostZerosRowIndex, mostZerosColumnIndex).toText()
                                    + "$, se kterou se dobře počítá; tuto pozici tedy necháme nenulovou. ");
                        }
                    } else {
                        mostZerosColumnIndex = m;
                        if (verbose) {
                            textOut("Nenulový necháme například sloupec " + mostZerosColumnIndex + ". ");
                            LaTeXOut("Nenulový necháme například sloupec " + mostZerosColumnIndex + ". ");
                        }
                    }
                    // we add appropriate multiples of this column to other columns, creating zeroes there
                    boolean capitalK = true;
                    for (int i = 1; i <= M.cols; i++) {
                        if (i != mostZerosColumnIndex) {
                            Fraction frac = NEG_ONE.multiply(M.get(mostZerosRowIndex, i).divide(M.get(mostZerosRowIndex, mostZerosColumnIndex)));
                            if (verbose && !frac.isEqual(ZERO)) {
                                if (capitalK) {
                                    textOut("K ");
                                    LaTeXOut("K~");
                                    capitalK = false;
                                } else {
                                    textOut(", k ");
                                    LaTeXOut(", k~");
                                }
                                textOut(i + ". sloupci přičteme " + frac.toText() + "-násobek " + mostZerosColumnIndex + ". sloupce");
                                LaTeXOut(i + ".~sloupci přičteme $" + frac.toTeX() + "$\\discretionary{-}{-}{-}násobek " + mostZerosColumnIndex + ".~sloupce");
                            }
                            M.addColumn(i, mostZerosColumnIndex, frac);
                        }
                    }
                    if (verbose && !capitalK) {
                        textOut(". Dostáváme tak matici:\r\n" + M.toText() + "\r\n");
                        LaTeXOut(". Dostáváme tak matici:\n\\[\n" + M.toTeX() + "\\]\n");
                    }
                }

                // we crop an (n-1)-by-(n-1) submatrix (excluding the column and row with most zeroes)
                Matrix N = new Matrix(M.rows - 1, ioHelper);
                for (int i = 1, I = 1; i <= M.rows; i++, I++) {
                    if (i != mostZerosRowIndex) {
                        for (int j = 1, J = 1; j <= M.cols; j++, J++) {
                            if (j != mostZerosColumnIndex)
                                N.set(M.get(i, j), I, J);
                            else J--;
                        }
                    } else I--;
                }

                if (verbose) {
                    int parity = (((mostZerosRowIndex + mostZerosColumnIndex) % 2 == 0) ? 1 : -1);
                    String elementText = (M.get(mostZerosRowIndex, mostZerosColumnIndex).compare(ZERO) == -1 ? "(" + M.get(mostZerosRowIndex, mostZerosColumnIndex).toText() + ")"
                            : M.get(mostZerosRowIndex, mostZerosColumnIndex).toText());
                    String elementTeX = (M.get(mostZerosRowIndex, mostZerosColumnIndex).compare(ZERO) == -1 ? "\\left(" + M.get(mostZerosRowIndex, mostZerosColumnIndex).toTeX() + "\\right)"
                            : M.get(mostZerosRowIndex, mostZerosColumnIndex).toTeX());
                    textOut("Protože (-1)^(" + mostZerosRowIndex + "+" + mostZerosColumnIndex + ") = " + parity + ", determinant nyní spočítáme det(" + label + ") = " + parity + "*"
                            + elementText + "*det(" + label + "'), kde " + label + "' je původní matice, z níž jsme odebrali " + mostZerosRowIndex
                            + ". řádek a " + mostZerosColumnIndex + ". sloupec, čili matice:\r\n" + N.toText());
                    LaTeXOut("Protože $(-1)^{(" + mostZerosRowIndex + "+" + mostZerosColumnIndex + ")} = " + parity + "$, determinant nyní spočítáme $\\det(" + label + ") = " + parity
                            + "\\cdot " + elementTeX + "\\cdot\\det(" + label + "')$, kde $" + label + "'$ je původní matice, z~níž jsme odebrali "
                            + mostZerosRowIndex + ".~řádek a " + mostZerosColumnIndex + ".~sloupec, čili matice:\n\\[" + N.toTeX() + "\\]\n");
                    Fraction result = new Fraction();
                    Fraction det = N.det(label + "'", true);
                    if (parity == 1)
                        result = M.get(mostZerosRowIndex, mostZerosColumnIndex).multiply(det);
                    else
                        result = NEG_ONE.multiply(M.get(mostZerosRowIndex, mostZerosColumnIndex).multiply(det));
                    textOut("Je tedy det(" + label + ") = " + parity + "*" + elementText + "*" + det.toText() + " = " + result.toText() + ". ");
                    LaTeXOut("Je tedy $\\det(" + label + ") = " + parity + "\\cdot " + elementTeX + "\\cdot " + det.toTeX() + " = " + result.toTeX() + "$. ");
                    return result;
                }

                // we will calculate the determinant of this submatrix and multiply it by the appropriate coeficient
                if ((mostZerosRowIndex + mostZerosColumnIndex) % 2 == 0)
                    return M.get(mostZerosRowIndex, mostZerosColumnIndex).multiply(N.det("X", false));
                else
                    return NEG_ONE.multiply(M.get(mostZerosRowIndex, mostZerosColumnIndex).multiply(N.det("X", false)));
        }
    }

    /**
     * Deletes all zero rows in this matrix. If this matrix consists only of zero rows, it will become a null matrix (having no rows, no columns and no data).
     */
    void deleteZeroRows() {
        int zeroRows = 0;   // number of zero rows
        boolean allZero;    // a flag that marks if all elements in the row are zero, ie. if it is a zero row
        Matrix tempM = new Matrix(ioHelper);
        tempM.copy(this);
        int i = 1;              // current position
        int toGo = this.rows;   // terminal position

        while (i <= toGo) {
            allZero = true;
            for (int j = 1; j <= tempM.cols; j++) { // check if ith row is zero
                if (!(tempM.get(i, j).isEqual(ZERO))) {
                    allZero = false; // if there is a non-zero element, turn the flag off
                    break;
                }
            }

            if (allZero) {  /* put the zero row to the bottom (let it fall all the way down to the 
                place where there is last potentially non-zero row) */
                for (int j = i; j < tempM.rows - zeroRows; j++) {
                    tempM.swapRows(j, j + 1);
                }
                zeroRows++; // increment the number of zero rows
                toGo--;     // on the last position, there already is a zero row we put there just now, so there's no need to check it again
            } else i++; /* if there was any row swapping, there is a new row on the current position, so we need to check it again;
            if we didn't swap any rows, we can move on to the next row. */
        }

        if (this.rows - zeroRows > 0) { // there are any non-zero rows
            this.copy(tempM.submatrix(1, this.rows - zeroRows, 1, this.cols));  // we crop only the non-zero rows
        } else { // there are only zero rows
            this.copy(new Matrix(ioHelper)); // we return a null matrix
        }
    }

    /**
     * Finds out if the matrix is in echelon form but only cares about first <code>howManyColumns</code> columns.
     */
    private boolean isInEchelonForm(int howManyColumns) {
        if (howManyColumns == 0) return true;
        if (howManyColumns == this.cols) return this.isInEchelonForm();
        int c;
        int pastLeadingZeroes = 0;

        for (int r = 1; r <= this.rows; r++) {
            c = 1;
            while (this.get(r, c).isEqual(ZERO) && c <= howManyColumns) {
                c++;
            }
            if (c <= pastLeadingZeroes && c - 1 < howManyColumns) return false;
            pastLeadingZeroes = c;
        }

        return true;
    }

    /**
     * Finds out if the matrix is in echelon form.
     */
    private boolean isInEchelonForm() {
        int c;
        int pastLeadingZeroes = 0;

        for (int r = 1; r <= this.rows; r++) {
            c = 1;
            while (this.get(r, c).isEqual(ZERO) && c <= this.cols) {
                c++;
            }
            if (c <= pastLeadingZeroes) return false;
            pastLeadingZeroes = c;
        }

        return true;
    }

    /**
     * Converts this matrix into its echelon form. If any rows will become zero rows during the process, they will be deleted. This method “talks” (creates text and TeX output).
     *
     * @return echelon form of this matrix (ie. every row begins with more leading zeroes than its predecessor)
     */
    Matrix getEchelonForm() {
        return this.getEchelonForm('M', Math.min(this.rows, this.cols), true);
    }

    /**
     * Converts this matrix into its echelon form. If any rows will become zero rows during the process, they will be deleted.
     *
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * @return echelon form of this matrix (ie. every row begins with more leading zeroes than its predecessor)
     */
    Matrix getEchelonForm(boolean verbose) {
        return this.getEchelonForm('M', Math.min(this.rows, this.cols), verbose);
    }

    /**
     * Converts this matrix into a semi-echelon form. That means that only a given number of columns from the left will be in echelon form, the rest needs not.
     * If any rows will become zero rows during the process, they will be deleted. This method “talks” (creates text and TeX output).
     *
     * @param maxRank the number of columns we want to have in echelon form
     * @return echelon form of this matrix (ie. every row begins with more leading zeroes than its predecessor)
     */
    Matrix getEchelonForm(int maxRank) {
        return this.getEchelonForm('M', maxRank, true);
    }

    /**
     * Converts this matrix into a semi-echelon form. That means that only a given number of columns from the left will be in echelon form, the rest needs not.
     * If any rows will become zero rows during the process, they will be deleted.
     *
     * @param label   label of the matrix
     * @param maxRank the number of columns we want to have in echelon form
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * @return a matrix with first <code>maxRank</code> columns in echelon form (ie. every row begins with more leading zeroes than its predecessor)
     */
    Matrix getEchelonForm(char label, int maxRank, boolean verbose) {
        Matrix M = new Matrix(ioHelper);
        M.copy(this);
        M.deleteZeroRows();
        maxRank = Math.min(Math.min(M.rows, M.cols), maxRank);

        if (M.isInEchelonForm(maxRank)) {
            if (verbose) {
                textOut("Matice " + label + ":\r\n" + M.toText() + "již je ve schodovitém tvaru. ");
                LaTeXOut("Matice $" + label + "$:\n\\[" + M.toTeX() + "\\]\njiž je ve schodovitém tvaru. ");
            }
            return M;
        }

        if (verbose) {
            textOut("Převedeme matici " + label + ":\r\n" + M.toText() + "na schodovitý tvar.");
            LaTeXOut("Převedeme matici $" + label + "$:\n\\[" + M.toTeX() + "\n\\]\nna schodovitý tvar.");
            if (maxRank < Math.min(M.rows, M.cols)) {
                String howMany = new String();
                switch (maxRank) {
                    case 1:
                        howMany = "první sloupec";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        howMany = "první " + maxRank + " sloupce";
                        break;
                    default:
                        howMany = "prvních " + maxRank + " sloupců";
                        break;
                }
                textOut(" Stačí nám přitom převést pouze " + howMany + " zleva.");
                LaTeXOut(" Stačí nám přitom převést pouze " + howMany + " zleva.");
            }
            textOut("\r\n\r\n");
            LaTeXOut("\n\n");
        }

        boolean capitalK = true;
        int lastRow = 1;
        Fraction frac = new Fraction();
        for (int r = 1, c = 1; !M.isInEchelonForm(maxRank); c++) {
            if (verbose && c > 1) {
                textOut("Pokračujeme dalším sloupcem" + (lastRow == r ? ". " : " a dalším řádkem. "));
                LaTeXOut("Pokračujeme dalším sloupcem" + (lastRow == r ? ". " : " a dalším řádkem. "));
                lastRow = r;
            }
            /* At first, we look at the leading coefficient of the row. If it isnt 1 or -1, we try to look for a row below that has 1 or -1 in this column, so as to make
            computations easier. If we can't find one, and the leading coefficient is 0, we look for a row that has any non-zero number in this column. */
            if (!(M.get(r, c).isEqual(ONE) || M.get(r, c).isEqual(NEG_ONE))) { // this is skipped, if we already have +-1 on the leading position
                int e = -1; // this is the index of row with +-1
                int nz = -1; // this is the index of row with any non-zero number
                for (int i = M.rows; i >= r; i--) { // we scan through the rows from bottom, so as to do "shortest swaps as possible"
                    if (M.get(i, c).isEqual(ONE) || M.get(i, c).isEqual(NEG_ONE)) e = i;
                    if (!(M.get(i, c).isEqual(ZERO))) nz = i;
                }
                if (M.get(r, c).isEqual(ZERO)) {     // if the leading coefficient is 0,
                    if (e >= 0) {                     // first try to swap this row with a row with +-1 on the leading position,
                        M.swapRows(r, e);
                        if (verbose) {
                            textOut("V " + String.valueOf(r) + ". řádku máme na vedoucí pozici nulu. Prohodíme tedy tento řádek s " + String.valueOf(e)
                                    + ". řádkem, neboť v něm je na vedoucí pozici " + M.get(r, c).toText() + ", což je pro počítání výhodné. "
                                    + "Matice " + label + " tedy nyní je:\r\n" + M.toText() + "\r\n");
                            LaTeXOut("V~" + String.valueOf(r) + ".~řádku máme na vedoucí pozici nulu. Prohodíme tedy tento řádek s~" + String.valueOf(e)
                                    + ".~řádkem, neboť v~něm je na vedoucí pozici $" + M.get(r, c).toTeX() + "$, což je pro počítání výhodné. "
                                    + "Matice $" + label + "$ tedy nyní je:\n\\[" + M.toTeX() + "\n\\]\n\n");
                        }
                    } else if (nz >= 0) {                // or with nearest row with a non-zero leading coefficient
                        M.swapRows(r, nz);
                        if (verbose) {
                            textOut("V " + String.valueOf(r) + ". řádku máme na vedoucí pozici nulu. Prohodíme tedy tento řádek s " + String.valueOf(nz)
                                    + ". řádkem, v němž je na vedoucí pozici nenulové číslo " + M.get(r, c).toText() + ". "
                                    + "Matice " + label + " tedy nyní je:\r\n" + M.toText() + "\r\n");
                            LaTeXOut("V~" + String.valueOf(r) + ".~řádku máme na vedoucí pozici nulu. Prohodíme tedy tento řádek s~" + String.valueOf(nz)
                                    + ".~řádkem, v němž je na vedoucí pozici nenulové číslo $" + M.get(r, c).toTeX() + "$. "
                                    + "Matice $" + label + "$ tedy nyní je:\n\\[" + M.toTeX() + "\n\\]\n\n");
                        }
                    }
                } else {                              // if the leading coefficient isn't 0,
                    if (e >= 0) {                     // we swap this row a row with +-1 on the leading position, if available
                        M.swapRows(r, e);
                        if (verbose) {
                            textOut("V " + String.valueOf(e) + ". řádku máme na vedoucí pozici " + M.get(r, c).toText()
                                    + ", což je pro počítání výhodné, a tak tento řádek prohodíme s řádkem " + String.valueOf(r) + ". "
                                    + "Matice " + label + " tedy nyní je:\r\n" + M.toText() + "\r\n");
                            LaTeXOut("V~" + String.valueOf(e) + ".~řádku máme na vedoucí pozici $" + M.get(r, c).toTeX()
                                    + "$, což je pro počítání výhodné, a tak tento řádek prohodíme s~řádkem~" + String.valueOf(r) + ". "
                                    + "Matice $" + label + "$ tedy nyní je:\n\\[" + M.toTeX() + "\n\\]\n\n");
                        }
                    }
                }
            }
            if (!(M.get(r, c).isEqual(ZERO))) {
                /* If we found a suitable row to put on the first position, we add its appropriate multiple to all other rows, so as to make their coefficients in this
                column zeroes. */
                for (int i = r + 1; i <= M.rows; i++) {
                    frac = NEG_ONE.multiply(M.get(i, c).divide(M.get(r, c)));
                    if (!frac.isEqual(ZERO)) {
                        if (verbose) {
                            if (capitalK) {
                                textOut("K ");
                                LaTeXOut("K~");
                                capitalK = false;
                            } else {
                                textOut(", k ");
                                LaTeXOut(", k~");
                            }

                            textOut(String.valueOf(i) + ". řádku přičteme " + frac.toText() + "-násobek " + String.valueOf(r) + ". řádku");
                            LaTeXOut(String.valueOf(i) + ".~řádku přičteme $" + frac.toTeX() + "$\\discretionary{-}{-}{-}násobek " + String.valueOf(r) + ".~řádku");
                        }
                        M.addRow(i, r, frac);
                    }
                }
                if (verbose && !capitalK) {
                    textOut(". ");
                    LaTeXOut(". ");
                }
                int rowsPreviously = M.getR();
                M.deleteZeroRows();                                  // we delete zero rows
                maxRank = Math.min(Math.min(M.rows, M.cols), maxRank); // and change maxRank appropriately
                r++;                                                 // only in this case are we done with this row and can move on;
                // if there was a column of zeroes, we must stay on the same row
                if (verbose && !capitalK) {
                    textOut((rowsPreviously == M.getR() ? "Matice " + label + " tedy nyní je:" : "Po odstranění nulových řádků tedy matice " + label + " je:")
                            + "\r\n" + M.toText() + "\r\n\r\n");
                    LaTeXOut((rowsPreviously == M.getR() ? "Matice $" + label + "$ tedy nyní je:" : "Po odstranění nulových řádků tedy matice $" + label + "$ je:")
                            + "\n\\[" + M.toTeX() + "\n\\]\n\n");
                    capitalK = true;
                }
            } else if (verbose) {
                textOut("Protože " + String.valueOf(c) + ". sloupec je nulový, tak jej přeskočíme.\r\n\r\n");
                LaTeXOut("Protože " + String.valueOf(c) + ".~sloupec je nulový, tak jej přeskočíme.\n\n");
            }
        }

        if (verbose) {
            textOut("Matici " + label + " jsme tak převedli do schodovitého tvaru.");
            LaTeXOut("Matici $" + label + "$ jsme tak převedli do schodovitého tvaru.");
        }

        return M;
    }

    /**
     * Rank of the matrix. This method “talks” (creates text and TeX output).
     *
     * @return the rank of this matrix, ie. the maximum number of linearly independent rows
     */
    int rank() {
        return rank('M', true);
    }

    /**
     * Rank of the matrix.
     *
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * @return the rank of this matrix, ie. the maximum number of linearly independent rows
     */
    int rank(boolean verbose) {
        return rank('M', verbose);
    }

    /**
     * Rank of the matrix.
     *
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * @return the rank of this matrix, ie. the maximum number of linearly independent rows
     * @label label of the matrix
     */
    int rank(char label, boolean verbose) {
        return this.getEchelonForm(label, Math.min(this.rows, this.cols), verbose).rows;
    }

    /**
     * Checks the solvability of this matrix. This method doesn't “talk” (creates no text nor TeX output).
     *
     * @return true if the matrix is solvable, false otherwise
     */
    boolean isSolvable() {
        return isSolvable(false, false);
    }

    boolean isSolvable(boolean verbose) {
        return isSolvable(verbose, false);
    }

    /**
     * Checks the solvability of this matrix.
     *
     * @param verbose   determines, if the method “talks” (creates any text and TeX output)
     * @param solveOnly determines, if the method prints only the solving stage without preparations or full solution
     * @return true if the matrix is solvable, false otherwise
     */
    boolean isSolvable(boolean verbose, boolean solveOnly) {
        Matrix M = new Matrix(ioHelper);
        if (verbose) {
            textOut("Soustava je řešitelná, jestliže její rozšířená matice má stejnou hodnost jako matice soustavy bez pravých stran."
                    + "\r\n\r\nrozhodnutí o řešitelnosti\r\n" + "-------------------------\r\n");
            LaTeXOut("Soustava je řešitelná, jestliže její rozšířená matice má stejnou hodnost jako matice soustavy bez pravých stran."
                    + "\n\\subsection*{rozhodnutí o~řešitelnosti}\n");
        }

        M = this.getEchelonForm(verbose && !solveOnly);
        int augMatrixRank = M.getR();
        M = M.getSystemCoeffMatrix();

        String howMany = "";
        if (verbose) {
            switch (augMatrixRank) {
                case 1:
                    howMany = "jeden řádek";
                    break;
                case 2:
                case 3:
                case 4:
                    howMany = augMatrixRank + " řádky";
                    break;
                default:
                    howMany = augMatrixRank + " řádků";
                    break;
            }
            textOut("Matice má nyní " + howMany + ", takže její hodnost je " + augMatrixRank + ". „Odstřihneme“ nyní sloupec absolutních členů a dostaneme matici:\r\n"
                    + M.toText());
            LaTeXOut("Matice má nyní " + howMany + ", takže její hodnost je " + augMatrixRank + ". \\uv{Odstřihneme} nyní sloupec absolutních členů a dostaneme matici:"
                    + "\n\\[\n" + M.toTeX() + ".\n\\]\n");
        }

        M.deleteZeroRows();
        int systemCoeffMatrixRank = M.getR();

        if (verbose) {
            if (augMatrixRank == systemCoeffMatrixRank) {
                textOut("V matici nevznikly žádné nulové řádky; matice má tedy stále " + howMany + " a její hodnost je také " + augMatrixRank
                        + ". Soustava je řešitelná a podprostory se protínají.");
                LaTeXOut("V~matici nevznikly žádné nulové řádky; matice má tedy stále " + howMany + " a její hodnost je také " + augMatrixRank
                        + ". Soustava je řešitelná a podprostory se protínají.");
            } else {
                switch (systemCoeffMatrixRank) {
                    case 1:
                        howMany = "jeden řádek";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        howMany = systemCoeffMatrixRank + " řádky";
                        break;
                    default:
                        howMany = systemCoeffMatrixRank + " řádků";
                        break;
                }
                textOut("V matici tak vznikly nulové řádky; ty odstraníme a matice má již jen " + howMany + " - její hodnost je tedy " + systemCoeffMatrixRank
                        + ". Protože " + augMatrixRank + "≠" + systemCoeffMatrixRank + ", soustava není řešitelná a podprostory se neprotínají.");
                LaTeXOut("V~matici tak vznikly nulové řádky; ty odstraníme a matice má již jen " + howMany + "~-- její hodnost je tedy " + systemCoeffMatrixRank
                        + ". Protože $" + augMatrixRank + "\\neq" + systemCoeffMatrixRank + "$, soustava není řešitelná a podprostory se neprotínají.");
            }
        }

        return (augMatrixRank == systemCoeffMatrixRank);
    }

    /**
     * Solves this matrix, or better converts it to a solved form. A solved form is a pseudo-identity matrix, completed in such manner that the corresponding matrix of
     * equation system coefficients is rectangular (ie. matrix of dimensions <i>n</i>&times;<i>n</i>+1). A pseudo-identity matrix is a form of matrix in which the
     * left rectangular submatrix <i>n</i>&times;<i>n</i>, where <i>n</i> is the rank of the whole matrix, is identity matrix.
     * <p>
     * This method doesn't “talk” (creates no text nor TeX output).
     *
     * @return solved form of this matrix; the rightmost column contains a numerical solution; if any free variables have been assigned
     * @throws UnsupportedOperationException Throws an exception if the matrix is not solvable.
     */
    Matrix solve() {
        return solve('U', false, false);
    }

    /**
     * Solves this matrix, or better converts it to a solved form. A solved form is a pseudo-identity matrix, completed in such manner that the corresponding matrix of
     * equation system coefficients is rectangular (ie. matrix of dimensions <i>n</i>&times;<i>n</i>+1). A pseudo-identity matrix is a form of matrix in which the
     * left rectangular submatrix <i>n</i>&times;<i>n</i>, where <i>n</i> is the rank of the whole matrix, is identity matrix.
     *
     * @param verbose determines, if the method “talks” (creates any text and TeX output); if it does, it prints full output
     * @return solved form of this matrix; the rightmost column contains a numerical solution; if any free variables have been assigned
     * @throws UnsupportedOperationException Throws an exception if the matrix is not solvable.
     * @label label of the matrix
     */
    Matrix solve(char label, boolean verbose) {
        return solve(label, verbose, false);
    }

    /**
     * Solves this matrix, or better converts it to a solved form. A solved form is a pseudo-identity matrix, completed in such manner that the corresponding matrix of
     * equation system coefficients is rectangular (ie. matrix of dimensions <i>n</i>&times;<i>n</i>+1). A pseudo-identity matrix is a form of matrix in which the
     * left rectangular submatrix <i>n</i>&times;<i>n</i>, where <i>n</i> is the rank of the whole matrix, is identity matrix.
     *
     * @param verbose determines, if the method “talks” (creates any text and TeX output); if it does, it prints only the solving stage without preparations
     * @return solved form of this matrix; the rightmost column contains a numerical solution; if any free variables have been assigned
     * @throws UnsupportedOperationException Throws an exception if the matrix is not solvable.
     * @label label of the matrix
     */
    Matrix solveOnly(char label, boolean verbose) {
        return solve(label, verbose, true);
    }

    /**
     * Solves this matrix, or better converts it to a solved form. A solved form is a pseudo-identity matrix, completed in such manner that the corresponding matrix of
     * equation system coefficients is rectangular (ie. matrix of dimensions <i>n</i>&times;<i>n</i>+1). A pseudo-identity matrix is a form of matrix in which the
     * left rectangular submatrix <i>n</i>&times;<i>n</i>, where <i>n</i> is the rank of the whole matrix, is identity matrix.
     *
     * @param verbose   determines, if the method “talks” (creates any text and TeX output)
     * @param solveOnly determines, if the method prints only the solving stage without preparations or full solution
     * @return solved form of this matrix; the rightmost column contains a numerical solution; if any free variables have been assigned
     * @throws UnsupportedOperationException Throws an exception if the matrix is not solvable.
     * @label label of the matrix
     */
    Matrix solve(char label, boolean verbose, boolean solveOnly) {
        if (!(this.isSolvable())) throw new UnsupportedOperationException("The matrix is not solvable!");

        Matrix M = new Matrix(ioHelper);
        M.copy(this.getEchelonForm(verbose && !solveOnly)); // we convert the matrix to its echelon form

        Subspace fake = new Subspace(M, ioHelper);
        int params = 0;
        if (verbose) {
            textOut("\r\n\r\nvyřešení soustavy\r\n-----------------\r\n");
            textOut("Máme tedy soustavu lineárních rovnic:\r\n" + fake.toTextGeneral());

            LaTeXOut("\n\n\\subsection*{vyřešení soustavy}\n");
            LaTeXOut("Máme tedy soustavu lineárních rovnic:\n\\[\n" + fake.toTeXGeneral() + "\\]\n");

            params = M.cols - M.rows - 1;
        }

        /* First, we create a vector of permutations (1, 2, 3, ..., n-1), where n is number of columns of the matrix (ie. n-1 is the number of variables). This is to keep 
        knowledge about any column swaps. We use Matrix class instead of Vector class, so that we can later use swapColumns() method. */
        Fraction[] tempFracArray = new Fraction[M.cols - 1];
        for (int i = 1; i <= tempFracArray.length; i++) tempFracArray[i - 1] = new Fraction(i);
        Matrix P = new Matrix(tempFracArray, 1, M.cols - 1, ioHelper);

        /* We reorder the matrix's columns so that we get an upper triangular matrix from left. */
        int maxRank = Math.min(M.cols - 1, M.rows); // we don't take the last column into account, however, as there are absolute terms
        for (int i = 1; i <= maxRank; i++) {
            if (M.get(i, i).isEqual(ZERO)) {
                for (int j = i + 1; i <= M.cols - 1; j++) {
                    if (!(M.get(i, j).isEqual(ZERO))) {
                        M.swapColumns(i, j);
                        P.swapColumns(i, j); // we mark all changes
                        break;
                    }
                }
            }
        }

        /* We normalize the rows, ie. we multiply them with such a coefficient that the leading numbers will equal +1. */
        for (int i = 1; i <= maxRank; i++) M.multiplyRow(i, ONE.divide(M.get(i, i)));

        /* We convert the upper triangular matrix into identity matrix (the whole matrix is now thus pseudo-identity). */
        for (int i = M.rows; i >= 2; i--) {
            for (int j = i - 1; j >= 1; j--) {
                M.addRow(j, i, NEG_ONE.multiply(M.get(j, i)));
            }
        }

        /* We complete the matrix to rectangular form. */

        Matrix N = new Matrix(ioHelper);
        N.copy(M);
        M.rows = M.cols - 1;
        M.data = new Fraction[M.rows][M.cols];
        for (int i = 1; i <= M.rows; i++) {
            for (int j = 1; j <= M.cols; j++) {
                if (i <= N.rows) M.set(N.get(i, j), i, j);
                else {
                    if (i == j) M.set(NEG_ONE, i, j); // putting -1 on the diagonal
                    else M.set(ZERO, i, j);       // and 0s elsewhere
                }
            }
        }

        /* We take care of wrong signs. */
        M.copy(M.transpose());
        for (int i = N.rows; i <= M.rows; i++) M.multiplyRow(i, NEG_ONE);
        M.copy(M.transpose());

        /* We reorder the rows so that the correspondence to variables is now correct. */
        Matrix POrig = new Matrix(ioHelper);
        POrig.copy(P);
        for (int i = 1; i < P.cols; i++) {
            int j = (int) P.get(1, i).n;
            if (j != i) {
                P.swapColumns(i, j);
                M.swapRows(i, j);
                i--;
            }
        }

        if (verbose) {
            String equations, unknowns, parText;
            Point[] refPoint = new Point[1];
            refPoint[0] = new Point(M.getConstTerms().transpose().rowsToVectors()[0], ioHelper);
            if (params == 0) {
                Vector[] noVectors = new Vector[0];
                fake = new Subspace(refPoint, noVectors, ioHelper);
            } else
                fake = new Subspace(refPoint, M.submatrix(1, M.rows, M.cols - params, M.cols - 1).transpose().rowsToVectors(), ioHelper);

            if (M.cols - params - 1 == 1) equations = "jedinou rovnici";
            else equations = "soustavu " + String.valueOf(M.cols - params - 1) + " rovnic";
            if (M.cols - 1 == 1) unknowns = "jedné neznámé";
            else unknowns = String.valueOf(M.cols - 1) + " neznámých";
            if (params == 0) parText = "nebude obsahovat parametr.";
            else {
                parText = "bude obsahovat " + String.valueOf(params) + " parametr";
                switch (params) {
                    case 1:
                        break;
                    case 2:
                    case 3:
                    case 4:
                        parText += "y";
                        break;
                    default:
                        parText += "ů";
                        break;
                }
                parText += ".";
            }

            textOut("Jde o " + equations + " o " + unknowns + ", a řešení tedy " + parText);
            LaTeXOut("Jde o~" + equations + " o~" + unknowns + ", a řešení tedy " + parText);

            if (params == 0) {
                textOut("\r\nZ poslední rovnice je\r\n\tx_" + String.valueOf(M.cols - 1) + " = " + M.get(M.cols - 1, M.cols).toText());
                LaTeXOut("\n\nZ~poslední rovnice je\n\\[x_{" + String.valueOf(M.cols - 1) + "} = " + M.get(M.cols - 1, M.cols).toTeX());

                for (int i = M.cols - 2; i >= 1; i--) {
                    if (i == 1) {
                        textOut("\r\na ");
                        LaTeXOut("\\]a ");
                    } else {
                        textOut(",\r\n");
                        LaTeXOut(",\\]");
                    }
                    textOut("z " + String.valueOf(i) + ". rovnice je\r\n\tx_" + String.valueOf(i) + " = " + M.get(i, M.cols).toText());
                    LaTeXOut("z~" + String.valueOf(i) + ".~rovnice je\n\\[x_{" + String.valueOf(i) + "} = " + M.get(i, M.cols).toTeX());
                }

                textOut(".\r\n\r\n");
                LaTeXOut(".\\]\n\n");
            } else {
                String result = fake.toTextParametric(false);
                String TeXresult = fake.toTeXParametric();
                String[] individualRows = result.split("\n");
                String[] individualRowsTeX = TeXresult.split("\n");

                textOut("\r\nZvolíme některé neznámé za volné:\r\n");
                LaTeXOut("\n\nZvolíme některé neznámé za volné:\n\\[" + individualRowsTeX[0] + "\n");
                for (int i = POrig.getC(), j = 1; i > POrig.getC() - params; i--, j++) {
                    textOut("\tx_" + String.valueOf(POrig.get(1, i).n) + " = " + individualRows[(int) POrig.get(1, i).n - 1] + "\r\n");
                    LaTeXOut(individualRowsTeX[(int) POrig.get(1, i).n] + "\n");
                }
                LaTeXOut(individualRowsTeX[individualRowsTeX.length - 1] + "\n\\]\n\n");

                boolean bigP = true;
                Fraction toPrint = new Fraction();

                for (int i = POrig.getC() - params; i >= 1; i--) {
                    textOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ". rovnice a úpravě dostáváme\r\n\tx_"
                            + String.valueOf(POrig.get(1, i).n) + " = " + individualRows[(int) POrig.get(1, i).n - 1]);
                    LaTeXOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ".~rovnice a úpravě dostáváme\n\\["
                            + individualRowsTeX[(int) POrig.get(1, i).n]);
                    bigP = false;

                    switch (i) {
                        case 1:
                            textOut(".\r\n\r\n");
                            LaTeXOut(".\n\\]\n\n");
                            break;
                        case 2:
                            textOut("\r\na ");
                            LaTeXOut("\n\\]\na ");
                            break;
                        default:
                            textOut(",\r\n");
                            LaTeXOut(",\n\\]");
                            break;
                    }
                }
            }

            textOut("Řešením dané soustavy rovnic je tedy:\r\n" + fake.toTextParametric(true));
            LaTeXOut("Řešením dané soustavy rovnic je tedy:\n\\[\n" + fake.toTeXParametric() + "\\]");
        }

        return M;
    }

    /**
     * Alternative function for solve with changing of variables.
     *
     * @param verbose   determines, if the method “talks” (creates any text and TeX output)
     * @param solveOnly determines, if the method prints only the solving stage without preparations or full solution
     * @param varChange number of equations with variable t
     * @return solved form of this matrix; the rightmost column contains a numerical solution; if any free variables have been assigned
     * @throws UnsupportedOperationException Throws an exception if the matrix is not solvable.
     * @label label of the matrix
     */
    Matrix solve2(char label, boolean verbose, boolean solveOnly, int varChange) {
        if (!(this.isSolvable())) throw new UnsupportedOperationException("The matrix is not solvable!");

        Matrix M = new Matrix(ioHelper);
        M.copy(this.getEchelonForm(verbose && !solveOnly)); // we convert the matrix to its echelon form

        Subspace fake = new Subspace(M, ioHelper);
        int params = 0;
        if (verbose) {
            textOut("\r\n\r\nvyřešení soustavy\r\n-----------------\r\n");
            textOut("Máme tedy soustavu lineárních rovnic:\r\n" + fake.toTextGeneral2(true, 'u', varChange));

            LaTeXOut("\n\n\\subsection*{vyřešení soustavy}\n");
            LaTeXOut("Máme tedy soustavu lineárních rovnic:\n\\[\n" + fake.toTeXGeneral2('u', varChange) + "\\]\n");

            params = M.cols - M.rows - 1;
        }

        /* First, we create a vector of permutations (1, 2, 3, ..., n-1), where n is number of columns of the matrix (ie. n-1 is the number of variables). This is to keep 
        knowledge about any column swaps. We use Matrix class instead of Vector class, so that we can later use swapColumns() method. */
        Fraction[] tempFracArray = new Fraction[M.cols - 1];
        for (int i = 1; i <= tempFracArray.length; i++) tempFracArray[i - 1] = new Fraction(i);
        Matrix P = new Matrix(tempFracArray, 1, M.cols - 1, ioHelper);

        /* We reorder the matrix's columns so that we get an upper triangular matrix from left. */
        int maxRank = Math.min(M.cols - 1, M.rows); // we don't take the last column into account, however, as there are absolute terms
        for (int i = 1; i <= maxRank; i++) {
            if (M.get(i, i).isEqual(ZERO)) {
                for (int j = i + 1; i <= M.cols - 1; j++) {
                    if (!(M.get(i, j).isEqual(ZERO))) {
                        M.swapColumns(i, j);
                        P.swapColumns(i, j); // we mark all changes
                        break;
                    }
                }
            }
        }

        /* We normalize the rows, ie. we multiply them with such a coefficient that the leading numbers will equal +1. */
        for (int i = 1; i <= maxRank; i++) M.multiplyRow(i, ONE.divide(M.get(i, i)));

        /* We convert the upper triangular matrix into identity matrix (the whole matrix is now thus pseudo-identity). */
        for (int i = M.rows; i >= 2; i--) {
            for (int j = i - 1; j >= 1; j--) {
                M.addRow(j, i, NEG_ONE.multiply(M.get(j, i)));
            }
        }

        /* We complete the matrix to rectangular form. */

        Matrix N = new Matrix(ioHelper);
        N.copy(M);
        M.rows = M.cols - 1;
        M.data = new Fraction[M.rows][M.cols];
        for (int i = 1; i <= M.rows; i++) {
            for (int j = 1; j <= M.cols; j++) {
                if (i <= N.rows) M.set(N.get(i, j), i, j);
                else {
                    if (i == j) M.set(NEG_ONE, i, j); // putting -1 on the diagonal
                    else M.set(ZERO, i, j);       // and 0s elsewhere
                }
            }
        }

        /* We take care of wrong signs. */
        M.copy(M.transpose());
        for (int i = N.rows + 1; i <= M.rows; i++) M.multiplyRow(i, NEG_ONE);
        M.copy(M.transpose());

        /* We reorder the rows so that the correspondence to variables is now correct. */
        Matrix POrig = new Matrix(ioHelper);
        POrig.copy(P);
        for (int i = 1; i < P.cols; i++) {
            int j = (int) P.get(1, i).n;
            if (j != i) {
                P.swapColumns(i, j);
                M.swapRows(i, j);
                i--;
            }
        }

        if (verbose) {
            String equations, unknowns, parText;
            Point[] refPoint = new Point[1];
            refPoint[0] = new Point(M.getConstTerms().transpose().rowsToVectors()[0], ioHelper);
            if (params == 0) {
                Vector[] noVectors = new Vector[0];
                fake = new Subspace(refPoint, noVectors, ioHelper);
            } else
                fake = new Subspace(refPoint, M.submatrix(1, M.rows, M.cols - params, M.cols - 1).transpose().rowsToVectors(), ioHelper);

            if (M.cols - params - 1 == 1) equations = "jedinou rovnici";
            else equations = "soustavu " + String.valueOf(M.cols - params - 1) + " rovnic";
            if (M.cols - 1 == 1) unknowns = "jedné neznámé";
            else unknowns = String.valueOf(M.cols - 1) + " neznámých";
            if (params == 0) parText = "nebude obsahovat parametr.";
            else {
                parText = "bude obsahovat " + String.valueOf(params) + " parametr";
                switch (params) {
                    case 1:
                        break;
                    case 2:
                    case 3:
                    case 4:
                        parText += "y";
                        break;
                    default:
                        parText += "ů";
                        break;
                }
                parText += ".";
            }

            textOut("Jde o " + equations + " o " + unknowns + ", a řešení tedy " + parText);
            LaTeXOut("Jde o~" + equations + " o~" + unknowns + ", a řešení tedy " + parText);

            if (params == 0) {
                if (M.cols - 1 > varChange) {
                    textOut("\r\nZ poslední rovnice je\r\n\tu_" + String.valueOf(M.cols - 1 - varChange) + " = " + M.get(M.cols - 1, M.cols).toText());
                    LaTeXOut("\n\nZ~poslední rovnice je\n\\[u_{" + String.valueOf(M.cols - 1 - varChange) + "} = " + M.get(M.cols - 1, M.cols).toTeX());
                    for (int i = M.cols - 2; i >= 1 + varChange; i--) {
                        if (i > varChange) {
                            if (i == 1) {
                                textOut("\r\na ");
                                LaTeXOut("\\]a ");
                            } else {
                                textOut(",\r\n");
                                LaTeXOut(",\\]");
                            }
                            textOut("z " + String.valueOf(i) + ". rovnice je\r\n\tu_" + String.valueOf(i - varChange) + " = " + M.get(i, M.cols).toText());
                            LaTeXOut("z~" + String.valueOf(i) + ".~rovnice je\n\\[u_{" + String.valueOf(i - varChange) + "} = " + M.get(i, M.cols).toTeX());
                        } else {
                            if (i == 1) {
                                textOut("\r\na ");
                                LaTeXOut("\\]a ");
                            } else {
                                textOut(",\r\n");
                                LaTeXOut(",\\]");
                            }
                            textOut("z " + String.valueOf(i) + ". rovnice je\r\n\tt_" + String.valueOf(i) + " = " + M.get(i, M.cols).toText());
                            LaTeXOut("z~" + String.valueOf(i) + ".~rovnice je\n\\[t_{" + String.valueOf(i) + "} = " + M.get(i, M.cols).toTeX());
                        }
                    }
                } else {
                    textOut("\r\nZ poslední rovnice je\r\n\tt_" + String.valueOf(M.cols - 1) + " = " + M.get(M.cols - 1, M.cols).toText());
                    LaTeXOut("\n\nZ~poslední rovnice je\n\\[t_{" + String.valueOf(M.cols - 1) + "} = " + M.get(M.cols - 1, M.cols).toTeX());
                    for (int i = M.cols - 2; i >= 1; i--) {

                        if (i == 1) {
                            textOut("\r\na ");
                            LaTeXOut("\\]a ");
                        } else {
                            textOut(",\r\n");
                            LaTeXOut(",\\]");
                        }
                        textOut("z " + String.valueOf(i) + ". rovnice je\r\n\tt_" + String.valueOf(i) + " = " + M.get(i, M.cols).toText());
                        LaTeXOut("z~" + String.valueOf(i) + ".~rovnice je\n\\[t_{" + String.valueOf(i) + "} = " + M.get(i, M.cols).toTeX());

                    }
                }

                /*
                if(M.cols - 1>varChange){
                textOut("\r\nZ poslední rovnice je\r\n\tu_" + String.valueOf(M.cols - 1-varChange) + " = " + M.get(M.cols - 1, M.cols).toText());
                LaTeXOut("\n\nZ~poslední rovnice je\n\\[u_{" + String.valueOf(M.cols - 1-varChange) + "} = " + M.get(M.cols - 1, M.cols).toTeX());
                }else{
                textOut("\r\nZ poslední rovnice je\r\n\tt_" + String.valueOf(M.cols - 1) + " = " + M.get(M.cols - 1, M.cols).toText());
                LaTeXOut("\n\nZ~poslední rovnice je\n\\[t_{" + String.valueOf(M.cols - 1) + "} = " + M.get(M.cols - 1, M.cols).toTeX());
                }
                for (int i = M.cols - 2; i >= 1+varChange; i--) {
                if(i > varChange){
                if (i == 1) {
                textOut("\r\na ");
                LaTeXOut("\\]a ");
                }
                else {
                textOut(",\r\n");
                LaTeXOut(",\\]");
                }
                textOut("z " + String.valueOf(i) + ". rovnice je\r\n\tu_" + String.valueOf(i-varChange) + " = " + M.get(i, M.cols).toText());
                LaTeXOut("z~" + String.valueOf(i) + ".~rovnice je\n\\[u_{" + String.valueOf(i-varChange) + "} = " + M.get(i, M.cols).toTeX());
                }else{
                if (i == 1) {
                textOut("\r\na ");
                LaTeXOut("\\]a ");
                }
                else {
                textOut(",\r\n");
                LaTeXOut(",\\]");
                }
                textOut("z " + String.valueOf(i) + ". rovnice je\r\n\tt_" + String.valueOf(i) + " = " + M.get(i, M.cols).toText());
                LaTeXOut("z~" + String.valueOf(i) + ".~rovnice je\n\\[t_{" + String.valueOf(i) + "} = " + M.get(i, M.cols).toTeX());
                }
                }
                 */
                textOut(".\r\n\r\n");
                LaTeXOut(".\\]\n\n");
            } else {
                String result = fake.toTextParametric(false, 'x', 'p');
                String TeXresult = fake.toTeXParametric(false, 't', 'p');
                String[] individualRows = result.split("\n");
                String[] individualRowsTeX = TeXresult.split("\n");

                textOut("\r\nZvolíme některé neznámé za volné:\r\n");
                LaTeXOut("\n\nZvolíme některé neznámé za volné:\n\\[" + individualRowsTeX[0] + "\n");
                for (int i = POrig.getC(), j = 1; i > POrig.getC() - params; i--, j++) {
                    if (POrig.get(1, i).n > varChange) {
                        //TeXresult = fake.toTeXParametric('u','p');
                        //individualRowsTeX = TeXresult.split("\n");
                        textOut("\tu_" + String.valueOf(POrig.get(1, i).n - varChange) + " = " + individualRows[(int) POrig.get(1, i).n - 1] + "\r\n");
                        //LaTeXOut(individualRowsTeX[(int) POrig.get(1, i).n] + "\n");
                        LaTeXOut("u_" + String.valueOf(POrig.get(1, i).n - varChange) + " = " + individualRowsTeX[(int) POrig.get(1, i).n] + "\n");
                    } else {

                        textOut("\tt_" + String.valueOf(POrig.get(1, i).n) + " = " + individualRows[(int) POrig.get(1, i).n - 1] + "\r\n");
                        //LaTeXOut(individualRowsTeX[(int) POrig.get(1, i).n] + "\n");
                        LaTeXOut("t_" + String.valueOf(POrig.get(1, i).n) + " = " + individualRowsTeX[(int) POrig.get(1, i).n] + "\n");
                    }
                }
                LaTeXOut(individualRowsTeX[individualRowsTeX.length - 1] + "\n\\]\n\n");

                boolean bigP = true;
                Fraction toPrint = new Fraction();

                if (POrig.getC() == varChange) {
                    for (int i = POrig.getC() - params; i >= 1; i--) {

                        textOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ". rovnice a úpravě dostáváme\r\n\tt_"
                                + String.valueOf(POrig.get(1, i).n) + " = " + individualRows[(int) POrig.get(1, i).n - 1]);
                        //LaTeXOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ".~rovnice a úpravě dostáváme\n\\[t_"
                        //    + individualRowsTeX[(int) POrig.get(1, i).n]);
                        LaTeXOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ".~rovnice a úpravě dostáváme\n\\[t_"
                                + String.valueOf(POrig.get(1, i).n) + " = " + individualRowsTeX[(int) POrig.get(1, i).n]);
                        bigP = false;

                        switch (i) {
                            case 1:
                                textOut(".\r\n\r\n");
                                LaTeXOut(".\n\\]\n\n");
                                break;
                            case 2:
                                textOut("\r\na ");
                                LaTeXOut("\n\\]\na ");
                                break;
                            default:
                                textOut(",\r\n");
                                LaTeXOut(",\n\\]");
                                break;
                        }
                    }
                } else {
                    for (int i = POrig.getC() - params; i >= 1 + varChange; i--) {

                        textOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ". rovnice a úpravě dostáváme\r\n\tu_"
                                + String.valueOf(POrig.get(1, i).n - varChange) + " = " + individualRows[(int) POrig.get(1, i).n - 1]);
                        //LaTeXOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ".~rovnice a úpravě dostáváme\n\\["
                        //    + individualRowsTeX[(int) POrig.get(1, i).n]);
                        LaTeXOut((bigP ? "P" : "p") + "o dosazení do " + String.valueOf(i) + ".~rovnice a úpravě dostáváme\n\\[u_"
                                + String.valueOf(POrig.get(1, i).n - varChange) + " = " + individualRowsTeX[(int) POrig.get(1, i).n]);
                        bigP = false;

                        switch (i - varChange) {
                            case 1:
                                textOut(".\r\n\r\n");
                                LaTeXOut(".\n\\]\n\n");
                                break;
                            case 2:
                                textOut("\r\na ");
                                LaTeXOut("\n\\]\na ");
                                break;
                            default:
                                textOut(",\r\n");
                                LaTeXOut(",\n\\]");
                                break;
                        }
                    }
                }
            }
            /*
            textOut("Řešením dané soustavy rovnic je tedy:\r\n" + fake.toTextParametric(true, 't', 'v'));
            LaTeXOut("Řešením dané soustavy rovnic je tedy:\n\\[\n" + fake.toTeXParametric() + "\\]");
             */
        }
        return M;
    }

    /**
     * Computes Gram determinant of this matrix.
     *
     * @param verbose determines, if the method “talks” (creates any text and TeX output)
     * @return Gram determinant of this matrix
     * @label label of the matrix
     */
    Fraction detGram(char label, boolean verbose) {
        // if the matrix is rectangular, GD is just D^2
        if (this.rows == this.cols) {
            if (verbose) {
                textOut("Vidíme, že matice je čtvercová. V takovém případě platí, že G(" + label + ") = det(" + label + ")^2. Vypočítáme tedy det(" + label + ")."
                        + "\r\n\r\nvýpočet det(" + label + ")\r\n--------------\r\n");
                LaTeXOut("Vidíme, že matice je čtvercová. V~takovém případě platí, že $G(" + label + ") = \\det(" + label + ")^2$. Vypočítáme tedy $\\det(" + label + ")$."
                        + "\n\n\\subsection*{výpočet $\\det(" + label + ")$}\n");
            }
            Fraction D = this.det(String.valueOf(label), verbose);
            if (verbose) {
                String squaredText, squaredTeX;
                if (D.d == 1 && D.compare(ZERO) != -1) {
                    squaredText = D.toText();
                    squaredTeX = D.toTeX();
                } else {
                    squaredText = "(" + D.toText() + ")";
                    squaredTeX = "\\left(" + D.toTeX() + "\\right)";
                }
                textOut("Nyní G(" + label + ") = " + squaredText + "^2 = " + D.multiply(D).toText() + ".");
                LaTeXOut("Nyní $G(" + label + ") = " + squaredTeX + "^2 = " + D.multiply(D).toTeX() + ".$");
            }
            return D.multiply(D);
        }

        // otherwise, it's determinant of a matrix of scalar products
        Vector[] vectSequence = this.rowsToVectors();
        Matrix G = new Matrix(this.rows, ioHelper);
        String gramMatrixGeneralText = "";
        String gramMatrixGeneralTeX = "\\begin{pmatrix}\n";
        for (int i = 1; i <= G.rows; i++) {
            if (verbose) gramMatrixGeneralText += "\t(";
            for (int j = 1; j <= G.cols; j++) {
                G.set(vectSequence[i - 1].scalarProduct(vectSequence[j - 1]), i, j);
                if (verbose) {
                    gramMatrixGeneralText += "\t" + "(a_" + i + "*a_" + j + ")";
                    gramMatrixGeneralTeX += "&" + "(\\vec{a}_{" + i + "}\\cdot\\vec{a}_{" + j + "})";
                }
            }
            if (verbose) {
                gramMatrixGeneralText += "\t)\r\n";
                gramMatrixGeneralTeX += "\\\\\n";
            }
        }
        if (verbose) gramMatrixGeneralTeX += "\\end{pmatrix}\n";

        if (verbose) {
            textOut("Vytvoříme nyní Gramovu matici. Jde o matici\r\n" + gramMatrixGeneralText + "kde a_i je vektor zapsaný v i-tém řádku původní matice.\r\n\r\n"
                    + "Po výpočtu jednotlivých skalárních součinů dostáváme Gramovu matici G_" + label + ":\r\n" + G.toText()
                    + "Gramův determinant je pak determinant této matice." + "\r\n\r\nvýpočet det(G_" + label + ")\r\n--------------\r\n");
            LaTeXOut("Vytvoříme nyní Gramovu matici. Jde o~matici\n\\[\n" + gramMatrixGeneralTeX + ",\\]\nkde $\\vec{a}_i$ je vektor zapsaný v~$i$\\discretionary{-}{-}{-}tém řádku "
                    + "původní matice.\n\nPo výpočtu jednotlivých skalárních součinů dostáváme Gramovu matici $G_{" + label + "}$:\n\\[" + G.toTeX()
                    + "\\]\nGramův determinant je pak determinant této matice." + "\n\n\\subsection*{výpočet $\\det(G_{" + label + "})$}\n");
        }

        return G.det("G", verbose);
    }

    /**
     * Computes the Gramian matrix. This matrix is in fact never used in computations (see {@link gramianMatrixProduct(Matrix B)}) but is output as a text.
     * The input format is the same as in {@link gramianMatrixProduct(Matrix B)}.)
     *
     * @param B the other matrix
     * @return the Gramian matrix of two sequences of vectors, ie. the matrix of their mutual scalar products
     * @throws UnsupportedOperationException Throws an exception, if the matrices have different number of columns, ie. the dimensions of vectors are not equal.
     */
    SquareRootMatrix gramianMatrixForOutput(Matrix B) {
        if (this.cols != B.cols) throw new UnsupportedOperationException("The vectors do not have the same dimension!");

        Vector[] firstBase = this.rowsToVectors();
        Vector[] secondBase = B.rowsToVectors();

        SquareRootMatrix R = new SquareRootMatrix(this.rows, B.rows);

        for (int i = 1; i <= R.getR(); i++) {
            for (int j = 1; j <= R.getC(); j++) {
                SquareRootFraction S = firstBase[i - 1].scalarProduct(secondBase[j - 1]).convertToSRF();
                S = S.divide(firstBase[i - 1].length());
                S = S.divide(secondBase[j - 1].length());
                R.set(S, i, j);
            }
        }

        return R;
    }

    /**
     * Computes product of Gramian matrix with its transposed self, ie. A*A<sup>T</sup>. What is <b>very important</b> is that the inputs are <b>not</b> the matrices themselves!
     * Instead, both matrices are constituted from the vectors that would constitute the Gramian matrix. The vectors will be in rows. Ie., if we have two subspaces P, Q with
     * difference spaces, say, L(a,b), or L(u,v,w), respectively, the elements of the first matrix won't be scalar products au, av, ..., bw, but rather coefficients
     * a<sub>1</sub>, a<sub>2</sub>, ..., a<sub>n</sub>, b<sub>1</sub>, b<sub>2</sub>, ..., b<sub>n</sub>. Similarly with the other matrix. This is for practical reasons.
     * Even more important is that the vectors <b>must not be normalized</b> (that will be taken care of in this method).
     *
     * @param B the other matrix
     * @return matrix A*A<sup>T</sup>, where A is Gramian matrix of the given two sequences of vectors
     * @throws UnsupportedOperationException Throws an exception, if the matrices have different number of columns, ie. the dimensions of vectors are not equal.
     * @see gramianMatrixForOutput(Matrix B)
     */
    SquareRootMatrix gramianMatrixProduct(Matrix B) {
        if (this.cols != B.cols) throw new UnsupportedOperationException("The vectors do not have the same dimension!");

        Vector[] firstBase = this.rowsToVectors();
        Vector[] secondBase = B.rowsToVectors();

        SquareRootMatrix R = new SquareRootMatrix(this.rows, this.rows);

        /* We fill upper triangular matrix of the result with appropriate data.
         *
         * The first matrix is: (a1b1 a1b2 ... a1bn)    The second matrix is: (a1b1 a2b1 ... amb1)      Note: There should really be normalized vectors!!! (Below we take this
         *                      (a2b1 a2b2 ... a2bn)                          (a1b2 a2b2 ... amb2)            into account.)
         *                      ( ..   ..   .   .. )                          ( ..   ..   .   .. )
         *                      (amb1 amb2 ... ambn)                          (a1bn a2bn ... ambn)
         *
         * Thence, the element on i-th row and in the j-th column of the result of the product is:
         * R(i,j) = SUM(k=1..n) (Ai,Bk)*(Aj,Bk), where in the parenthesis, there are scalar products and * is product of two numbers;
         *                                     and A, B are _normalized_ vectors a, b.
         * That mean we can write this in form:
         * R(i,j) = SUM(k=1..n) (ai,bk)/(|ai|*|bk|)*(aj,bk)/(|aj|*|bk|), because norms are only scalars and scalars can be factored out from scalar products.
         * Because we sum through k and not through i or j, we can factor 1/|ai| and 1/|aj| out to obtain:
         * R(i,j) = 1/(|ai|*|aj|) * SUM(k=1..n) (1/|bk|^2)*(ai,bk)*(aj,bk)
         *
         * Notice that both scalar products (ai,bk), (aj,bk) are scalar products of _non-normalized_ vectors, ie. vectors whose coefficients are known to be rational.
         * Therefore both scalar products are rational numbers (positions [1] and [2]). Also, even though |bk| may be a square roots of fraction (SRF), in the sum,
         * it is always squared, thus is a rational number also (position [3]). Therefore (using that product of rational numbers is a rational number), in the sum,
         * we _only sum rational numbers_! That is very important, as we can't add up SRFs because (SRF,+) is not a groupoid: eg. sqrt(2) + sqrt(3) is not an SRF.
         * Only now do we divide by numbers that are generally SRFs (positions [4] and [5]).
         *
         * By that, we not only proved that this method yields correct results, but most notably also that all the elements indeed are SRFs, and so we can use SquareRootMatrix.
         */
        for (int i = 1; i <= R.getR(); i++) {
            for (int j = i; j <= R.getC(); j++) {
                Fraction F = new Fraction(0);
                for (int k = 1; k <= B.rows; k++) {
                    Fraction f = new Fraction(0);
                    f = f.add(firstBase[i - 1].scalarProduct(secondBase[k - 1]));       // [1]
                    f = f.multiply(firstBase[j - 1].scalarProduct(secondBase[k - 1]));  // [2]
                    f = f.divide(secondBase[k - 1].length().square());               // [3]
                    F = F.add(f);
                }
                SquareRootFraction S = F.convertToSRF(); // We convert the fraction into an SRF.
                S = S.divide(firstBase[i - 1].length());   // [4]
                S = S.divide(firstBase[j - 1].length());   // [5]
                R.set(S, i, j);
            }
        }

        /* Now, we only fill the remaining elements of the matrix (remeber we have only filled the upper triangular matrix so far). We use that A*AT is always rectangular and
         * symmetric. By this, instead of n^2 computations, we only do n*(n-1)/2 computations.
         */
        for (int i = 2; i <= R.getR(); i++) {
            for (int j = 1; j < i; j++) {
                R.set(R.get(j, i), i, j);
            }
        }

        return R;
    }

    /**
     * Orthogonalizes sequence of vectors using the Gram-Schmidt orthogonalization process. The process is carried upon a matrix of vectors (in rows).
     *
     * @return matrix of vectors (in rows) that constitute an orthogonal sequence
     */
    Matrix orthogonalizeWithGramSchmidt(char label, boolean verbose) {
        if (this.rank(false) == 0) return new Matrix(ioHelper); // returns a null matrix if all given vectors are zero

        Vector[] vectors = this.rowsToVectors();           // original sequence
        Vector[] orthVectors = new Vector[vectors.length]; // its orthogonalized variant
        Fraction[] p = new Fraction[vectors.length - 1];     // coefficients of vectors

        if (verbose) {
            String vectorsText = "";
            String vectorsTeX = "\\begin{align*}\n";
            for (int i = 1; i <= vectors.length; i++) {
                vectorsText += "\t" + "v_" + label + "," + i + " = " + vectors[i - 1].toText() + "\r\n";
                vectorsTeX += "\\vec{v}_{\\mathcal{" + label + "}," + i + "} &= " + vectors[i - 1].toTeX() + (i == vectors.length ? "\n\\end{align*}\n" : "\\\\\n");
            }
            textOut("\r\nnalezení ortogonální posloupnosti vektorů pomocí Gramova-Schmidtova ortogonalizačního procesu"
                    + "\r\n---------------------------------------------------------------------------------------------\r\n"
                    + "Máme posloupnost vektorů:\r\n" + vectorsText + "Tuto posloupnost chceme ortogonalizovat."
                    + (vectors.length == 1 ? " Posloupnost jediného vektoru však je vždy ortogonální." : "\r\n\r\n"));
            LaTeXOut("\n\\subsection*{nalezení ortogonální posloupnosti vektorů pomocí Gramova\\discretionary{-}{-}{-}Schmidtova ortogonalizačního procesu}\n"
                    + "Máme posloupnost vektorů:\n" + vectorsTeX + "Tuto posloupnost chceme ortogonalizovat."
                    + (vectors.length == 1 ? " Posloupnost jediného vektoru však je vždy ortogonální." : "\n\n"));
            if (vectors.length == 1) verbose = false;
        }

        // the first vector will be the first non-zero vector in the sequence
        int i = 0;
        do {
            orthVectors[0] = vectors[i];
            i++;
        } while (orthVectors[0].isZeroVector());
        if (verbose) {
            textOut("První ortogonalizovaný vektor bude první nenulový vektor v posloupnosti:\r\n\tw_" + label + ",1 = v_" + label + "," + i + " = " + orthVectors[0].toText()
                    + ".\r\n\r\n");
            LaTeXOut("První ortogonalizovaný vektor bude první nenulový vektor v~posloupnosti:\n\\[\\vec{w}_{\\mathcal{" + label + "},1} = \\vec{v}_{\\mathcal{" + label + "},"
                    + i + "} = " + orthVectors[0].toTeX() + ".\\]\n\n");
        }

        int j = 1;
        for (; i < vectors.length; i++) { // i indexes vectors, j indexes orthVectors
            if (!(vectors[i].isZeroVector())) {
                String formulaText = "w_" + label + "," + String.valueOf(i + 1) + " = v_" + label + "," + String.valueOf(i + 1);
                String formulaTeX = "\\vec{w}_{\\mathcal{" + label + "}," + String.valueOf(i + 1) + "} = \\vec{v}_{\\mathcal{" + label + "}," + String.valueOf(i + 1) + "}";
                String coeffText = "";
                String coeffTeX = "\\begin{align*}\n";
                String formulaWithValuesText = "\tw_" + label + "," + String.valueOf(i + 1) + " = " + vectors[i].toText();
                String formulaWithValuesTeX = "\\begin{align*}\\vec{w}_{\\mathcal{" + label + "}," + String.valueOf(i + 1) + "} &= " + vectors[i].toTeX();
                for (int k = 0; k < j; k++) {
                    formulaText += " + p_" + String.valueOf(i + 1) + "," + String.valueOf(k + 1) + "*w_" + label + "," + String.valueOf(k + 1);
                    formulaTeX += " + p_{" + String.valueOf(i + 1) + "," + String.valueOf(k + 1) + "}\\vec{w}_{\\mathcal{" + label + "}," + String.valueOf(k + 1) + "}";
                    if (orthVectors[k].isZeroVector()) {
                        p[k] = ONE;
                        coeffText += "\tp_" + String.valueOf(i + 1) + "," + String.valueOf(k + 1) + " = 1\r\n";
                        coeffTeX += "p_" + String.valueOf(i + 1) + "," + String.valueOf(k + 1) + " &= 1" + (k == j - 1 ? "\n\\end{align*}\n" : " \\\\\n");
                    } else {
                        p[k] = NEG_ONE.multiply(vectors[i].scalarProduct(orthVectors[k])).divide(orthVectors[k].scalarProduct(orthVectors[k]));
                        coeffText += "\tp_" + String.valueOf(i + 1) + "," + String.valueOf(k + 1) + "\t= -(v_" + label + "," + String.valueOf(i + 1) + "*w_" + label
                                + "," + String.valueOf(k + 1) + ")/(w_" + label + "," + String.valueOf(k + 1) + "*w_" + label + "," + String.valueOf(k + 1) + ") =\r\n"
                                + "\t\t= -(" + vectors[i].toText() + "*" + orthVectors[k].toText() + ")/(" + orthVectors[k].toText() + "*" + orthVectors[k].toText() + ") =\r\n"
                                + "\t\t= -(" + vectors[i].scalarProduct(orthVectors[k]).toText() + ")/(" + orthVectors[k].scalarProduct(orthVectors[k]).toText() + ") = "
                                + p[k].toText() + "\r\n";
                        coeffTeX += "p_" + String.valueOf(i + 1) + "," + String.valueOf(k + 1) + " &= -\\frac{\\vec{v}_{\\mathcal{" + label + "}," + String.valueOf(i + 1) + "}"
                                + "\\cdot\\vec{w}_{\\mathcal{" + label + "}," + String.valueOf(k + 1) + "}}{\\vec{w}_{\\mathcal{" + label + "}," + String.valueOf(k + 1)
                                + "}\\cdot\\vec{w}_{\\mathcal{" + label + "}," + String.valueOf(k + 1) + "}} =\\\\\n"
                                + "&= -\\frac{" + vectors[i].toTeX() + "\\cdot" + orthVectors[k].toTeX() + "}{" + orthVectors[k].toTeX() + "\\cdot" + orthVectors[k].toTeX() + "} =\\\\\n"
                                + "&= -\\frac{" + vectors[i].scalarProduct(orthVectors[k]).toTeX() + "}{" + orthVectors[k].scalarProduct(orthVectors[k]).toTeX() + "} = "
                                + p[k].toTeX() + (k == j - 1 ? "\n\\end{align*}\n" : " \\\\\n");
                    }
                    formulaWithValuesText += " + " + (p[k].compare(ZERO) == -1 ? "(" + p[k].toText() + ")" : p[k].toText()) + "*" + orthVectors[k].toText();
                    formulaWithValuesTeX += " + " + (p[k].compare(ZERO) == -1 ? "\\left(" + p[k].toTeX() + "\\right)" : p[k].toTeX()) + "\\cdot" + orthVectors[k].toTeX();
                }
                formulaWithValuesText += "\r\n\t      = " + vectors[i].toText();
                ;
                formulaWithValuesTeX += "\\\\\n &= " + vectors[i].toTeX();
                orthVectors[j] = vectors[i];
                for (int k = 0; k < j; k++) {
                    Vector temp = orthVectors[k].multiply(p[k]);
                    formulaWithValuesText += " + " + temp.toText();
                    formulaWithValuesTeX += " + " + temp.toTeX();
                    orthVectors[j] = orthVectors[j].add(temp);
                }
                formulaWithValuesText += "\r\n\t      = " + orthVectors[j].toText();
                formulaWithValuesTeX += "\\\\\n & = " + orthVectors[j].toTeX() + "\\end{align*}";
                if (verbose) {
                    textOut("Další ortogonalizovaný vektor zkostruujeme pomocí dřívějších ortogonalizovaných vektorů podle vzorce:\r\n\t" + formulaText + ",\r\n"
                            + "kde p_i,j jsou koeficienty, pro které platí:\r\n\t" + "p_i,j = 1\t\t\t\tpokud w_" + label + ",j = o,\r\n\t"
                            + "p_i,j = -(v_" + label + ",i*w_" + label + ",j)/(w_" + label + ",j*w_" + label + ",j)\tpokud w_" + label + ",j ≠ o.\r\n\r\n"
                            + "Konkrétně je tedy:\r\n" + coeffText + "odkud dosadíme do výše zmíněného vzorce:\r\n\t" + formulaWithValuesText + ".\r\n\r\n");
                    LaTeXOut("Další ortogonalizovaný vektor zkostruujeme pomocí dřívějších ortogonalizovaných vektorů podle vzorce:\n\\[" + formulaTeX + ",\\]\n"
                            + "kde $p_{i,j}$ jsou koeficienty, pro které platí:\n\\[p_i,j = \\begin{cases}\n"
                            + "1 & \\text{pokud }\\vec{w}_{\\mathcal{" + label + "},j} = \\vec{o},\\\\\n"
                            + "-\\dfrac{\\vec{v}_{\\mathcal{" + label + "},i}\\cdot \\vec{w}_{\\mathcal{" + label + "},j}}{\\vec{w}_{\\mathcal{" + label
                            + "},j}\\cdot \\vec{w}_{\\mathcal{" + label + "},j}} & \\text{pokud }\\vec{w}_{\\mathcal{" + label + "},j} \\neq \\vec{o}.\n\\end{cases}\\]\n"
                            + "Konkrétně je tedy:\r\n" + coeffTeX + "odkud dosadíme do výše zmíněného vzorce:\n" + formulaWithValuesTeX + ".\n\n");
                }
                j++;
            }
        }

        Fraction[] coeffs = new Fraction[this.cols * j];

        for (i = 0; i < coeffs.length; i++) coeffs[i] = orthVectors[i / this.cols].get((i % this.cols) + 1);

        Matrix R = new Matrix(coeffs, j, this.cols, ioHelper);
        R.deleteZeroRows();

        if (verbose) {
            vectors = R.rowsToVectors();
            String vectorsText = "";
            String vectorsTeX = "\\begin{align*}\n";
            for (i = 1; i <= j; i++) {
                String resultText = "(";
                String resultTeX = "\\left(";
                for (int k = 1; k <= R.getC(); k++) {
                    SquareRootFraction result = vectors[i - 1].get(k).convertToSRF();
                    result = result.divide(vectors[i - 1].length());
                    resultText += (k == 1 ? "" : ", ") + result.toText();
                    resultTeX += (k == 1 ? "" : ", ") + result.toTeX();
                }
                resultText += ")";
                resultTeX += "\\right)";
                vectorsText += "\t" + "e_" + label + "," + i + " = (1//||w_" + label + "," + i + "||)*w_" + label + "," + i + " = (1/" + vectors[i - 1].length().toText()
                        + ")*" + vectors[i - 1].toText() + " = " + resultText + "\r\n";
                vectorsTeX += "\\vec{e}_{\\mathcal{" + label + "}," + i + "} &= \\frac{1}{\\|\\vec{w}_{\\mathcal{" + label + "}," + i + "}\\|}"
                        + "\\vec{w}_{\\mathcal{" + label + "}," + i + "} = \\frac{1}{" + vectors[i - 1].length().toTeX()
                        + "}" + vectors[i - 1].toTeX() + " = " + resultTeX + (i == j ? "\n\\end{align*}\n" : "\\\\\n");
            }
            textOut("\r\nnormalizace posloupnosti" + "\r\n------------------------\r\n"
                    + "Vybereme nyní z posloupnosti pouze nenulové vektory a znormujeme je, tzn. každý vydělíme jeho velikostí. Dostaneme ortonormální bázi:\r\n"
                    + vectorsText);
            LaTeXOut("\n\\subsection*{normalizace posloupnosti}\n"
                    + "Vybereme nyní z~posloupnosti pouze nenulové vektory a znormujeme je, tzn.~každý vydělíme jeho velikostí. Dostaneme ortonormální bázi:\r\n"
                    + vectorsTeX);
        }

        return R;
    }

    /**
     * Generate array of lineary independent vectors
     *
     * @param vectorNum number of vectors to generate
     * @param dim       dimension of superspcae of vectors
     * @return array of lineary independent vectors
     */
    Vector[] generateRandomLinearyIndependentVectors(int vectorNum, int dim) {
        Matrix A = new Matrix(ioHelper);
        A.copy(this);
        int originalRank = A.rank(false);
        Vector[] vectorField = new Vector[vectorNum];

        Matrix B = new Matrix(ioHelper);
        for (int i = 0; i < vectorNum; i++) {
            Vector a = new Vector(dim, ioHelper);
            while (B.rank(false) != originalRank + i + 1) {
                B.copy(A);
                a = new Vector(dim, ioHelper);
                while (a.isZeroVector()) {
                    int[] coeffs = MathUtilities.randNTuple(-20, 20, dim);
                    Fraction[] fracCoeffs = new Fraction[dim];
                    for (int j = 0; j < dim; j++) fracCoeffs[j] = new Fraction(coeffs[j]);
                    a = new Vector(fracCoeffs, dim, ioHelper);
                }
                B.mergeFromBottom(a.toMatrixRow());
            }
            A.mergeFromBottom(a.toMatrixRow());
            vectorField[i] = a;
        }

        return vectorField;
    }

    /**
     * Prints this matrix formatted as text.
     *
     * @return plain text description of this matrix; the rows will begin with <code>(</code> and end with <code>)</code>.
     */
    String toText() {
        String S = "";

        for (int i = 1; i <= this.rows; i++) {
            S += "\t(";
            for (int j = 1; j <= this.cols; j++) {
                S += "\t" + this.get(i, j).toText() + " ";
            }
            S += "\t)\r\n";
        }

        return S;
    }

    /**
     * Prints this matrix formatted as TeX code.
     *
     * @return TeX code for this matrix
     */
    String toTeX() {
        String S = "";

        S += "\\left(" + "\\begin{array}{*{" + String.valueOf(this.cols) + "}r}";

        for (int i = 1; i <= this.rows; i++) {
            for (int j = 1; j <= this.cols; j++) {
                if (j != this.cols)
                    S += this.get(i, j).toTeX() + " & ";
                else
                    S += this.get(i, j).toTeX();
            }
            if (i != this.rows)
                S += "\\\\";
        }

        S += "\\end{array}" + "\\right)";

        return S;
    }

    /**
     * Imports {@link IOHelper#textOut(String)} textOut(str)}.
     *
     * @see {@link IOHelper#textOut(String) textOut(str)}
     */
    private void textOut(String str) {
        ioHelper.textOut(str);
    }

    /**
     * Imports {@link IOHelper#LaTeXOut(String)}  LaTeXOut(str)}.
     *
     * @see {@link IOHelper#LaTeXOut(String) LaTeXOut(str)}
     */
    private void LaTeXOut(String str) {
        ioHelper.LaTeXOut(str);
    }
}
