package com.janseris.bp;

/**
 * The input reader gets the subspace representation from the string entered by user
 *
 * @author Jakub Klejch <323464@mail.muni.cz>
 * @version 1.0
 * @since 2014-12-01
 */

public final class InputReader {
    private IOHelper ioHelper;
    public InputReader(IOHelper ioHelper) {
        this.ioHelper = ioHelper;
    }

    /**
     * Determine the method for transforming String into Subspace and transform it.
     * @param input String input
     * @return Subspace
     */
    public Subspace getSubspace(String input) {
        input = input.replaceAll(" ", "");
        input = input.replaceAll("\n\n", "\n");
        if (input.contains("t")) return getParametricSubspace(input);
        if (input.contains("x")) return getGeneralSubspace(input);
        if (input.contains("(") || input.contains("[")) return getVecPointSubspace(input);
        if (input.contains(",")) return getMatrixSubspace(input);
        return new Subspace(ioHelper);
    }

    /**
     * Transform parametric String input into subspace.
     *
     * @param in String input
     * @return Subspace
     */
    private Subspace getParametricSubspace(String in) {

        String[] equationField = in.split("\n");
        int dim = equationField.length;
        int vectorNum = 0;
        for (int i = 0; i < dim; i++) {
            String[] tempField = equationField[i].split("=");
            equationField[i] = tempField[tempField.length - 1];
        }
        for (int i = 0; i < dim; i++) {
            if (equationField[i].contains("t")) {
                String[] cut = equationField[i].split("_");
                if (Integer.parseInt(cut[cut.length - 1]) > vectorNum)
                    vectorNum = Integer.parseInt(cut[cut.length - 1]);
            }
        }
        Vector[] vectorField = new Vector[vectorNum];
        String[] restField = new String[dim];
        Fraction[] point = new Fraction[dim];
        for (int i = 0; i < dim; i++) {
            if (equationField[i].contains("t")) {
                String[] cut = equationField[i].split("t", 2);
                String[] cutcut = new String[2];
                if (cut[0].lastIndexOf("+") >= cut[0].lastIndexOf("-")) {
                    if (cut[0].endsWith("+")) cut[0] = cut[0] + " ";
                    cutcut = cut[0].split("\\+");
                    if (cutcut.length == 2)
                        cutcut[1] = "+" + cutcut[1];
                    else cutcut[0] = "+" + cutcut[0];
                } else {
                    if (cut[0].startsWith("-")) {
                        cut[0] = cut[0] + " ";
                        cut[0] = cut[0].substring(1);
                        cutcut = cut[0].split("-");
                        if (cutcut.length == 2) {
                            cutcut[0] = "-" + cutcut[0];
                            cutcut[1] = "-" + cutcut[1];
                        } else {
                            cutcut[0] = "-" + cutcut[0];
                        }
                    } else {
                        cut[0] = cut[0] + " ";
                        cutcut = cut[0].split("-");
                        cutcut[1] = "-" + cutcut[1];
                    }
                }
                if (equationField[i].endsWith("t_1")) {
                    if (cutcut.length == 2) restField[i] = cutcut[1] + "t_1";
                    else restField[i] = cutcut[0] + "t_1";
                } else {
                    if (cutcut.length == 2) restField[i] = cutcut[1] + "t" + cut[1];
                    else restField[i] = cutcut[0] + "t" + cut[1];
                }
                if (cutcut.length == 2) point[i] = stringToFraction(cutcut[0]);
                else point[i] = stringToFraction("0");
            } else {
                restField[i] = "";
                point[i] = stringToFraction(equationField[i]);
            }
        }

        for (int i = 0; i < vectorNum; i++) {
            Fraction[] fractionField = new Fraction[dim];
            for (int j = 0; j < dim; j++) {
                String t = "t_" + String.valueOf(i + 1);
                if (restField[j].contains(t)) {
                    String[] cut = restField[j].split(t);
                    if (!restField[j].endsWith(t)) restField[j] = cut[1];
                    fractionField[j] = stringToFraction(cut[0]);
                } else {
                    fractionField[j] = stringToFraction("0");
                }
            }
            vectorField[i] = new Vector(fractionField, dim, ioHelper);
        }

        Point[] pointField = new Point[1];
        pointField[0] = new Point(point, dim, ioHelper);
        Subspace sub = new Subspace(pointField, vectorField, ioHelper);

        return sub;
    }

    /**
     * Transform general String input into subspace.
     *
     * @param in String input
     * @return Subspace
     */
    private Subspace getGeneralSubspace(String in) {
        String[] equationField = in.split("\n");
        int equationNum = equationField.length;
        int dim = 0;
        for (String s : equationField) {
            String cut[] = s.split("_");
            int dimI = 0;
            if (cut[cut.length - 1].contains("+")) {
                String cutcut[] = cut[cut.length - 1].split("\\+");
                dimI = Integer.parseInt(cutcut[0]) + 1;
            } else {
                if (cut[cut.length - 1].contains("-")) {
                    String cutcut[] = cut[cut.length - 1].split("-");
                    dimI = Integer.parseInt(cutcut[0]) + 1;
                } else {
                    String cutcut[] = cut[cut.length - 1].split("=");
                    dimI = Integer.parseInt(cutcut[0]) + 1;
                }
            }
            if (dimI > dim)
                dim = dimI;
        }

        Fraction[] outputField = new Fraction[dim * equationField.length];
        for (int i = 0; i < equationNum; i++) {
            String rest = equationField[i];
            if (!rest.startsWith("-")) rest = "+" + rest;
            for (int a = 0; a < (dim - 1); a++) {
                String x = "x_" + String.valueOf(a + 1);
                if (rest.contains(x)) {
                    rest = " " + rest;
                    String[] cut = rest.split(x);
                    rest = cut[1];
                    outputField[i * dim + a] = stringToFraction(cut[0]);
                } else {
                    outputField[i * dim + a] = stringToFraction("0");
                }
            }
            String[] cut = rest.split("=");
            if (cut[0].isEmpty()) outputField[i * dim + dim - 1] = stringToFraction("0");
            else outputField[i * dim + dim - 1] = stringToFraction(cut[0]);
        }
        Matrix mat = new Matrix(outputField, equationField.length, dim, ioHelper);
        Subspace sub = new Subspace(mat, ioHelper);
        Matrix m = sub.getAugMatrix();

        sub.getAugMatrix().solve();

        return sub;
    }

    /**
     * Transform matrix String input into subspace.
     * Only rectangle-shaped matrices are accepted
     * @param in String input
     * @return Subspace
     */
    private Subspace getMatrixSubspace(String in) {
        String[] splitPoints = in.split("\n");
        int a = splitPoints[0].split(",").length;
        Fraction[] fracField = new Fraction[a * splitPoints.length];
        for (int i = 0; i < splitPoints.length; i++) {
            String[] stringField = splitPoints[i].split(",");
            for (int j = 0; j < stringField.length; j++) {
                fracField[stringField.length * i + j] = stringToFraction(stringField[j]);
            }
        }
        Matrix mat = new Matrix(fracField, splitPoints.length, a, ioHelper);
        Subspace sub = new Subspace(mat, ioHelper);

        sub.getAugMatrix().solve();

        return sub;
    }

    /**
     * Transform vectors and points String input into subspace.
     *
     * @param in String input
     * @return Subspace
     */
    private Subspace getVecPointSubspace(String in) {
        String[] splitRows = in.split("\n");
        int dim = splitRows[0].split(",").length;
        int pointNum = 0, vecNum = 0, num = 0;
        for (String splitRow : splitRows) {
            if (splitRow.contains("(")) vecNum++;
            else pointNum++;
        }
        Vector[] vectorField = new Vector[vecNum];
        Point[] pointField = new Point[pointNum];
        for (int i = 0; i < splitRows.length; i++) {
            if (splitRows[i].contains("(")) {
                Fraction[] fracField = new Fraction[dim];
                splitRows[i] = splitRows[i].replace("(", "");
                splitRows[i] = splitRows[i].replace(")", "");
                String[] stringField = splitRows[i].split(",");
                for (int j = 0; j < stringField.length; j++) {
                    fracField[j] = stringToFraction(stringField[j]);
                }
                vectorField[num] = new Vector(fracField, dim, ioHelper);
                num++;
            }
        }
        num = 0;
        for (int i = 0; i < splitRows.length; i++) {
            if (splitRows[i].contains("[")) {
                Fraction[] fracField = new Fraction[dim];
                splitRows[i] = splitRows[i].replace("[", "");
                splitRows[i] = splitRows[i].replace("]", "");
                String[] stringField = splitRows[i].split(",");
                for (int j = 0; j < stringField.length; j++) {
                    fracField[j] = stringToFraction(stringField[j]);
                }
                pointField[num] = new Point(fracField, dim, ioHelper);
                num++;
            }
        }
        Subspace sub = new Subspace(pointField, vectorField, ioHelper);
        return sub;
    }

    /**
     * Transform fraction String input into Fraction.
     *
     * @param in String input
     * @return Fraction
     */
    private static Fraction stringToFraction(String in) {
        Fraction f = new Fraction();
        if (in.replaceAll("\\s", "").equals("+")) return new Fraction(1);
        if (in.replaceAll("\\s", "").equals("-")) return new Fraction(-1);
        if (in.contains("/")) {
            String[] frac = in.split("/");
            Long num = Long.parseLong(frac[0].replaceAll("\\s", ""));
            Long denom = Long.parseLong(frac[1].replaceAll("\\s", ""));
            f = new Fraction(num, denom);
        } else {
            Long integer = Long.parseLong(in.replaceAll("\\s", ""));
            f = new Fraction(integer);
        }
        return f;
    }

}
