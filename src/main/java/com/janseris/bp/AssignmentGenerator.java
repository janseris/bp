package com.janseris.bp;


import com.janseris.bp.model.GenerateSubspacesResultDTO;

/**
 * A class used to generate assignments and validate generation input
 * Each method is used for different "generate assignment" type
 * Service-like
 * @author Jan Seris <456435@mail.muni.cz>
 * @version 1.0
 * @since 2020-05-12
 */

public final class AssignmentGenerator {

    private IOHelper ioHelper;
    private InputReader inputReader;

    public AssignmentGenerator(IOHelper ioHelper, InputReader inputReader){
        this.ioHelper = ioHelper;
        this.inputReader = inputReader;
    }

    public GenerateSubspacesResultDTO validateAndGenerateRandomSubspaces(boolean firstParam, boolean secondParam, boolean generateFirst, boolean generateSecond,
                                                                         int dim1, int dim2, int wholeSpaceDimension){
        String generatedFirstSubspaceRepresentation = new String();
        String generatedSecondSubspaceRepresentation = new String();

        Subspace first;
        Subspace second;

        if (generateFirst) {
            if (firstParam) {
                first = Subspace.generateRandomParametricSubspace(dim1, wholeSpaceDimension, ioHelper);
                first.beautifyParametric();
                generatedFirstSubspaceRepresentation = first.toTextParametric(false);
            } else {
                first = Subspace.generateRandomGeneralSubspace(dim1, wholeSpaceDimension, ioHelper);
                first.beautifyGeneral();
                generatedFirstSubspaceRepresentation = first.toTextGeneralOut(false, 'x');
            }
        }
        if (generateSecond) {
            if (secondParam) {
                second = Subspace.generateRandomParametricSubspace(dim2, wholeSpaceDimension, ioHelper);
                second.beautifyParametric();
                generatedSecondSubspaceRepresentation = second.toTextParametric(false);
            } else {
                second = Subspace.generateRandomGeneralSubspace(dim2, wholeSpaceDimension, ioHelper);
                second.beautifyGeneral();
                generatedSecondSubspaceRepresentation = second.toTextGeneralOut(false, 'x');
            }
        }

        return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation);
    }

    public GenerateSubspacesResultDTO validateAndGenerateSubspacesWithGivenDistance(boolean firstParam, boolean secondParam, boolean generateFirst, boolean generateSecond,
                                                                                    int dim1, int dim2, int wholeSpaceDimension, String distanceTextfieldContents){
        String generatedFirstSubspaceRepresentation = new String(); //string which to be later inserted into input text field 1
        String generatedSecondSubspaceRepresentation = new String(); //string which to be later inserted into input text field 2

        Subspace first = new Subspace(ioHelper);
        Subspace second = new Subspace(ioHelper);
        Subspace[] firstAndSecond;

        //both subspaces are required
        if (!generateFirst || !generateSecond) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 0);
        }

        if (dim1 == wholeSpaceDimension || dim2 == wholeSpaceDimension) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 1);
        }

        Fraction distance = new Fraction(0);
        try {
            String dist = distanceTextfieldContents;
            if (dist.contains("/")) {
                String[] fractionParts = dist.split("/");
                distance = new Fraction(Integer.parseInt(fractionParts[0]), Integer.parseInt(fractionParts[1]));
            } else distance = new Fraction(Integer.parseInt(dist));
            if (distance.compare(0) == -1) throw new NumberFormatException(); //if fraction value is less than 0
        } catch (NumberFormatException ex) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 2);
        }

        firstAndSecond = Subspace.generateRandomSubspacesWithGivenDistance(dim1, dim2, wholeSpaceDimension, distance, ioHelper);
        first.copy(firstAndSecond[0]);
        second.copy(firstAndSecond[1]);

        if (firstParam) {
            first.checkParametricForm();
            first.beautifyParametric();
            generatedFirstSubspaceRepresentation = first.toTextParametric(false);
        } else {
            first.beautifyGeneral();
            generatedFirstSubspaceRepresentation = first.toTextGeneralOut(false, 'x');
        }

        if (secondParam) {
            second.checkParametricForm();
            second.beautifyParametric();
            generatedSecondSubspaceRepresentation = second.toTextParametric(false);
        } else {
            second.beautifyGeneral();
            generatedSecondSubspaceRepresentation = second.toTextGeneralOut(false, 'x');
        }

        return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation);
    }

    public GenerateSubspacesResultDTO validateAndGenerateSubspacesWithGivenAngle(boolean firstParam, boolean secondParam, boolean generateFirst, boolean generateSecond,
                                                                                 int dim1, int dim2, int wholeSpaceDimension, String angleComboboxSelectedItemContents){
        String generatedFirstSubspaceRepresentation = new String(); //string which to be later inserted into input text field 1
        String generatedSecondSubspaceRepresentation = new String(); //string which to be later inserted into input text field 2

        Subspace first = new Subspace(ioHelper);
        Subspace second = new Subspace(ioHelper);
        Subspace[] firstAndSecond;

        //both subspaces are required
        if (!generateFirst || !generateSecond) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 0);
        }


        if (dim1 == 0 || dim2 == 0 || dim1 == wholeSpaceDimension || dim2 == wholeSpaceDimension) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 3);
        }

        PiFraction angle = new PiFraction(0);
        if (angleComboboxSelectedItemContents.contains("30°")) {
            if (wholeSpaceDimension < 3) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 4);
            }
            angle = new PiFraction(1, 6);
        } else if (angleComboboxSelectedItemContents.contains("60°")) {
            if (wholeSpaceDimension < 3) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 4);
            }
            angle = new PiFraction(1, 3);
        } else if (angleComboboxSelectedItemContents.contains("45°")) angle = new PiFraction(1, 4);
        else if (angleComboboxSelectedItemContents.contains("90°")) angle = new PiFraction(1, 2);

        firstAndSecond = Subspace.generateRandomSubspacesWithGivenAngle(dim1, dim2, wholeSpaceDimension, angle, ioHelper);
        first.copy(firstAndSecond[0]);
        second.copy(firstAndSecond[1]);

        if (firstParam) {
            first.checkParametricForm();
            first.beautifyParametric();
            generatedFirstSubspaceRepresentation = first.toTextParametric(false);
        } else {
            first.checkGeneralForm();
            first.beautifyGeneral();
            generatedFirstSubspaceRepresentation = first.toTextGeneralOut(false, 'x');
        }

        if (secondParam) {
            second.checkParametricForm();
            second.beautifyParametric();
            generatedSecondSubspaceRepresentation = second.toTextParametric(false);
        } else {
            second.checkGeneralForm();
            second.beautifyGeneral();
            generatedSecondSubspaceRepresentation = second.toTextGeneralOut(false, 'x');
        }

        return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation);
    }

    public GenerateSubspacesResultDTO validateAndGenerateSubspacesWithGivenRelation(boolean firstParam, boolean secondParam, boolean generateFirst, boolean generateSecond,
                                                                                    int dim1, int dim2, int wholeSpaceDimension,
                                                                                    String relationComboboxSelectedItemContents, String disjointComboboxSelectedItemContents,
                                                                                    String intersectionDimensionTextFieldContents){
        String generatedFirstSubspaceRepresentation = new String();
        String generatedSecondSubspaceRepresentation = new String();

        Subspace first = new Subspace(ioHelper);
        Subspace second = new Subspace(ioHelper);
        Subspace[] firstAndSecond;

        //both subspaces are required
        if (!generateFirst || !generateSecond) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 0);
        }


        /*
            if (dim1 == DIM || dim2 == DIM) {
            JOptionPane.showMessageDialog(null, "Pro tento typ úlohy lze generovat pouze podprostory, které nejsou celý prostor.",
            "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
            }
             */
        if (relationComboboxSelectedItemContents.equals(Constants.relationList[0])) {
            if (dim1 == wholeSpaceDimension || dim2 == wholeSpaceDimension) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 1);
            }
            if (dim1 == 0 || dim2 == 0) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 5);
            }

            int minDim = (dim1 < dim2 ? dim1 : dim2);
            int intersectionDim;
            if (intersectionDimensionTextFieldContents.isEmpty()) {
                intersectionDim = MathUtilities.randIntUniform(Math.max(0, dim1 + dim2 - wholeSpaceDimension), minDim - 1);
            } else {
                String dim = intersectionDimensionTextFieldContents;
                try{
                    intersectionDim = Integer.parseInt(dim);
                } catch (NumberFormatException ex){
                    return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 6); //NaN
                }
            }

            if (intersectionDim > minDim - 1 || intersectionDim < Math.max(0, dim1 + dim2 - wholeSpaceDimension)) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 14); //dim not in allowed range
            }

            firstAndSecond = Subspace.generateRandomSubspacesWithGivenIntersectionDimension(dim1, dim2, wholeSpaceDimension, intersectionDim, ioHelper);
            first.copy(firstAndSecond[0]);
            second.copy(firstAndSecond[1]);

            if (firstParam) {
                first.checkParametricForm();
                first.beautifyParametric();
                generatedFirstSubspaceRepresentation = first.toTextParametric(false);
            } else {
                first.checkGeneralForm();
                first.beautifyGeneral();
                generatedFirstSubspaceRepresentation = first.toTextGeneralOut(false, 'x');
            }

            if (secondParam) {
                second.checkParametricForm();
                second.beautifyParametric();
                generatedSecondSubspaceRepresentation = second.toTextParametric(false);
            } else {
                second.checkGeneralForm();
                second.beautifyGeneral();
                generatedSecondSubspaceRepresentation = second.toTextGeneralOut(false, 'x');
            }
        }

        if (relationComboboxSelectedItemContents.equals(Constants.relationList[1])) {
            if (dim1 == wholeSpaceDimension || dim2 == wholeSpaceDimension) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 1);
            }
            if (dim1 == 0 || dim2 == 0) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 7);
            }
            if (dim1 > wholeSpaceDimension - 2 || dim2 > wholeSpaceDimension - 2) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 8);
            }

            int minDim = (dim1 < dim2 ? dim1 : dim2);
            int intersectionDim;

            if (intersectionDimensionTextFieldContents.isEmpty()) {
                intersectionDim = MathUtilities.randIntUniform(Math.max(0, dim1 + dim2 - wholeSpaceDimension + 1), minDim - 1);
            } else {
                String dim = intersectionDimensionTextFieldContents;
                try{
                    intersectionDim = Integer.parseInt(dim);
                } catch (NumberFormatException ex){
                    return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 9);
                }
            }

            if (intersectionDim > minDim - 1 || intersectionDim < Math.max(0, dim1 + dim2 - wholeSpaceDimension + 1)) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 9);
            }

            firstAndSecond = Subspace.generateRandomSkewSubspaces(dim1, dim2, wholeSpaceDimension, intersectionDim, ioHelper);
            first.copy(firstAndSecond[0]);
            second.copy(firstAndSecond[1]);

            if (firstParam) {
                first.checkParametricForm();
                first.beautifyParametric();
                generatedFirstSubspaceRepresentation = first.toTextParametric(false);
            } else {
                first.checkGeneralForm();
                first.beautifyGeneral();
                generatedFirstSubspaceRepresentation = first.toTextGeneralOut(false, 'x');
            }

            if (secondParam) {
                second.checkParametricForm();
                second.beautifyParametric();
                generatedSecondSubspaceRepresentation = second.toTextParametric(false);
            } else {
                second.checkGeneralForm();
                second.beautifyGeneral();
                generatedSecondSubspaceRepresentation = second.toTextGeneralOut(false, 'x');
            }
        }

        if (relationComboboxSelectedItemContents.equals(Constants.relationList[2])) {
            if (disjointComboboxSelectedItemContents.equals(Constants.generatedSubspacesDisjointOptionList[1] /* Ne */)) {
                int minDim = (dim1 < dim2 ? dim1 : dim2);
                int intersectionDim = minDim;

                firstAndSecond = Subspace.generateRandomSubspacesWithGivenIntersectionDimension(dim1, dim2, wholeSpaceDimension, intersectionDim, ioHelper);
            } else if (disjointComboboxSelectedItemContents.equals(Constants.generatedSubspacesDisjointOptionList[0] /* Ano */)) {
                if (dim1 == wholeSpaceDimension || dim2 == wholeSpaceDimension) {
                    return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 1);
                }
                firstAndSecond = Subspace.generateRandomParallelDisjointSubspaces(dim1, dim2, wholeSpaceDimension, ioHelper);
            } else {
                throw new IllegalArgumentException("Invalid value selected in generatedSubspace -> DisjointOption"); //shall not happen, html edit is checked in web app
            }
            first.copy(firstAndSecond[0]);
            second.copy(firstAndSecond[1]);

            if (firstParam) {
                first.checkParametricForm();
                //first.beautifyParametric();
                generatedFirstSubspaceRepresentation = first.toTextParametric(false);
            } else {
                first.checkGeneralForm();
                first.beautifyGeneral();
                generatedFirstSubspaceRepresentation = first.toTextGeneralOut(false, 'x');
            }

            if (secondParam) {
                second.checkParametricForm();
                //second.beautifyParametric();
                generatedSecondSubspaceRepresentation = second.toTextParametric(false);
            } else {
                second.checkGeneralForm();
                second.beautifyGeneral();
                generatedSecondSubspaceRepresentation = second.toTextGeneralOut(false, 'x');
            }
        }

        return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation);
    }

    public GenerateSubspacesResultDTO validateAndGenerateSubspacesWithGivenIntersection(boolean firstParam, boolean secondParam, boolean generateFirst, boolean generateSecond,
                                                                                        int dim1, int dim2, int wholeSpaceDimension, String outputTextAreaContents){
        String generatedFirstSubspaceRepresentation = new String();
        String generatedSecondSubspaceRepresentation = new String();

        Subspace first = new Subspace(ioHelper);
        Subspace second = new Subspace(ioHelper);
        Subspace[] firstAndSecond;

        //both subspaces are required
        if (!generateFirst || !generateSecond) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 0);
        }


        boolean intNull;
        Subspace intersection;
        try {
            String in = outputTextAreaContents;
            intersection = inputReader.getSubspace(in);
            intNull = intersection.isNull();
        } catch (Exception e) {
            if (e.getMessage().equals("The matrix is not solvable!")) {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 10);
            } else {
                return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 11);
            }
        }

        if (intNull) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 12);
        }

        int minDim = (dim1 < dim2 ? dim1 : dim2);
        int intersectionDim = intersection.getDim();

        if (intersection.getDIM() != wholeSpaceDimension) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 13);
        }

        if (intersectionDim > minDim || intersectionDim < Math.max(0, dim1 + dim2 - wholeSpaceDimension)) {
            return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation, 14);
        }

        firstAndSecond = Subspace.generateRandomSubspacesWithGivenIntersection(dim1, dim2, wholeSpaceDimension, intersection, ioHelper);
        first.copy(firstAndSecond[0]);
        second.copy(firstAndSecond[1]);

        if (firstParam) {
            first.checkParametricForm();
            first.beautifyParametric();
            generatedFirstSubspaceRepresentation = first.toTextParametric(false);
        } else {
            first.checkGeneralForm();
            first.beautifyGeneral();
            generatedFirstSubspaceRepresentation = first.toTextGeneral(false);
        }

        if (secondParam) {
            second.checkParametricForm();
            second.beautifyParametric();
            generatedSecondSubspaceRepresentation = second.toTextParametric(false);
        } else {
            second.checkGeneralForm();
            second.beautifyGeneral();
            generatedSecondSubspaceRepresentation = second.toTextGeneral(false);
        }

        return new GenerateSubspacesResultDTO(generatedFirstSubspaceRepresentation, generatedSecondSubspaceRepresentation);
    }
}
