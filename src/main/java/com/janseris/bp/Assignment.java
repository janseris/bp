package com.janseris.bp;

import com.janseris.bp.model.AssignmentValidityCheckDTO;
import com.janseris.bp.model.SolveInputDTO;

import java.util.*;

import static com.janseris.bp.IOHelper.WINDOWS_LINE_SEPARATOR_STRING;

/**
 * A class used to store an assignment in a text form, provides easier usability and data passing through program layers
 * Represents a valid assignment which can be directly appended to assignments library
 * @see #toString() direct serialization into text format
 * The content shall be always validated before object construction
 * Contains static methods to parse library file contents into self instance (with library content validation in parallel)
 * @author Jan Seris <456435@mail.muni.cz>
 * @version 1.1
 * @since 2020-02-26
 */

public class Assignment {

    public static List<Integer> assignmentTypesAcceptingSingleSubspace = Arrays.asList(1, 2, 8);

    private String name;
    private int type;
    private List<String> subspace1Lines;
    private List<String> subspace2Lines; //always empty if there is only 1 subspace

    /**
     * Constructs an object which represents an assignment from the library.
     * If only subspace lines are provided, they are always stored as subspace 1 lines.
     * @param name The name of the assignment as displayed in library
     * @param type The type of the assignment - a number which represents an index to accepted assignment types list {@link Constants#solveTaskList}
     * @param subspace1Lines Already validated subspace 1 lines
     * @param subspace2Lines Already validated 2 lines
     */
    public Assignment(String name, int type, List<String> subspace1Lines, List<String> subspace2Lines){
        if(type < 1 /* list[0] is "choose option" which is not a valid option */ || type >= Constants.solveTaskList.length){
            throw new LibrarySyntaxErrorException("Neplatné číslo typu příkladu (" + type + "), prosím, vyberte jen z nabídky známých typů příkladů!");
        }
        this.name = name;
        this.type = type;
        if(subspace1Lines.isEmpty() && !subspace2Lines.isEmpty()){ //if only subspace 2 is set, save it as subspace 1
            this.subspace1Lines = subspace2Lines;
            this.subspace2Lines = subspace1Lines;
        } else {
            this.subspace1Lines = subspace1Lines;
            this.subspace2Lines = subspace2Lines;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public List<String> getSubspace1Lines() {
        return subspace1Lines;
    }

    public List<String> getSubspace2Lines() {
        return subspace2Lines;
    }

    /**
     * Checks if there is any input on the line
     * @param line the content to be checked
     * @return true if and only if the length of {@code line.trim()} is 0
     * @see String#trim()
     */
    public static boolean isEmptyLine(String line){
        return line.trim().isEmpty();
    }

    /**
     * Checks if there is any non-whitespace content in the remaining lines representing an assignment
     * @param assignmentLines the assignment string representation split into lines
     * @param lineIndex to perform the check from
     * @return any non-whitespace lines left
     */
    private static boolean anyMoreInputLeftFromIndex(String[] assignmentLines, int lineIndex){
        if(lineIndex >= assignmentLines.length){
            return false; //no more input found
        }
        while(lineIndex < assignmentLines.length){
            if(!isEmptyLine(assignmentLines[lineIndex])){
                return true; //there is some more input
            }
            lineIndex++;
        }
        return false; //no more input found
    }

    /**
     * Checks if the assignment can be directly used in program
     * @return object with null message as an indication of validity (no error message was set during checking the assignment to be used in program),<br>
     *     otherwise object contains a message specifying the error
     */
    public AssignmentValidityCheckDTO checkLogicalValidity(){
        SolveInputDTO inputToSolve = AssignmentSolver.loadInputToSolve(Constants.solveTaskList[getType()],
                    String.join("\n", getSubspace1Lines()),
                    String.join("\n", getSubspace2Lines()),
                    new InputReader(new IOHelper("", "", "", "")));
        return new AssignmentValidityCheckDTO(inputToSolve.getErrorMessage());
    }


    /**
     * Builds an Assignment object from string representation.
     * Uniform line endings (\r\n) are required
     * @param assignmentStringRepresentation the assignment in text format from the library split by {@link Constants#ASSIGNMENT_DISTINGUISHING_WORD}
     * @return a valid assignment object
     * @throws LibrarySyntaxErrorException if the string format doesn't match or if the number defining assignment type is unknown
     */
    private static Assignment buildFromString(String assignmentStringRepresentation) throws LibrarySyntaxErrorException {
        String[] assignmentLines = assignmentStringRepresentation.split(WINDOWS_LINE_SEPARATOR_STRING);
        int lineIndex = 0;

        String name;
        int type;
        List<String> subspace1Lines = new LinkedList<>();
        List<String> subspace2Lines = new LinkedList<>();

        //load assignment name
        if(isEmptyLine(assignmentLines[lineIndex])){ //line [0] = name
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + ": Příklad nemá jméno!");
        }
        name = assignmentLines[lineIndex].trim();
        lineIndex++;

        if(!isEmptyLine(assignmentLines[lineIndex])){ //line [1] = empty
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + " mezi jménem příkladu a číselným typem příkladu není prázdný!");
        }
        lineIndex++;

        //load assignment type
        if(!assignmentLines[lineIndex].matches("\\d+")){ //line [2] = type
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + " obsahující typ příkladu není číslo!");
        }
        type = Integer.parseInt(assignmentLines[lineIndex]);
        lineIndex++;

        if(!isEmptyLine(assignmentLines[lineIndex])){ //line [3] = empty
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + " mezi typem příkladu a rovnicemi prvního podprostoru není prázdný!");
        }
        lineIndex++;

        //load subspace 1 representation
        while(lineIndex < assignmentLines.length && !isEmptyLine(assignmentLines[lineIndex])){
            subspace1Lines.add(assignmentLines[lineIndex]);
            lineIndex++;
        }
        if(subspace1Lines.isEmpty()){
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + ": Zadání příkladu neobsahuje rovnice prvního podprostoru!");
        }

        //subspace 1 loaded

        boolean needSecondSubspace = !assignmentTypesAcceptingSingleSubspace.contains(type); //v typech příkladů 1,2,8 je možné akceptovat jen jeden podprostor

        if(!anyMoreInputLeftFromIndex(assignmentLines, lineIndex)){
            if(needSecondSubspace){
                throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + ": Zadání příkladu neobsahuje rovnice druhého podprostoru!"); //subspace 2 needed
            } else {
                return new Assignment(name, type, subspace1Lines, subspace2Lines); //subspace 2 not needed
            }
        }

        //find empty line (subspace 1 and 2 separator)
        if(!isEmptyLine(assignmentLines[lineIndex])){ //line must be empty
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + " mezi rovnicemi prvního a druhého podprostoru není prázdný!");
        }
        lineIndex++; //skip the empty line (subspace 1 and 2 separator)

        //load subspace 2 representation
        while(lineIndex < assignmentLines.length && !isEmptyLine(assignmentLines[lineIndex])){
            subspace2Lines.add(assignmentLines[lineIndex]);
            lineIndex++;
        }
        if(subspace2Lines.isEmpty()){
            throw new LibrarySyntaxErrorException("Řádek " + (lineIndex + 1) + ": Zadání příkladu neobsahuje rovnice druhého podprostoru!");
        }

        //ignore other (possibly empty) lines
        return new Assignment(name, type, subspace1Lines, subspace2Lines);
    }

    /**
     * Generates Assignment objects from a list of their string representations.<br>
     * Assignment objects provide easier latter manipulation
     * Uniform line endings (\r\n) are required
     * @param assignments input strings (library.txt split by {@link Constants#ASSIGNMENT_DISTINGUISHING_WORD} with omitted first empty string generated by the split)
     * @return Assignment objects to be later used
     * @throws LibrarySyntaxErrorException with error message if any syntax error within parsing any assignment is encountered
     */
    public static ArrayList<Assignment> buildAssignments(String[] assignments) throws LibrarySyntaxErrorException{
        ArrayList<Assignment> assignmentList = new ArrayList<>(assignments.length); //set initial capacity
        int i = 0;
        try{
            for (i = 0; i < assignments.length; i++) {
                assignmentList.add(Assignment.buildFromString(assignments[i]));
            }
        } catch (LibrarySyntaxErrorException ex){
            throw new LibrarySyntaxErrorException((i + 1) + ". příklad v pořadí: " + ex.getMessage());
        }

        return assignmentList;
    }

    /**
     * Creates a serializable version of the assignment to be directly appended to a library text file
     * Matches the program input format
     * @return string representation
     */
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(WINDOWS_LINE_SEPARATOR_STRING).append(WINDOWS_LINE_SEPARATOR_STRING)
                .append(Constants.ASSIGNMENT_DISTINGUISHING_WORD).append(" ").append(name).append(WINDOWS_LINE_SEPARATOR_STRING)
                .append(WINDOWS_LINE_SEPARATOR_STRING)
                .append(type).append(WINDOWS_LINE_SEPARATOR_STRING)
                .append(WINDOWS_LINE_SEPARATOR_STRING);
        for(String line : subspace1Lines){
            builder.append(line).append(WINDOWS_LINE_SEPARATOR_STRING);
        }
        if(!subspace2Lines.isEmpty()){
            builder.append(WINDOWS_LINE_SEPARATOR_STRING);
        }
        for(String line : subspace2Lines){
            builder.append(line).append(WINDOWS_LINE_SEPARATOR_STRING);
        }
        return builder.toString();
    }


}
