package com.janseris.bp.model;

public enum LibraryOrderingColumn {
    NAME,
    DATE_CREATED,
    DATE_MODIFIED,
    DATE_USED,
    USAGE_COUNT
}
