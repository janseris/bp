package com.janseris.bp.model;

public enum SQLResultOrderingMode {
    ASCENDING,
    DESCENDING
}
