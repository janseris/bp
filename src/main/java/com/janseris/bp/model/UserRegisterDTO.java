package com.janseris.bp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-01
 */

public class UserRegisterDTO {
    private String Name;
    private String Email;
    private String Password;

    public UserRegisterDTO(String name, String email, String password) {
        Name = name;
        Email = email;
        Password = password;
    }

    public String getName() {
        return Name;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }
}
