package com.janseris.bp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-08
 */

public enum GenerateSubspacesResult {
    OK,
    ERROR1,
    ERROR2,
    ERROR3,
    ERROR4,
    ERROR5,
    ERROR6,
    ERROR7
}
