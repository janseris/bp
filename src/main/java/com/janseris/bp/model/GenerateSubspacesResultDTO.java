package com.janseris.bp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-08
 */

public class GenerateSubspacesResultDTO {
    private boolean errorSet;
    private int errorMessageIndex;
    private String generatedFirstSubspaceRepresentation;
    private String generatedSecondSubspaceRepresentation;

    public GenerateSubspacesResultDTO(String generatedFirstSubspaceRepresentation, String generatedSecondSubspaceRepresentation) {
        this.errorSet = false;
        this.errorMessageIndex = -1;
        this.generatedFirstSubspaceRepresentation = generatedFirstSubspaceRepresentation;
        this.generatedSecondSubspaceRepresentation = generatedSecondSubspaceRepresentation;
    }

    public GenerateSubspacesResultDTO(String generatedFirstSubspaceRepresentation, String generatedSecondSubspaceRepresentation, int errorMessageIndex) {
        this.errorSet = true;
        this.errorMessageIndex = errorMessageIndex;
        this.generatedFirstSubspaceRepresentation = generatedFirstSubspaceRepresentation;
        this.generatedSecondSubspaceRepresentation = generatedSecondSubspaceRepresentation;
    }

    public String getGeneratedFirstSubspaceRepresentation() {
        if(errorSet){
            throw new IllegalStateException("Cannot retrieve generate first subspace string representation if a problem while parsing input occurred");
        }
        return generatedFirstSubspaceRepresentation;
    }

    public String getGeneratedSecondSubspaceRepresentation() {
        if(errorSet){
            throw new IllegalStateException("Cannot retrieve generated second subspace string representation if a problem while parsing input occurred");
        }
        return generatedSecondSubspaceRepresentation;
    }

    public boolean isErrorSet() {
        return errorSet;
    }

    public int getErrorMessageIndex() {
        if(!errorSet){
            throw new IllegalStateException("Cannot retrieve error message index if no error occured");
        }
        return errorMessageIndex;
    }
}
