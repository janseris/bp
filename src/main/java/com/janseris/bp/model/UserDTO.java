package com.janseris.bp.model;

import java.sql.Timestamp;
import java.util.UUID;

import static com.janseris.bp.dataAccessLayer.Utilities.bytesToHex;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-01
 */

public class UserDTO {
    private UUID Id;
    private String email; //unique in users database table
    private String name;
    private String passwordHash;
    private byte[] passwordSalt;
    private Timestamp registrationDate;
    private Timestamp lastLogInDate;

    public UserDTO() {

    }

    public UUID getId() {
        return Id;
    }

    public void setId(UUID id) {
        Id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {  this.email = email;   }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public byte[] getPasswordSalt() {  return passwordSalt;  }

    public void setPasswordSalt(byte[] passwordSalt) {  this.passwordSalt = passwordSalt;   }

    public Timestamp getRegistrationDate() {        return registrationDate;    }

    public void setRegistrationDate(Timestamp registrationDate) {        this.registrationDate = registrationDate;    }

    public Timestamp getLastLogInDate() {        return lastLogInDate;    }

    public void setLastLogInDate(Timestamp lastLogInDate) {        this.lastLogInDate = lastLogInDate;    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "Id=" + Id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", passwordSalt=" + bytesToHex(passwordSalt) +
                ", registrationDate=" + registrationDate +
                ", lastLogInDate=" + lastLogInDate +
                '}';
    }

}
