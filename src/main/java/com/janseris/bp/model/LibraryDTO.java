package com.janseris.bp.model;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-01
 */

public class LibraryDTO {
    private UUID creatorUserId;
    private UUID id;
    private String name;
    private String contents;
    private Timestamp dateCreated;
    private Timestamp lastDateModified;
    private Timestamp lastDateUsed;
    private PublicAccessibility publicAccessibility;
    private int usageCount;

    public LibraryDTO() {

    }

    public UUID getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(UUID creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getLastDateModified() {
        return lastDateModified;
    }

    public void setLastDateModified(Timestamp lastDateModified) {
        this.lastDateModified = lastDateModified;
    }

    public Timestamp getLastDateUsed() {
        return lastDateUsed;
    }

    public void setLastDateUsed(Timestamp lastDateUsed) {
        this.lastDateUsed = lastDateUsed;
    }

    public PublicAccessibility getPublicAccessibility() {
        return publicAccessibility;
    }

    public void setPublicAccessibility(PublicAccessibility publicAccessibility) {
        this.publicAccessibility = publicAccessibility;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    @Override
    public String toString() {
        return "LibraryDTO{" +
                "creatorUserId=" + creatorUserId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", contents='" + contents + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastDateModified=" + lastDateModified +
                ", lastDateUsed=" + lastDateUsed +
                ", publicAccessibility=" + publicAccessibility +
                ", usageCount=" + usageCount +
                '}';
    }
}

