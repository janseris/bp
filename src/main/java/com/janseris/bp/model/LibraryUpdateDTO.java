package com.janseris.bp.model;

import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-28
 */

public class LibraryUpdateDTO {
    private UUID id;
    private String name;
    private String textContents; //unassigned -> automatic null value
    private byte[] contentsFile; //unassigned -> automatic null value

    private PublicAccessibility publicAccessibility;

    public LibraryUpdateDTO() {

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextContents() {
        return textContents;
    }

    public void setTextContents(String textContents) {
        this.textContents = textContents;
    }

    public byte[] getContentsFile() {
        return contentsFile;
    }

    public void setContentsFile(byte[] contentsFile) {
        this.contentsFile = contentsFile;
    }

    public PublicAccessibility getPublicAccessibility() {
        return publicAccessibility;
    }

    public void setPublicAccessibility(PublicAccessibility publicAccessibility) {
        this.publicAccessibility = publicAccessibility;
    }

    public boolean hasContents(){
        return getContentsFile() != null || getTextContents() != null;
    }
}

