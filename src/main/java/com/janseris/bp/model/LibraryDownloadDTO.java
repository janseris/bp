package com.janseris.bp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-25
 */

public class LibraryDownloadDTO {
    private String name;
    private byte[] contents;

    public LibraryDownloadDTO() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContents() {
        return contents;
    }

    public void setContents(byte[] contents) {
        this.contents = contents;
    }
}

