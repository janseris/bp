package com.janseris.bp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-06
 */

public class SolveResultDTO {
    private String shortTextOutput;

    public SolveResultDTO(String shortTextOutput) {
        this.shortTextOutput = shortTextOutput;
    }

    public String getShortTextOutput() {
        return shortTextOutput;
    }

    public void setShortTextOutput(String shortTextOutput) {
        this.shortTextOutput = shortTextOutput;
    }
}
