package com.janseris.bp.model;

/**
 * Used to check assignment validity<br>
 * If valid, then no error message is set<br>
 * Else, a message specifying the problem is set<br>
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-05-19
 */

public class AssignmentValidityCheckDTO {
    String errorMessage;

    public AssignmentValidityCheckDTO(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean generatedAnyErrorMessage(){
        return errorMessage != null;
    }

    public String getErrorMessage(){
        if(!generatedAnyErrorMessage()){
            throw new IllegalStateException("A valid assignment does not generate an error message");
        }
        return errorMessage;
    }
}
