package com.janseris.bp.model;

import java.sql.Timestamp;

/**
 * Used to show data about a user (publicly accessible information)
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-28
 */

public class UserShowDTO {
    private String name;
    private Timestamp registrationDate;
    private Timestamp lastLogInDate;

    public UserShowDTO() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getRegistrationDate() {        return registrationDate;    }

    public void setRegistrationDate(Timestamp registrationDate) {        this.registrationDate = registrationDate;    }

    public Timestamp getLastLogInDate() {        return lastLogInDate;    }

    public void setLastLogInDate(Timestamp lastLogInDate) {        this.lastLogInDate = lastLogInDate;    }

    @Override
    public String toString() {
        return "UserShowDTO{" +
                ", name='" + name + '\'' +
                ", registrationDate=" + registrationDate +
                ", lastLogInDate=" + lastLogInDate +
                '}';
    }
}
