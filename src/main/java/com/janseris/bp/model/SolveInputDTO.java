package com.janseris.bp.model;

import com.janseris.bp.Subspace;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-06
 */

public class SolveInputDTO {
    private Subspace first;
    private Subspace second;
    private String task;
    private String errorMessage;

    public SolveInputDTO(Subspace first, Subspace second, String task, String errorMessage) {
        this.first = first;
        this.second = second;
        this.task = task;
        this.errorMessage = errorMessage;
    }

    public Subspace getFirst() {
        return first;
    }

    public void setFirst(Subspace first) {
        this.first = first;
    }

    public Subspace getSecond() {
        return second;
    }

    public void setSecond(Subspace second) {
        this.second = second;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean containsErrorMessage(){
        return errorMessage != null;
    }
}
