package com.janseris.bp.model;

public enum PublicAccessibility {
    PRIVATE,
    PUBLIC;

    public static PublicAccessibility fromInt(int n) {
        switch(n) {
            case 0:
                return PublicAccessibility.PRIVATE;
            case 1:
                return PublicAccessibility.PUBLIC;
        }
        return null;
    }
}
