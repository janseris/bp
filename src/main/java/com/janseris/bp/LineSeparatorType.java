package com.janseris.bp;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @version 1.0
 * @since 2020-02-25
 */

public enum LineSeparatorType {
    UNIX_LINE_SEPARATOR,
    WINDOWS_LINE_SEPARATOR,
    APPLE_LINE_SEPARATOR,
    NO_LINE_SEPARATOR
}
