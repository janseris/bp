package com.janseris.bp;

/**
 * A checked exception to be raised if a logical error is detected in a library text file
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-20
 */
public class LibraryLogicalErrorException extends Exception {
    public LibraryLogicalErrorException(String errorMessage) {
        super(errorMessage);
    }

    public LibraryLogicalErrorException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}