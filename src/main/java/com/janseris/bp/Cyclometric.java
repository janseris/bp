package com.janseris.bp;

/**
 * This class is used to store results of angles of subspaces. It carries information about the
 * angle itself and whether the angle is one of 0, &pi;/6, &pi;/4, &pi;/3, or &pi;/2.
 *
 * @author Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since 2014-10-28
 */

class Cyclometric {
    /**
     * the sine or cosine of the angle
     */
    private SquareRootFraction principalValue;
    /**
     * the sine or cosine of the angle, if computed numerically
     */
    private double principalValueNumerical;
    /**
     * a flag, marking if the angle is any of 0, &pi;/6, &pi;/4, &pi;/3, or &pi;/2
     */
    private boolean angleIsPiFraction;
    /**
     * a flag, marking if the angle was computed using arcsine (<code>true</code>) or arccosine (<code>false</code>)
     */
    private boolean fromArcsine;
    /**
     * result of the function in fractions of pi
     */
    private PiFraction resultPi;
    /**
     * result of the function as a decimal number
     */
    private double resultDbl;

    /**
     * Constructs all information about the cyclometric function, given its principal value and type of the cyclometric function.
     *
     * @param princVal  the principal value, ie. the result of the respective trigonometric function
     * @param isArcsine a flag, controlling if <code>princVal</code> is the result of arcsin (<code>true</code>) or arccos (<code>false</code>)
     */
    Cyclometric(SquareRootFraction princVal, boolean isArcsine) {
        principalValue = princVal;
        fromArcsine = isArcsine;
        angleIsPiFraction = false;

        if (principalValue.n == 0) {
            resultPi = isArcsine ? new PiFraction(0) : new PiFraction(1, 2);
            angleIsPiFraction = true;
        }

        long sgn = 1;
        if (principalValue.n < 0) {
            sgn = -1;
            principalValue.n *= -1;
        }

        if (principalValue.isEqual(new SquareRootFraction(1, 4))) {
            resultPi = isArcsine ? new PiFraction(sgn, 6) : new PiFraction((3 - sgn) / 2, 3);
            angleIsPiFraction = true;
        }

        if (principalValue.isEqual(new SquareRootFraction(1, 2))) {
            resultPi = isArcsine ? new PiFraction(sgn, 4) : new PiFraction(2 - sgn, 4);
            angleIsPiFraction = true;
        }

        if (principalValue.isEqual(new SquareRootFraction(3, 4))) {
            resultPi = isArcsine ? new PiFraction(sgn, 3) : new PiFraction(3 - 2 * sgn, 6);
            angleIsPiFraction = true;
        }

        if (principalValue.isEqual(new SquareRootFraction(1))) {
            resultPi = isArcsine ? new PiFraction(sgn, 2) : new PiFraction(1 - sgn, 2);
            angleIsPiFraction = true;
        }
        principalValue.n *= sgn;

        if (!(angleIsPiFraction)) resultPi = null;

        double phi = (1.0 * principalValue.n / principalValue.d) * Math.sqrt(principalValue.nSqrt);
        principalValueNumerical = phi;
        if (isArcsine) resultDbl = Math.asin(phi);
        else resultDbl = Math.acos(phi);
    }

    /**
     * Constructs all information about the cyclometric function, given its principal value as a double (in cases when we could not find a rational solution) and type of
     * the cyclometric function.
     *
     * @param princVal  the principal value, ie. the result of the respective trigonometric function that we were unable to convert to a rational number
     * @param isArcsine a flag, controlling if <code>princVal</code> is the result of arcsin (<code>true</code>) or arccos (<code>false</code>)
     */
    Cyclometric(double princVal, boolean isArcsine) {
        principalValue = null;
        principalValueNumerical = princVal;
        fromArcsine = isArcsine;
        angleIsPiFraction = false;
        resultPi = null;

        if (isArcsine) resultDbl = Math.asin(princVal);
        else resultDbl = Math.acos(princVal);
    }

    String toText() {
        if (this.angleIsPiFraction) return this.resultPi.toText() + " rad (tj. " + this.resultPi.toDegrees() + "°)";

        String function = (this.fromArcsine) ? "arcsin" : "arccos";
        String princValText;
        if (this.principalValue != null) princValText = this.principalValue.toText();
        else princValText = String.format("%.3f", this.principalValueNumerical);
        String rads = String.format("%.3f", this.resultDbl);
        String degs = String.format("%.2f", this.resultDbl * 180 / Math.PI);
        return function + "(" + princValText + "), tedy přibližně " + rads + " rad (tj. " + degs + "°)";
    }

    String toTeX() {
        if (this.angleIsPiFraction)
            return this.resultPi.toTeX() + "\\text{ rad (tj.\\," + this.resultPi.toDegrees() + "\\textdegree)}";

        String function = (this.fromArcsine) ? "\\arcsin" : "\\arccos";
        String princValText;
        if (this.principalValue != null) princValText = this.principalValue.toTeX();
        else princValText = String.format("%.3f", this.principalValueNumerical).replace(",", "{,}");
        String rads = String.format("%.3f", this.resultDbl).replace(",", "{,}");
        String degs = String.format("%.2f", this.resultDbl * 180 / Math.PI).replace(",", "{,}");
        return function + "\\left(" + princValText + "\\right)\\text{, tedy přibližně " + rads + " rad (tj. " + degs + "\\textdegree)}";
    }
}
