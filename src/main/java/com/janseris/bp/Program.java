package com.janseris.bp;

/**
 * Dependency injector class for GUI
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-07
 */
public class Program {
    public static void main(String[] args) {
        IOHelper ioHelper = new IOHelper(
                Constants.DESKTOP_VERSION_OUTPUT_DIRECTORY_PATH,
                Constants.DESKTOP_VERSION_OUTPUT_TXT_NAME,
                Constants.DESKTOP_VERSION_OUTPUT_TEX_NAME,
                Constants.DESKTOP_VERSION_OUTPUT_PDF_NAME);

        System.setProperty("swing.defaultlaf", "javax.swing.plaf.nimbus.NimbusLookAndFeel");
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                                                       public void run() {
                                                           new GUI(ioHelper);
                                                       }
                                                   }
                                                   );
    }
}
