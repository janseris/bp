package com.janseris.bp;

/**
 * This class introduces fractions of &pi;. The only actual difference between a {@link Fraction} and a {@link PiFraction} is their output formatting. Also,
 * {@link PiFraction} is only used for outputting.
 *
 * @author Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since 2014-04-01
 */

class PiFraction {
    /**
     * numerator
     */
    long n;
    /**
     * denominator
     */
    long d;

    /**
     * Constructs a fraction from its numerator and denominator.
     *
     * @param nn nominator
     * @param dd denominator
     * @throws IllegalArgumentException Throws an exception if the denominator would be zero.
     */
    PiFraction(long nn, long dd) {
        if (dd == 0) throw new IllegalArgumentException("The denominator of a fraction must not be zero!");

        long g = MathUtilities.gcd(nn, dd);
        n = nn / g;
        d = dd / g;

        if (d < 0) {
            n *= -1;
            d *= -1;
        }
    }

    /**
     * Constructs a fraction from an integer.
     *
     * @param nn the integer (constructs fraction nn/1)
     */
    PiFraction(long nn) {
        n = nn;
        d = 1;
    }

    /**
     * Simplifies this fraction, so that denominator and numerator are coprime and denominator is positive.
     */
    private void simplify() {
        long g = MathUtilities.gcd(this.n, this.d);
        this.n /= g;
        this.d /= g;

        if (d < 0) {
            n *= -1;
            d *= -1;
        }
    }

    /**
     * Checks if this {@link PiFraction} is equal to another {@link PiFraction}.
     *
     * @param c comparand
     * @return true if this {@link PiFraction} is equal to the comparand, false otherwise
     */
    boolean isEqual(PiFraction c) {
        Fraction a = new Fraction(this.n, this.d);
        Fraction b = new Fraction(c.n, c.d);
        return a.isEqual(b);
    }

    /**
     * Converts the angle from rads to degrees.
     *
     * @return magnitude in degrees of the angle given by this {@link PiFraction}
     */
    long toDegrees() {
        return 180 * this.n / this.d;
    }

    /**
     * Prints this PiFraction formatted as text.
     *
     * @return plain text description of this SRF; for example, -3&pi;/2 yields <code>-3*pi/2</code>
     */
    String toText() {
        this.simplify();

        if (n == 0) return "0";

        if (n == 1) {
            if (d == 1) {
                return "pi";
            }
            return "pi/" + d;
        }

        if (n == -1) {
            if (d == 1) {
                return "-pi";
            }
            return "-pi/" + d;
        }

        if (d == 1) return n + "*pi";

        return n + "*pi/" + d;
    }

    /**
     * Prints this PiFraction formatted as TeX code.
     *
     * @return TeX code for this SRF; for example, -3&pi;/2 yields <code>-\frac{3\pi}{2}</code>
     */
    String toTeX() {
        this.simplify();

        if (n == 0) return "0";

        if (n == 1) {
            if (d == 1) {
                return "\\pi";
            }
            return "\\frac{\\pi}{" + d + "}";
        }

        if (n == -1) {
            if (d == 1) {
                return "-\\pi";
            }
            return "-\\frac{\\pi}{" + d + "}";
        }

        if (d == 1) return n + "\\pi";

        if (this.n < -1)
            return "-\\frac{" + (-1 * n) + "\\pi}{" + d + "}";
        else
            return "\\frac{" + n + "\\pi}{" + d + "}";
    }
}
