package com.janseris.bp;

/**
 * This class introduces fractions and enables basic operations with them.
 *
 * @author Pavel Sikora <pavelsikora@mail.muni.cz>
 * @version 1.0
 * @since 2014-04-01
 */

class Fraction {
    /**
     * the numerator
     */
    long n;
    /**
     * the denominator
     */
    long d;

    /**
     * Constructs a fraction 0/1, ie. zero.
     */
    Fraction() {
        n = 0;
        d = 1;
    }

    /**
     * Constructs a fraction from its numerator and denominator.
     *
     * @param nn nominator
     * @param dd denominator
     * @throws IllegalArgumentException Throws an exception if the denominator would be zero.
     */
    Fraction(long nn, long dd) {
        if (dd == 0) throw new IllegalArgumentException("The denominator of a fraction must not be zero!");

        long g = MathUtilities.gcd(nn, dd);
        n = nn / g;
        d = dd / g;

        if (d < 0) {
            n *= -1;
            d *= -1;
        }
    }

    /**
     * Constructs a fraction from an integer.
     *
     * @param nn the integer (constructs fraction <code>nn</code>/1)
     */
    Fraction(long nn) {
        n = nn;
        d = 1;
    }

    /**
     * Constructs an estimate fraction from a real number. The method uses convergents of continued fractions and always construct a <code>Fraction</code>, even if the real
     * number is an irrational number - the resulting fraction should be correct at least to 10 decimal places. For example, for &pi;, we'll obtain 80143857/25510582.
     * In any case, the number we obtain should be the best approximation in the sense that there is no better approximation with an equal or lower denominator.
     *
     * @param r the real number
     */
    Fraction(double r) {
        // maximum number of iterations
        int MAX = 15;
        // maximum error - if the error is less, we assume we have found the value we have been looking for
        double epsilon = 1e-10;

        long[] nn = new long[MAX], dd = new long[MAX];
        long currentValue;
        // the index of values when the end has been reached
        int finalIndex = MAX - 1;

        nn[0] = 0;
        nn[1] = 1;
        dd[0] = 1;
        dd[1] = 0;

        for (int i = 2; i < MAX; i++) {
            currentValue = (long) Math.floor(r);
            nn[i] = currentValue * nn[i - 1] + nn[i - 2];
            dd[i] = currentValue * dd[i - 1] + dd[i - 2];
            if (Math.abs(r - currentValue) < epsilon) {
                finalIndex = i; // we note down the value of the result
                i = MAX;        // and we terminate
            }
            r = 1.0 / (r - currentValue);
        }

        n = nn[finalIndex];
        d = dd[finalIndex];
    }

    /**
     * Simplifies this fraction, so that denominator and numerator are coprime and denominator is positive.
     */
    private void simplify() {
        long g = MathUtilities.gcd(this.n, this.d);
        this.n /= g;
        this.d /= g;

        if (d < 0) {
            this.n *= -1;
            this.d *= -1;
        }
    }

    /**
     * Adds another fraction to this fraction.
     *
     * @param a addend
     * @return the sum
     */
    Fraction add(Fraction a) {
        Fraction sum = new Fraction();
        sum.n = this.n * a.d + this.d * a.n;
        sum.d = this.d * a.d;
        sum.simplify();

        return sum;
    }

    /**
     * Subtracts another fraction from this fraction.
     *
     * @param s subtrahend
     * @return the difference
     */
    Fraction subtract(Fraction s) {
        Fraction diff = new Fraction();
        diff.n = this.n * s.d - this.d * s.n;
        diff.d = this.d * s.d;
        diff.simplify();

        return diff;
    }

    /**
     * Multiplies this fraction by another fraction.
     *
     * @param f factor
     * @return the product
     */
    Fraction multiply(Fraction f) {
        Fraction prod = new Fraction();
        prod.n = this.n * f.n;
        prod.d = this.d * f.d;
        prod.simplify();

        return prod;
    }

    /**
     * Multiplies this fraction by an integer.
     *
     * @param f factor
     * @return the product
     */
    Fraction multiply(long f) {
        Fraction prod = new Fraction();
        prod.n = this.n * f;
        prod.d = this.d;
        prod.simplify();

        return prod;
    }

    /**
     * Divides this fraction by another fraction.
     *
     * @param d divisor
     * @return the ratio
     * @throws ArithmeticException Throws an exception if the nominator of the divisor is zero.
     */
    Fraction divide(Fraction d) {
        if (d.n == 0) throw new ArithmeticException("You must not try to divide by zero!");

        Fraction ratio = new Fraction();
        ratio.n = this.n * d.d;
        ratio.d = this.d * d.n;
        ratio.simplify();

        return ratio;
    }

    /**
     * Squares this fraction.
     *
     * @return the square
     */
    Fraction squared() {
        Fraction square = new Fraction();
        square.n = this.n * this.n;
        square.d = this.d * this.d;
        square.simplify();

        return square;
    }

    /**
     * Absolute value of the fraction.
     *
     * @return absolute value of this fraction
     */
    Fraction abs() {
        return this.multiply(this.compare(0));
    }

    /**
     * Compares this fraction to another fraction.
     *
     * @param c comparand
     * @return 1 if this fraction is larger than the comparand, -1 if it is smaller than the comparand, and 0 if they are equal
     */
    int compare(Fraction c) {
        long left = this.n * c.d;
        long right = this.d * c.n;

        if (left < right) return -1;
        if (left > right) return 1;
        return 0;
    }

    /**
     * Compares this fraction with an integer.
     *
     * @param c comparand
     * @return 1 if this fraction is larger than the comparand, -1 if it is smaller than the comparand, and 0 if they are equal
     */
    int compare(long c) {
        long left = this.n;
        long right = this.d * c;

        if (left < right) return -1;
        if (left > right) return 1;
        return 0;
    }

    /**
     * Checks if this fraction is equal to another fraction.
     *
     * @param c comparand
     * @return true if this fraction is equal to the comparand, false otherwise
     */
    boolean isEqual(Fraction c) {
        return this.compare(c) == 0;
    }

    /**
     * Checks if this fraction is equal to an integer.
     *
     * @param c comparand
     * @return true if this fraction is equal to the comparand, false otherwise
     */
    boolean isEqual(int c) {
        return this.compare(c) == 0;
    }

    /**
     * Converts this fraction to a <code>SquareRootFraction</code>. Basically this is just re-typing, numerical value will be preserved.
     *
     * @return a copy of this fraction, of type <code>SquareRootFraction</code>
     */
    SquareRootFraction convertToSRF() {
        SquareRootFraction S = new SquareRootFraction(this.squared());
        S.n *= this.compare(0);

        return S;
    }

    /**
     * Prints this fraction formatted as text.
     *
     * @return plain text description of this fraction; for example, -3/2 yields <code>-3/2</code>
     */
    String toText() {
        this.simplify();

        if (this.n == 0) return "0";

        if (this.d == 1) return this.n + "";

        return this.n + "/" + this.d;
    }

    /**
     * Prints this fraction formatted as TeX code.
     *
     * @return TeX code for this fraction; for example, -3/2 yields <code>-\frac{3}{2}</code>
     */
    String toTeX() {
        this.simplify();

        if (this.n == 0) return "0";

        if (this.d == 1) return this.n + "";

        if (this.compare(0) == -1)
            return "-\\frac{" + (-1 * this.n) + "}{" + this.d + "}";
        else
            return "\\frac{" + this.n + "}{" + this.d + "}";
    }
}
