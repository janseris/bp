package com.janseris.bp;

import com.janseris.bp.model.AssignmentValidityCheckDTO;
import com.janseris.bp.model.GenerateSubspacesResultDTO;
import com.janseris.bp.model.SolveInputDTO;
import com.janseris.bp.model.SolveResultDTO;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import static com.janseris.bp.Constants.*;

/**
 * This is the computational core of the package. It introduces subspaces and enables basic operations with them, using all the other classes for that.
 * Edit: transformed into GUI input handler
 *
 * @author Jakub Klejch <323464@mail.muni.cz>
 * @author Jan Seriš <456435@mail.muni.cz>
 * @version 1.1
 * @since 2014-12-01
 */

public class GUI extends JFrame {
    private JMenuBar menuBar;
    private JButton solveButton, generateButton, textOutputButton, pdfOutputButton, firstInputTitleButton, secondInputTitleButton, validateIntersectionInputButton, loadSelectedAssignmentFromLibraryButton, closeLibraryButton, deleteSelectedAssignmentFromLibraryButton;
    private JComboBox solve_AssignmentTypeCombobox; /* výběr typu příkladu */
    private JComboBox generate_AssignmentRelationTypeCombobox; /* výběr druhů podprostoru k náhodnému vygenerování */
    private JComboBox angleCombobox, relationCombobox, disjointCombobox, libraryCombobox;
    private JRadioButton firstGeneralRadioButton, firstParametricRadioButton, secondGeneralRadioButton, secondParametricRadioButton;
    private JScrollPane firstInputScroll, secondInputScroll, outputScroll;
    private JTextArea firstInputTextarea, secondInputTextarea, outputTextarea;
    private JTextField dim1Textfield, dim2Textfield, distanceTextfield;
    private JTextField intersectionDimensionTextField; //dimenze průniku podprostorů
    private JTextField wholeSubspaceDimensionTextField; //Dimenze celého prostoru při náhodném generování podprostorů
    private JLabel firstInputInvalidRedCircle, secondInputInvalidRedCircle, dim1Label, dim2Label, DIMLabel, distanceLabel, angleLabel, outputLabel, disjointLabel, dimLabel, dimSubLabel, intersectionInputInvalidRedCircle, intersectionInputLabel;
    private JPanel inputPanel, taskPanel, generatePanel, firstGeneratePanel, secondGeneratePanel, outputPanel, libraryPanel;
    private Border blackLineBorder = BorderFactory.createLineBorder(Color.black), titledBorder;

    private IOHelper ioHelper; //service-like
    private InputReader inputReader; //service-like
    private AssignmentGenerator assignmentGenerator; //service-like

    private static final int RED_CIRCLE_TIMEOUT_MILISECONDS = 3000;

    public GUI(IOHelper ioHelper) {
        this.ioHelper = ioHelper;
        this.inputReader = new InputReader(ioHelper);
        this.assignmentGenerator = new AssignmentGenerator(ioHelper, inputReader);

        BufferedImage smallRedCircle = new BufferedImage(20, 20, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D img = smallRedCircle.createGraphics();
        img.setColor(Color.RED);
        img.fillOval(0, 0, 10, 10);

        setTitle("CVIČEBNICE ANALYTICKÉ GEOMETRIE v2");
        setSize(633, 492);
        generateMenu();
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel(null);
        contentPane.setPreferredSize(new Dimension(633, 492));
        contentPane.setBackground(new Color(192, 192, 192));

        inputPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "vstup pro řešení / výstup pro generování");
        inputPanel.setBorder(titledBorder);
        inputPanel.setBounds(25, 25, 584, 196);
        inputPanel.setBackground(new Color(192, 192, 192));
        inputPanel.setForeground(new Color(0, 0, 0));
        inputPanel.setEnabled(true);
        inputPanel.setFont(new Font("sansserif", 0, 12));
        inputPanel.setVisible(true);
        inputPanel.setLayout(null);


        libraryPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "Zadejte číslo příkladu");
        libraryPanel.setBorder(titledBorder);
        libraryPanel.setBounds(92, 60, 400, 110);
        libraryPanel.setBackground(new Color(214, 217, 223));
        libraryPanel.setForeground(new Color(0, 0, 0));
        libraryPanel.setEnabled(true);
        libraryPanel.setFont(new Font("sansserif", 0, 12));
        libraryPanel.setVisible(false);
        libraryPanel.setLayout(null);

        closeLibraryButton = new JButton();
        closeLibraryButton.setBounds(160, 73, 80, 20);
        closeLibraryButton.setBackground(new Color(214, 217, 223));
        closeLibraryButton.setForeground(new Color(0, 0, 0));
        closeLibraryButton.setEnabled(true);
        closeLibraryButton.setFont(new Font("sansserif", 0, 12));
        closeLibraryButton.setText("zavřít");
        closeLibraryButton.setVisible(true);
        closeLibraryButton.addActionListener(new ActionListener() {
                                                 public void actionPerformed(ActionEvent evt) {
                                                     libraryPanel.setVisible(false);
                                                     libraryCombobox.setVisible(false);
                                                     //closeLibraryButton.setVisible(false);
                                                 }
                                             }
        );

        loadSelectedAssignmentFromLibraryButton = new JButton();
        loadSelectedAssignmentFromLibraryButton.setBounds(290, 30, 90, 30);
        loadSelectedAssignmentFromLibraryButton.setBackground(new Color(214, 217, 223));
        loadSelectedAssignmentFromLibraryButton.setForeground(new Color(0, 0, 0));
        loadSelectedAssignmentFromLibraryButton.setEnabled(true);
        loadSelectedAssignmentFromLibraryButton.setFont(new Font("sansserif", 0, 12));
        loadSelectedAssignmentFromLibraryButton.setText("nahrát");
        loadSelectedAssignmentFromLibraryButton.setVisible(true);
        loadSelectedAssignmentFromLibraryButton.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent evt) { //register onclick action
                                                libraryPanel.setVisible(false);
                                                libraryCombobox.setVisible(false);

                                                try {
                                                    loadAssignmentBySelectedComboboxIndex();
                                                } catch (LibrarySyntaxErrorException syntaxError){
                                                    JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny: " + syntaxError.getMessage() + ", zkontorolujte obsah souboru " + LIBRARY_FILE_PATH, "Chyba", JOptionPane.ERROR_MESSAGE);

                                                } catch (Exception e) {
                                                    JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny, zkontorolujte obsah souboru " + LIBRARY_FILE_PATH, "Chyba", JOptionPane.ERROR_MESSAGE);
                                                }
                                            }
                                        }
        );

        deleteSelectedAssignmentFromLibraryButton = new JButton();
        deleteSelectedAssignmentFromLibraryButton.setBounds(290, 30, 90, 30);
        deleteSelectedAssignmentFromLibraryButton.setBackground(new Color(214, 217, 223));
        deleteSelectedAssignmentFromLibraryButton.setForeground(new Color(0, 0, 0));
        deleteSelectedAssignmentFromLibraryButton.setEnabled(true);
        deleteSelectedAssignmentFromLibraryButton.setFont(new Font("sansserif", 0, 12));
        deleteSelectedAssignmentFromLibraryButton.setText("odstranit");
        deleteSelectedAssignmentFromLibraryButton.setVisible(true);
        deleteSelectedAssignmentFromLibraryButton.addActionListener(new ActionListener() {
                                                  public void actionPerformed(ActionEvent evt) { //register onclick action
                                                      libraryPanel.setVisible(false);
                                                      libraryCombobox.setVisible(false);

                                                      try {
                                                          removeSelectedAssignmentFromLibrary();
                                                      } catch (Exception e) {
                                                          JOptionPane.showMessageDialog(null, "Chyba v syntaxi knihovny, zkontorolujte obsah souboru " + LIBRARY_FILE_PATH, "Chyba", JOptionPane.ERROR_MESSAGE);
                                                      }
                                                  }
                                              }
        );

        firstInputTitleButton = new JButton();
        firstInputTitleButton.setBounds(87-50, 20, 112+50+50, 20);
        firstInputTitleButton.setBackground(new Color(214, 217, 223));
        firstInputTitleButton.setForeground(new Color(0, 0, 0));
        firstInputTitleButton.setEnabled(true);
        firstInputTitleButton.setFont(new Font("sansserif", 0, 12));
        firstInputTitleButton.setText("podprostor A (kontrola kliknutím)");
        firstInputTitleButton.setVisible(true);
        firstInputTitleButton.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent evt) {
                                                try {
                                                    String in = firstInputTextarea.getText();
                                                    Subspace firstSubspace = inputReader.getSubspace(in);
                                                    if (firstSubspace.isNull()){
                                                        //subspace 1 format is not valid (application core threw an exception during parsing input)
                                                        // or the matrix is not solvable (a set of equations is not solvable)
                                                        activateRedCircle(firstInputInvalidRedCircle);
                                                        return;
                                                    }

                                                    TeXIcon formulaGraphics = firstSubspace.getTeXFormulaGraphics();
                                                    showTeXImageWindow("Podprostor A", formulaGraphics);
                                                } catch (Exception e) {
                                                    //an exception was raised during subspace input parsing or during parsing its TeX representation while TeX image was created
                                                    activateRedCircle(firstInputInvalidRedCircle);
                                                }
                                            }
                                        }
        );


        firstInputInvalidRedCircle = new JLabel(new ImageIcon(smallRedCircle));
        firstInputInvalidRedCircle.setBounds(263, 25, 20, 20);
        firstInputInvalidRedCircle.setVisible(false);

        firstInputTextarea = new JTextArea();
        firstInputTextarea.setBackground(new Color(255, 255, 255));
        firstInputTextarea.setForeground(new Color(0, 0, 0));
        firstInputTextarea.setEnabled(true);
        firstInputTextarea.setFont(new Font("sansserif", 0, 12));
        firstInputTextarea.setText("");
        firstInputTextarea.setVisible(true);

        firstInputScroll = new JScrollPane(firstInputTextarea);
        firstInputScroll.setBounds(5, 45, 278, 145);
        firstInputScroll.setBackground(new Color(255, 255, 255));
        firstInputScroll.setForeground(new Color(0, 0, 0));
        firstInputScroll.setEnabled(true);
        firstInputScroll.setBorder(BorderFactory.createBevelBorder(1));
        firstInputScroll.setVisible(true);

        secondInputTitleButton = new JButton();
        secondInputTitleButton.setBounds(382-50, 20, 112+50+50, 20);
        secondInputTitleButton.setBackground(new Color(214, 217, 223));
        secondInputTitleButton.setForeground(new Color(0, 0, 0));
        secondInputTitleButton.setEnabled(true);
        secondInputTitleButton.setFont(new Font("sansserif", 0, 12));
        secondInputTitleButton.setText("podprostor B (kontrola kliknutím)");
        secondInputTitleButton.setVisible(true);
        secondInputTitleButton.addActionListener(new ActionListener() {
                                             public void actionPerformed(ActionEvent evt) {
                                                 try {
                                                     String in = secondInputTextarea.getText();
                                                     Subspace secondSubspace = inputReader.getSubspace(in);
                                                     if (secondSubspace.isNull()){
                                                         //subspace 2 format is not valid (application core threw an exception during parsing input)
                                                         // or the matrix is not solvable (a set of equations is not solvable)
                                                         activateRedCircle(firstInputInvalidRedCircle);
                                                         return;
                                                     }

                                                     TeXIcon formulaGraphics = secondSubspace.getTeXFormulaGraphics();
                                                     showTeXImageWindow("Podprostor B", formulaGraphics);
                                                 } catch (Exception e) {
                                                     //an exception was raised during subspace input parsing or during parsing its TeX representation while TeX image was created
                                                     activateRedCircle(secondInputInvalidRedCircle);
                                                 }
                                             }
                                         }
        );

        secondInputInvalidRedCircle = new JLabel(new ImageIcon(smallRedCircle));
        secondInputInvalidRedCircle.setBounds(558, 25, 20, 20);
        secondInputInvalidRedCircle.setVisible(false);

        secondInputTextarea = new JTextArea();
        secondInputTextarea.setBackground(new Color(255, 255, 255));
        secondInputTextarea.setForeground(new Color(0, 0, 0));
        secondInputTextarea.setEnabled(true);
        secondInputTextarea.setFont(new Font("sansserif", 0, 12));
        secondInputTextarea.setText("");
        secondInputTextarea.setVisible(true);

        secondInputScroll = new JScrollPane(secondInputTextarea);
        secondInputScroll.setBounds(300, 45, 278, 145);
        secondInputScroll.setBackground(new Color(255, 255, 255));
        secondInputScroll.setForeground(new Color(0, 0, 0));
        secondInputScroll.setEnabled(true);
        secondInputScroll.setBorder(BorderFactory.createBevelBorder(1));
        secondInputScroll.setVisible(true);

        taskPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "parametry úlohy");
        taskPanel.setBorder(titledBorder);
        taskPanel.setBounds(25, 222, 413, 160);
        taskPanel.setBackground(new Color(214, 217, 223));
        taskPanel.setForeground(new Color(0, 0, 0));
        taskPanel.setEnabled(true);
        taskPanel.setFont(new Font("sansserif", 0, 12));
        taskPanel.setVisible(true);
        taskPanel.setLayout(null);


        solve_AssignmentTypeCombobox = new JComboBox<String>(Constants.solveTaskList);
        solve_AssignmentTypeCombobox.setBounds(20, 30, 260, 35);
        solve_AssignmentTypeCombobox.setBackground(new Color(214, 217, 223));
        solve_AssignmentTypeCombobox.setForeground(new Color(0, 0, 0));
        solve_AssignmentTypeCombobox.setEnabled(true);
        solve_AssignmentTypeCombobox.setFont(new Font("sansserif", 0, 12));
        solve_AssignmentTypeCombobox.setVisible(true);
        solve_AssignmentTypeCombobox.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent evt) {
                                                if (solve_AssignmentTypeCombobox.getSelectedItem().toString().equals("Vyber úlohu:")){
                                                    solveButton.setEnabled(false);
                                                }
                                                else solveButton.setEnabled(true);
                                            }
                                        }
        );

        solveButton = new JButton();
        solveButton.setBounds(293, 30, 100, 35);
        solveButton.setBackground(new Color(214, 217, 223));
        solveButton.setForeground(new Color(0, 0, 0));
        solveButton.setEnabled(false);
        solveButton.setFont(new Font("sansserif", 0, 12));
        solveButton.setText("Vyřeš");
        solveButton.setVisible(true);
        solveButton.addActionListener(new ActionListener() {
                                          public void actionPerformed(ActionEvent evt) { //register onclick action
                                              try {
                                                  String task = solve_AssignmentTypeCombobox.getSelectedItem().toString();
                                                  String input1 = firstInputTextarea.getText();
                                                  String input2 = secondInputTextarea.getText();

                                                  SolveInputDTO inputToSolve = AssignmentSolver.loadInputToSolve(task, input1, input2, inputReader);
                                                  if(inputToSolve.containsErrorMessage()){
                                                      JOptionPane.showMessageDialog(null, inputToSolve.getErrorMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
                                                  }
                                                  //input was successfully parsed
                                                  SolveResultDTO result = AssignmentSolver.solve(inputToSolve, ioHelper);
                                                  clearOutputFiles();
                                                  ioHelper.writeBuffersToOutputFiles();
                                                  outputTextarea.setText(result.getShortTextOutput());
                                                  textOutputButton.setEnabled(true);
                                                  pdfOutputButton.setEnabled(true);
                                              } catch (IOException ex) {
                                                  JOptionPane.showMessageDialog(null, "Došlo k problému při zápisu výsledků do souboru, prosím, opakujte operaci.", "Chyba", JOptionPane.ERROR_MESSAGE);
                                              } catch(Exception e) {
                                                  JOptionPane.showMessageDialog(null, "Při řešení došlo k neočekávané chybě. Pravděpodobně došlo k přetečení číselného formátu, tzn. že při řešení "
                                                          + "se neúměrně zvyšovala některá čísla. Zejména případě náhodně generovaných úloh k takovýmto chybám "
                                                          + "může docházet. Zadejte prosím jinou úlohu.", "Chyba", JOptionPane.ERROR_MESSAGE);
                                              }
                                          }
                                      }
        );

        generate_AssignmentRelationTypeCombobox = new JComboBox<String>(Constants.generateTaskList);
        generate_AssignmentRelationTypeCombobox.setBounds(20, 70, 260, 35);
        generate_AssignmentRelationTypeCombobox.setBackground(new Color(214, 217, 223));
        generate_AssignmentRelationTypeCombobox.setForeground(new Color(0, 0, 0));
        generate_AssignmentRelationTypeCombobox.setEnabled(true);
        generate_AssignmentRelationTypeCombobox.setFont(new Font("sansserif", 0, 12));
        generate_AssignmentRelationTypeCombobox.setVisible(true);
        generate_AssignmentRelationTypeCombobox.addActionListener(new ActionListener() {
                                               public void actionPerformed(ActionEvent evt) {
                                                   if (generate_AssignmentRelationTypeCombobox.getSelectedItem().toString().equals("Vyber úlohu:")) {
                                                       generatePanel.setVisible(false);
                                                       generateButton.setEnabled(false);
                                                   } else {
                                                       generatePanel.setVisible(true);
                                                       generateButton.setEnabled(true);
                                                   }

                                                   if (generate_AssignmentRelationTypeCombobox.getSelectedItem().toString().equals(generateTaskList[4])) {
                                                       distanceLabel.setVisible(true);
                                                       distanceTextfield.setVisible(true);
                                                   } else {
                                                       distanceLabel.setVisible(false);
                                                       distanceTextfield.setVisible(false);
                                                   }

                                                   if (generate_AssignmentRelationTypeCombobox.getSelectedItem().toString().equals(generateTaskList[2])) {
                                                       relationCombobox.setVisible(true);
                                                       if (relationCombobox.getSelectedItem().toString().equals(Constants.relationList[2])) {
                                                           disjointLabel.setVisible(true);
                                                           disjointCombobox.setVisible(true);
                                                       } else {
                                                           disjointLabel.setVisible(false);
                                                           disjointCombobox.setVisible(false);
                                                       }

                                                       if (relationCombobox.getSelectedItem().toString().equals(Constants.relationList[0])) {
                                                           dimSubLabel.setVisible(true);
                                                           intersectionDimensionTextField.setVisible(true);
                                                       } else {
                                                           dimSubLabel.setVisible(false);
                                                           intersectionDimensionTextField.setVisible(false);
                                                       }

                                                       if (relationCombobox.getSelectedItem().toString().equals(Constants.relationList[1])) {
                                                           dimLabel.setVisible(true);
                                                           intersectionDimensionTextField.setVisible(true);
                                                       } else {
                                                           dimLabel.setVisible(false);
                                                       }
                                                   } else {
                                                       relationCombobox.setVisible(false);
                                                       dimLabel.setVisible(false);
                                                       intersectionDimensionTextField.setVisible(false);
                                                       dimSubLabel.setVisible(false);
                                                       disjointLabel.setVisible(false);
                                                       disjointCombobox.setVisible(false);
                                                   }

                                                   if (generate_AssignmentRelationTypeCombobox.getSelectedItem().toString().equals(generateTaskList[3])) {
                                                       outputTextarea.setEnabled(true);
                                                       validateIntersectionInputButton.setVisible(true);
                                                       intersectionInputLabel.setVisible(true);
                                                       outputTextarea.setText("");
                                                   } else {
                                                       outputTextarea.setEnabled(false);
                                                       validateIntersectionInputButton.setVisible(false);
                                                       intersectionInputLabel.setVisible(false);
                                                   }

                                                   if (generate_AssignmentRelationTypeCombobox.getSelectedItem().toString().equals(Constants.generateTaskList[5])) {
                                                       angleLabel.setVisible(true);
                                                       angleCombobox.setVisible(true);
                                                   } else {
                                                       angleLabel.setVisible(false);
                                                       angleCombobox.setVisible(false);
                                                   }
                                               }
                                           }
        );

        generateButton = new JButton();
        generateButton.setBounds(293, 70, 100, 35);
        generateButton.setBackground(new Color(214, 217, 223));
        generateButton.setForeground(new Color(0, 0, 0));
        generateButton.setEnabled(false);
        generateButton.setFont(new Font("sansserif", 0, 12));
        generateButton.setText("Vygeneruj");
        generateButton.setVisible(true);
        generateButton.addActionListener(new ActionListener() {
                                             public void actionPerformed(ActionEvent evt) {
                                                 try {
                                                     generate(generate_AssignmentRelationTypeCombobox.getSelectedItem().toString());
                                                 } catch (Exception e) {
                                                     JOptionPane.showMessageDialog(null, "Při řešení došlo k neočekávané chybě. Opakujte prosím požadavek.", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                 }
                                             }
                                         }
        );

        distanceLabel = new JLabel();
        distanceLabel.setBounds(20, 110, 150, 35);
        distanceLabel.setBackground(new Color(255, 255, 255));
        distanceLabel.setForeground(new Color(0, 0, 0));
        distanceLabel.setEnabled(true);
        distanceLabel.setFont(new Font("sansserif", 0, 12));
        distanceLabel.setText("Požadovaná vzdálenost:");
        distanceLabel.setVisible(false);

        distanceTextfield = new JTextField();
        distanceTextfield.setBounds(153, 110, 50, 35);
        distanceTextfield.setBackground(new Color(255, 255, 255));
        distanceTextfield.setForeground(new Color(0, 0, 0));
        distanceTextfield.setEnabled(true);
        distanceTextfield.setFont(new Font("sansserif", 0, 12));
        distanceTextfield.setText("");
        distanceTextfield.setVisible(false);

        angleLabel = new JLabel();
        angleLabel.setBounds(20, 110, 150, 35);
        angleLabel.setBackground(new Color(255, 255, 255));
        angleLabel.setForeground(new Color(0, 0, 0));
        angleLabel.setEnabled(true);
        angleLabel.setFont(new Font("sansserif", 0, 12));
        angleLabel.setText("Požadovaná odchylka:");
        angleLabel.setVisible(false);

        angleCombobox = new JComboBox<String>(Constants.angleList);
        angleCombobox.setBounds(153, 110, 127, 35);
        angleCombobox.setBackground(new Color(214, 217, 223));
        angleCombobox.setForeground(new Color(0, 0, 0));
        angleCombobox.setEnabled(true);
        angleCombobox.setFont(new Font("sansserif", 0, 12));
        angleCombobox.setVisible(false);

        relationCombobox = new JComboBox<String>(Constants.relationList);
        relationCombobox.setBounds(20, 110, 110, 35);
        relationCombobox.setBackground(new Color(214, 217, 223));
        relationCombobox.setForeground(new Color(0, 0, 0));
        relationCombobox.setEnabled(true);
        relationCombobox.setFont(new Font("sansserif", 0, 12));
        relationCombobox.setVisible(false);
        relationCombobox.addActionListener(new ActionListener() {
                                               public void actionPerformed(ActionEvent evt) {
                                                   if (relationCombobox.getSelectedItem().toString().equals(Constants.relationList[2])) {
                                                       disjointLabel.setVisible(true);
                                                       disjointCombobox.setVisible(true);
                                                   } else {
                                                       disjointLabel.setVisible(false);
                                                       disjointCombobox.setVisible(false);
                                                   }

                                                   if (relationCombobox.getSelectedItem().toString().equals(Constants.relationList[0])) {
                                                       dimSubLabel.setVisible(true);
                                                       intersectionDimensionTextField.setVisible(true);
                                                   } else {
                                                       dimSubLabel.setVisible(false);
                                                       intersectionDimensionTextField.setVisible(false);
                                                   }

                                                   if (relationCombobox.getSelectedItem().toString().equals(Constants.relationList[1])) {
                                                       dimLabel.setVisible(true);
                                                       intersectionDimensionTextField.setVisible(true);
                                                   } else {
                                                       dimLabel.setVisible(false);
                                                   }
                                               }
                                           }
        );

        disjointLabel = new JLabel();
        disjointLabel.setBounds(155, 110, 150, 35);
        disjointLabel.setBackground(new Color(255, 255, 255));
        disjointLabel.setForeground(new Color(0, 0, 0));
        disjointLabel.setEnabled(true);
        disjointLabel.setFont(new Font("sansserif", 0, 12));
        disjointLabel.setText("disjunktní");
        disjointLabel.setVisible(false);

        disjointCombobox = new JComboBox<String>(Constants.generatedSubspacesDisjointOptionList);
        disjointCombobox.setBounds(220, 110, 60, 35);
        disjointCombobox.setBackground(new Color(214, 217, 223));
        disjointCombobox.setForeground(new Color(0, 0, 0));
        disjointCombobox.setEnabled(true);
        disjointCombobox.setFont(new Font("sansserif", 0, 12));
        disjointCombobox.setVisible(false);

        dimLabel = new JLabel();
        dimLabel.setBounds(156, 110, 240, 35);
        dimLabel.setBackground(new Color(255, 255, 255));
        dimLabel.setForeground(new Color(0, 0, 0));
        dimLabel.setEnabled(true);
        dimLabel.setFont(new Font("sansserif", 0, 12));
        dimLabel.setText("dim. průniku zaměření (nepovinné):");
        dimLabel.setVisible(false);

        dimSubLabel = new JLabel();
        dimSubLabel.setBounds(140, 110, 240, 35);
        dimSubLabel.setBackground(new Color(255, 255, 255));
        dimSubLabel.setForeground(new Color(0, 0, 0));
        dimSubLabel.setEnabled(true);
        dimSubLabel.setFont(new Font("sansserif", 0, 12));
        dimSubLabel.setText("dim. průniku podprostorů (nepovinné):");
        dimSubLabel.setVisible(false);

        intersectionDimensionTextField = new JTextField();
        intersectionDimensionTextField.setBounds(358, 110, 35, 35);
        intersectionDimensionTextField.setBackground(new Color(255, 255, 255));
        intersectionDimensionTextField.setForeground(new Color(0, 0, 0));
        intersectionDimensionTextField.setEnabled(true);
        intersectionDimensionTextField.setFont(new Font("sansserif", 0, 12));
        intersectionDimensionTextField.setText("");
        intersectionDimensionTextField.setVisible(false);

        intersectionInputLabel = new JLabel();
        intersectionInputLabel.setBounds(20, 110, 290, 35);
        intersectionInputLabel.setBackground(new Color(255, 255, 255));
        intersectionInputLabel.setForeground(new Color(0, 0, 0));
        intersectionInputLabel.setEnabled(true);
        intersectionInputLabel.setFont(new Font("sansserif", 0, 12));
        intersectionInputLabel.setText("Průnik zadejte jako podprostor do pole výsledek");
        intersectionInputLabel.setVisible(false);

        validateIntersectionInputButton = new JButton();
        validateIntersectionInputButton.setBounds(293, 115, 80, 25);
        validateIntersectionInputButton.setBackground(new Color(214, 217, 223));
        validateIntersectionInputButton.setForeground(new Color(0, 0, 0));
        validateIntersectionInputButton.setEnabled(true);
        validateIntersectionInputButton.setFont(new Font("sansserif", 0, 12));
        validateIntersectionInputButton.setText("ověřit");
        validateIntersectionInputButton.setVisible(false);
        validateIntersectionInputButton.addActionListener(new ActionListener() {
                                          public void actionPerformed(ActionEvent evt) {
                                              try {
                                                  String in = outputTextarea.getText();
                                                  Subspace subspacesIntersection = inputReader.getSubspace(in);
                                                  if (subspacesIntersection.isNull()){
                                                      //subspaces intersection input format is not valid (application core threw an exception during parsing input)
                                                      // or the matrix is not solvable (a set of equations is not solvable
                                                      activateRedCircle(intersectionInputInvalidRedCircle);
                                                      return;
                                                  }

                                                  TeXIcon formulaGraphics = subspacesIntersection.getTeXFormulaGraphics();
                                                  showTeXImageWindow("Průnik podprostorů", formulaGraphics);
                                              } catch (Exception e) {
                                                  //an exception was raised during subspace input parsing or during parsing its TeX representation while TeX image was created
                                                  activateRedCircle(intersectionInputInvalidRedCircle);
                                              }
                                          }
                                      }
        );

        intersectionInputInvalidRedCircle = new JLabel(new ImageIcon(smallRedCircle));
        intersectionInputInvalidRedCircle.setBounds(382, 122, 20, 20);
        intersectionInputInvalidRedCircle.setVisible(false);

        outputPanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "výstup pro řešení");
        outputPanel.setBorder(titledBorder);
        outputPanel.setBounds(25, 383, 584, 85);
        outputPanel.setBackground(new Color(192, 192, 192));
        outputPanel.setForeground(new Color(0, 0, 0));
        outputPanel.setEnabled(true);
        outputPanel.setFont(new Font("sansserif", 0, 12));
        outputPanel.setVisible(true);
        outputPanel.setLayout(null);

        outputLabel = new JLabel();
        outputLabel.setBounds(5, 20, 55, 60);
        outputLabel.setBackground(new Color(255, 255, 255));
        outputLabel.setForeground(new Color(0, 0, 0));
        outputLabel.setEnabled(true);
        outputLabel.setFont(new Font("sansserif", 0, 12));
        outputLabel.setText("Výsledek:");
        outputLabel.setVisible(true);

        outputTextarea = new JTextArea();
        outputTextarea.setBackground(new Color(255, 255, 255));
        outputTextarea.setForeground(new Color(0, 0, 0));
        outputTextarea.setEnabled(false);
        outputTextarea.setDisabledTextColor(new Color(0, 0, 0));
        outputTextarea.setFont(new Font("sansserif", 0, 12));
        outputTextarea.setText("Zatím není k dispozici žádný výsledek.");
        outputTextarea.setVisible(true);

        outputScroll = new JScrollPane(outputTextarea);
        outputScroll.setBounds(60, 20, 360, 60);
        outputScroll.setBackground(new Color(255, 255, 255));
        outputScroll.setForeground(new Color(0, 0, 0));
        outputScroll.setEnabled(true);
        outputScroll.setBorder(BorderFactory.createBevelBorder(1));
        outputScroll.setVisible(true);

        textOutputButton = new JButton();
        textOutputButton.setBounds(423, 20, 75, 60);
        textOutputButton.setBackground(new Color(214, 217, 223));
        textOutputButton.setForeground(new Color(0, 0, 0));
        textOutputButton.setEnabled(false);
        textOutputButton.setFont(new Font("sansserif", 0, 12));
        textOutputButton.setText("<html><center>Postup<br>(text)</center></html>");
        textOutputButton.setVisible(true);
        textOutputButton.addActionListener(new ActionListener() {
                                               public void actionPerformed(ActionEvent evt) {
                                                   openTextOutputFile();
                                               }
                                           }
        );

        pdfOutputButton = new JButton();
        pdfOutputButton.setBounds(501, 20, 75, 60);
        pdfOutputButton.setBackground(new Color(214, 217, 223));
        pdfOutputButton.setForeground(new Color(0, 0, 0));
        pdfOutputButton.setEnabled(false);
        pdfOutputButton.setFont(new Font("sansserif", 0, 12));
        pdfOutputButton.setText("<html><center>Postup<br>(pdf)</center></html>");
        pdfOutputButton.setVisible(true);
        pdfOutputButton.addActionListener(new ActionListener() {
                                              public void actionPerformed(ActionEvent evt) {
                                                  try {
                                                      ioHelper.generateOutputPDF();
                                                      tryOpenPDF();
                                                  } catch (IOException ex) {
                                                      JOptionPane.showMessageDialog(null, "Nebyl nalezen program pdflatex k vytváření PDF souborů. Nainstalujte prosím LaTex/MiKTeX!", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                  } catch (InterruptedException ex) {
                                                      JOptionPane.showMessageDialog(null, "Proces byl přerušen v průběhu generování pdf.", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                  }
                                              }
                                          }
        );

        generatePanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "parametry podprostorů");
        generatePanel.setBorder(titledBorder);
        generatePanel.setBounds(438, 222, 170, 160);
        generatePanel.setBackground(new Color(214, 217, 223));
        generatePanel.setForeground(new Color(0, 0, 0));
        generatePanel.setEnabled(true);
        generatePanel.setFont(new Font("sansserif", 0, 12));
        generatePanel.setVisible(false);
        generatePanel.setLayout(null);

        firstGeneratePanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "podpr. 1");
        firstGeneratePanel.setBorder(titledBorder);
        firstGeneratePanel.setBounds(5, 15, 80, 110);
        firstGeneratePanel.setBackground(new Color(214, 217, 223));
        firstGeneratePanel.setForeground(new Color(0, 0, 0));
        firstGeneratePanel.setEnabled(true);
        firstGeneratePanel.setFont(new Font("sansserif", 0, 12));
        firstGeneratePanel.setVisible(true);
        firstGeneratePanel.setLayout(null);

        firstGeneralRadioButton = new JRadioButton();
        firstGeneralRadioButton.setBounds(5, 15, 70, 30);
        firstGeneralRadioButton.setBackground(new Color(214, 217, 223));
        firstGeneralRadioButton.setForeground(new Color(0, 0, 0));
        firstGeneralRadioButton.setEnabled(true);
        firstGeneralRadioButton.setFont(new Font("sansserif", 0, 12));
        firstGeneralRadioButton.setText("obecné");
        firstGeneralRadioButton.setVisible(true);
        firstGeneralRadioButton.setSelected(true);

        firstParametricRadioButton = new JRadioButton();
        firstParametricRadioButton.setBounds(5, 45, 70, 30);
        firstParametricRadioButton.setBackground(new Color(214, 217, 223));
        firstParametricRadioButton.setForeground(new Color(0, 0, 0));
        firstParametricRadioButton.setEnabled(true);
        firstParametricRadioButton.setFont(new Font("sansserif", 0, 12));
        firstParametricRadioButton.setText("param.");
        firstParametricRadioButton.setVisible(true);

        ButtonGroup subspace1 = new ButtonGroup();
        subspace1.add(firstGeneralRadioButton);
        subspace1.add(firstParametricRadioButton);

        dim1Label = new JLabel();
        dim1Label.setBounds(5, 75, 70, 30);
        dim1Label.setBackground(new Color(214, 217, 223));
        dim1Label.setForeground(new Color(0, 0, 0));
        dim1Label.setEnabled(true);
        dim1Label.setFont(new Font("sansserif", 0, 12));
        dim1Label.setText("dim.:");
        dim1Label.setVisible(true);

        dim1Textfield = new JTextField();
        dim1Textfield.setBounds(35, 75, 40, 30);
        dim1Textfield.setBackground(new Color(255, 255, 255));
        dim1Textfield.setForeground(new Color(0, 0, 0));
        dim1Textfield.setEnabled(true);
        dim1Textfield.setFont(new Font("sansserif", 0, 12));
        dim1Textfield.setText("");
        dim1Textfield.setVisible(true);

        secondGeneratePanel = new JPanel();
        titledBorder = BorderFactory.createTitledBorder(blackLineBorder, "podpr. 2");
        secondGeneratePanel.setBorder(titledBorder);
        secondGeneratePanel.setBounds(85, 15, 80, 110);
        secondGeneratePanel.setBackground(new Color(214, 217, 223));
        secondGeneratePanel.setForeground(new Color(0, 0, 0));
        secondGeneratePanel.setEnabled(true);
        secondGeneratePanel.setFont(new Font("sansserif", 0, 12));
        secondGeneratePanel.setVisible(true);
        secondGeneratePanel.setLayout(null);

        secondGeneralRadioButton = new JRadioButton();
        secondGeneralRadioButton.setBounds(5, 15, 70, 30);
        secondGeneralRadioButton.setBackground(new Color(214, 217, 223));
        secondGeneralRadioButton.setForeground(new Color(0, 0, 0));
        secondGeneralRadioButton.setEnabled(true);
        secondGeneralRadioButton.setFont(new Font("sansserif", 0, 12));
        secondGeneralRadioButton.setText("obecné");
        secondGeneralRadioButton.setVisible(true);
        secondGeneralRadioButton.setSelected(true);

        secondParametricRadioButton = new JRadioButton();
        secondParametricRadioButton.setBounds(5, 45, 70, 30);
        secondParametricRadioButton.setBackground(new Color(214, 217, 223));
        secondParametricRadioButton.setForeground(new Color(0, 0, 0));
        secondParametricRadioButton.setEnabled(true);
        secondParametricRadioButton.setFont(new Font("sansserif", 0, 12));
        secondParametricRadioButton.setText("param.");
        secondParametricRadioButton.setVisible(true);

        ButtonGroup subspace2 = new ButtonGroup();
        subspace2.add(secondGeneralRadioButton);
        subspace2.add(secondParametricRadioButton);

        dim2Label = new JLabel();
        dim2Label.setBounds(5, 75, 70, 30);
        dim2Label.setBackground(new Color(214, 217, 223));
        dim2Label.setForeground(new Color(0, 0, 0));
        dim2Label.setEnabled(true);
        dim2Label.setFont(new Font("sansserif", 0, 12));
        dim2Label.setText("dim.:");
        dim2Label.setVisible(true);

        dim2Textfield = new JTextField();
        dim2Textfield.setBounds(35, 75, 40, 30);
        dim2Textfield.setBackground(new Color(255, 255, 255));
        dim2Textfield.setForeground(new Color(0, 0, 0));
        dim2Textfield.setEnabled(true);
        dim2Textfield.setFont(new Font("sansserif", 0, 12));
        dim2Textfield.setText("");
        dim2Textfield.setVisible(true);

        DIMLabel = new JLabel();
        DIMLabel.setBounds(5, 125, 160, 30);
        DIMLabel.setBackground(new Color(214, 217, 223));
        DIMLabel.setForeground(new Color(0, 0, 0));
        DIMLabel.setEnabled(true);
        DIMLabel.setFont(new Font("sansserif", 0, 12));
        DIMLabel.setText("dim. celého prostoru:");
        DIMLabel.setVisible(true);

        wholeSubspaceDimensionTextField = new JTextField();
        wholeSubspaceDimensionTextField.setBounds(125, 125, 40, 30);
        wholeSubspaceDimensionTextField.setBackground(new Color(255, 255, 255));
        wholeSubspaceDimensionTextField.setForeground(new Color(0, 0, 0));
        wholeSubspaceDimensionTextField.setEnabled(true);
        wholeSubspaceDimensionTextField.setFont(new Font("sansserif", 0, 12));
        wholeSubspaceDimensionTextField.setText("");
        wholeSubspaceDimensionTextField.setVisible(true);

        //adding components to contentPane panel
        inputPanel.add(libraryPanel);
        libraryPanel.add(loadSelectedAssignmentFromLibraryButton);
        libraryPanel.add(closeLibraryButton);
        libraryPanel.add(deleteSelectedAssignmentFromLibraryButton);
        contentPane.add(inputPanel);
        inputPanel.add(firstInputTitleButton);
        inputPanel.add(firstInputInvalidRedCircle);
        inputPanel.add(firstInputScroll);
        inputPanel.add(secondInputTitleButton);
        inputPanel.add(secondInputInvalidRedCircle);
        inputPanel.add(secondInputScroll);
        contentPane.add(taskPanel);
        taskPanel.add(solve_AssignmentTypeCombobox);
        taskPanel.add(solveButton);
        taskPanel.add(generate_AssignmentRelationTypeCombobox);
        taskPanel.add(generateButton);
        taskPanel.add(distanceLabel);
        taskPanel.add(distanceTextfield);
        taskPanel.add(angleLabel);
        taskPanel.add(angleCombobox);
        taskPanel.add(relationCombobox);
        taskPanel.add(disjointLabel);
        taskPanel.add(disjointCombobox);
        taskPanel.add(intersectionDimensionTextField);
        taskPanel.add(dimLabel);
        taskPanel.add(dimSubLabel);
        taskPanel.add(validateIntersectionInputButton);
        taskPanel.add(intersectionInputInvalidRedCircle);
        taskPanel.add(intersectionInputLabel);
        contentPane.add(generatePanel);
        generatePanel.add(firstGeneratePanel);
        firstGeneratePanel.add(firstGeneralRadioButton);
        firstGeneratePanel.add(firstParametricRadioButton);
        firstGeneratePanel.add(dim1Label);
        firstGeneratePanel.add(dim1Textfield);
        generatePanel.add(secondGeneratePanel);
        secondGeneratePanel.add(secondGeneralRadioButton);
        secondGeneratePanel.add(secondParametricRadioButton);
        secondGeneratePanel.add(dim2Label);
        secondGeneratePanel.add(dim2Textfield);
        generatePanel.add(DIMLabel);
        generatePanel.add(wholeSubspaceDimensionTextField);
        contentPane.add(outputPanel);
        outputPanel.add(outputLabel);
        outputPanel.add(outputScroll);
        outputPanel.add(textOutputButton);
        outputPanel.add(pdfOutputButton);
        //adding panel to JFrame and seting of window position and close operation
        getContentPane().add(contentPane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
    } //END constructor


    /**
     * Shows a little red circle for 3 seconds as an indication of invalid input
     */
    private void activateRedCircle(JLabel redCircle){
        redCircle.setVisible(true);
        Timer t = new Timer(RED_CIRCLE_TIMEOUT_MILISECONDS, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                redCircle.setVisible(false);
            }
        }
        );
        t.setRepeats(false);
        t.start();
    }

    private void showTeXImageWindow(String title, TeXIcon formulaGraphics){
        JLabel texLabel = new JLabel(formulaGraphics);
        JFrame texFrame = new JFrame(title);
        texFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        texFrame.getContentPane().add(texLabel, BorderLayout.CENTER);
        texFrame.setLocationRelativeTo(firstInputTextarea);
        texFrame.pack();
        texFrame.setVisible(true);
    }

    /**
     * Invoked by pressing "Vygeneruj" button
     * 1) checks assignment generate input
     * 2) generates the assignment
     * 3) prints generated assignment to input1 and input2 text fields
     *
     * Shows error message dialog on any invalid input
     */
    private void generate(String task) {
        boolean generateFirst = true, generateSecond = true;
        int dim1 = 0, dim2 = 0, wholeSpaceDimension;

        try {
            try {
                dim1 = Integer.parseInt(dim1Textfield.getText());
            } catch (NumberFormatException exception) {
                generateFirst = false;
            }
            try {
                dim2 = Integer.parseInt(dim2Textfield.getText());
            } catch (NumberFormatException exception) {
                generateSecond = false;
            }
            if (!generateFirst && !generateSecond) throw new NumberFormatException();
            wholeSpaceDimension = Integer.parseInt(wholeSubspaceDimensionTextField.getText());
            if (dim1 < 0 || dim2 < 0 || wholeSpaceDimension < 0) throw new NumberFormatException();

            if (dim1 > wholeSpaceDimension || dim2 > wholeSpaceDimension)
                throw new IllegalArgumentException("Subspace cannot have dimension larger than its superspace.");
            if ((dim1 == wholeSpaceDimension && firstGeneralRadioButton.isSelected()) || (dim2 == wholeSpaceDimension) && secondGeneralRadioButton.isSelected())
                throw new IllegalArgumentException("Whole space does not have a general form available.");
        } catch (NumberFormatException exception) {
            //if subspace 1 or subspace 2 dimensions are not set or any dimension is a negative number or whole space dimension is NaN
            JOptionPane.showMessageDialog(null, "Do políček označujících dimenze podprostorů a prostorů pro generování je nutné vepsat nezáporná čísla."
                            + (!generateFirst || !generateSecond ?
                            "\n(Pozn.: při generování náhodných podprostorů: pokud zadáte pouze jednu z dimenzí podprostorů, bude vygenerován jen tento podprostor.)" : ""),
                    "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        } catch (IllegalArgumentException exception) {
            if (exception.getMessage().equals("Subspace cannot have dimension larger than its superspace."))
                JOptionPane.showMessageDialog(null, "Dimenze podprostoru nemůže být větší než dimenze celého prostoru!", "Chyba", JOptionPane.ERROR_MESSAGE);
            if (exception.getMessage().equals("Whole space does not have a general form available."))
                JOptionPane.showMessageDialog(null, "Celý prostor nemá obecné vyjádření, takže dimenze podprostoru musí být při volbě obecného vyjádření "
                        + "menší než dimenze celého prostoru!", "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        //dimensions for assignment generation are loaded

        if (task.equals("Vyber úlohu:")) return; //if "Vygeneruj" button is pressed while "Vyber úlohu:" is selected (shall never happen)

        boolean firstParam = firstParametricRadioButton.isSelected();
        boolean secondParam = secondParametricRadioButton.isSelected();

        GenerateSubspacesResultDTO result = null;

        //the assignment generation type "náhodné prostory" does not require both subspaces but other types require both subspaces
        if (task.equals(generateTaskList[1])) {
            result = assignmentGenerator.validateAndGenerateRandomSubspaces(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension);
        }

        if (task.equals(generateTaskList[4])) {
            result = assignmentGenerator.validateAndGenerateSubspacesWithGivenDistance(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, distanceTextfield.getText());
        }

        if (task.equals(generateTaskList[5])) {
            result = assignmentGenerator.validateAndGenerateSubspacesWithGivenAngle(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, angleCombobox.getSelectedItem().toString());
        }

        if (task.equals(generateTaskList[2])) {
            result = assignmentGenerator.validateAndGenerateSubspacesWithGivenRelation(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, relationCombobox.getSelectedItem().toString(),
                    disjointCombobox.getSelectedItem().toString(), intersectionDimensionTextField.getText());
        }

        if (task.equals(generateTaskList[3])) {
           result = assignmentGenerator.validateAndGenerateSubspacesWithGivenIntersection(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, outputTextarea.getText());
        }

        if(result == null){
            JOptionPane.showMessageDialog(null, "Zvolena neplatná úloha pro generování!",
                    "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if(result.isErrorSet()){
            JOptionPane.showMessageDialog(null, generateSubspaceErrorMessages[result.getErrorMessageIndex()],"Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String generatedFirst = result.getGeneratedFirstSubspaceRepresentation();
        String generatedSecond = result.getGeneratedSecondSubspaceRepresentation();

        firstInputTextarea.setText(generatedFirst);
        secondInputTextarea.setText(generatedSecond);
    }


    /**
     * Initializes various menus and buttons
     */
    private void generateMenu() {
        menuBar = new JMenuBar();
        JMenu libraryMenu = new JMenu("Knihovna příkladů");
        JMenu helpMenu = new JMenu("Nápověda");

        JMenuItem help = new JMenuItem("Nápověda k zadávání podprostorů");
        help.addActionListener(new ActionListener() {
                                   public void actionPerformed(ActionEvent evt) { //register onclick action
                                       JOptionPane.showMessageDialog(null,
                                               Constants.INPUT_HELP_WINDOW_TEXT,
                                               "Nápověda k zadávání podprostorů", JOptionPane.INFORMATION_MESSAGE);
                                   }
                               }
        );
        JMenuItem library = new JMenuItem("Nahrát příklad z knihovny");
        library.addActionListener(new ActionListener() {
                                      public void actionPerformed(ActionEvent evt) { //register onclick action
                                          if (IOHelper.libraryExists()) {
                                              ArrayList<String> assignmentNames = getAssignmentNames();
                                              libraryCombobox = new JComboBox<String>(assignmentNames.toArray(new String[assignmentNames.size()]));

                                              libraryCombobox.setBounds(20, 30, 260, 30);
                                              libraryCombobox.setBackground(new Color(214, 217, 223));
                                              libraryCombobox.setForeground(new Color(0, 0, 0));
                                              libraryCombobox.setEnabled(true);
                                              libraryCombobox.setFont(new Font("sansserif", 0, 12));
                                              libraryCombobox.setVisible(true);

                                              libraryPanel.add(libraryCombobox);

                                              libraryPanel.setVisible(true);
                                              //libraryCombobox.setVisible(true);
                                              loadSelectedAssignmentFromLibraryButton.setVisible(true);
                                              deleteSelectedAssignmentFromLibraryButton.setVisible(false);
                                          } else {
                                              int yesNo = JOptionPane.showConfirmDialog(null, "Knihovna " + LIBRARY_FILE_PATH + " nenalezena, chcete ji vytvořit?", "Chyba knihovny!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                              if (yesNo == JOptionPane.YES_OPTION) {
                                                  try{
                                                      ioHelper.libraryCreate();
                                                      JOptionPane.showMessageDialog(null, "Knihovna příkladů byla vytvořena.", "Vytváření knihovny", JOptionPane.PLAIN_MESSAGE);
                                                  } catch (IOException ex) {
                                                      JOptionPane.showMessageDialog(null, "Chyba při vytváření " + LIBRARY_FILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                  }
                                              }
                                          }
                                      }
                                  }
        );
        JMenuItem librarySave = new JMenuItem("Uložit příklad do knihovny");
        librarySave.addActionListener(new ActionListener() {
                                          public void actionPerformed(ActionEvent evt) { //register onclick action listener
                                              if (IOHelper.libraryExists()) {
                                                  try {
                                                      saveNewAssignmentIntoLibrary();
                                                  } catch (Exception e) {
                                                      JOptionPane.showMessageDialog(null, "Chyba při zapisování do " + LIBRARY_FILE_PATH, "Chyba", JOptionPane.ERROR_MESSAGE);
                                                  }
                                              } else {
                                                  int yesNo = JOptionPane.showConfirmDialog(null, "Knihovna " + LIBRARY_FILE_PATH + " nenalezena, chcete ji vytvořit?", "Chyba knihovny!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                                  if(yesNo == JOptionPane.YES_OPTION){
                                                      try{
                                                          ioHelper.libraryCreate();
                                                          JOptionPane.showMessageDialog(null, "Knihovna příkladů byla vytvořena.", "Vytváření knihovny", JOptionPane.PLAIN_MESSAGE);
                                                      } catch (IOException ex) {
                                                          JOptionPane.showMessageDialog(null, "Chyba při vytváření " + LIBRARY_FILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                      }
                                                  }
                                              }
                                          }
                                      }
        );
        JMenuItem libraryDelete = new JMenuItem("Odstranit příklad z knihovny");
        libraryDelete.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent evt) {
                                                if (IOHelper.libraryExists()) {
                                                    ArrayList<String> assignmentNames = new ArrayList<String>();
                                                    try {
                                                        assignmentNames = getAssignmentNames();
                                                    } catch (Exception e) {
                                                        JOptionPane.showMessageDialog(null,
                                                                e.getStackTrace()[0].getLineNumber() + " Chyba v syntaxi knihovny, zkontorolujte obsah souboru " + LIBRARY_FILE_PATH + " podle návodu v " + LIBRARY_HELPFILE_PATH, "Chyba", JOptionPane.ERROR_MESSAGE);
                                                    }

                                                    libraryCombobox = new JComboBox<String>(assignmentNames.toArray(new String[assignmentNames.size()]));

                                                    libraryCombobox.setBounds(20, 30, 260, 30);
                                                    libraryCombobox.setBackground(new Color(214, 217, 223));
                                                    libraryCombobox.setForeground(new Color(0, 0, 0));
                                                    libraryCombobox.setEnabled(true);
                                                    libraryCombobox.setFont(new Font("sansserif", 0, 12));
                                                    libraryCombobox.setVisible(true);

                                                    libraryPanel.add(libraryCombobox);

                                                    libraryPanel.setVisible(true);
                                                    //libraryCombobox.setVisible(true);
                                                    loadSelectedAssignmentFromLibraryButton.setVisible(false);
                                                    deleteSelectedAssignmentFromLibraryButton.setVisible(true);
                                                } else {
                                                    int yesNo = JOptionPane.showConfirmDialog(null, "Knihovna " + LIBRARY_FILE_PATH + " nenalezena, chcete ji vytvořit?", "Chyba knihovny!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                                    if (yesNo == JOptionPane.YES_OPTION) {
                                                        try{
                                                            ioHelper.libraryCreate();
                                                            JOptionPane.showMessageDialog(null, "Knihovna příkladů byla vytvořena.", "Vytváření knihovny", JOptionPane.PLAIN_MESSAGE);
                                                        } catch (IOException ex) {
                                                            JOptionPane.showMessageDialog(null, "Chyba při vytváření " + LIBRARY_FILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                        }
                                                    }
                                                }
                                            }
                                        }
        );
        JMenuItem libraryCreate = new JMenuItem("Vytvořit ukázkovou knihovnu příkladů");
        libraryCreate.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent evt) {
                                                if (IOHelper.libraryExists()) {
                                                    int yesNo = JOptionPane.showConfirmDialog(null, "Knihovna " + LIBRARY_FILE_PATH + " již existuje, vytvořením nové knihovny vymažete veškerý její obsah. Přejete si vytvořit novou knihovnu?", "Vymazat knihovnu?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                                    if (yesNo == JOptionPane.YES_OPTION) {
                                                        try {
                                                            ioHelper.libraryCreate();
                                                            JOptionPane.showMessageDialog(null, "Knihovna příkladů byla vytvořena.", "Vytváření knihovny", JOptionPane.PLAIN_MESSAGE);
                                                        } catch (IOException ex) {
                                                            JOptionPane.showMessageDialog(null, "Chyba při vytváření " + LIBRARY_FILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                        }
                                                    }
                                                } else {
                                                    try {
                                                        ioHelper.libraryCreate();
                                                        JOptionPane.showMessageDialog(null, "Knihovna příkladů byla vytvořena.", "Vytváření knihovny", JOptionPane.PLAIN_MESSAGE);
                                                    } catch (IOException ex) {
                                                        JOptionPane.showMessageDialog(null, "Chyba při vytváření " + LIBRARY_FILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                    }
                                                }
                                            }
                                        }
        );
        JMenuItem libraryCreateHelpFile = new JMenuItem("Vytvořit návod ke knihovně");
        libraryCreateHelpFile.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent evt) {
                                                if (IOHelper.libraryHelpFileExists()) {
                                                    int yesNo = JOptionPane.showConfirmDialog(null, "Návod " + LIBRARY_HELPFILE_PATH + " již existuje, chcete jej obnovit?", "Přepsat soubor?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                                    if (yesNo == JOptionPane.YES_OPTION) {
                                                        try {
                                                            ioHelper.libraryHelpCreate();
                                                            JOptionPane.showMessageDialog(null, "Návod ke knihovně příkladů byl vytvořen (soubor " + LIBRARY_HELPFILE_PATH + ").", "Vytváření návodu", JOptionPane.INFORMATION_MESSAGE);
                                                        } catch (IOException ex) {
                                                            JOptionPane.showMessageDialog(null, "Chyba při přepisu " + LIBRARY_HELPFILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                        }
                                                    }
                                                } else {
                                                    try {
                                                        ioHelper.libraryHelpCreate();
                                                        JOptionPane.showMessageDialog(null, "Návod ke knihovně příkladů byl vytvořen (soubor " + LIBRARY_HELPFILE_PATH + ").", "Vytváření návodu", JOptionPane.INFORMATION_MESSAGE);
                                                    } catch (IOException ex) {
                                                        JOptionPane.showMessageDialog(null, "Chyba při vytváření " + LIBRARY_HELPFILE_PATH + ".", "Chyba", JOptionPane.ERROR_MESSAGE);
                                                    }
                                                }
                                            }
                                        }
        );

        menuBar.add(helpMenu);
        menuBar.add(libraryMenu);
        helpMenu.add(help);
        libraryMenu.add(libraryCreate);
        libraryMenu.add(libraryCreateHelpFile);
        libraryMenu.add(library);
        libraryMenu.add(librarySave);
        libraryMenu.add(libraryDelete);
    }

    /**
     * Loads assignment names from the current library file state to be used to display list of assignments to choose one to load
     * Shows message dialog on read error
     */
    private ArrayList<String> getAssignmentNames(){
        String libraryContents = new String();
        try {
            libraryContents = ioHelper.loadLibraryForGUIDesktopApp();
        } catch (IOException ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
        }
        return IOHelper.getAssignmentNamesFromLibrary(libraryContents);
    }

    /**
     * Loads an assignment according to currently selected index in the assignment names combobox
     * Assignment is validated if it can be directly used to solve, if invalid, an error dialog is shown
     */
    private void loadAssignmentBySelectedComboboxIndex(){
        String libraryContents = new String();
        try {
            libraryContents = ioHelper.loadLibraryForGUIDesktopApp();
        } catch (IOException ex){
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
        }

        Assignment chosenAssignment = IOHelper.loadAssignmentByIndex(libraryCombobox.getSelectedIndex(), libraryContents);
        AssignmentValidityCheckDTO validityCheck = chosenAssignment.checkLogicalValidity();
        if(validityCheck.generatedAnyErrorMessage()){
            JOptionPane.showMessageDialog(null, validityCheck.getErrorMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }

        solve_AssignmentTypeCombobox.setSelectedIndex(chosenAssignment.getType());

        firstInputTextarea.setText(String.join("\n", chosenAssignment.getSubspace1Lines()));
        secondInputTextarea.setText(String.join("\n", chosenAssignment.getSubspace2Lines()));
    }

    /**
     * Checks user input and if valid, then saves input as a new assignment into the library,
     * otherwise shows an error dialog
     */
    private void saveNewAssignmentIntoLibrary(){
        int assignmentType = solve_AssignmentTypeCombobox.getSelectedIndex();
        if(assignmentType == 0) {
            JOptionPane.showMessageDialog(null, "Musíte vybrat typ příkladu.", "Chyba!", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        String task = solve_AssignmentTypeCombobox.getSelectedItem().toString();
        String input1 = firstInputTextarea.getText();
        String input2 = secondInputTextarea.getText();

        SolveInputDTO inputToSolve = AssignmentSolver.loadInputToSolve(task, input1, input2, inputReader);
        if(inputToSolve.containsErrorMessage()){
            JOptionPane.showMessageDialog(null, inputToSolve.getErrorMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
            return;
        }
        //input was successfully parsed & validated

        String description = JOptionPane.showInputDialog(null, "Sem napište název/krátký popis ukládaného příkladu:",
                "Popis příkladu", JOptionPane.INFORMATION_MESSAGE);
        if (description == null) { //user cancelled the input
            return;
        } else if (description.isEmpty()){
            JOptionPane.showMessageDialog(null, "Musíte zadat název/popis příkladu.", "Chyba!", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        ArrayList<String> subspace1Lines = new ArrayList<>();
        ArrayList<String> subspace2Lines = new ArrayList<>();

        try(Scanner scanner = new Scanner(input1)){
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                subspace1Lines.add(line);
            }
        }

        try(Scanner scanner = new Scanner(input2)){
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                subspace2Lines.add(line);
            }
        }

        //save
        Assignment newAssignment = new Assignment(description, assignmentType, subspace1Lines, subspace2Lines);
        try{
            ioHelper.addAssignmentToLibrary(newAssignment);
        } catch (IOException ex){
            JOptionPane.showMessageDialog(null, "Nastal problém při ukládání knihovny: " + ex.getLocalizedMessage(), "Chyba!", JOptionPane.PLAIN_MESSAGE);
        }
        JOptionPane.showMessageDialog(null, "Zadání příkladu bylo uloženo do knihovny.", "Uložit zadání", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Get the selected index of assignment names from combobox and delete associated assignment from library
     */
    private void removeSelectedAssignmentFromLibrary(){
        int yesNo = JOptionPane.showConfirmDialog(null, "Opravdu chcete toto zadání odstranit?", "Odstranit zadání příkladu", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (yesNo == JOptionPane.YES_OPTION) {
            String libraryContents = new String();
            try {
                libraryContents = ioHelper.loadLibraryForGUIDesktopApp();
            } catch (IOException ex){
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
            }

            int taskIndex = libraryCombobox.getSelectedIndex();
            String editedLibrary = IOHelper.removeAssignmentFromLibraryByIndex(taskIndex, libraryContents);
            try (OutputStreamWriter textFile = new OutputStreamWriter(new FileOutputStream(LIBRARY_FILE_PATH), ioHelper.getDetectedLibraryCharset().newEncoder())){
                textFile.write(editedLibrary);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Nastal problém při ukládání knihovny: " + ex.getLocalizedMessage(), "Chyba!", JOptionPane.PLAIN_MESSAGE);
            }
            JOptionPane.showMessageDialog(null, "Zadání příkladu bylo odstraněno z knihovny.", "Odstranit zadání", JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * Open generated .txt file with results in default program
     */
    private void openTextOutputFile() {
        try {
            IOHelper.guaranteeDirectoryExistence(ioHelper.getOutputFolderPath());
        } catch (SecurityException ex){
            JOptionPane.showMessageDialog(null, "Program/uživatel nemá oprávnění k vytváření složek!", "Chyba", JOptionPane.ERROR_MESSAGE);
        }

        String textFilePath = Paths.get(ioHelper.getOutputFolderPath(), ioHelper.getOutputTxtFileName()).toString(); //.NET Path.Combine

        if (Desktop.isDesktopSupported()) {
            try {
                File outputTxt = new File(textFilePath);
                Desktop.getDesktop().open(outputTxt);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Nelze otevírat textové soubory!", "Chyba", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Open generated .pdf file with results in default program
     * If file cannot be opened (no available application), error message is displayed
     */
    private void tryOpenPDF() {
        try {
            IOHelper.guaranteeDirectoryExistence("results");
        } catch (SecurityException ex){
            JOptionPane.showMessageDialog(null, "Program/uživatel nemá oprávnění k vytváření složek!", "Chyba", JOptionPane.ERROR_MESSAGE);
        }

        String PDFFilePath = Paths.get(ioHelper.getOutputFolderPath(), ioHelper.getOutputPDFFileName()).toString(); //.NET Path.Combine

        if (Desktop.isDesktopSupported()) {
            try {
                File outputPDF = new File(PDFFilePath);
                Desktop.getDesktop().open(outputPDF); //open PDF in its default application
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "V tomto systému není přiřazena žádná aplikace k otevření souborů pdf!", "Chyba", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Clear anything inside output .txt and .tex files (cut contents to 0 bytes)
     */
    private void clearOutputFiles(){
        try {
            IOHelper.guaranteeDirectoryExistence("results");
        } catch (SecurityException ex){
            JOptionPane.showMessageDialog(null, "Program/uživatel nemá oprávnění k vytváření složek!", "Chyba", JOptionPane.ERROR_MESSAGE);
        }

        clearFile(Paths.get(ioHelper.getOutputFolderPath(), ioHelper.getOutputTxtFileName()).toString());
        clearFile(Paths.get(ioHelper.getOutputFolderPath(), ioHelper.getOutputTeXFileName()).toString());
    }

    /**
     * Clears anything inside specified file (cut contents to 0 bytes)<br>
     * An error message dialog is displayed if an IOException occurs<br>
     * The file is created if it doesn't exist
     */
    private void clearFile(String filePath) {
        try {
            IOHelper.clearFile(filePath);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Došlo k problému při vyprazdňování souboru " + filePath + ":\n" + ex.getLocalizedMessage(), "Chyba", JOptionPane.ERROR_MESSAGE);
        }
    }
}