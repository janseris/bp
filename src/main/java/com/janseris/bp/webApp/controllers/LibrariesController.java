package com.janseris.bp.webApp.controllers;

import com.janseris.bp.Assignment;
import com.janseris.bp.LibraryLogicalErrorException;
import com.janseris.bp.LibrarySyntaxErrorException;
import com.janseris.bp.businessLayer.LibraryService;
import com.janseris.bp.businessLayer.LibraryUploadResult;
import com.janseris.bp.businessLayer.UserService;
import com.janseris.bp.model.PublicAccessibility;
import com.janseris.bp.model.SQLResultOrderingMode;
import com.janseris.bp.model.LibraryOrderingColumn;
import com.janseris.bp.model.*;
import com.janseris.bp.webApp.ControllerUtilities;
import com.janseris.bp.webApp.model.LibraryUseDTO;
import com.janseris.bp.webApp.model.PublicLibraryShowDTO;
import com.janseris.bp.webApp.model.SaveAssignmentResultDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-20
 */

@Controller
public class LibrariesController {

    private final LibraryService service;
    private final UserService userService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public LibrariesController(LibraryService service, UserService userService) {
        this.service = service;
        this.userService = userService;
    }

    /**
     * Loads libraries accessible to public with defined ordering
     * @param request
     * @return
     */
    @RequestMapping(value = "/publicLibraries", method = RequestMethod.POST)
    public String showPublicLibrariesWithCustomOrdering(ModelMap model, HttpServletRequest request){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }

        String orderingColumn = request.getParameter("orderingColumn");
        String orderingModeStr = request.getParameter("orderingMode");

        LibraryOrderingColumn column;
        SQLResultOrderingMode orderingMode;
        try{
            column = service.getSelectedOrderingColumn(orderingColumn);
            orderingMode = service.getSelectedOrderingMode(orderingModeStr);
        } catch (IllegalArgumentException ex){
            log.warn("Invalid ordering option for listing public libraries was used");
            model.addAttribute("errorMessage","Byla zadána neplatná volba pro řazení knihoven."); //flash attributes are passed even when out of sessions
            return "publicLibraries";
        }

        //logic
        List<PublicLibraryShowDTO> publicLibraries;
        try {
            publicLibraries = service.getAllPublicLibraries(column, orderingMode);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading public libraries");
            model.addAttribute("errorMessage","Nastala chyba při komunikaci s databází při načítání knihoven. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "publicLibraries";
        }

        model.addAttribute("libraries", publicLibraries);
        return "publicLibraries";
    }

    @RequestMapping(value = "/publicLibraries", method = RequestMethod.GET)
    public String showPublicLibraries(ModelMap model, HttpServletRequest request){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }

        //logic
        List<PublicLibraryShowDTO> publicLibraries;
        try {
            publicLibraries = service.getAllPublicLibraries();
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading public libraries");
            model.addAttribute("errorMessage", "Nastala chyba při komunikaci s databází při načítání knihoven. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "publicLibraries";
        }

        model.addAttribute("libraries", publicLibraries);
        return "publicLibraries";
    }


    @RequestMapping(value = "/deleteLibrary", method = RequestMethod.POST)
    public String deleteLibrary(HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        //if user is not owner then deny form processing
        String libraryIdStr = request.getParameter("libraryId");
        UUID libraryId = UUID.fromString(libraryIdStr);
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);

        try{
            if(!service.isOwner(currentUser.getId(), libraryId)){
                log.warn("User " + currentUser.getId() + " was trying to delete someone else's library");
                redirectAttributes.addFlashAttribute("errorMessage", "Smazat knihovnu může jen uživatel, který ji vytvořil."); //flash attributes are passed even when out of sessions
                return "redirect:libraries";
            }
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during library " + libraryIdStr + " ownership check for deletion");
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází."); //flash attributes are passed even when out of sessions
            return "redirect:libraries";
        }

        //logic
        try{
            service.deleteLibrary(libraryId);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during library " + libraryIdStr + " deletion");
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází."); //flash attributes are passed even when out of sessions
            return "redirect:libraries";
        }

        redirectAttributes.addFlashAttribute("infoMessage", "Knihovna byla úspěšně smazána"); //flash attributes are passed even when out of sessions
        return "redirect:libraries";
    }


    @RequestMapping(value = "/editLibrary", method = RequestMethod.GET)
    public String showEditLibraryPage(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        //logic
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        String libraryIdStr = request.getParameter("libraryId");

        LibraryDTO editedLibraryData;
        try{
            UUID libraryId = UUID.fromString(libraryIdStr);
            if(!service.isOwner(currentUser.getId(), libraryId)){
                log.warn("User " + currentUser.getId() + " was trying to edit someone else's library");
                redirectAttributes.addFlashAttribute("errorMessage", "Upravit knihovnu může jen uživatel, který ji vytvořil."); //flash attributes are passed even when out of sessions
                return "redirect:libraries";
            }
            editedLibraryData = service.getLibraryDetails(libraryId);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during library " + libraryIdStr + " details retrieval");
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází."); //flash attributes are passed even when out of sessions
            return "redirect:libraries";
        }
        //prepare data to show in edit form
        model.addAttribute("library", editedLibraryData);
        model.addAttribute("libraryId", libraryIdStr);
        return "editLibrary";
    }

    private PublicAccessibility getSelectedLibraryAccessibility(String value) throws IllegalArgumentException{
        switch(value){
            case "private":
                return PublicAccessibility.PRIVATE;
            case "public":
                return PublicAccessibility.PUBLIC;
            default:
                throw new IllegalArgumentException("Zvolený typ veřejné přístupnosti knihovny není platný.");
        }
    }


    @RequestMapping(value = "/editLibrary", method = RequestMethod.POST)
    public String editLibrary(@RequestParam("libraryFile") MultipartFile libraryFile, HttpServletRequest request, RedirectAttributes redirectAttributes) throws IOException {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        //if user is not owner then deny form processing
        String libraryIdStr = request.getParameter("libraryId");
        UUID libraryId = UUID.fromString(libraryIdStr);
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);

        try{
            if(!service.isOwner(currentUser.getId(), libraryId)){
                log.warn("User " + currentUser.getId() + " was trying to edit someone else's library");
                redirectAttributes.addFlashAttribute("errorMessage", "Upravit knihovnu může jen uživatel, který ji vytvořil."); //flash attributes are passed even when out of sessions
                return "redirect:libraries";
            }
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during user " + currentUser.getId() + " ownership check of library " + libraryIdStr);
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází."); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        }

        //logic
        LibraryUpdateDTO updateData = new LibraryUpdateDTO();
        updateData.setId(libraryId);
        String libraryName = request.getParameter("name");
        if (libraryName.trim().isEmpty()) {
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zadejte název knihovny."); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        }
        updateData.setName(libraryName);
        PublicAccessibility accessibility;
        try{
            accessibility = getSelectedLibraryAccessibility(request.getParameter("publicAccessibility"));
        } catch (IllegalArgumentException ex){
            log.warn("User " + currentUser.getId() + " was trying to edit a library with providing invalid option for PublicAccessibility");
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Byla zadána neplatná volba pro typ přístupnosti knihovny."); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        }
        updateData.setPublicAccessibility(accessibility);

        //get contents
        String contentSourceType = request.getParameter("contentSourceType");

        if(contentSourceType.equals("textArea")){
            if(request.getParameter("textFieldAccessed").equals("true")){
                //WARNING: parameter name does not match name="..." in form input, instead, it is defined by th:field value for some unknown reason!
                updateData.setTextContents(request.getParameter("contents"));
            }
        } else if (contentSourceType.equals("file")){
            //empty file -> redirect with error message
            if(libraryFile.isEmpty()){
                redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
                redirectAttributes.addFlashAttribute("errorMessage", "Nezvolili jste textový soubor obsahující příklady."); //flash attributes are passed even when out of sessions
                return "redirect:editLibrary";
            }
            updateData.setContentsFile(libraryFile.getBytes());
        } else {
            log.warn("User " + currentUser.getId() + " was trying to edit a library with providing invalid option for library contents source (text or file)");
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Byla zadána neplatná volba pro zdroj obsahu knihovny při úpravě."); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        }


        //syntax check - Any input before first assignment (defined by Constants#ASSIGNMENT_DISTINGUISHING_WORD) is ignored (space for comments)
        LibraryUploadResult result = null;
        try {
            result = service.updateLibrary(updateData);
        } catch (LibrarySyntaxErrorException syntaxError){
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Chyba v syntaxi knihovny:<br>" + syntaxError.getMessage()); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        } catch (LibraryLogicalErrorException logicalError){
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Chyba v knihovně:<br>" + logicalError.getMessage().replaceAll("\n", "<br>")); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        }


        if (result == LibraryUploadResult.INVALID_ENCODING) {
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte soubor v kódování UTF-8 nebo Windows-1250."); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        } else if (result == LibraryUploadResult.ERROR){
            log.info("Database communication error emerged during library " + libraryIdStr + " updating from edit form");
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //view content is loaded using library id
            redirectAttributes.addFlashAttribute("errorMessage", "Nastala chyba při komunikaci s databází při úpravě knihovny. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "redirect:editLibrary";
        }

        if(result == LibraryUploadResult.SUCCESS_ONLINE_CHARSET_DETECTION){
            redirectAttributes.addFlashAttribute("infoMessage", "Knihovna " + libraryName + " byla úspěšně upravena.<br> Detekce kódování proběhla skrze projekt <a href='https://nlp.fi.muni.cz/projects/chared/'>Chared</a> (Centrum zpracování přirozeného jazyka FI MUNI)."); //flash attributes are passed even when out of sessions
        } else { //success, detection was not required or offline encoding detection had to be used
            redirectAttributes.addFlashAttribute("infoMessage", "Knihovna " + libraryName + " byla úspěšně upravena."); //flash attributes are passed even when out of sessions
        }
        return "redirect:libraries";
    }


    @RequestMapping(value = "/libraryDetails", method = RequestMethod.GET)
    public String showLibraryDetails(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        //logic
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        String libraryIdStr = request.getParameter("libraryId");
        UUID libraryId = UUID.fromString(libraryIdStr);
        LibraryDTO library;
        try {
            library = service.getLibraryDetails(libraryId);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading library " + libraryIdStr + " details");
            redirectAttributes.addFlashAttribute("errorMessage", "Nastala chyba při komunikaci s databází při načítání knihovny. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            if(currentUser == null){
                return "redirect:publicLibraries";
            }
            return "redirect:libraries";
        }

        if(library == null){
            log.warn("User " + (currentUser == null ? "anonymous" : currentUser.getId())  + " was trying view library details of a non-existing library " + libraryIdStr);
            redirectAttributes.addFlashAttribute("errorMessage", "Požadovaná knihovna neexistuje!"); //flash attributes are passed even when out of sessions
            return "redirect:error";
        }

        //does not happen during regular application use, only by using old links or sending manual requests
        if((library.getPublicAccessibility() == PublicAccessibility.PRIVATE) && (currentUser == null || (!library.getCreatorUserId().equals(currentUser.getId())))){
            log.info("User " + (currentUser == null ? "anonymous" : currentUser.getId())  + " was trying view library details of a private library " + libraryIdStr);
            redirectAttributes.addFlashAttribute("errorMessage", "Tato knihovna příkladů není veřejně přístupná."); //flash attributes are passed even when out of sessions
            return "redirect:libraries";
        }

        model.addAttribute("library", library);

        //show library creator with a link to user details
        UUID libraryCreatorId = library.getCreatorUserId();
        model.addAttribute("libraryCreatorId", libraryCreatorId);
        if(currentUser != null && currentUser.getId().equals(libraryCreatorId)){
            //current user is library owner -> loading user info from db is not necessary
            model.addAttribute("libraryCreatorName", currentUser.getName());
            model.addAttribute("libraryEditAvailable", true);
        } else {
            //current user is anonymous or not owner -> loading user info from db is necessary
            try {
                UserShowDTO creator = userService.showUserForPublic(libraryCreatorId);
                model.addAttribute("libraryCreatorName", creator.getName());
            } catch (SQLException ex){
                log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading user " + libraryCreatorId + " details");
                redirectAttributes.addFlashAttribute("errorMessage", "Nastala chyba při komunikaci s databází při načítání informací o vlastníkovi knihovny. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
                if(currentUser == null){
                    return "redirect:publicLibraries";
                }
                return "redirect:libraries";
            }
        }
        return "libraryDetails";
    }


    @RequestMapping(value = "/addLibrary", method = RequestMethod.GET)
    public String showAddLibraryPage(HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }
        return "addLibrary";
    }


    @RequestMapping(value = "/addLibrary", method = RequestMethod.POST)
    public String addLibrary(@RequestParam("libraryFile") MultipartFile libraryFile, HttpServletRequest request, RedirectAttributes redirectAttributes) throws IOException {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        //logic
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        String libraryName = request.getParameter("name");
        if (libraryName.trim().isEmpty()) {
            redirectAttributes.addFlashAttribute("previousLibraryAddAttemptName", libraryName); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zadejte název knihovny."); //flash attributes are passed even when out of sessions
            return "redirect:addLibrary";
        }
        if (libraryFile.isEmpty()) {
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte neprázdný soubor."); //flash attributes are passed even when out of sessions
            return "redirect:addLibrary";
        }
        byte[] libraryBytes = libraryFile.getBytes();


        //syntax check - Any input before first assignment (defined by Constants#ASSIGNMENT_DISTINGUISHING_WORD) is ignored (space for comments)
        LibraryUploadResult result = null;
        try {
            result = service.addLibrary(currentUser, libraryName, libraryBytes);
        } catch (LibrarySyntaxErrorException syntaxError){
            redirectAttributes.addFlashAttribute("previousLibraryAddAttemptName", libraryName); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Chyba v syntaxi knihovny:<br>" + syntaxError.getMessage()); //flash attributes are passed even when out of sessions
            return "redirect:addLibrary";
        } catch (LibraryLogicalErrorException logicalError){
            redirectAttributes.addFlashAttribute("previousLibraryAddAttemptName", libraryName); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Chyba v knihovně:<br>" + logicalError.getMessage().replaceAll("\n", "<br>")); //flash attributes are passed even when out of sessions
            return "redirect:addLibrary";
        }


        if (result == LibraryUploadResult.INVALID_ENCODING) {
            redirectAttributes.addFlashAttribute("previousLibraryAddAttemptName", libraryName); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte soubor v kódování UTF-8 nebo Windows-1250."); //flash attributes are passed even when out of sessions
            return "redirect:addLibrary";
        } else if (result == LibraryUploadResult.ERROR){
            log.info("Database communication error emerged during adding a library by user " + currentUser.getId());
            redirectAttributes.addFlashAttribute("previousLibraryAddAttemptName", libraryName); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Nastala chyba při komunikaci s databází při přidávání knihovny. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "redirect:addLibrary";
        }
        if(result == LibraryUploadResult.SUCCESS_ONLINE_CHARSET_DETECTION){
            redirectAttributes.addFlashAttribute("infoMessage", "Knihovna " + libraryName + " byla úspěšně přidána.<br> Detekce kódování proběhla skrze projekt <a href='https://nlp.fi.muni.cz/projects/chared/'>Chared</a> (Centrum zpracování přirozeného jazyka FI MUNI)."); //flash attributes are passed even when out of sessions
        } else { //success but offline charset detection
            log.info("Project Chared @ nlp.fi.muni.cz used for online encoding detection was not accessible");
            redirectAttributes.addFlashAttribute("infoMessage", "Knihovna " + libraryName + " byla úspěšně přidána."); //flash attributes are passed even when out of sessions
        }
        return "redirect:libraries";
    }

    @RequestMapping(value = "/libraries", method = RequestMethod.GET)
    public String showAllLibraries(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }

        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);


        List<LibraryDTO> userLibraries = new LinkedList<>(); //never null in response attributes
        try {
            userLibraries = service.getAllUserLibraries(currentUser.getId()); //default ordering = by library name
        } catch (SQLException ex) {
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading user " + currentUser.getId() + "'s libraries");
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při načítání knihoven z databáze."); //flash attributes are passed even when out of sessions
            return "redirect:error";
        }

        for (LibraryDTO userLibrary : userLibraries) {
            userLibrary.setContents(ControllerUtilities.trimLibraryContentsForPreview(userLibrary.getContents()));
        }

        model.put("libraries", userLibraries);
        return "libraries";
    }

    /*
    @RequestMapping(value = "/checkLibraryAjax", method = RequestMethod.POST)
    public ResponseEntity<CheckLibraryDTO> checkLibraryAjax(HttpServletRequest request){
        String input = request.getParameter("input");
        System.out.println(input);
        System.out.println("AJAX request content type: " + request.getHeader("Content-Type"));
        CheckSubspaceDTO dto = service.checkLibrarySyntax(session.getId(), input, true);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    */

    @RequestMapping(value = "/useLibrary", method = RequestMethod.POST)
    public String useLibrary(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        //load library and check access rights
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        String libraryIdStr = request.getParameter("libraryId");
        UUID libraryId = UUID.fromString(libraryIdStr);
        LibraryDTO library;
        try {
            library = service.getLibraryDetails(libraryId);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading library " + libraryIdStr + " for use in program");
            redirectAttributes.addFlashAttribute("libraryErrorMessage", "Nastala chyba při komunikaci s databází při načítání knihovny. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "redirect:program";
        }

        //does not happen during regular application use, only by using old links or sending manual requests
        if((library.getPublicAccessibility() == PublicAccessibility.PRIVATE) && (currentUser == null || (!library.getCreatorUserId().equals(currentUser.getId())))){
            log.info("User " + (currentUser == null ? "anonymous" : currentUser.getId())  + " had requested an other user's private library " + libraryIdStr + " for use in program");
            redirectAttributes.addFlashAttribute("libraryErrorMessage", "Tato knihovna příkladů není veřejně přístupná."); //flash attributes are passed even when out of sessions
            return "redirect:program";
        }

        try {
            //parse library and construct show DTO
            session.setAttribute("library", service.loadLibraryForUse(library, true));
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during updating library " + libraryIdStr + " LastUsedDate while it was being loaded for use in program");
            redirectAttributes.addFlashAttribute("libraryErrorMessage", "Nastala chyba při komunikaci s databází při aktualizování informací o knihovně. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "redirect:program";
        } catch (LibrarySyntaxErrorException syntaxError) { //shall not happen without hacking library contents into an invalid syntax state
            log.warn("An invalid library " + libraryIdStr + " was being parsed to be used in program (it shall have been valid!). Syntax error: " + syntaxError.getMessage());
            redirectAttributes.addFlashAttribute("libraryErrorMessage", "Chyba v syntaxi knihovny:<br>" + syntaxError.getMessage()); //flash attributes are passed even when out of sessions
            return "redirect:program";
        }

        return "program";
    }

    @RequestMapping(value = "/useSampleLibrary", method = RequestMethod.POST)
    public String useSampleLibrary(HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        LibraryUseDTO sampleLibraryToUse = service.loadSampleLibraryForUse(true);

        session.setAttribute("library", sampleLibraryToUse);

        return "redirect:program";
    }


    @RequestMapping(value = "/addSampleLibrary", method = RequestMethod.POST)
    public String addSampleLibraryForUser(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        ProgramController.assignOldFormValues(request, redirectAttributes);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("infoMessage", "Přidat ukázkovou knihovnu do svých knihoven mohou jen přihlášení uživatelé."); //flash attributes are passed even when out of sessions
            return "redirect:program";
        }

        UserDTO currentUser = (UserDTO)session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);

        try {
            service.addSampleLibraryForUser(currentUser);
        } catch (SQLException ex){
            log.warn("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged while trying to clone the sample library to user " + currentUser.getId() + "'s libraries\"");
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při přidávání ukázkové knihovny příkladů do vlastních knihoven.");
        }
        redirectAttributes.addFlashAttribute("infoMessage", "Ukázková knihovna příkladů byla úspěšně přidána do Vašich knihoven.");

        return "redirect:program";
    }


    @RequestMapping(value = "/downloadLibrary", method = RequestMethod.POST)
    public ResponseEntity<byte[]> downloadLibrary(HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession(false);
        if(!ControllerUtilities.isLoggedInUser(session)){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        UUID libraryId = UUID.fromString(request.getParameter("libraryId"));

        UserDTO currentUser = (UserDTO)session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        if(!service.isOwner(currentUser.getId(), libraryId)){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        LibraryDownloadDTO library = service.downloadLibrary(libraryId);
        System.out.println("Library obtained");
        System.out.println(library);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM); //bytes

        // Set the actual downloaded file name
        String filename = library.getName() + ".txt";

        headers.setContentDispositionFormData("attachment", filename); //force download file ("attachment" does not matter)
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(library.getContents(), headers, HttpStatus.OK);
    }


    @RequestMapping(value="/saveAssignment", method = RequestMethod.POST)
    public ResponseEntity<SaveAssignmentResultDTO> saveAssignmentIntoLibrary(HttpServletRequest request){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }

        SaveAssignmentResultDTO dto = new SaveAssignmentResultDTO();

        if(!ControllerUtilities.isLoggedInUser(request.getSession(false))){
            dto.setErrorMessage("Příklad si do některé ze svých knihoven můžou uložit jen přihlášení uživatelé.");
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }

        HttpSession session = request.getSession(false);
        Assignment assignment = (Assignment)session.getAttribute(ControllerUtilities.TEMPORARY_ASSIGNMENT_TO_BE_SAVED_INTO_LIBRARY_SESSION_ATTRIBUTE_NAME);
        String assignmentName = request.getParameter("savedAssignmentName");
        if(assignmentName.isEmpty()){
            dto.setInfoMessage("Prosím, zadejte nějaký název.");
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        assignment.setName(assignmentName);
        UserDTO currentUser = (UserDTO)session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        String libraryIdStr = request.getParameter("selectedLibraryId");
        UUID libraryId = UUID.fromString(libraryIdStr);
        try{
            if(!service.isOwner(currentUser.getId(), libraryId)){
                log.warn("User " + currentUser.getId() + " was trying to add an assignment to someone else's library");
                dto.setErrorMessage("Přidat příklad do knihovny může jen vlastník.");
                return new ResponseEntity<>(dto, HttpStatus.OK);
            }
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during library " + libraryIdStr + " ownership check for adding assignment");
            dto.setErrorMessage("Nastal problém při komunkaci s databází. Zkuste to, prosím, znovu.");
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }

        try{
            service.addAssignmentToLibrary(libraryId, assignment);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged trying to append a new assignment to library " + libraryIdStr);
            dto.setErrorMessage("Nastal problém při komunkaci s databází při ukládání příkladu. Zkuste to, prosím, znovu.");
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        dto.setInfoMessage("Příklad byl úspěšně uložen.");
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @RequestMapping(value = "/deleteLibraryAssignment", method = RequestMethod.GET)
    public String showDeleteAssignmentsPage(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        //logic
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        String libraryIdStr = request.getParameter("libraryId");

        LibraryDTO library;
        try{
            UUID libraryId = UUID.fromString(libraryIdStr);
            if(!service.isOwner(currentUser.getId(), libraryId)){
                log.warn("User " + currentUser.getId() + " was trying to edit someone else's library");
                redirectAttributes.addFlashAttribute("errorMessage", "Upravit knihovnu může jen uživatel, který ji vytvořil."); //flash attributes are passed even when out of sessions
                return "redirect:libraries";
            }
            library = service.getLibraryDetails(libraryId);
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during library " + libraryIdStr + " details retrieval");
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází."); //flash attributes are passed even when out of sessions
            return "redirect:libraries";
        }

        LibraryUseDTO libraryForUse = null;

        try{
            libraryForUse = service.loadLibraryForUse(library, false);
        } catch (SQLException ex) {
            log.info("Database communication error " + ex.getLocalizedMessage() + " emerged during loading library " + libraryIdStr + " for deleting assignments");
            redirectAttributes.addFlashAttribute("libraryErrorMessage", "Nastala chyba při komunikaci s databází při načítání knihovny. Zkuste to prosím, znovu."); //flash attributes are passed even when out of sessions
            return "redirect:program";
        }

        //prepare data to show in form
        model.addAttribute("library", libraryForUse);
        model.addAttribute("libraryId", libraryIdStr);
        return "deleteLibraryAssignment";
    }


    //in redirect to the same path, some parameters must be passed as normal and some as flash
    @RequestMapping(value = "/deleteLibraryAssignment", method = RequestMethod.POST)
    public String deleteAssignment(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        if (!ControllerUtilities.isLoggedInUser(session)) {
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are passed even when out of sessions
            return "redirect:login";
        }

        String libraryIdStr = request.getParameter("libraryId");
        UUID libraryId = UUID.fromString(libraryIdStr);

        String selectedIndexStr = request.getParameter("selectedAssignmentIndex");
        if(selectedIndexStr == null){
            //continue deleting assignments - force reload from database by redirecting with Id set
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte příklad pro smazání."); //flash attributes are passed even when out of sessions
            return "redirect:deleteLibraryAssignment";
        }

        int selectedIndex = -1;
        try {
            selectedIndex = Integer.parseInt(selectedIndexStr);
        } catch (NumberFormatException ex){
            //continue deleting assignments - force reload from database by redirecting with Id set
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Zvolen neplatný příklad pro smazání."); //flash attributes are passed even when out of sessions
            return "redirect:deleteLibraryAssignment";
        }

        //logic
        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);

        LibraryDTO libraryToEdit;
        try{
            if(!service.isOwner(currentUser.getId(), libraryId)){
                log.warn("User " + currentUser.getId() + " was trying to edit someone else's library");
                redirectAttributes.addFlashAttribute("errorMessage", "Upravit knihovnu může jen uživatel, který ji vytvořil.");
                return "redirect:libraries";
            }
            libraryToEdit = service.getLibraryDetails(libraryId); //load lastest library version
        } catch (SQLException ex){
            log.info("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during library " + libraryIdStr + " details retrieval");
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází při načítání knihovny.");
            return "redirect:libraries";
        }
        boolean success = false;
        try{
            success = service.deleteAssignmentByIndex(libraryToEdit, selectedIndex);
        } catch (SQLException ex){
            //continue deleting assignments - force reload from database by redirecting with Id set
            redirectAttributes.addAttribute("libraryId", libraryIdStr); //flash attributes are passed even when out of sessions
            redirectAttributes.addFlashAttribute("errorMessage", "Nastal problém při komunkaci s databází při ukládání knihovny.");
            return "redirect:deleteLibraryAssignment";
        }
        if(success){
            redirectAttributes.addFlashAttribute("infoMessage", "Příklad byl úspěšně smazán.");
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Při mazání příkladu došlo k problému, prosím, zkuste to znovu.");
        }

        //continue deleting assignments - force reload from database by redirecting with Id set
        redirectAttributes.addAttribute("libraryId", libraryIdStr); //flash attributes are passed even when out of sessions
        return "redirect:deleteLibraryAssignment";
    }

}
