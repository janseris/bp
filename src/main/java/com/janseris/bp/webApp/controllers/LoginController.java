package com.janseris.bp.webApp.controllers;

import com.janseris.bp.businessLayer.AuthenticationResult;
import com.janseris.bp.businessLayer.LogInService;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserLoginDTO;
import com.janseris.bp.webApp.ControllerUtilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.servlet.http.HttpServletRequest;

import java.sql.SQLException;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-20
 */

@Controller
public class LoginController {

    private final LogInService loginService;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public LoginController(LogInService loginService){
        this.loginService = loginService;
    }

    //if first login attempt, then pass empty previousLoginAttemptEmail,password object, if not first login attempt, pass the old filled object
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String showLoginPage(HttpServletRequest request){
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        //values in html views are taken from model attributes set in controller
        //values in controller from html views are obtained using request parameters
        //redirect attributes are used to pass model attibutes to next request

        return "login"; //html view called login
    }

    /**
     * Anonymous user login -> anonymous session is invalidated and a new logged-in session is created
     * Logged in user login to a different account -> logged in session is invalidated and a new logged-in session is created
     * Logged in user login to self -> logged in session is invalidated and a new logged-in session is created
     */
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public String login(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) throws SQLException {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UserLoginDTO loginInput = new UserLoginDTO(email, password);
        AuthenticationResult loginResult = loginService.validateCredentials(loginInput);

        boolean redirectBackToLogin = loginResult == AuthenticationResult.FAILURE || loginResult == AuthenticationResult.UNKNOWN_EMAIL || loginResult == AuthenticationResult.WRONG_PASSWORD; //login fail

        String errorMessageOnRedirect = null;
        if (loginResult == AuthenticationResult.FAILURE) {
            errorMessageOnRedirect = "Přihlášení selhalo.";
        }
        else if (loginResult == AuthenticationResult.UNKNOWN_EMAIL) {
            errorMessageOnRedirect = "Neznámý email";
        }
        else if (loginResult == AuthenticationResult.WRONG_PASSWORD) {
            errorMessageOnRedirect = "Špatné heslo";
        }

        if(redirectBackToLogin){
            //https://stackoverflow.com/questions/19266427/what-are-ways-for-pass-parameters-from-controller-after-redirect-in-spring-mvc
            //https://stackoverflow.com/questions/47740583/spring-mvc-how-to-pass-a-parameter-to-thymeleaf-view-when-redirecting -> redirect addAttribute is not working, only flash attributes are working
            redirectAttributes.addFlashAttribute("errorMessage", errorMessageOnRedirect); //flash attributes are carried even when out of sessions
            redirectAttributes.addFlashAttribute("previousLoginAttemptEmail", email); //flash attributes are carried even when out of sessions
            redirectAttributes.addFlashAttribute("previousLoginAttemptPassword", password); //flash attributes are carried even when out of sessions
            return "redirect:login"; //redirect must be used to reload page using GET request otherwise nothing happens (return "login" from method mapped to path login does nothing)
        }

        //else -> login success
        UserDTO userData = loginService.loadUserDataAfterLogin(loginInput.getEmail());

        ControllerUtilities.manageSessionsOnLogIn(request, userData);

        //redirectAttributes.addFlashAttribute("userData", userData); //pass data through redirect attributes or through session
        redirectAttributes.addFlashAttribute("infoMessage", "Vítejte, " + userData.getName()); //flash attributes are carried even when out of sessions
        return "redirect:index"; //login success, redirect to change URL path to /user (otherwise path remains /login)
    }
}
