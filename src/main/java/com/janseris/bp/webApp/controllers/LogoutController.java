package com.janseris.bp.webApp.controllers;

import com.janseris.bp.businessLayer.LogInService;
import com.janseris.bp.businessLayer.UserService;
import com.janseris.bp.webApp.ControllerUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-25
 */


@Controller
public class LogoutController {

    private final UserService userService;
    private final LogInService loginService;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public LogoutController(UserService userService, LogInService loginService){
        this.userService = userService;
        this.loginService = loginService;
    }

    /**
     * Currently logged in user's session is invalidated
     * Anonymous user is redirected to login page
     */
    @RequestMapping(value="/logout", method = RequestMethod.POST)
    public String logoutPOST(HttpServletRequest request, RedirectAttributes redirectAttributes){
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);
        if(!ControllerUtilities.isLoggedInUser(session)){
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are carried even when out of sessions
            return"redirect:login";
        }
        if(request.isRequestedSessionIdValid()){
            session.invalidate(); //only if user is logged in, otherwise, a non-existing entry in userTracker data would be deleted on repeatedly sent logout request
        }
        redirectAttributes.addFlashAttribute("infoMessage", "Byli jste odhlášeni."); //flash attributes are carried even when out of sessions

        ControllerUtilities.createAnonymousUserSession(request);
        return "redirect:index";
    }
}
