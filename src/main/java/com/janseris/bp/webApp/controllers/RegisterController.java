package com.janseris.bp.webApp.controllers;

import com.janseris.bp.businessLayer.RegisterService;
import com.janseris.bp.businessLayer.RegistrationResult;
import com.janseris.bp.model.UserRegisterDTO;
import com.janseris.bp.webApp.ControllerUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-02
 */

@Controller
public class RegisterController {

    private final RegisterService service;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public RegisterController(RegisterService service){
        this.service = service;
    }

    @RequestMapping(value="/register", method = RequestMethod.GET)
    public String showRegistrationPage(HttpServletRequest request){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        return "register";
    }

    /**
     * Checks registration data
     * Invalid email -> redirect to registration with pre-filled old data
     * Invalid password -> redirect to registration with pre-filled old data
     * Succes -> redirect to login page with info message
     */
    @RequestMapping(value="/register", method = RequestMethod.POST)
    public String register(HttpServletRequest request, RedirectAttributes redirectAttributes){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }


        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UserRegisterDTO userInput = new UserRegisterDTO(name, email, password);
        RegistrationResult registrationResult = service.registerUser(userInput);

        boolean redirectBackToRegister =  registrationResult != RegistrationResult.SUCCESS; //registration fail

        String errorMessageOnRedirect = "";
        if(registrationResult == RegistrationResult.EMAIL_ALREADY_USED){
            errorMessageOnRedirect = "Email je už používán.";
        } else if (registrationResult == RegistrationResult.ERROR){
            errorMessageOnRedirect = "Došlo k problému při registraci, prosím zkuste to znovu nebo kontaktujte administrátora.";
        } else if (registrationResult == RegistrationResult.INVALID_EMAIL){
            errorMessageOnRedirect = "Email je neplatný, prosím, použijte platný email";
        } else if (registrationResult == RegistrationResult.INVALID_PASSWORD){
            errorMessageOnRedirect = RegisterService.INSUFFICIENT_PASSWORD_LENGTH_ERROR_MESSAGE;
        } else if (registrationResult == RegistrationResult.INVALID_NAME){
            errorMessageOnRedirect = "Prosím, zadejte platné jméno.";
        }

        if(redirectBackToRegister){
            redirectAttributes.addFlashAttribute("errorMessage", errorMessageOnRedirect); //flash attributes are carried even when out of sessions
            redirectAttributes.addFlashAttribute("previousRegisterAttemptName", name); //flash attributes are carried even when out of sessions
            redirectAttributes.addFlashAttribute("previousRegisterAttemptEmail", email); //flash attributes are carried even when out of sessions
            redirectAttributes.addFlashAttribute("previousRegisterAttemptPassword", password); //flash attributes are carried even when out of sessions
            return "redirect:register"; //redirect must be used to reload page using GET request otherwise nothing happens (return "register" from method mapped to path register does nothing)
        }

        //else -> registration success
        redirectAttributes.addFlashAttribute("infoMessage", "Registrace proběhla úspěšně, nyní se můžete přihlásit."); //flash attributes are carried even when out of sessions
        redirectAttributes.addFlashAttribute("previousLoginAttemptEmail", email);
        return "redirect:login"; //login success
    }
}
