package com.janseris.bp.webApp.controllers;

import com.janseris.bp.LibrarySyntaxErrorException;
import com.janseris.bp.businessLayer.EmailService;

import com.janseris.bp.businessLayer.ProgramService;
import com.janseris.bp.webApp.BpSpringWebApplication;
import com.janseris.bp.webApp.ControllerUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.NestedServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.PipedReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.NoSuchFileException;
import java.sql.SQLException;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-23
 */

@ControllerAdvice
public class WebAppExceptionHandler extends ResponseEntityExceptionHandler {

    private final EmailService emailService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public WebAppExceptionHandler(EmailService emailService) {
        this.emailService = emailService;
    }

    @ExceptionHandler(Exception.class)
    public String handleException(Exception ex, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String requestPath = request.getServletPath().replaceFirst("/", "");
        log.trace("Exception " + ex.getClass() + " caught by exception handler controller, request destination: " + requestPath + ", exception details: " + ex.getLocalizedMessage());
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);

        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);
        String currentUserName = ControllerUtilities.getCurrentUserName(session);

        //stack overflow error caused by long int overflow in "solve or "generate"
        if(ex.getClass().equals(NestedServletException.class)){
            if(ex.getCause().getClass().equals(StackOverflowError.class)){
                //return back to program
                log.info("User " + currentUserName + " caused StackOverflowError in GCD computation (long int overflow)");
                ProgramController.assignOldFormValues(request, redirectAttributes);
                redirectAttributes.addFlashAttribute("errorMessage", "Při výpočtu došlo k přetečení číselného formátu! Prosím, zadejte jednodušší zadání!");
                return "redirect:program";
            }
        }

        if(ex.getClass().equals(LibrarySyntaxErrorException.class) && requestPath.equals("deleteLibraryAssignment")) {
            redirectAttributes.addFlashAttribute("errorMessage", ex.getMessage());
            return "redirect:libraries";
        }

        log.trace("Request path which raised the exception: " + requestPath);

        //other than long int overflow error during solve will be automatically reported to email
        if(requestPath.equals("solve")){ //change to "solve"
            emailService.sendComputationErrorEmail(request, ex);
            redirectAttributes.addFlashAttribute("errorMessage", "Při výpočtu nastala neznámá chyba, byla automaticky nahlášena.");
            ProgramController.assignOldFormValues(request, redirectAttributes);
            return "redirect:program";
        }

        if(ex.getClass().equals(NoSuchFileException.class) && (ex.getMessage().contains(ProgramService.ONLINE_VERSION_OUTPUT_TXT_NAME) || ex.getMessage().contains(ProgramService.ONLINE_VERSION_OUTPUT_PDF_NAME))){
            ProgramController.assignOldFormValues(request, redirectAttributes);
            redirectAttributes.addFlashAttribute("errorMessage", "Dočasné soubory obsahující postup řešení již nejsou k dispozici.<br>Byly automaticky smazány kvůli neaktivitě uživatele.");
            return "redirect:program";
        }

        if(ex.getClass().equals(RuntimeException.class)){
            if(ex.getMessage().contains("Connection to database could not be established")){
                log.warn(ex.getMessage());
                redirectAttributes.addFlashAttribute("errorMessage", "Databázový server není dostupný.");
                return "redirect:error";
            } else if (ex.getMessage().equals(ControllerUtilities.SESSION_TIMEOUT_MESSAGE)){
                ProgramController.assignOldFormValues(request, redirectAttributes);
                redirectAttributes.addFlashAttribute("errorMessage", "Dočasné soubory obsahující postup řešení již nejsou k dispozici.<br>Byly automaticky smazány kvůli neaktivitě uživatele.");
                return "redirect:program";
            }
        }

        if(ex.getClass().equals(SQLException.class)){
            redirectAttributes.addFlashAttribute("errorMessage", "Došlo k chybě při komunikací s databází.");
            return "redirect:error";
        }

        if(BpSpringWebApplication.DEVELOPMENT_EXCEPTION_VIEW_ON_ERROR_PAGE){
            redirectAttributes.addFlashAttribute("errorMessage", "Nastala neznámá chyba:<br>" + ex.getClass() + sw.toString()); //for developing environment
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", "Nastala neznámá chyba."); //for deploying environment
        }
        return "redirect:error";
    }
}
