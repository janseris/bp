package com.janseris.bp.webApp.controllers;

import com.janseris.bp.webApp.ControllerUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-20
 */


//@RestController combines @Controller and @ResponseBody, two annotations that results in web requests returning data rather than a view.

//two options to map controller methods to request paths:
// @RequestMapping(value = "path", method = RequestMethod.POST) or
// @PostMapping("path")

@Controller //to Spring: this is a controller (returned string specifies the view.html)
//@RestController -> to Spring: this is a REST API controller (returned value does not show view)
public class MainController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value={"/", "/index"}) //any request sent to path / is handled by this function
    public String index(HttpServletRequest request)
    {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        HttpSession session = request.getSession(false);
        if(ControllerUtilities.isLoggedInUser(session)){
            log.trace("Logged user accessing index.html");
        } else if(session.getAttribute(ControllerUtilities.ANONYMOUS_USER_DATA_CTX_ATTR_NAME) != null){
            log.trace("Anonymous user accessing index.html");
        } else {
            log.warn("Suspicious access to index page");
            ControllerUtilities.printSessionAttributes(session);
            throw new IllegalStateException("Suspicious access to index page");
        }

        return "index"; //show index.html
    }

}
