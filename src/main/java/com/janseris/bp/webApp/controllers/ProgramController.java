package com.janseris.bp.webApp.controllers;

import com.janseris.bp.*;
import com.janseris.bp.businessLayer.LibraryService;
import com.janseris.bp.businessLayer.ProgramService;
import com.janseris.bp.model.GenerateSubspacesResultDTO;
import com.janseris.bp.model.SolveInputDTO;
import com.janseris.bp.model.SolveResultDTO;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.webApp.ControllerUtilities;
import com.janseris.bp.webApp.model.CheckSubspaceWithImageDTO;
import com.janseris.bp.webApp.model.UserLibrariesSimpleListDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

import static com.janseris.bp.Constants.generateSubspaceErrorMessages;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-05
 */


@Controller
public class ProgramController {

    private final ProgramService service;
    private final LibraryService libraryService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public ProgramController(ProgramService service, LibraryService libraryService) {
        this.service = service;
        this.libraryService = libraryService;
    }

    @RequestMapping(value="/program", method = RequestMethod.GET)
    public String showProgramPage(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes)
    {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        return "program";
    }


    @RequestMapping(value="/solve", method = RequestMethod.POST)
    public String solveAssignment(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes)
    {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        assignOldFormValues(request, redirectAttributes);
        HttpSession session = request.getSession(false);

        String userName = ControllerUtilities.getCurrentUserName(session); //for logging purposes

        String sid = session.getId();

        //load and check input -> solveInputDTO
        String task = request.getParameter("assignmentType");

        if (task == null || task.equals("Vyber úlohu:")){
            //solve button was pressed while "Vyber úlohu:" option was selected (that was not supposed to happen)
            log.warn("User " + userName + " managed to use the \"Solve\" button on program page with no valid assignment type selected");
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte typ úlohy pro vyřešení!");
            return "redirect:program";
        }

        String input1 = request.getParameter("subspace1Input");
        input1 = input1.replaceAll("\r\n", "\n"); //split by \n in program core
        String input2 = request.getParameter("subspace2Input");
        input2 = input2.replaceAll("\r\n", "\n"); //split by \n in program core


        IOHelper ioHelper = service.createIOHelperInstance(sid);
        InputReader inputReader = service.createInputReaderInstance(ioHelper);
        SolveResultDTO result = null;

        try{
            SolveInputDTO inputToSolve = AssignmentSolver.loadInputToSolve(task, input1, input2, inputReader);
            if(inputToSolve.containsErrorMessage()){
                redirectAttributes.addFlashAttribute("enableTextSolutionButton", false);
                redirectAttributes.addFlashAttribute("enablePdfSolutionButton", false);
                redirectAttributes.addFlashAttribute("errorMessage", inputToSolve.getErrorMessage());
                return "redirect:program";
            }
            //input was successfully parsed
            service.clearOutputFiles(ioHelper);
            result = AssignmentSolver.solve(inputToSolve, ioHelper);
            ioHelper.writeBuffersToOutputFiles(); //creates txt and tex output
            // txt and tex cannot be created later because ioHelper with its write buffers is accessible only within this request processing scope
        } catch (IOException ex){
            redirectAttributes.addFlashAttribute("errorMessage", "Došlo k chybě při vytváření souborů s postupem výpočtu, prosím, opakujte operaci nebo kontaktujte správce.");
            return "redirect:program";
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("errorMessage", "Při řešení došlo k neočekávané chybě. Pravděpodobně došlo k přetečení číselného formátu, tzn. že při řešení "
                    + "se neúměrně zvyšovala některá čísla. Zejména případě náhodně generovaných úloh k takovýmto chybám "
                    + "může docházet. Zadejte prosím jinou úlohu.");
            return "redirect:program";
        }

        redirectAttributes.addFlashAttribute("solveOutput", result.getShortTextOutput());
        log.trace("User " + userName + " solved an assignment with result: " + result.getShortTextOutput());
        return "redirect:program";
    }

    @RequestMapping(value="/generate", method = RequestMethod.POST)
    public String generateSubspaces(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes)
    {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }

        assignOldFormValues(request, redirectAttributes);

        HttpSession session = request.getSession(false);
        String userName = ControllerUtilities.getCurrentUserName(session); //for logging purposes
        IOHelper ioHelper = service.createIOHelperInstance(session.getId());
        InputReader inputReader = service.createInputReaderInstance(ioHelper);
        AssignmentGenerator assignmentGenerator = service.createAssignmentGeneratorInstance(ioHelper, inputReader);


        String subspace1Dimension = request.getParameter("generatedSubspace1Dimension");
        String subspace2Dimension = request.getParameter("generatedSubspace2Dimension");
        String wholeSubspaceDimension = request.getParameter("generatedSubspacesSpaceDimension");
        String intersectionDimension = request.getParameter("intersectionDimension");
        String subspace1Form = request.getParameter("generatedSubspace1Form");
        String subspace2Form = request.getParameter("generatedSubspace2Form");

        //<select>
        String task = request.getParameter("generateType");
        String relation = request.getParameter("relationType");
        if(relation.equals(Constants.relationList[1])){ //mimoběžné
            intersectionDimension = request.getParameter("intersectionDimension2"); //"dimenze průniku zaměření"
        }
        String angle = request.getParameter("subspacesAngle");
        String disjoint = request.getParameter("generatedSubspacesDisjointBool");

        String distance = request.getParameter("subspacesDistance");
        String intersectionSubspaceInput = request.getParameter("intersectionSubspaceInput");

        if(subspace1Form == null){ //no option selected
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte tvar prvního podprostoru (parametrický nebo obecný).");
            return "redirect:program";
        }

        if(subspace2Form == null){ //no option selected
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte tvar druhého podprostoru (parametrický nebo obecný).");
            return "redirect:program";
        }

        boolean firstGeneral = subspace1Form.contains("general"); //defined by .contains() because the value of the request parameter is "subspace1Formgeneral" for some reason (<input type="radio"> strange behavior)
        boolean firstParam = subspace1Form.contains("pointNormal");

        boolean secondGeneral = subspace2Form.contains("general");
        boolean secondParam = subspace2Form.contains("pointNormal");

        if(!(firstParam || firstGeneral)){ //user edited html in radio options
            log.warn("User " + userName + " sent a \"generate assignment\" request on program page with an invalid subspace form selected (radio button input)");
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte platný tvar prvního podprostoru (parametrický nebo obecný).");
            return "redirect:program";
        }

        if(!(secondParam || secondGeneral)){ //html edit radio options
            log.warn("User " + userName + " sent a \"generate assignment\" request on program page with an invalid subspace form selected (radio button input)");
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zvolte platný tvar druhého podprostoru (parametrický nebo obecný).");
            return "redirect:program";
        }


        boolean generateFirst = true, generateSecond = true;
        int dim1 = 0, dim2 = 0, wholeSpaceDimension;

        try {
            dim1 = Integer.parseInt(subspace1Dimension);
        } catch (NumberFormatException ex) { //input empty or NaN
            generateFirst = false;
        }
        try {
            dim2 = Integer.parseInt(subspace2Dimension);
        } catch (NumberFormatException ex) { //input empty or NaN
            generateSecond = false;
        }
        if (!generateFirst && !generateSecond){
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím, zadejte dimenzi alespoň jednoho generovaného podprostoru.");
            redirectAttributes.addFlashAttribute("infoMessage", "Pokud je zadána pouze jedna dimenze, vygeneruje se pouze tento podprostor.");
            return "redirect:program";
        }
        try{
            wholeSpaceDimension = Integer.parseInt(wholeSubspaceDimension);
        } catch (NumberFormatException ex){
            redirectAttributes.addFlashAttribute("errorMessage", "Do políčka určujícího dimenzi celého prostoru je nutné vepsat nezáporné celé číslo.");
            redirectAttributes.addFlashAttribute("infoMessage", "Dimenze celého prostoru musí být vždy zadána.");
            return "redirect:program";
        }
        if (dim1 < 0 || dim2 < 0 || wholeSpaceDimension < 0){
            redirectAttributes.addFlashAttribute("errorMessage", "Do políček určujících dimenze podprostorů a prostorů pro generování je nutné vepsat nezáporná celá čísla.");
            return "redirect:program";
        }
        if (dim1 > wholeSpaceDimension || dim2 > wholeSpaceDimension){
            redirectAttributes.addFlashAttribute("errorMessage", "Dimenze podprostoru nemůže být větší než dimenze celého prostoru!");
            return "redirect:program";
        }
        if (((dim1 == wholeSpaceDimension) && firstGeneral) || ((dim2 == wholeSpaceDimension) && secondGeneral)){
            redirectAttributes.addFlashAttribute("errorMessage", "Celý prostor nemá obecné vyjádření, takže dimenze podprostoru musí být při volbě obecného vyjádření menší než dimenze celého prostoru!");
            return "redirect:program";
        }

        //subspace dimensions for assignment generate are loaded

        if (task == null || task.equals("Vyber úlohu:")){
            //the generate button was pressed while option "Vyber úlohu:" was selected (that was not supposed to happen)
            log.warn("User " + userName + " managed to use the \"Generate\" button on program page with no valid assignment type selected");
            redirectAttributes.addFlashAttribute("errorMessage", "Prosím zvolte úlohu pro generování!");
            return "redirect:program";
        }

        GenerateSubspacesResultDTO result = null;

        //the type "náhodné podprostory" does not require both subspaces to be defined but other generate assignment task types require both subspaces
        if (task.equals(Constants.generateTaskList[1])) {
            result = assignmentGenerator.validateAndGenerateRandomSubspaces(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension);
        }

        if (task.equals(Constants.generateTaskList[4])) {
            result = assignmentGenerator.validateAndGenerateSubspacesWithGivenDistance(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, distance);
        }

        if (task.equals(Constants.generateTaskList[5])) {
            result = assignmentGenerator.validateAndGenerateSubspacesWithGivenAngle(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, angle);
        }

        if (task.equals(Constants.generateTaskList[2])) {
            try {
                result = assignmentGenerator.validateAndGenerateSubspacesWithGivenRelation(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, relation,
                        disjoint, intersectionDimension);
            } catch (IllegalArgumentException ex){
                //selected a different option than provided in HTML (by HTML hacking)
                log.warn("User " + userName + " sent a \"generate assignment\" request on program page with an invalid option for \"subspaces disjoint property\" selected");
                redirectAttributes.addFlashAttribute("errorMessage", "Prosím zvolte platnou volbu pro disjunktnost podprostorů!");
                return "redirect:program";
            }
        }

        if (task.equals(Constants.generateTaskList[3])) {
            result = assignmentGenerator.validateAndGenerateSubspacesWithGivenIntersection(firstParam, secondParam, generateFirst, generateSecond, dim1, dim2, wholeSpaceDimension, intersectionSubspaceInput);
        }

        if(result == null){
            //selected a different option for generate assignment task type name than provided in HTML (by HTML hacking)
            log.warn("User " + userName + " sent a \"generate assignment\" request on program page with an invalid assignment type selected");
            redirectAttributes.addFlashAttribute("errorMessage", "Zvolena neplatná úloha pro generování!");
            return "redirect:program";
        }

        if(result.isErrorSet()){
            redirectAttributes.addFlashAttribute("errorMessage", generateSubspaceErrorMessages[result.getErrorMessageIndex()]);
            return "redirect:program";
        }

        String generatedFirst = result.getGeneratedFirstSubspaceRepresentation();
        String generatedSecond = result.getGeneratedSecondSubspaceRepresentation();

        redirectAttributes.addFlashAttribute("oldSubspace1Input", generatedFirst); //attribute is replaced if already present
        redirectAttributes.addFlashAttribute("oldSubspace2Input", generatedSecond); //attribute is replaced if already present

        log.trace("User " + userName + " generated an assignment with dimensions s1: " + subspace1Dimension + ", s2: " + subspace2Dimension + ", space: " + wholeSubspaceDimension);
        return "redirect:program";
    }



    @RequestMapping(value="/checkSubspaceAjax", method = RequestMethod.POST)
    public ResponseEntity<CheckSubspaceWithImageDTO> checkSubspaceAjax(HttpServletRequest request){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        HttpSession session = request.getSession(false);

        String input = request.getParameter("input");

        CheckSubspaceWithImageDTO dto = service.checkSubspace(service.createIOHelperInstance(session.getId()), input, true);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    //https://www.baeldung.com/spring-response-entity
    @RequestMapping(value="/txtSolution", method=RequestMethod.POST)
    public ResponseEntity<byte[]> getTXT(HttpServletRequest request) throws IOException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN); //manual request
        }

        if(!ControllerUtilities.isSessionValid(request)){ //session timed out -> redirect to program with error message
            throw new RuntimeException(ControllerUtilities.SESSION_TIMEOUT_MESSAGE);
        }

        //txt and tex files shall be written right after assignment solving is completed

        byte[] contents = service.getTxtSolution(session.getId());


        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "text/plain; charset=UTF-8");
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;
    }

    //maybe both modelAndView and responseEntity could be returned if return type was Object
    //https://stackoverflow.com/questions/48077540/spring-mvc-how-do-i-return-a-view-in-a-responseentity-method
    @RequestMapping(value="/pdfSolution", method=RequestMethod.POST)
    public ResponseEntity<byte[]> getPDF(HttpServletRequest request) throws IOException, InterruptedException {
        HttpSession session = request.getSession(false);
        if(session == null){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN); //manual request
        }

        if(!ControllerUtilities.isSessionValid(request)){ //session timed out -> redirect to program with error message
            throw new RuntimeException(ControllerUtilities.SESSION_TIMEOUT_MESSAGE);
        }

        IOHelper ioHelper = service.createIOHelperInstance(session.getId());
        try{
            ioHelper.generateOutputPDF();
        } catch (Exception ex){
            log.warn("Exception raised during PDF result generation: " + ex.getClass() + ": " + ex.getLocalizedMessage());
            throw ex; //pass exception to exception handling controller
        }
        byte[] contents = service.getPdfSolution(session.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        // Here you have to set the actual filename of your pdf
        String filename = "output.pdf";
        //headers.setContentDispositionFormData(filename, filename); //force download file
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;
    }


    /**
     * Ensures that all elements on program page will have their contents filled back after a POST request was sent
     * @param request current request
     * @param redirectAttributes response
     */
    /* package-private */ static void assignOldFormValues(HttpServletRequest request, RedirectAttributes redirectAttributes){
        String requestPath = request.getServletPath().replace("/", "");
        //System.out.println("Assigned old form values for " + request.getMethod() + " request with path: " + requestPath);

        //set up form 1
        if(requestPath.equals("solve")){
            redirectAttributes.addFlashAttribute("enableTextSolutionButton", true);
            redirectAttributes.addFlashAttribute("enablePdfSolutionButton", true);
            //solve result textarea is shown if parameter solveOutput is set (that is however handled directly in /solve POST handling method)
        }
        redirectAttributes.addFlashAttribute("oldSubspace1Input", request.getParameter("subspace1Input"));
        redirectAttributes.addFlashAttribute("oldSubspace2Input", request.getParameter("subspace2Input"));
        redirectAttributes.addFlashAttribute("oldAssignmentTypeSelectedOption", request.getParameter("assignmentType"));

        //set up form 2
        redirectAttributes.addFlashAttribute("oldGenerateTypeSelectedOption", request.getParameter("generateType"));
        redirectAttributes.addFlashAttribute("oldRelationTypeSelectedOption", request.getParameter("relationType"));
        redirectAttributes.addFlashAttribute("oldSubspacesAngleSelectedOption", request.getParameter("subspacesAngle"));
        redirectAttributes.addFlashAttribute("oldSubspacesDisjointBoolSelectedOption", request.getParameter("generatedSubspacesDisjointBool"));

        redirectAttributes.addFlashAttribute("oldIntersectionDimension", request.getParameter("intersectionDimension"));
        redirectAttributes.addFlashAttribute("oldIntersectionDimension2", request.getParameter("intersectionDimension2"));
        redirectAttributes.addFlashAttribute("oldSubspacesIntersectionInput", request.getParameter("intersectionSubspaceInput"));
        redirectAttributes.addFlashAttribute("oldSubspacesDistance ", request.getParameter("subspacesDistance"));
        String generatedSubspace1SelectedForm = request.getParameter("generatedSubspace1Form");
        if("general".equals(generatedSubspace1SelectedForm)){ //reversed equals order to handle null
            redirectAttributes.addFlashAttribute("oldGeneratedSubspace1GeneralForm", true);
        } else if ("pointNormal".equals(generatedSubspace1SelectedForm)) { //reversed equals order to handle null
            redirectAttributes.addFlashAttribute("oldGeneratedSubspace1PointNormalForm", true);
        }

        String generatedSubspace2SelectedForm = request.getParameter("generatedSubspace2Form");
        if("general".equals(generatedSubspace2SelectedForm)){ //reversed equals order to handle null
            redirectAttributes.addFlashAttribute("oldGeneratedSubspace2GeneralForm", true);
        } else if ("pointNormal".equals(generatedSubspace2SelectedForm)) { //reversed equals order to handle null
            redirectAttributes.addFlashAttribute("oldGeneratedSubspace2PointNormalForm", true);
        }

        redirectAttributes.addFlashAttribute("oldGeneratedSubspace1Dimension", request.getParameter("generatedSubspace1Dimension"));

        redirectAttributes.addFlashAttribute("oldGeneratedSubspace2Dimension", request.getParameter("generatedSubspace2Dimension"));
        redirectAttributes.addFlashAttribute("oldGeneratedSubspacesSpaceDimension", request.getParameter("generatedSubspacesSpaceDimension"));

        //indexes to trigger switchable content in form 1
        redirectAttributes.addFlashAttribute("oldAssignmentTypeSelectedOptionIndex", ProgramService.getElementIndex(request.getParameter("assignmentType"), Constants.solveTaskList));

        //indexes to trigger switchable content in form 2
        int generateTaskTypeSelectedOptionIndex = ProgramService.getElementIndex(request.getParameter("generateType"), Constants.generateTaskList);
        if(generateTaskTypeSelectedOptionIndex == 2){
            //if this attribute was always added, additional unexpected switchable content would be displayed in the reloaded page
            redirectAttributes.addFlashAttribute("oldRelationTypeSelectedOptionIndex", ProgramService.getElementIndex(request.getParameter("relationType"), Constants.relationList));
        } else if(generateTaskTypeSelectedOptionIndex == 5){
            //if this attribute was always added, additional unexpected switchable content would be displayed in the reloaded page
            redirectAttributes.addFlashAttribute("oldSubspacesAngleSelectedOptionIndex", ProgramService.getElementIndex(request.getParameter("subspacesAngle"), Constants.angleList));
        }
        redirectAttributes.addFlashAttribute("oldGenerateTypeSelectedOptionIndex", ProgramService.getElementIndex(request.getParameter("generateType"), Constants.generateTaskList));
        redirectAttributes.addFlashAttribute("oldSubspacesDisjointBoolSelectedOptionIndex", ProgramService.getElementIndex(request.getParameter("generatedSubspacesDisjointBool"), Constants.generatedSubspacesDisjointOptionList));
    }

    /**
     * Shows a list of user library names to save a new assignment into in the next menu
     * Meanwhile, checks that assignment input, so any syntax or logic error is displayed even before library choosing
     * @param request
     * @return
     */
    @RequestMapping(value="/loadUserLibraryNames", method = RequestMethod.POST)
    public ResponseEntity<UserLibrariesSimpleListDTO> loadUserLibraryNamesToSaveAssignment(HttpServletRequest request){
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        if(!ControllerUtilities.isLoggedInUser(request.getSession(false))){
            UserLibrariesSimpleListDTO dto = new UserLibrariesSimpleListDTO("Příklad si do některé ze svých knihoven můžou uložit jen přihlášení uživatelé.", null, null);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
        HttpSession session = request.getSession(false);

        //check whole input if it can be saved as a valid assignment after all user's library names are loaded
        String input1 = request.getParameter("subspace1Input");
        String input2 = request.getParameter("subspace2Input");
        String task = request.getParameter("selectedTask");
        if(task == null || task.equals(Constants.solveTaskList[0])){
            UserLibrariesSimpleListDTO dto = new UserLibrariesSimpleListDTO("Prosím, zvolte typ úlohy!", null, null);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }

        IOHelper ioHelper = service.createIOHelperInstance(session.getId());
        InputReader inputReader = service.createInputReaderInstance(ioHelper);

        SolveInputDTO inputToSolve = AssignmentSolver.loadInputToSolve(task, input1, input2, inputReader);
        if(inputToSolve.containsErrorMessage()){
            UserLibrariesSimpleListDTO dto = new UserLibrariesSimpleListDTO(inputToSolve.getErrorMessage(), null, null);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }


        //the input is now a valid assignment which can be used in /solve

        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        UserLibrariesSimpleListDTO dto = null;
        try{
            dto = libraryService.loadUserLibraryNames(currentUser.getId());
        } catch (SQLException ex){
            dto = new UserLibrariesSimpleListDTO("Nastala chyba při načítání seznamu Vašich knihoven. Prosím, zkuste to znovu.", null, null);
        }

        Assignment possiblySaved = service.getAssignmentFromProgramInput(task, input1, input2);
        session.setAttribute(ControllerUtilities.TEMPORARY_ASSIGNMENT_TO_BE_SAVED_INTO_LIBRARY_SESSION_ATTRIBUTE_NAME, possiblySaved);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
