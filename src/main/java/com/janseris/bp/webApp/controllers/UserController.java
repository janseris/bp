package com.janseris.bp.webApp.controllers;

import com.janseris.bp.businessLayer.EmailService;
import com.janseris.bp.businessLayer.RegisterService;
import com.janseris.bp.businessLayer.UserService;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserShowDTO;
import com.janseris.bp.webApp.ControllerUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-20
 */

@Controller
public class UserController {

    private final UserService userService;
    private final EmailService emailService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    //https://blog.marcnuri.com/field-injection-is-not-recommended/
    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public UserController(UserService userService, EmailService emailService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    @RequestMapping(value="/user", method = RequestMethod.GET)
    public String showPrivateUser(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes)
    {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        if(!ControllerUtilities.isLoggedInUser(request.getSession(false))){
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are carried even when out of sessions
            return "redirect:login";
        }

        return "user";
    }

    @RequestMapping(value="/userPublic", method = RequestMethod.GET)
    public String showPublicUser(ModelMap model, HttpServletRequest request)
    {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        UUID userId = UUID.fromString((String)request.getParameter("userId"));
        UserShowDTO userData;
        try{
            userData = userService.showUserForPublic(userId);
            model.addAttribute("userData", userData);
        } catch (SQLException ex){
            log.warn("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during loading public user " + userId + " details");
            model.addAttribute("errorMessage", "Data o uživateli se nepodařilo načíst.");
        }
        return "userPublic";
    }

    @RequestMapping(value="/forgottenPassword", method = RequestMethod.GET)
    public String showForgottenPasswordForm(ModelMap model, HttpServletRequest request)
    {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        return "forgottenPassword";
    }

    @RequestMapping(value="/resetPassword", method = RequestMethod.POST)
    public String sendResetPasswordEmail(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }

        String email = request.getParameter("email");

        UserDTO user = null;
        try{
            user = userService.checkUserExistence(email);

            if(user == null){
                model.addAttribute("previousAttemptEmail", email);
                model.addAttribute("errorMessage", "Žádný uživatel se zadaným emailem neexistuje.");
                return "forgottenPassword";
            }

            String newPassword = userService.resetPassword(user.getId());
            emailService.sendPasswordResetEmail(newPassword, email);

        } catch (SQLException ex){
            model.addAttribute("previousAttemptEmail", email);
            log.warn("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during checking user " + email + " existence while resetting password");
            model.addAttribute("errorMessage", "Došlo k problému při komunikací s databází. Prosím, zkuste to znovu.");
            return "forgottenPassword";
        }

        log.trace("User " + email + " reset their password");
        redirectAttributes.addFlashAttribute("infoMessage", "Na zadanou emailovou adresu byl odeslán informační email.");
        return "redirect:index";
    }


    @RequestMapping(value="/changePassword", method = RequestMethod.GET)
    public String showChangePasswordForm(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }
        if(!ControllerUtilities.isLoggedInUser(request.getSession(false))){
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are carried even when out of sessions
            return "redirect:login";
        }

        return "changePassword";
    }

    @RequestMapping(value="/changePassword", method = RequestMethod.POST)
    public String changePassword(ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        if(!ControllerUtilities.isSessionValid(request)){
            ControllerUtilities.createAnonymousUserSession(request);
        }
        if(!ControllerUtilities.isLoggedInUser(request.getSession(false))){
            redirectAttributes.addFlashAttribute("errorMessage", ControllerUtilities.ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE); //flash attributes are carried even when out of sessions
            return "redirect:login";
        }
        HttpSession session = request.getSession(false);

        String newPassword = request.getParameter("newPassword");

        if(!RegisterService.isPasswordValid(newPassword)){
            model.addAttribute("errorMessage", RegisterService.INSUFFICIENT_PASSWORD_LENGTH_ERROR_MESSAGE); //flash attributes are carried even when out of sessions
            model.addAttribute("previousAttemptPassword", newPassword);
            return "changePassword";
        }

        UserDTO currentUser = (UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME);
        UUID userId = currentUser.getId();

        try{
            userService.changePassword(userId, newPassword);
        } catch( SQLException ex){
            log.warn("Database communication error \"" + ex.getLocalizedMessage() + "\" emerged during user " + currentUser.getName() + " password change attempt");
            model.addAttribute("previousAttemptPassword", newPassword);
            model.addAttribute("errorMessage", "Došlo k problému při komunikací s databází. Prosím, zkuste to znovu.");
            return "changePassword";
        }

        log.trace("User " + currentUser.getEmail() + " changed their password");
        redirectAttributes.addFlashAttribute("infoMessage", "Vaše heslo bylo úspěšně změněno.");
        return "redirect:user";
    }
}
