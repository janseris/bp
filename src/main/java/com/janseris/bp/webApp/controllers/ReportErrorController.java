package com.janseris.bp.webApp.controllers;

import com.janseris.bp.businessLayer.EmailService;
import com.janseris.bp.webApp.ControllerUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-23
 */

@Controller
public class ReportErrorController {

    private final EmailService emailService;

    @Autowired //injection through constructor instead of direct field injection with no constructor provides "final" keyword for injected objects
    public ReportErrorController(EmailService emailService) {
        this.emailService = emailService;
    }

    @RequestMapping(value="/reportError", method = RequestMethod.GET)
    public String showReportProblemPage(HttpServletRequest request)
    {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }
        return "reportError";
    }


    @RequestMapping(value="/reportError", method = RequestMethod.POST)
    public String sendErrorDetailsByUser(HttpServletRequest request, RedirectAttributes redirectAttributes)
    {
        if (!ControllerUtilities.isSessionValid(request)) {
            ControllerUtilities.createAnonymousUserSession(request);
        }

        String sender = request.getParameter("sender");
        if(sender.isEmpty()){
            sender = "nevyplněno";
        }
        String errorDetailsByUser = request.getParameter("errorDetails");
        if(errorDetailsByUser.isEmpty()){
            redirectAttributes.addFlashAttribute("infoMessage", "Prosím, popište problém.");
            return "redirect:reportError";
        }

        emailService.sendErrorReportEmail(sender, errorDetailsByUser);

        redirectAttributes.addFlashAttribute("infoMessage", "Děkujeme Vám za zaslání komentáře k chybě/problému.");
        return "redirect:index"; //to index page
    }

}
