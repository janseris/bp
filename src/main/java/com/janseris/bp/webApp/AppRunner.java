package com.janseris.bp.webApp;

import com.janseris.bp.Constants;
import com.janseris.bp.businessLayer.EmailService;
import com.janseris.bp.businessLayer.LibraryService;
import com.janseris.bp.businessLayer.RegisterService;
import com.janseris.bp.dataAccessLayer.ConnectionFactory;
import com.janseris.bp.dataAccessLayer.LibraryDAO;
import com.janseris.bp.dataAccessLayer.UserDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-26
 */

@Component
public class AppRunner implements ApplicationRunner {

    private ApplicationContext ctx;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String sampleTeXFileContents = "\\documentclass[12pt,a4paper]{article}\n\n" +
                "\\usepackage{cmap}\n" +

                "\\usepackage[utf8]{inputenc}\n" +
                "\\usepackage[czech]{babel}\n" +
                "\\usepackage[T1]{fontenc}\n\n" +

                "\\usepackage{amsmath}\n" +
                "\\usepackage{amsfonts}\n" +
                "\\usepackage{amssymb}\n" +
                "\\usepackage{textcomp}\n" +
                "\\usepackage{mathtools}\n\n" +

                "\\usepackage{array}\n\n" +

                "\\newcommand{\\abs}[1]{\\left|#1\\right|}\n" +
                "\\let\\phi\\varphi\n\n" +

                "\\setlength{\\parindent}{0pt}\n" +
                "\\setlength{\\parskip}{3mm plus 2mm minus 1mm}\n\n" +

                "\\begin{document}\n" +
                "\\section*{Sample header}\\noindent Sample text\n" +
                "\\end{document}";
    private static final String sampleTeXFileName = "sampleTeX";
    private static final String testingDirectoryPath = "testDirectory";

    /**
     * used to pre-load image names on startup but not necessarily used as image source path
     * the set of image names in this folder shall be a subset of image names in the real resources/static/img folder used in 'src' property of 'img' elements in HTML pages
     */
    private static final String backgroundImagesFolderPath = "src/main/resources/static/img/background";

    /**
     * used to pre-load image names on startup but not necessarily used as image source path
     * the set of image names in this folder shall be a subset of image names in the real resources/static/img folder used in 'src' property of 'img' elements in HTML pages
     */
    private static String deployedApplicationBackgroundImagesFolderPath = null;

    /**
     * loaded from the configuration file and used as a path to pdflatex executable
     * if the PATH environment variable path cannot provide a path to this file
     */
    private static String pdflatexAbsolutePath = null;

    private static final String pdflatexAbsolutePathConfigurationFilePropertyName = "pdflatexAbsolutePath";
    private static final String configurationFilePath = "analyticGeometryOnlineConfig.properties";

    @Autowired
    public AppRunner(ApplicationContext ctx){
        this.ctx = ctx;
    }

    /**
     * Performs actions after spring application start
     * @param args
     */
    @Override
    public void run(ApplicationArguments args) throws IOException {
        loadConfigurationFile();
        log.trace("BEGIN Executing additional operations after application start");
        DatabaseSeeder sampleDataSeeder = new DatabaseSeeder(ctx.getBean(UserDAO.class), ctx.getBean(RegisterService.class), ctx.getBean(LibraryDAO.class), ctx.getBean(LibraryService.class));
        sampleDataSeeder.prepareSampleUserAndLibrary();
        log.trace("END Executing additional operations after application start");
        log.trace("BEGIN Validate pdflatex functionality");
        try{
            ensurePdfLatexAvailability(Constants.PDFLATEX_EXECUTABLE_NAME);
        } catch (RuntimeException ex){
            log.info("PdfLatex is not accessible through path variable, trying to use absolute path from configuration file");
            String path = pdflatexAbsolutePath;
            if(pdflatexAbsolutePath == null){
                log.error("The configuration file did not contain the propety " + pdflatexAbsolutePathConfigurationFilePropertyName);
            }
            ensurePdfLatexAvailability(path);
            Constants.PDFLATEX_EXECUTABLE_NAME = path;
        }
        log.trace("END Validate pdflatex functionality");
        try {
            //application context is loaded directly
            loadBackgroundImageNames(backgroundImagesFolderPath);
        } catch (RuntimeException ex){
            //application context is loaded from WAR archive - direct access is not available, a copy of the folder must be used
            loadBackgroundImageNames(deployedApplicationBackgroundImagesFolderPath);
        }
        log.trace("Background image names used to generate random background on webpage loaded");
        log.info(" ---- Web application is ready ---- ");
    }

    private void loadConfigurationFile() throws IOException {
        Properties properties = new Properties();
        try (FileInputStream in = new FileInputStream(configurationFilePath)) {
            properties.load(in);
        } catch (FileNotFoundException ex) {
            log.error("The configuration file " + configurationFilePath + " does not exist!");
            throw ex;
        } catch (IOException ex){
            log.error("The configuration file " + configurationFilePath + " could not be read!");
            throw ex;
        }

        pdflatexAbsolutePath = properties.getProperty("pdflatexAbsolutePath"); //null if doesnt contain

        String[] propertyNames = new String[]{
                "deployedApplicationBackgroundImagesFolderPath",
                "applicationMaintainerEmail", "applicationSourceEmail", "applicationSourceEmailPassword",
                "applicationSourceEmailHost", "applicationSourceEmailPort", "applicationSourceEmailSSLEnableBoolean", "applicationSourceEmailAuthEnableBoolean",
                "dbHostName", "dbName", "dbUser", "dbPassword"};

        for(String propertyName : propertyNames){
            if(properties.getProperty(propertyName) == null){
                String message = "The application configuration file " + configurationFilePath + " did not contain property " + propertyName;
                log.error(message);
                throw new RuntimeException(message);
            }
        }
        //values loaded
        deployedApplicationBackgroundImagesFolderPath = properties.getProperty(propertyNames[0]);

        EmailService.setApplicationMaintainerEmail(properties.getProperty(propertyNames[1]));
        EmailService.setApplicationSourceEmail(properties.getProperty(propertyNames[2]));
        EmailService.setApplicationSourceEmailPassword(properties.getProperty(propertyNames[3]));
        EmailService.setApplicationSourceEmailHost(properties.getProperty(propertyNames[4]));
        EmailService.setApplicationSourceEmailPort(properties.getProperty(propertyNames[5]));
        EmailService.setApplicationSourceEmailSSLEnableBoolean(properties.getProperty(propertyNames[6]));
        EmailService.setApplicationSourceEmailAuthEnableBoolean(properties.getProperty(propertyNames[7]));

        ConnectionFactory.setHostName(properties.getProperty(propertyNames[8]));
        ConnectionFactory.setDbName(properties.getProperty(propertyNames[9]));
        ConnectionFactory.setUser(properties.getProperty(propertyNames[10]));
        ConnectionFactory.setPassword(properties.getProperty(propertyNames[11]));
        ConnectionFactory.buildUrl();
    }

    private boolean deleteDirectory(File directory) {
        File[] allContents = directory.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directory.delete();
    }

    /**
     * Tries to run a test conversion TeX -> PDF using dummy TeX file (o page consisting of a header and a line of text)
     * to make sure that pdf can be generated from tex files using pdflatex<br>
     * If any problem occurs, it is logged and RuntimeException is thrown to prevent web application usage
     * @param pdfLatexPath
     */
    private void ensurePdfLatexAvailability(String pdfLatexPath){
        File outputDirectory = new File(testingDirectoryPath);
        if(!outputDirectory.exists()){
            boolean testingDirectoryCreated = outputDirectory.mkdir();
            if(!testingDirectoryCreated){
                String message = "The directory " + outputDirectory.getAbsolutePath() + " could not be created!";
                log.error(message);
                throw new RuntimeException(message);
            }
        }
        log.trace("Testing pdflatex functionality in directory \"" + outputDirectory.getAbsolutePath() + "\"");

        try {
            createSampleTeXFile();
        } catch (IOException ex){
            log.error("An error occurred during creating sample TeX file: " + ex.getLocalizedMessage());
            throw new RuntimeException("Application could not create a sample TeX file!");
        }
        try{
            Process pdfLatexExecution = Runtime.getRuntime().exec(
                    pdfLatexPath + " -interaction=batchmode -output-directory=" + testingDirectoryPath + " " + sampleTeXFileName + ".tex");
            pdfLatexExecution.waitFor();
        } catch (IOException | InterruptedException ex){
            String message1 = "An error occurred during generating sample PDF file from sample TeX file: " + ex.getLocalizedMessage();
            String message2 = "Please check if pdflatex.exe is accessible through PATH environment variable (e.g. C:\\Program Files\\MiKTeX 2.9\\miktex\\bin\\x64\\ is in PATH)";
            if(pdfLatexPath.equals(Constants.PDFLATEX_EXECUTABLE_NAME)){
                log.warn(message1);
                log.warn(message2);
            } else {
                log.error(message1);
                log.error(message2);
            }
            throw new RuntimeException("Application could not generate a sample PDF file using pdflatex! Is pdflatex on PATH? Exception: " + ex.getLocalizedMessage());
        }

        File pdfFile = Paths.get(testingDirectoryPath, sampleTeXFileName + ".pdf").toFile();
        if(pdfFile.exists()){
            log.trace("Sample PDF file creation success");
        } else {
            String message = "Sample PDF file generated during server initialization using pdflatex could not be found!";
            log.error(message);
            throw new RuntimeException(message);
        }


        boolean deleteConversionLogFile = true;
        if(!deleteConversionFile("tex") || !deleteConversionFile("pdf") || !deleteConversionFile("aux")){
            deleteConversionLogFile = false;
            log.error("There was an issue during performing sample conversion TeX -> PDF. Please check the conversion output log file " + Paths.get(testingDirectoryPath, sampleTeXFileName + ".log"));
            throw new RuntimeException("Some of pdflatex TeX -> PDF conversion output files did not exist! Check log file " + Paths.get(testingDirectoryPath, sampleTeXFileName + ".log)"));
        }
        
        if(deleteConversionLogFile){
            deleteConversionFile("log");
            if(!outputDirectory.delete()){
                throw new RuntimeException("Directory " + outputDirectory.getAbsolutePath() + " could not be deleted");
            }
            log.info("pdfLatex functionality successfully validated");
        }
    }


    private void createSampleTeXFile() throws IOException {
        File TeXFilePath = Paths.get(testingDirectoryPath, sampleTeXFileName + ".tex").toFile();
        try(OutputStreamWriter TeXFile = new OutputStreamWriter(new FileOutputStream(TeXFilePath, false), StandardCharsets.UTF_8.newEncoder())){
            TeXFile.write(sampleTeXFileContents);
        }
    }

    /**
     * Deletes a test conversion file (accepted extensions: aux, tex, pdf, log)
     * @param fileExtension
     * @return true iff file was deleted
     */
    private boolean deleteConversionFile(String fileExtension){
        if(!fileExtension.equals("aux") && !fileExtension.equals("tex") && !fileExtension.equals("pdf") && !fileExtension.equals("log")){
            throw new IllegalArgumentException("Invalid extension for conversion file deletion. Pdflatex conversion doesn't create files *." + fileExtension);
        }

        File conversionFile = Paths.get(testingDirectoryPath, sampleTeXFileName + "." + fileExtension).toFile();

        if(!conversionFile.exists()){
            log.error("A pdflatex conversion result file " + conversionFile.getName() + " doesn't exist!");
            throw new RuntimeException("Test run of generate PDF using pdflatex failed - file " + conversionFile.getName() + " does not exist!");
        }

        try {
            if(!conversionFile.delete()){
                log.error("A pdflatex conversion result file " + conversionFile.getName() + " was not be deleted!");
                return false;
            }
        } catch (SecurityException ex){
            log.error("A pdflatex conversion result file " + conversionFile.getName() + " could not be deleted, details: " + ex.getLocalizedMessage());
            return false;
        }
        log.trace("Test file " + conversionFile.getName() + " deleted");
        return true;
    }

    private void loadBackgroundImageNames(String path){
        File backgroundImagesFolder = new File(path);
        if(!backgroundImagesFolder.exists() || !backgroundImagesFolder.isDirectory()){
            throw new RuntimeException("The folder " + path + " containing background images does not exist!");
        }

        File[] images = backgroundImagesFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File path) {
                String name = path.getName().toLowerCase();
                return name.endsWith(".jpg") && path.isFile();
            }
        });
        if(images.length == 0){
            throw new RuntimeException("There are no jpg images in " + path + " folder to be used for webpage background randomization!");
        }

        ArrayList<String> imageNames = new ArrayList<>();
        for(File image : images){
            imageNames.add(image.getName());
        }
        ControllerUtilities.backgroundImagesFileNames = imageNames;
    }
}