package com.janseris.bp.webApp;

import com.janseris.bp.webApp.userTracker.OnlineUsersTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-23
 */

@WebListener //The listener is only initialized by application if this annotation is provided.
public class ServerStartListener implements ServletContextListener {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Initializes the UserTracker (online users counter)<br>
     * This happens before any filter or servlet in the web application is initialized.<br>
     * @param event The notification that the web application initialization process is starting which is automatically fired
     */
    @Override
    public void contextInitialized(ServletContextEvent event){
        //OnlineUsersTracker had been already instantiated by Spring by this moment
        log.info("ServletContext was initialized");
        ServletContext ctx = event.getServletContext();
        ctx.setAttribute(ControllerUtilities.SESSION_COUNTER_CTX_ATTR_NAME, OnlineUsersTracker.getInstance()); //use getInstance of already existing OnlineUsersTracker to assure single instance existence
    }
}
