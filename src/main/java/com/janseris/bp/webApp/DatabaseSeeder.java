package com.janseris.bp.webApp;

import com.janseris.bp.Constants;
import com.janseris.bp.LibrarySyntaxErrorException;
import com.janseris.bp.businessLayer.*;
import com.janseris.bp.dataAccessLayer.LibraryDAO;
import com.janseris.bp.model.PublicAccessibility;
import com.janseris.bp.dataAccessLayer.UserDAO;
import com.janseris.bp.model.LibraryDTO;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.model.UserRegisterDTO;
import com.janseris.bp.webApp.model.LibraryUseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-25
 */

public class DatabaseSeeder {
    private UserDAO userDAO;
    private RegisterService registerService;
    private LibraryDAO libraryDAO;
    private LibraryService libraryService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public DatabaseSeeder(UserDAO userDAO, RegisterService registerService, LibraryDAO libraryDAO, LibraryService libraryService){
        this.userDAO = userDAO;
        this.registerService = registerService;
        this.libraryDAO = libraryDAO;
        this.libraryService = libraryService;
    }

    private static final String SAMPLE_USER_NAME = "Analytic Geometry Online";
    private static final String SAMPLE_USER_EMAIL = "sampleUser@analyticGeometryOnline.com";
    private static final String SAMPLE_USER_PASSWORD = "sampleUserPassword1234";

    /**
     * Ensures that sample user with his sample library are present in database on server launch
     */
    public void prepareSampleUserAndLibrary(){
        UUID sampleUserId = null;
        UUID sampleLibraryId = null;
        try{
            sampleUserId = ensureSampleUserCreated();
        } catch (SQLException ex){
            throw new RuntimeException("The sample user could not be retrieved!");
        }
        try{
            prepareSampleLibrary(sampleUserId);
        } catch (SQLException ex){
            throw new RuntimeException("The sample library could not be created or retrieved!", ex);
        } catch (LibrarySyntaxErrorException syntaxError){
            throw new RuntimeException("The sample library contains syntax error: " + syntaxError.getMessage());
        }
    }

    /**
     * Registers a user if user doesn't already exist
     * @param name user name
     * @param email user email
     * @param password user password
     * @throws SQLException on db error while checking user existence
     * @return userId used for sample library insert
     */
    private UUID simulateRegistration(String name, String email, String password) throws SQLException {
        UserDTO maybeSampleUser = userDAO.getUserByEmail(email);
        if(maybeSampleUser != null){
            log.trace("Sample user is already present in the database.");
            return maybeSampleUser.getId();
        }

        RegistrationResult result = registerService.registerUser(new UserRegisterDTO(name, email, password));
        if(result != RegistrationResult.SUCCESS){
            throw new SQLException("The sample user could not be created!");
        }
        log.trace("Sample user was inserted into database.");
        return userDAO.getUserByEmail(email).getId();
    }

    private UUID ensureSampleUserCreated() throws SQLException {
        return simulateRegistration(SAMPLE_USER_NAME, SAMPLE_USER_EMAIL, SAMPLE_USER_PASSWORD);
    }

    /**
     * Ensures that sample library is present in database and that it is cached in LibraryService
     * @param sampleUserId sample user's ID
     * @throws SQLException if the sample library could not be created or retrieved
     */
    private void prepareSampleLibrary(UUID sampleUserId) throws SQLException {
        List<LibraryDTO> sampleUserLibraries = libraryDAO.getLibrariesCreatedByUser(sampleUserId);
        LibraryDTO sampleLibrary = null;
        if(sampleUserLibraries.size() == 1){
            sampleLibrary = sampleUserLibraries.get(0);
            if(sampleLibrary.getPublicAccessibility() != PublicAccessibility.PUBLIC){
                libraryDAO.updateLibrary(sampleLibrary.getId(), sampleLibrary.getName(), PublicAccessibility.PUBLIC);
                log.warn("Sample library was not public but it was forced public.");
            }
            log.trace("Sample library is already present in the database.");
        } else if(sampleUserLibraries.size() == 0){
            libraryDAO.addLibrary(sampleUserId, Constants.SAMPLE_LIBRARY_NAME, Constants.SAMPLE_LIBRARY_CONTENTS.getBytes(StandardCharsets.UTF_8), PublicAccessibility.PUBLIC);
            log.trace("Sample library was inserted into database.");
            sampleUserLibraries = libraryDAO.getLibrariesCreatedByUser(sampleUserId);
            sampleLibrary = sampleUserLibraries.get(0);
        } else {
            throw new IllegalStateException("The sample user is only permitted to have exactly 1 library which is the sample library!");
        }

        libraryService.setSampleLibraryId(sampleLibrary.getId());
        LibraryUseDTO parsedSampleLibrary = libraryService.loadLibraryForUse(sampleLibrary, false, true);
        libraryService.updateCachedSampleLibrary(sampleLibrary, parsedSampleLibrary);
    }

}
