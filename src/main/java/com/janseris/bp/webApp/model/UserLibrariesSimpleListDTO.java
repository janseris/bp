package com.janseris.bp.webApp.model;

import java.util.List;

/**
 * Carries either a list of user's libraries and a valid assignment object
 * or an error message if any problem occurs during libraries retrieval or if input is invalid
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-05-05
 */

public class UserLibrariesSimpleListDTO {
    private String message;
    private List<String> libraryNames;
    private List<String> libraryIds;

    public UserLibrariesSimpleListDTO(String message, List<String> libraryNames, List<String> libraryIds) {
        this.message = message;
        this.libraryNames = libraryNames;
        this.libraryIds = libraryIds;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getLibraryNames() {
        return libraryNames;
    }

    public void setLibraryNames(List<String> libraryNames) {
        this.libraryNames = libraryNames;
    }

    public List<String> getLibraryIds() {
        return libraryIds;
    }

    public void setLibraryIds(List<String> libraryIds) {
        this.libraryIds = libraryIds;
    }

}

