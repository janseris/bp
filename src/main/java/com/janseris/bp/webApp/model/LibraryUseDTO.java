package com.janseris.bp.webApp.model;

import java.util.List;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-20
 */

public class LibraryUseDTO {
    private String name;
    private UUID id; //for link to library details
    private List<AssignmentShowDTO> assignments;
    private String errorMessage;

    public LibraryUseDTO(){

    }

    public LibraryUseDTO(String name, UUID id, List<AssignmentShowDTO> assignments, String errorMessage) {
        this.name = name;
        this.id = id;
        this.assignments = assignments;
        this.errorMessage = errorMessage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<AssignmentShowDTO> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<AssignmentShowDTO> assignments) {
        this.assignments = assignments;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
