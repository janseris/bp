package com.janseris.bp.webApp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-13
 */

public class CheckSubspaceWithImageDTO {
    private String message;
    private byte[] imageData;

    public CheckSubspaceWithImageDTO(String message, byte[] imageData) {
        this.imageData = imageData;
        this.message = message;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
