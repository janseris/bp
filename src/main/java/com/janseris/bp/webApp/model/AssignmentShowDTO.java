package com.janseris.bp.webApp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-20
 */

public class AssignmentShowDTO {
    private String name;
    private String subspace1;
    private String subspace2;
    private int assignmentTypeIndex;
    private int index;

    public AssignmentShowDTO(int index, String name, String subspace1, String subspace2, int assignmentTypeIndex) {
        this.index = index;
        this.name = name;
        this.subspace1 = subspace1;
        this.subspace2 = subspace2;
        this.assignmentTypeIndex = assignmentTypeIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubspace1() {
        return subspace1;
    }

    public void setSubspace1(String subspace1) {
        this.subspace1 = subspace1;
    }

    public String getSubspace2() {
        return subspace2;
    }

    public void setSubspace2(String subspace2) {
        this.subspace2 = subspace2;
    }

    public int getAssignmentTypeIndex() {
        return assignmentTypeIndex;
    }

    public void setAssignmentTypeIndex(int assignmentTypeIndex) {
        this.assignmentTypeIndex = assignmentTypeIndex;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
