package com.janseris.bp.webApp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-05-05
 */

public class SaveAssignmentResultDTO {
    private String infoMessage;
    private String errorMessage;

    public SaveAssignmentResultDTO(){

    }

    public SaveAssignmentResultDTO(String infoMessage, String errorMessage) {
        this.infoMessage = infoMessage;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getInfoMessage() {
        return infoMessage;
    }

    public void setInfoMessage(String infoMessage) {
        this.infoMessage = infoMessage;
    }
}
