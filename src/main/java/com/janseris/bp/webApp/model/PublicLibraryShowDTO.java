package com.janseris.bp.webApp.model;

import com.janseris.bp.model.LibraryDTO;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-31
 */

public class PublicLibraryShowDTO {
    private UUID creatorUserId;
    private String creatorName;
    private UUID id;
    private String name;
    private String contents;
    private Timestamp dateCreated;
    private Timestamp lastDateModified;
    private Timestamp lastDateUsed;
    private int usageCount;

    public PublicLibraryShowDTO() {

    }

    public PublicLibraryShowDTO(LibraryDTO plainLibraryData) {
        this.creatorUserId = plainLibraryData.getCreatorUserId();
        this.id = plainLibraryData.getId();
        this.name = plainLibraryData.getName();
        this.contents = plainLibraryData.getContents();
        this.dateCreated = plainLibraryData.getDateCreated();
        this.lastDateModified = plainLibraryData.getLastDateModified();
        this.lastDateUsed = plainLibraryData.getLastDateUsed();
        this.usageCount = plainLibraryData.getUsageCount();
    }


    public UUID getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(UUID creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getLastDateModified() {
        return lastDateModified;
    }

    public void setLastDateModified(Timestamp lastDateModified) {
        this.lastDateModified = lastDateModified;
    }

    public Timestamp getLastDateUsed() {
        return lastDateUsed;
    }

    public void setLastDateUsed(Timestamp lastDateUsed) {
        this.lastDateUsed = lastDateUsed;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }

    @Override
    public String toString() {
        return "PublicLibraryShowDTO{" +
                "creatorUserId=" + creatorUserId +
                ", creatorName='" + creatorName + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", contents='" + contents + '\'' +
                ", dateCreated=" + dateCreated +
                ", lastDateModified=" + lastDateModified +
                ", lastDateUsed=" + lastDateUsed +
                ", usageCount=" + usageCount +
                '}';
    }
}
