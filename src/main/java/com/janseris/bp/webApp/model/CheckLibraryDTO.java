package com.janseris.bp.webApp.model;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-20
 */

public class CheckLibraryDTO {
    private String message;
    private boolean error;

    public CheckLibraryDTO(String message, boolean error) {
        this.message = message;
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
