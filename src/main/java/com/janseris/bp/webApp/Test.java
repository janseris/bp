package com.janseris.bp.webApp;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Test {
    public static void main(String[] args) throws IOException {

        disableHttpClientLogging();

        File folder = new File("./txt");
        File[] listOfFiles = folder.listFiles();
        List<Charset> encodings = new ArrayList<>();

        byte[] source;
        for (File file : listOfFiles) {
            if (file.isFile() && file.getName().contains(".txt")) {
                encodings.add(detectCharsetOnline(Files.readAllBytes(Paths.get(file.getCanonicalPath()))));
            }
        }

        int i = 0;
        Charset charset;
        for (File file : listOfFiles) {
            if (file.isFile() && file.getName().contains(".txt")) {
                charset = encodings.get(i);
                if(charset != null && !charset.equals("utf_8")){
                    System.out.println(file.getName() + " charset: " + charset);
                }
                i++;
            }
        }
    }

    /**
     * Detects charset using nlp.fi.muni.cz tool "Chared"
     * @see <a href="https://nlp.fi.muni.cz/projects/chared/">https://nlp.fi.muni.cz/projects/chared/</a>
     * @param source text file bytes
     * @return
     * @throws IOException on communication error with nlp.fi.muni.cz
     * @throws IllegalArgumentException if the charset name returned from nlp.fi.muni.cz was not recognized
     * @throws UnsupportedOperationException if nlp.fi.muni.cz request does not succeed or their response body changes -> switch to regular detection
     */
    public static Charset detectCharsetOnline(byte[] source) throws IOException {
        String url = "https://nlp.fi.muni.cz/projects/chared/";
        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpEntity entity = MultipartEntityBuilder
                .create()
                .addTextBody("language", "czech")
                .addBinaryBody("file", source, ContentType.create("application/octet-stream"), "filename")
                .addTextBody("submit_file", "Submit Query")
                .build();
        HttpPost request = new HttpPost(url);
        request.setEntity(entity);
        HttpResponse response = null;
        try{
            response = httpclient.execute(request);
        } catch (Exception ex) { //communication error
            throw new IOException(ex);
        }
        return getCharsetStrFromResponse(response);
    }


    /**
     * Gets charset from nlp.fi.muni.cz http response
     * @param response
     * @return
     * @throws IOException
     * @throws IllegalArgumentException if the charset name returned from nlp.fi.muni.cz was not recognized
     * @throws UnsupportedOperationException if nlp.fi.muni.cz request does not succeed or their response body changes -> switch to regular detection
     */
    private static Charset getCharsetStrFromResponse(HttpResponse response) throws IllegalArgumentException, UnsupportedOperationException{
        int responseStatusCode = response.getStatusLine().getStatusCode();
        if(responseStatusCode!= 200){
            throw new UnsupportedOperationException("Unexpected nlp.fi.muni charset detection tool response status code " + responseStatusCode);
        }
        String responseString;
        try{
            responseString = new BasicResponseHandler().handleResponse(response);
        } catch (IOException ex){
            return StandardCharsets.UTF_8;
        }
        String searched = "Detected by Chared: ";
        int searchedIndex = responseString.indexOf("Detected by Chared: ");
        int startIndex = responseString.indexOf("Detected by Chared: ") + searched.length();
        int endH3Index = responseString.indexOf("</h3>", startIndex);
        if(searchedIndex == -1 || endH3Index == -1){
            throw new UnsupportedOperationException("nlp.fi.muni charset detection tool response body pattern changed");
        }
        String charsetStr = responseString.substring(startIndex, endH3Index);
        return parseCharsetStr(charsetStr);
    }

    /**
     * Tries to recognize the charset by charset name returned from nlp.fi.muni.cz
     * @param charsetStr charset name returned from nlp.fi.muni.cz
     * @return
     * @throws IllegalArgumentException if the charset name returned from nlp.fi.muni.cz was not recognized
     */
    private static Charset parseCharsetStr(String charsetStr) throws IllegalArgumentException{
        if(charsetStr != null && charsetStr.contains("_")){
            charsetStr = charsetStr.replace('_', '-'); //nlp.fi.muni.cz returns "utf_8" etc.
        }
        try{
            return Charset.forName(charsetStr);
        } catch (Exception ex){
            throw new IllegalArgumentException(charsetStr); //charset name not recognized (null or invalid charset name)
        }
    }

    private static void disableHttpClientLogging(){
        Set<String> loggers = new HashSet<>(Arrays.asList("org.apache.http", "groovyx.net.http"));

        for(String log:loggers) {
            Logger logger = (Logger)LoggerFactory.getLogger(log);
            logger.setLevel(Level.ERROR);
            logger.setAdditive(false);
        }
    }
}
