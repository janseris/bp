package com.janseris.bp.webApp;

import ch.qos.logback.classic.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.util.unit.DataSize;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.MultipartConfigElement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/*
 Did you notice that there was not a single line of XML?
 There is no web.xml file, either.
 This web application is 100% pure Java and you did not have to deal with configuring any plumbing or infrastructure.
 */

//By default, Spring Boot serves static content from resources in the classpath at /static (or /public) (.html and .css etc)

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-20
 */

/*
    @SpringBootApplication is a convenience annotation that adds all of the following:
        @Configuration:            Tags the class as a source of bean definitions for the application context.
        @EnableAutoConfiguration:  Tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings.
                                   For example, if spring-webmvc is on the classpath, this annotation flags the application as a web application and activates key behaviors,
                                     such as setting up a DispatcherServlet.
        @ComponentScan:            Tells Spring to look for other components, configurations, and services in a specified package, letting it find the controllers
 */

@SpringBootApplication
@ComponentScan(basePackages={"com.janseris.bp"}) //makes sure that all annotated classes for Spring are found (@Service, @Repository, @Controller, ...) and instantiated
@ServletComponentScan //will scan also for classes annotated by @WebServlet, @WebFilter and @WebListener
@EnableAsync //https://dzone.com/articles/spring-boot-creating-asynchronous-methods-using-as
public class BpSpringWebApplication extends SpringBootServletInitializer /* needed for WAR packaging */{

    @Autowired
    private ApplicationContext appContext; //provides access to objects instantiated by Spring framework IoC container
    private static final Logger log = LoggerFactory.getLogger(BpSpringWebApplication.class);

    public static boolean DEVELOPMENT_EXCEPTION_VIEW_ON_ERROR_PAGE = false;
    private static final int MAXIMUM_UPLOAD_FILE_SIZE_KB = 32;

    //how to log form any class: declare private static final Logger log = LoggerFactory.getLogger(BpSpringWebApplication.class); or this.getClass()
    //Log levels: OFF > FATAL > ERROR > WARN > INFO > DEBUG > TRACE > ALL
    //set log level: logging.level.root = LEVEL v application.properties

    private static void disableApacheHttpClientLogging(){
        Set<String> loggers = new HashSet<>(Arrays.asList("org.apache.http", "groovyx.net.http"));

        for(String log:loggers) {
            ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(log);
            logger.setLevel(Level.ERROR);
            logger.setAdditive(false);
        }
    }


    //needed for WAR packaging
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); //register the driver (needed for running with real Tomcat, otherwise, driver is registered upon any second db call so first db call always fails)
            log.info("SQLServerDriver registered");
        } catch (ClassNotFoundException ex){
            log.error("Class com.microsoft.sqlserver.jdbc.SQLServerDriver not found.");
        }
        return app.sources(BpSpringWebApplication.class);
    }


    public static void main(String[] args){
        disableApacheHttpClientLogging();
        SpringApplication.run(BpSpringWebApplication.class, args);
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.ofKilobytes(MAXIMUM_UPLOAD_FILE_SIZE_KB));
        factory.setMaxRequestSize(DataSize.ofKilobytes(MAXIMUM_UPLOAD_FILE_SIZE_KB*2));
        return factory.createMultipartConfig();
    }
}



@Configuration
class WebContentCachingConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        //https://www.boraji.com/spring-mvc-5-static-resources-handling-example

        // Register resource handler for images (stay cached in browser for 1 days)
        registry.addResourceHandler("/img/**").addResourceLocations("classpath:/static/img/")
                .setCacheControl(CacheControl.maxAge(1, TimeUnit.DAYS).cachePublic());

        //cachePublic - can be stored in any shared or private cache (no user-private content)

        //other static content is not cached at all (spring web default behavior)
    }
}
