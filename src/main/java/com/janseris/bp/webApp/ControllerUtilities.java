package com.janseris.bp.webApp;

import com.janseris.bp.model.UserDTO;
import com.janseris.bp.webApp.userTracker.AnonymousUserInfo;
import com.janseris.bp.webApp.userTracker.LoggedInUserInfo;
import com.janseris.bp.webApp.userTracker.OnlineUserInfo;
import com.janseris.bp.webApp.userTracker.OnlineUsersTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.*;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-23
 */

public final class ControllerUtilities {

    private static final Logger log = LoggerFactory.getLogger(ControllerUtilities.class);

    private ControllerUtilities(){

    }

    public static final int LIBRARY_CONTENT_PREVIEW_MAX_LENGTH = 255;

    public static final int ANONYMOUS_USER_SID_SUBSTRING_LENGTH = 6; //if number exceeds sid length, then it is normalized
    public static final String ANONYMOUS_USER_PREFIX_STR = "anonymní uživatel ";
    public static final String ANONYMOUS_USER_DATA_CTX_ATTR_NAME = "anonymousUserName";
    public static final String LOGGED_IN_USER_DATA_CTX_ATTR_NAME = "userData";
    public static final String SESSION_COUNTER_CTX_ATTR_NAME = "userTracker";

    public static final String ONLY_LOGGED_IN_USER_ALLOWED_MESSAGE = "Pro tuto akci musíte být přihlášen(a)";
    public static final String SESSION_TIMEOUT_MESSAGE = "Session timed out";
    public static final int SESSION_INACTIVE_TIMEOUT_SECONDS = 1800; //session is automatically invalidated after this period of time

    public static final String TEMPORARY_ASSIGNMENT_TO_BE_SAVED_INTO_LIBRARY_SESSION_ATTRIBUTE_NAME = "tempAssignment";

    public static ArrayList<String> backgroundImagesFileNames;

    public static final double ELIMINATE_BACKGROUND_IMAGE_REPEAT_CHANCE = 0.7; //0.0 (never pick next random on collision) to 1.0 (always pick different random on collision)


    public static String getFormattedDate(long unixTimeStamp){
        Date sessionCreationDate =  new Date(unixTimeStamp);
        DateFormat f = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM, Locale.getDefault());
        return f.format(sessionCreationDate);
    }

    public static String getFormattedDate(Timestamp sqlTimeStamp){
        Date sessionCreationDate =  new Date(sqlTimeStamp.getTime());
        DateFormat f = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM, Locale.getDefault());
        return f.format(sessionCreationDate);
    }


    /**
     * Assigns a new session to the request
     * @param request
     */
    //The session data is stored on the server, so it's perfectly safe to save some private data in session attributes.
    //Faster SID generation?: https://stackoverflow.com/questions/28201794/slow-startup-on-tomcat-7-0-57-because-of-securerandom#answer-40647727
    public static HttpSession createSession(HttpServletRequest request){
        //https://stackoverflow.com/questions/1577236/how-to-check-whether-a-user-is-logged-in-or-not-in-servlets
        if(isSessionValid(request)){
            throw new IllegalStateException("A new session can be generated only if the old one has been invalidated or no session existed!");
        }
        HttpSession session = request.getSession(); //create new session
        return session;
    }

    /**
     * This shall be called in every controller to ensure that user always has at least anonymous session (to be able to use the math computing part)<br>
     * This is called when<br>
     * 1) the session is null (first access)<br>
     * 2) the old session is no longer valid (timeout or logout)
     * @param request current request
     */
    public static void createAnonymousUserSession(HttpServletRequest request){
        HttpSession session = ControllerUtilities.createSession(request);
        String anonymousUserName = ControllerUtilities.generateAnonymousUsername(session.getId());
        session.setAttribute(ControllerUtilities.ANONYMOUS_USER_DATA_CTX_ATTR_NAME, anonymousUserName);
        OnlineUsersTracker userTracker = (OnlineUsersTracker)request.getServletContext().getAttribute(ControllerUtilities.SESSION_COUNTER_CTX_ATTR_NAME);
        OnlineUserInfo anonymousUserInfo = new AnonymousUserInfo(session.getId());
        userTracker.addOnlineUser(anonymousUserInfo, session);
    }

    /**
     * Checks if the current session exists and is valid
     * @param request processed HTTP request
     * @return false -> session is null or invalidated
     */
    public static boolean isSessionValid(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if (session == null) { //session didn't exist
            return false;
        }
        if(!request.isRequestedSessionIdValid()) { //session has been invalidated in the past
            return false;
        }
        return true;
    }

    public static String generateAnonymousUsername(String sid){
        int substringLength = ANONYMOUS_USER_SID_SUBSTRING_LENGTH;
        if(substringLength > sid.length()){
            substringLength = sid.length();
        }
        return ANONYMOUS_USER_PREFIX_STR + sid.substring(0,substringLength);
    }

    /**
     * Checks if user is logged in (if the session attribute "userData" is used)<br>
     * And if the logged in session is valid
     * @param session whose session is checked
     * @return true if session.userName exists, else false
     */
    public static boolean isLoggedInUser(HttpSession session){
        if(session == null){
            return false;
        }
        if(session.getAttribute(LOGGED_IN_USER_DATA_CTX_ATTR_NAME) == null){
            return false;
        }
        return true;
    }

    /**
     * Returns current user's name (both anonymous or logged in)<br>
     * Currently only used for logging purposes
     * @param session a valid session
     * @return user name of the current user represented by request session attributes
     */
    public static String getCurrentUserName(HttpSession session){
        boolean isLoggedInUser = ControllerUtilities.isLoggedInUser(session);
        String userName = isLoggedInUser ? ((UserDTO)session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME)).getName() : (String)session.getAttribute(ControllerUtilities.ANONYMOUS_USER_DATA_CTX_ATTR_NAME);
        return userName;
    }

    /**
     * When a user logs in, his old anonymous or logged in session is invalidated<br>
     * If a user logs in twice, his old logged in session is invalidated (OnlineUserTracker map onlineUserEntries takes care of this)<br>
     * @see OnlineUsersTracker
     * Every user has anonymous session or logged in session (null session is not permitted)
     * @param request
     * @param loggedInUserData
     */
    public static void manageSessionsOnLogIn(HttpServletRequest request, UserDTO loggedInUserData){
        HttpSession oldSession = request.getSession(false);
        boolean wasLoggedInUser = isLoggedInUser(oldSession);
        String oldSessionId = oldSession.getId();

        //if was logged in then logged in name else anonymous name
        String previousUsername = wasLoggedInUser ?
                ((UserDTO)oldSession.getAttribute(LOGGED_IN_USER_DATA_CTX_ATTR_NAME)).getName() :
                (String)oldSession.getAttribute(ANONYMOUS_USER_DATA_CTX_ATTR_NAME);
        //invalidate old session (logged in or anonymous), user whose session is invalidated,
        // is automatically removed from online users list
        oldSession.invalidate();
        HttpSession newSession = createSession(request);

        log.trace("Logging in: " + previousUsername + " (" + oldSessionId + ") -> " +
                loggedInUserData.getName() + " (" + newSession.getId() + ")");

        newSession.setAttribute(LOGGED_IN_USER_DATA_CTX_ATTR_NAME, loggedInUserData); //add user data to logged in user session

        OnlineUsersTracker userTracker = (OnlineUsersTracker)request.getServletContext()
                .getAttribute(ControllerUtilities.SESSION_COUNTER_CTX_ATTR_NAME);
        OnlineUserInfo loggedInUserInfo = new LoggedInUserInfo(loggedInUserData.getName(), loggedInUserData.getEmail());
        if(userTracker.isUserOnline(loggedInUserInfo)){ //user logs in but he is already online (from a different browser)
            userTracker.invalidateLoggedInUserSession(loggedInUserInfo);
        }
        userTracker.addOnlineUser(loggedInUserInfo, newSession);
    }

    //access classes in Thymeleaf https://stackoverflow.com/questions/43841458/how-to-call-a-service-method-with-thymeleaf
    //call static class which is not connected with Spring MVC: T() with the full class name, like ${T(com.company.package.SomeClass).staticMethod()}

    public static void printSessionAttributes(HttpSession session){
        Enumeration<String> attributes = session.getAttributeNames();
        while (attributes.hasMoreElements()) {
            String attribute = attributes.nextElement();
            System.out.println(attribute + " : " + session.getAttribute(attribute));
        }
    }

    public static String trimLibraryContentsForPreview(String libraryContents){
        String end = ". . .";
        if(libraryContents.length() < LIBRARY_CONTENT_PREVIEW_MAX_LENGTH){
            return libraryContents;
        }
        return libraryContents.substring(0, LIBRARY_CONTENT_PREVIEW_MAX_LENGTH - end.length()) + end;
    }

    public static ArrayList<String> getRandomBackgroundImages(){
        return getRandomBackgroundImages(backgroundImagesFileNames.size());
    }

    public static ArrayList<String> getRandomBackgroundImages(int amount){
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int backgroundImagesCount = backgroundImagesFileNames.size();

        if(amount < 0){
            throw new IllegalArgumentException("Invalid negative count of background images");
        }
        if(amount > backgroundImagesCount){
            amount %= backgroundImagesCount; //normalise any input number to 0 ... n
        }

        ArrayList<String> selectedImageNames = new ArrayList<>();

        Random randomImageIndex = new Random();
        boolean[] alreadySelected = new boolean[backgroundImagesCount]; //values initialized to false
        Random randomDouble = new Random(); //0.0 to 1.0

        int iterationsTotal = 0;
        for(int i = 0; i < amount; i++){
            int randomPickedImageIndex = randomImageIndex.nextInt(backgroundImagesCount);
            if(randomDouble.nextDouble() < ELIMINATE_BACKGROUND_IMAGE_REPEAT_CHANCE){
                if(alreadySelected[randomPickedImageIndex]){
                    //System.out.println(backgroundImagesFileNames.get(randomPickedImageIndex) + " colission");
                    i--;
                    iterationsTotal++;
                    continue;
                }
            }
            alreadySelected[randomPickedImageIndex] = true;
            selectedImageNames.add(backgroundImagesFileNames.get(randomPickedImageIndex));
            //System.out.println(selectedImageNames.get(i) + " picked");
            iterationsTotal++;
        }

        double percent = ((double)iterationsTotal / (double)amount)*100;
        stopWatch.stop();
        log.trace("Picking random background images took " + ((double)stopWatch.getTotalTimeNanos())/1000000 + "ms, total iterations: " + iterationsTotal + " (" + percent + "% of images count)");
        return selectedImageNames;
    }
}
