package com.janseris.bp.webApp.userTracker;

import com.janseris.bp.IOHelper;
import com.janseris.bp.model.UserDTO;
import com.janseris.bp.webApp.ControllerUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-25
 */

@WebListener //The listener is only initialized by application if this annotation is provided.
//The only instance which listens, is the one automatically instantiated by Spring (actually, Servlet API)
public class OnlineUsersTracker implements HttpSessionListener {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static OnlineUsersTracker instance = null; //Singleton

    //Singleton
    public static OnlineUsersTracker getInstance(){
        if(instance == null){
            instance = new OnlineUsersTracker();
        }
        return instance;
    }

    private Map<OnlineUserInfo, HttpSession> onlineUserEntries = new HashMap<>(); //sorted by keys to enable quick key removal and also quick value removal
    //keys and values are stored by reference
    private int numberOfUsersOnline = 0;

    //The @WebListener annotation works even when the class is a real Singleton. Good.

    /**
     * Constructs an online user tracker.
     */
    public OnlineUsersTracker(){
        if(instance == null){
            instance = this; //this must be done because application context initializes all classes via constructors
        } else {
            throw new IllegalStateException("Online users tracker instance already exists");
        }
    }

    public Map<OnlineUserInfo, HttpSession> getOnlineUserEntries() {
        return onlineUserEntries;
    }

    public int getNumberOfUsersOnline() {
        return numberOfUsersOnline;
    }

    /**
     * Adds the email (logged in user) or sid (anonymous user) and the associated session to tracking
     * @param userInfo containing a unique identifier (sid or email)
     * @param session to be tracked
     */
    public void addOnlineUser(OnlineUserInfo userInfo, HttpSession session){
        if(session == null){
            throw new IllegalArgumentException("Null sessions cannot be added to online user entries");
        }
        synchronized (this) {
            if(isUserOnline(userInfo)){
                throw new IllegalStateException("This method shall be only called when user is not already online");
            }
            numberOfUsersOnline++;
            onlineUserEntries.put(userInfo, session);
            log.trace("Online users tracker: starting tracking user " + userInfo.getNameShown());
        }
    }

    /**
     * Used for<br>
     * 1) logout (user's logged in session is invalidated and removed from online user tracker)<br>
     * 2) automatic logout (if user tries to log in from second device, his old logged in session is invalidated, only the new logged in instance is kept)
     */
    public void invalidateLoggedInUserSession(OnlineUserInfo userInfo){
        synchronized (this){
            HttpSession session = onlineUserEntries.get(userInfo);
            session.invalidate(); //on session invalidate, user is automatically removed from online users
        }
    }

    /**
     * Checks if logged in user already has a session on the server ("is logged in")
     * @param userInfo containing unique identifier (unique in this web-app instance) - for logged in user, that is email.
     * @return
     */
    public boolean isUserOnline(OnlineUserInfo userInfo){
        HttpSession session = onlineUserEntries.get(userInfo);
        if(session == null){
            return false;
        }
        try {
            long ignored = session.getCreationTime();
        } catch (IllegalStateException ise) {
            // it's been invalidated (timeout)
            return false;
        }
        return true;
    }

    /**
     * Performs actions on session creation<br>
     * 1) set session destruction interval on inactivity<br>
     * 2) generate random images for background
     * @param se event that was triggered by session creation
     */
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        session.setMaxInactiveInterval(ControllerUtilities.SESSION_INACTIVE_TIMEOUT_SECONDS);
        long sessionInvalidationPeriodMs = session.getMaxInactiveInterval() * 1000;
        String formattedInvalidationDate = ControllerUtilities.getFormattedDate(session.getCreationTime() + sessionInvalidationPeriodMs);
        session.setAttribute("backgroundImages", ControllerUtilities.getRandomBackgroundImages());
        log.trace("New session created, SID: " + session.getId() + ", session will be invalidated after inactive for: " + session.getMaxInactiveInterval() + " seconds (on " + formattedInvalidationDate + ")");
    }

    /**
     * "session finalizer"<br>
     * User name is removed from online users and the counter is decremented<br>
     * Folder containing temporary user files (txt, TeX and pdf output) is removed<br>
     * Happens right before session and its data are invalidated
     * @param event event which is triggered by session invalidation (probably right before invalidation)
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        //this also automatically happens on session timeout (user inactivity)

        HttpSession session = event.getSession(); //session which is to be invalidated

        String uniqueIdentifier;
        if (ControllerUtilities.isLoggedInUser(session)) {
            uniqueIdentifier = ((UserDTO) session.getAttribute(ControllerUtilities.LOGGED_IN_USER_DATA_CTX_ATTR_NAME)).getEmail();
        } else {
            uniqueIdentifier = (String) session.getId();
        }

        OnlineUserInfo toBeRemoved = new MapSearchUserInfo(uniqueIdentifier); //no matter if anonymous or logged in is instantiated here

        synchronized (this) {
            numberOfUsersOnline--;
            HttpSession val = onlineUserEntries.remove(toBeRemoved);
            if (val == null) {
                throw new IllegalStateException("Trying to remove user entry from onlineUsers but the associated HttpSession for this user info in map was null??");
            }
        }
        deleteUserFolder(session.getId());
    }

    /**
     * Removes folder with user's result files if it existed (created by {@link com.janseris.bp.businessLayer.ProgramService#clearOutputFiles(IOHelper) upon initiating first solve request}
     * To be called inside session finalizer<br>
     * @see OnlineUsersTracker#sessionDestroyed(HttpSessionEvent)
     * The folder name matches SID to ensure name uniqueness among all current web application users
     * @param sid session ID which is to be invalidated
     */
    private void deleteUserFolder(String sid){
        File userFolder = new File(sid);
        if (!userFolder.isDirectory() || !userFolder.exists()) {
            return; //directory didn't exist
        }
        for (File file : userFolder.listFiles()){
            if (file.isDirectory()) {
                throw new RuntimeException("Unexpected directory inside user folder SID: " + sid + " which was intended to be removed on session destroy!");
            }
            boolean deleted;
            try{
                deleted = file.delete();
                if(!deleted){
                    throw new IOException();
                }
            } catch (Exception ex){
                throw new RuntimeException("A file in the folder for user SID: " + sid + " intended to be removed on session destroy could not be deleted!");
            }
        }

        try{
            boolean deleted = userFolder.delete();
            if(!deleted){
                throw new IOException();
            }
        } catch (Exception ex){
            throw new RuntimeException("The folder for user SID: " + sid + " intended to be removed on session destroy could not be deleted!");
        }
    }
}