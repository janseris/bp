package com.janseris.bp.webApp.userTracker;

import com.janseris.bp.webApp.ControllerUtilities;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-25
 */

public class AnonymousUserInfo extends OnlineUserInfo {
    private String sid;
    private String name;

    public AnonymousUserInfo(String sid){
        this.sid = sid;
        this.name = ControllerUtilities.generateAnonymousUsername(sid);
    }

    @Override
    public String getNameShown() {
        return name;
    }

    @Override
    public String getUniqueIdentifier() {
        return sid;
    }
}
