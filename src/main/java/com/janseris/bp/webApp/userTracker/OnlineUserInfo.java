package com.janseris.bp.webApp.userTracker;

import java.util.Objects;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-25
 */


public abstract class OnlineUserInfo {
    public abstract String getNameShown();
    public abstract String getUniqueIdentifier();

    @Override
    public int hashCode() {
        return Objects.hash(getUniqueIdentifier());
    }

    /**
     * The equality of two user infos is defined by their uniqueidentifier
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OnlineUserInfo)){
            //Comparing an object which is not even OnlineUserInfo
            return false;
        }
        OnlineUserInfo other = (OnlineUserInfo) o;
        return getUniqueIdentifier().equals(other.getUniqueIdentifier());
    }
}
