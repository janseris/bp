package com.janseris.bp.webApp.userTracker;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-25
 */

public class LoggedInUserInfo extends OnlineUserInfo {
    private String name;
    private String email;

    public LoggedInUserInfo(String name, String email){
        this.name = name;
        this.email = email;
    }

    @Override
    public String getNameShown() {
        return name;
    }

    @Override
    public String getUniqueIdentifier() {
        return email;
    }
}
