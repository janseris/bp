package com.janseris.bp.webApp.userTracker;

/**
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-03-25
 */


public class MapSearchUserInfo extends OnlineUserInfo {
    private String uniqueIdentifier;

    public MapSearchUserInfo(String uniqueIdentifier){
        this.uniqueIdentifier = uniqueIdentifier;
    }

    @Override
    public String getNameShown() {
        return null;
    }

    @Override
    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }
}
