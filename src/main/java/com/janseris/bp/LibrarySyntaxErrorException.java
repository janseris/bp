package com.janseris.bp;

/**
 * A unchecked exception to be raised if a syntax error is detected in a library text file
 * It is unchecked because it cannot be raised in some scenarios at all
 * @author Jan Seris <456435@mail.muni.cz>
 * @since 2020-04-20
 */
public class LibrarySyntaxErrorException extends RuntimeException {
    public LibrarySyntaxErrorException(String errorMessage) {
        super(errorMessage);
    }

    public LibrarySyntaxErrorException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}