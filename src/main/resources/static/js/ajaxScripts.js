
/* REGION ajax check subspace handler */

//použít pro všechny 3 subspaces stejnou funkci, podle id tlačítka zjistit, obsah kterého textarea se má kontrolovat
function checkSubspaceAjax(senderButton){
    var senderId = senderButton.id;
    var inputElementId = getSubspaceInputElementId(senderId);
    var subspaceInputElement = document.getElementById(inputElementId);
    var subspaceInput = subspaceInputElement.value;
    $.ajax({
        type: "POST",
        url: "checkSubspaceAjax",
        data: {"input" : subspaceInput, "senderId" : senderId},
        dataType: "json", //response content type
        contentType: "application/x-www-form-urlencoded; charset=UTF-8", //request content type
        success: function(response){
            handleCheckSubspaceAjaxResponse(response, subspaceInputElement);
        },
        error: function(JQxhr, errorType, exception){
            alert('Unhandled exception caused by AJAX request processing.');
        }
    });
}

function loadUserLibraryNamesAjax(){
    var subspace1InputTextarea = document.getElementById("subspace1Input");
    var subspace2InputTextarea = document.getElementById("subspace2Input");
    var subspace1Input = subspace1InputTextarea.value;
    var subspace2Input = subspace2InputTextarea.value;
    var selectedTask = document.getElementById("assignmentType").value;
    var whereInsertMessageAfter = document.getElementById("programButtonsContainer");

    $.ajax({
        type: "POST",
        url: "loadUserLibraryNames",
        data: {"subspace1Input" : subspace1Input, "subspace2Input" : subspace2Input, "selectedTask" : selectedTask},
        dataType: "json", //response content type
        contentType: "application/x-www-form-urlencoded; charset=UTF-8", //request content type
        success: function(response){
            handleLoadUserLibrariesAjaxResponse(response, whereInsertMessageAfter);
        },
        error: function(JQxhr, errorType, exception){
            alert('Unhandled exception caused by AJAX request processing: ' + exception);
        }
    });
}

function addAssignmentToLibraryAjax(){
    var savedAssignmentName = document.getElementById("savedAssignmentName").value;
    var selectedLibraryId = document.getElementById("selectedLibrary").value;
    var whereInsertMessageAfter = document.getElementById("savedAssignmentName");

    $.ajax({
        type: "POST",
        url: "saveAssignment",
        data: {"savedAssignmentName" : savedAssignmentName, "selectedLibraryId": selectedLibraryId},
        dataType: "json", //response content type
        contentType: "application/x-www-form-urlencoded; charset=UTF-8", //request content type
        success: function(response){
            handleSaveAssignmentAjaxResponse(response, whereInsertMessageAfter);
        },
        error: function(JQxhr, errorType, exception){
            alert('Unhandled exception caused by AJAX request processing: ' + exception);
        }
    });
}

function getSubspaceInputElementId(senderButtonId){
    if(senderButtonId == "subspace1InputCheckButton") {
        return "subspace1Input";
    }
    if (senderButtonId == "subspace2InputCheckButton") {
        return "subspace2Input";
    }
    if (senderButtonId == "subspacesIntersectionInputCheckButton") {
        return "intersectionSubspaceInput";
    }
}

//sample how to add an html element into webpage
function bodyAppend(tagName, innerHTML) {
    var elem;
    elem = document.createElement(tagName);
    elem.innerHTML = innerHTML; //the html code contained inside this element (for paragraph, this results in text attribute if no html tag detected)
    elem.setAttribute("class", "errorMessage"); //for class style applied from css
    elem.setAttribute("id", "fileUploadWarning"); //for identification while checking content
    document.body.appendChild(elem);
}

//insert a new html element
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function insertBefore(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode);
}

//with .remove() on null object, all latter js code silently fails to execute (can be seen through console)
function safeDeleteElement(id){
    var elem = document.getElementById(id);
    if(elem != null){
        elem.remove();
    }
}

//response object containing string "message" and byte[] "imageData"
function handleCheckSubspaceAjaxResponse(response, messageLocation){
    //clean workspace
    safeDeleteElement("checkSubspaceErrorMessage");
    safeDeleteElement("checkSubspaceInfoMessage");
    safeDeleteElement("checkSubspaceImageContainer");

    //generate elements

    if(response.imageData == undefined){
        generateCheckSubspaceErrorMessage(response.message, messageLocation);
    } else {
        generateCheckSubspaceInfoMessage(response.message, messageLocation);
        var imageContainer = document.createElement("div");
        imageContainer.setAttribute("class", "checkSubspaceImageContainer");
        imageContainer.setAttribute("id", "checkSubspaceImageContainer");
        imageContainer.innerHTML = '<p class="closeButton" onclick="closeTeXImage();">&#x2718;</p><p style="margin: 0; cursor: move;" id="checkSubspaceImageContainerHeader">Reprezentace podprostoru programem</p><img class="checkSubspaceImage" src="data:image/png;base64,' + response.imageData + '" />'

        var form = document.getElementById("mainForm");
        form.insertBefore(imageContainer, form.firstChild);

        //imageContainer width and height gives wrong values here
        // Make the DIV element draggable:
        dragElement(imageContainer);
    }
}

//response object containing string "message" and String[] "libraryNames"
function handleLoadUserLibrariesAjaxResponse(response, messageLocation){
    //clean workspace
    safeDeleteElement("loadUserLibraryNamesErrorMessage");
    safeDeleteElement("loadUserLibraryNamesInfoMessage");
    safeDeleteElement("userLibraryNamesContainer");

    //generate elements

    if(response.libraryNames == undefined){
        generateLoadUserLibraryNamesErrorMessage(response.message, messageLocation);
    } else {
        //generateLoadUserLibraryNamesInfoMessage(response.message, messageLocation);
        var imageContainer = document.createElement("div");
        imageContainer.setAttribute("class", "userLibraryNamesContainer");
        imageContainer.setAttribute("id", "userLibraryNamesContainer");
        imageContainer.innerHTML = '<p class="closeButton" onclick="closeUserLibraryNamesContainer();">&#x2718;</p>' +
            '<p style="margin: 0;">Uložení příkladu</p>';

        var tmpHtml = '<select id="selectedLibrary" style="margin: 5px;">';

        var arrayLength = response.libraryNames.length;
        for (var i = 0; i < arrayLength; i++) {
            tmpHtml += '<option value="' + response.libraryIds[i] + '">' + response.libraryNames[i] + '</option>';
        }
        tmpHtml += '</select>';
        tmpHtml += '<label for="savedAssignmentName" class="registrationLabel">Název příkladu:</label>' +
            '<input type="text" class="registrationInput" id="savedAssignmentName"/>';
        tmpHtml += '<button class="submitFormButton" style="margin-right: 5px;" onclick="addAssignmentToLibraryAjax();" type="button">' +
            '<p class="submitButtonTextNoImage">Uložit příklad do této knihovny</p></button>';

        imageContainer.innerHTML += tmpHtml;
        var form = document.getElementById("mainForm");
        form.insertBefore(imageContainer, form.firstChild);
    }
}

//response object containing string "infoMessage" or String "errorMessage"
function handleSaveAssignmentAjaxResponse(response, messageLocation){
    //clean workspace
    safeDeleteElement("saveAssignmentInfoMessage");
    safeDeleteElement("saveAssignmentErrorMessage");

    //generate elements
    if(response.infoMessage == undefined){
        generateSaveAssignmentErrorMessage(response.errorMessage, messageLocation);
    } else {
        generateSaveAssignmentInfoMessage(response.infoMessage, messageLocation);
    }
    //TODO: auto-close dialog and display info message in main program page on info message
}


function generateSaveAssignmentErrorMessage(message, where){
    var paragraph = document.createElement("p");
    var id = "saveAssignmentErrorMessage";
    paragraph.innerHTML = message;
    paragraph.setAttribute("id", id);
    paragraph.setAttribute("class", "errorMessage");

    //insert element into page
    insertAfter(paragraph, where);

    setTimeout(function(){
        paragraph.remove();
    }, 3000); //supported in more browsers than varargs setTimeout() style call
}

function generateSaveAssignmentInfoMessage(message, where){
    var paragraph = document.createElement("p");
    var id = "saveAssignmentInfoMessage";
    paragraph.innerHTML = message;
    paragraph.setAttribute("id", id);
    paragraph.setAttribute("class", "infoMessage");

    //insert element into page
    insertAfter(paragraph, where);

    setTimeout(function(){
        paragraph.remove();
    }, 3000); //supported in more browsers than varargs setTimeout() style call
}

function generateLoadUserLibraryNamesErrorMessage(message, where){
    var container = document.createElement("div");
    container.setAttribute("id", "loadUserLibraryNamesErrorMessage");
    container.setAttribute("class", "centeredContent");
    container.innerHTML = '<p class="errorMessage">' + message + '</p>';

    //insert element into page
    insertAfter(container, where);

    setTimeout(function(){
        container.remove();
    }, 3000); //supported in more browsers than varargs setTimeout() style call
}

function generateLoadUserLibraryNamesInfoMessage(message, where){
    var container = document.createElement("div");
    container.setAttribute("id", "loadUserLibraryNamesErrorMessage");
    container.setAttribute("class", "centeredContent");
    container.innerHTML = '<p class="infoMessage">' + message + '</p>';

    //insert element into page
    insertAfter(container, where);

    setTimeout(function(){
        container.remove();
    }, 3000); //supported in more browsers than varargs setTimeout() style call
}

function closeTeXImage(){
    document.getElementById("checkSubspaceImageContainer").remove();
}

function closeUserLibraryNamesContainer(){
    document.getElementById("userLibraryNamesContainer").remove();
}

function generateCheckSubspaceErrorMessage(message, where){
    var paragraph = document.createElement("p");
    var id = "checkSubspaceErrorMessage";
    paragraph.innerHTML = message;
    paragraph.setAttribute("id", id);
    paragraph.setAttribute("class", "errorMessage");

    //insert element into page
    insertAfter(paragraph, where);

    setTimeout(function(){
        paragraph.remove();
    }, 3000); //supported in more browsers than varargs setTimeout() style call
}

function generateCheckSubspaceInfoMessage(message, where){
    var paragraph = document.createElement("p");
    var id = "checkSubspaceInfoMessage";
    paragraph.innerHTML = message;
    paragraph.setAttribute("id", id);
    paragraph.setAttribute("class", "infoMessage");

    //insert element into page
    insertAfter(paragraph, where);

    setTimeout(function(){
        paragraph.remove();
    }, 3000); //supported in more browsers than varargs setTimeout() style call
}

/* ENDREGION ajax check subspace handler */


//vrací object offset, který má property top a left
//src: https://plainjs.com/javascript/styles/get-the-position-of-an-element-relative-to-the-document-24/
function offset(el) {
    var rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}


//get relative position to element's rectangle
//src: https://jsfiddle.net/u3xbhps6/ - nefunguje protože https://stackoverflow.com/questions/30711548/is-getboundingclientrect-left-returning-a-wrong-value



//enable dragging, id+'Header' is element which will be used for dragging (usually inside the desired element)
//src: https://www.w3schools.com/howto/howto_js_draggable.asp
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

    //bugfix for element which has position: absolute; left: 0; right: 0; set as default style
    var position = offset(elmnt); //absolute offset from page topleft
    var startPositionX = position.left; //toto je špatně, např 909 místo 797
    var startPositionY = position.top;

    //end bugfix

    if (document.getElementById(elmnt.id + "Header")) {
        // if present, the header is where you move the DIV from:
        document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
        console.log(elmnt);
    } else {
        // otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }


    function dragMouseDown(e) {
        //bugfix for element which has position: absolute; left: 0; right: 0; set as default style
        if(elmnt.className == "checkSubspaceImageContainer" || elmnt.className == "userLibraryNamesContainer"){
            //console.log("div class changed (left, right position removed)");
            elmnt.className = elmnt.className + 'Draggable';

            //console.log("e.pageX: " + e.pageX);
            //console.log("e.pageY: " + e.pageY);
            //console.log("startPositionX: " + startPositionX);
            //console.log("startPositionY: " + startPositionY);
            //console.log("e.clientX: " + e.clientX);
            //console.log("e.clientY: " + e.clientY);
            var xOffsetInside = e.clientX - startPositionX; //toto funguje, ale x ne
            var yOffsetInside = e.clientY - startPositionY; //toto funguje, ale x ne
            //console.log("xOffsetInside: " + xOffsetInside);
            //console.log("yOffsetInside: " + yOffsetInside);

            var windowWidth = window.innerWidth
                || document.documentElement.clientWidth
                || document.body.clientWidth;

            var windowHeight = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

            //console.log("windowWidth: " + windowWidth);
            //console.log("windowHeight: " + windowHeight);

            //console.log("imageContainer.offsetWidth: " + elmnt.offsetWidth);
            //console.log("imageContainer.offsetHeight: " + elmnt.offsetHeight);

            //console.log("něco pozice x: " + (startPositionX - elmnt.offsetWidth/2));
            //nic prostě

            elmnt.style.left = startPositionX + "px";
            elmnt.style.top = startPositionY + "px";
        }
        //end bugfix

        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;

        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}


